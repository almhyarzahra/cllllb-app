import 'package:com.tad.cllllb/core/data/models/apis/all_clubs_model.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/utils.dart';
import '../all_clubs_view/all_clubs_view.dart';
import 'filter_all_clubs_controller.dart';

class FilterAllClubsView extends StatefulWidget {
  const FilterAllClubsView({super.key, required this.clubsModel, required this.currentlocation});
  final List<AllClubsModel> clubsModel;
  final LocationData? currentlocation;

  @override
  State<FilterAllClubsView> createState() => _FilterAllClubsViewState();
}

class _FilterAllClubsViewState extends State<FilterAllClubsView> {
  late FilterAllClubsController controller;
  @override
  void initState() {
    controller = Get.put(FilterAllClubsController(clubsModel: widget.clubsModel));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsetsDirectional.symmetric(
                  horizontal: screenWidth(75), vertical: screenWidth(25)),
              child: Card(
                elevation: 4,
                child: Column(
                  children: [
                    Obx(
                      () {
                        return DropdownButton(
                            hint: Padding(
                              padding:
                                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(3.5)),
                              child: CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_Filter_By_Clubs'),
                                textColor: AppColors.mainYellowColor,
                              ),
                            ),
                            items: controller.clubs.map(
                              (element) {
                                return DropdownMenuItem(
                                  value: element,
                                  child: Obx(
                                    () {
                                      return Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: element,
                                            textColor: AppColors.mainBlackColor,
                                          ),
                                          Checkbox(
                                            value: controller.isCheckedClub[
                                                controller.clubs.toList().indexOf(element)],
                                            onChanged: (value) {
                                              controller.isCheckedClub[controller.clubs
                                                  .toList()
                                                  .indexOf(element)] = value!;
                                              controller.addClubToFilter(nameClub: element);
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                );
                              },
                            ).toList(),
                            onChanged: (value) {});
                      },
                    ),
                    screenWidth(30).ph,
                    Obx(
                      () {
                        return Padding(
                          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(15)),
                          child: DropdownButton(
                            hint: Padding(
                              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(5)),
                              child: CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_Filter_By_Type'),
                                textColor: AppColors.mainYellowColor,
                              ),
                            ),
                            items: controller.type.map(
                              (element) {
                                return DropdownMenuItem(
                                  value: element,
                                  child: Obx(
                                    () {
                                      return Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: element,
                                            textColor: AppColors.mainBlackColor,
                                          ),
                                          Checkbox(
                                            value: controller.isCheckedType[
                                                controller.type.toList().indexOf(element)],
                                            onChanged: (value) {
                                              controller.isCheckedType[controller.type
                                                  .toList()
                                                  .indexOf(element)] = value!;
                                              controller.addTypeToFilter(type: element);
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                );
                              },
                            ).toList(),
                            onChanged: (value) {},
                          ),
                        );
                      },
                    ),
                    screenWidth(30).ph,
                    Obx(
                      () {
                        return Padding(
                          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(15)),
                          child: DropdownButton(
                            hint: Padding(
                              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(5)),
                              child: CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_Filter_By_City'),
                                textColor: AppColors.mainYellowColor,
                              ),
                            ),
                            items: controller.city.map(
                              (element) {
                                return DropdownMenuItem(
                                  value: element,
                                  child: Obx(
                                    () {
                                      return Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: element,
                                            textColor: AppColors.mainBlackColor,
                                          ),
                                          Checkbox(
                                            value: controller.isCheckedCity[
                                                controller.city.toList().indexOf(element)],
                                            onChanged: (value) {
                                              controller.isCheckedCity[controller.city
                                                  .toList()
                                                  .indexOf(element)] = value!;
                                              controller.addCityToFilter(city: element);
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                );
                              },
                            ).toList(),
                            onChanged: (value) {},
                          ),
                        );
                      },
                    ),
                    screenWidth(30).ph,
                    widget.clubsModel[0].distance == null
                        ? const SizedBox.shrink()
                        : Obx(
                            () {
                              return DropdownButton(
                                hint: Padding(
                                  padding:
                                      EdgeInsetsDirectional.symmetric(horizontal: screenWidth(5)),
                                  child: CustomText(
                                    textType: TextStyleType.CUSTOM,
                                    text: tr('key_Filter_By_Distance'),
                                    textColor: AppColors.mainYellowColor,
                                  ),
                                ),
                                items: controller.distance.map(
                                  (element) {
                                    return DropdownMenuItem(
                                      value: element,
                                      child: Obx(
                                        () {
                                          return Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              CustomText(
                                                textType: TextStyleType.CUSTOM,
                                                text: element,
                                                textColor: AppColors.mainBlackColor,
                                              ),
                                              Checkbox(
                                                value: controller.isCheckedDistance[
                                                    controller.distance.toList().indexOf(element)],
                                                onChanged: (value) {
                                                  controller.isCheckedDistance[controller.distance
                                                      .toList()
                                                      .indexOf(element)] = value!;
                                                  controller.addDistanceToFilter(distance: element);
                                                },
                                              ),
                                            ],
                                          );
                                        },
                                      ),
                                    );
                                  },
                                ).toList(),
                                onChanged: (value) {},
                              );
                            },
                          ),
                  ],
                ),
              ),
            ),
            screenWidth(20).ph,
            Padding(
              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
              child: CustomButton(
                circularBorder: screenWidth(20),
                text: tr('key_Finish'),
                onPressed: () async {
                  await controller.filter();

                  Get.off(
                    AllClubsView(
                      currentlocation: widget.currentlocation,
                      clubs: controller.clubsChangedModel,
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
