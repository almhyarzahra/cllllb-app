import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/data/models/apis/all_clubs_model.dart';
import '../main_view/home_view/home_controller.dart';

class FilterAllClubsController extends BaseController {
  FilterAllClubsController({required List<AllClubsModel> clubsModel}) {
    this.clubsModel.value = clubsModel;
  }
  RxList<AllClubsModel> clubsModel = <AllClubsModel>[].obs;
  RxList<AllClubsModel> clubsChangedModel = <AllClubsModel>[].obs;
  RxList<AllClubsModel> test1 = <AllClubsModel>[].obs;
  RxList<AllClubsModel> test2 = <AllClubsModel>[].obs;
  RxList<AllClubsModel> test3 = <AllClubsModel>[].obs;
  RxList<AllClubsModel> test4 = <AllClubsModel>[].obs;
  RxList<AllClubsModel> test5 = <AllClubsModel>[].obs;
  RxList<AllClubsModel> test6 = <AllClubsModel>[].obs;
  RxList<AllClubsModel> test7 = <AllClubsModel>[].obs;

  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxSet<String> clubs = <String>{}.obs;
  RxList<bool> isCheckedClub = <bool>[].obs;
  RxList<String> finalNameClub = <String>["All Clubs"].obs;

  RxSet<String> type = <String>{}.obs;
  RxList<bool> isCheckedType = <bool>[].obs;
  RxList<String> finalType = <String>["All Type"].obs;

  RxSet<String> city = <String>{}.obs;
  RxList<bool> isCheckedCity = <bool>[].obs;
  RxList<String> finalCity = <String>["All City"].obs;

  RxList<String> distance =
      <String>["1 to 10 km", "1 to 25 km", "1 to 50 km", "1 to 100 km", "All Distance"].obs;
  RxList<bool> isCheckedDistance = <bool>[false, false, false, false, true].obs;
  RxList<String> finalDistance = <String>["All Distance"].obs;

  @override
  void onInit() {
    clubsChangedModel = clubsModel;
    getClubs();
    super.onInit();
  }

  @override
  void onClose() {
    Get.find<HomeController>().getAllClubs();
    super.onClose();
  }

  void getClubs() {
    clubsModel.map((element) {
      clubs.add(element.name!);
      city.add(element.city!);
      type.add(element.clubTypeName!);
    }).toSet();

    city.map((element) {
      isCheckedCity.add(false);
    }).toSet();
    city.add("All City");
    isCheckedCity.add(true);

    type.map((element) {
      isCheckedType.add(false);
    }).toSet();
    type.add("All Type");
    isCheckedType.add(true);

    clubs.map((element) {
      isCheckedClub.add(false);
    }).toSet();
    clubs.add("All Clubs");

    isCheckedClub.add(true);
  }

  void addClubToFilter({required String nameClub}) {
    if (finalNameClub.contains(nameClub)) {
      finalNameClub.remove(nameClub);
    } else {
      finalNameClub.add(nameClub);
    }
    // print(finalNameClub);
  }

  void addTypeToFilter({required String type}) {
    if (finalType.contains(type)) {
      finalType.remove(type);
    } else {
      finalType.add(type);
    }
    //print(finalType);
  }

  void addCityToFilter({required String city}) {
    if (finalCity.contains(city)) {
      finalCity.remove(city);
    } else {
      finalCity.add(city);
    }
    //print(finalCity);
  }

  void addDistanceToFilter({required String distance}) {
    if (finalDistance.contains(distance)) {
      finalDistance.remove(distance);
    } else {
      finalDistance.add(distance);
    }
    //print(finalDistance);
  }

  Future<void> filter() async {
    if (!finalNameClub.contains("All Clubs")) {
      finalNameClub.map((e) {
        clubsChangedModel.map((element) {
          if (element.name == e) {
            test1.add(element);
          }
        }).toSet();
      }).toSet();
      clubsChangedModel = test1;
    }
    if (!finalType.contains("All Type")) {
      finalType.map((el) {
        clubsChangedModel.map((element) {
          if (element.clubTypeName == el) test2.add(element);
        }).toSet();
      }).toSet();
      clubsChangedModel = test2;
    }
    if (!finalCity.contains("All City")) {
      finalCity.map((el) {
        clubsChangedModel.map((element) {
          if (element.city == el) test3.add(element);
        }).toSet();
      }).toSet();
      clubsChangedModel = test3;
    }

    if (!finalDistance.contains("All Distance")) {
      if (finalDistance.contains("1 to 10 km")) {
        clubsChangedModel.map((element) {
          if (element.distance! <= 10000) {
            test4.add(element);
          }
        }).toSet();
        clubsChangedModel = test4;
      } else if (finalDistance.contains("1 to 25 km")) {
        clubsChangedModel.map((element) {
          if (element.distance! <= 25000) {
            test5.add(element);
          }
        }).toSet();
        clubsChangedModel = test5;
      } else if (finalDistance.contains("1 to 50 km")) {
        clubsChangedModel.map((element) {
          if (element.distance! <= 50000) {
            test6.add(element);
          }
        }).toSet();
        clubsChangedModel = test6;
      } else if (finalDistance.contains("1 to 100 km")) {
        clubsChangedModel.map((element) {
          if (element.distance! <= 100000) {
            test7.add(element);
          }
        }).toSet();
        clubsChangedModel = test7;
      }
    }
  }
}
