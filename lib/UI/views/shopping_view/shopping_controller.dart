import 'dart:developer';

import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/UI/views/orders_view/orders_view.dart';
import 'package:com.tad.cllllb/UI/views/shopping_view/stripe_payment/payment_manager.dart';
import 'package:com.tad.cllllb/core/data/models/cart_request_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:lottie/lottie.dart';

import '../../../core/data/models/cart_model.dart';
import '../../../core/data/repositories/payment_repositories.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/utils.dart';

class ShoppingController extends BaseController {
  ShoppingController({this.currentlocation});
  LocationData? currentlocation;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<FormState> codeKey = GlobalKey<FormState>();

  TextEditingController nameController =
      TextEditingController(text: storage.getUserName());
  TextEditingController emailController =
      TextEditingController(text: storage.getEmail());
  TextEditingController phoneController =
      TextEditingController(text: storage.getmobileNumber() ?? "");

  TextEditingController streetController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController countryController =
      TextEditingController(text: "United Arab Emirates");
  TextEditingController suburbController = TextEditingController();
  TextEditingController codeController = TextEditingController();
  List<CartModel> get cartList => cartService.cartList;
  RxDouble wallet = 0.0.obs;
  RxBool hidden = true.obs;
  RxDouble discount = 0.0.obs;
  RxBool enableDiscount = false.obs;

  void checkVoucher({required String paymntMethod}) {
    Get.defaultDialog(
      title: tr('enterYourVouchers'),
      titleStyle: const TextStyle(color: AppColors.mainYellowColor),
      content: Form(
          key: codeKey,
          child: Obx(() {
            return Column(
              children: [
                CustomTextField(
                  hintText: "Code",
                  controller: codeController,
                  borderColor: AppColors.mainYellowColor,
                  circularSize: screenWidth(30),
                  contentPaddingLeft: screenWidth(10),
                  validator: (value) {
                    return value!.isEmpty ? tr('key_field_required') : null;
                  },
                ),
                if (enableDiscount.value)
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: "${tr('newTotal')} ${discount.value}",
                    textColor: AppColors.mainYellowColor,
                  ),
                screenWidth(20).ph,
                CustomButton(
                  text: tr('confirm'),
                  onPressed: enableDiscount.value
                      ? null
                      : () {
                          if (codeKey.currentState!.validate()) {
                            voucherApi();
                          }
                        },
                  widthButton: 3,
                  circularBorder: screenWidth(10),
                ),
                enableDiscount.value
                    ? screenWidth(20).ph
                    : const SizedBox.shrink(),
                if (enableDiscount.value)
                  CustomButton(
                    text: tr('withCoupon'),
                    onPressed: () {
                      payment(
                          code: codeController.text.trim(),
                          paymntMethod: paymntMethod,
                          total: discount.value);
                    },
                    widthButton: 2.3,
                    circularBorder: screenWidth(10),
                  ),
                screenWidth(20).ph,
                CustomButton(
                  text: tr('withoutCoupon'),
                  onPressed: () {
                    payment(
                        paymntMethod: paymntMethod,
                        total: cartService.subTotal.value,
                        code: null);
                  },
                  widthButton: 2.3,
                  circularBorder: screenWidth(10),
                )
              ],
            );
          })),
    );
  }

  void voucherApi() {
    runFullLoadingFutureFunction(
        function: UserRepository()
            .checkVoucher(code: codeController.text.trim())
            .then((value) {
      value.fold((l) {
        if (l == null) {
          CustomToast.showMessage(
              message: tr('key_try_again'), messageType: MessageType.REJECTED);
        } else {
          CustomToast.showMessage(
              message: tr('vouchersInvalid'), messageType: MessageType.INFO);
        }
      }, (r) {
        discount.value = 0.0;
        enableDiscount.value = true;
        log(cartService.subTotal.value.toString());
        log(r.couponValue!.toString());

        discount.value = cartService.subTotal.value -
            ((cartService.subTotal.value * r.couponValue!) / 100);
        discount.value = double.parse(discount.value.toStringAsFixed(2));
        CustomToast.showMessage(
            message: "${tr('willGetDiscount')}${r.couponValue}%",
            messageType: MessageType.SUCCSESS);
      });
    }));
  }

  void payment(
      {required String paymntMethod,
      required double total,
      required String? code}) {
    if (formKey.currentState!.validate()) {
      List<CartRequestModel> myCart = [];
      // ignore: avoid_function_literals_in_foreach_calls
      cartList.forEach((element) {
        myCart.add(CartRequestModel(
            id: element.productsModel!.id,
            name: element.productsModel!.name,
            price: element.productsModel!.price.toString(),
            quantity: element.count));
      });
      runFullLoadingFutureFunction(
        function: PaymentRepositories()
            .addOrder(
          name: nameController.text.trim().toString(),
          email: emailController.text.trim().toString(),
          phone: phoneController.text.trim().toString(),
          street: streetController.text.trim().toString(),
          city: cityController.text.trim().toString(),
          country: countryController.text.trim().toString(),
          userId: storage.getUserId(),
          suburb: suburbController.text.trim().isEmpty
              ? null
              : suburbController.text.trim().toString(),
          total: total,
          cart: myCart,
          code: code,
        )
            .then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: tr('key_try_again'),
                messageType: MessageType.REJECTED);
          }, (r) {
            if (paymntMethod == "Card") {
              PaymentManager.makePayment(total.toInt(), "AED")
                  .then((value) async {
                await Stripe.instance.presentPaymentSheet().then((value) {
                  checkOrder(orderId: r.orderId!, paymentMethod: paymntMethod);
                });
              });
            } else if (paymntMethod == "Wallet") {
              getWalletBalance(orderId: r.orderId!);
            } else if (paymntMethod == "Cash") {
              checkOrder(orderId: r.orderId!, paymentMethod: paymntMethod);
            }
          });
        }),
      );
    }
  }

  void checkOrder({required int orderId, required String paymentMethod}) async {
    log("orderid=$orderId");
    runFullLoadingFutureFunction(
      visible: false,
        function: PaymentRepositories()
            .checkOrder(orderId: orderId, paymentMethod: paymentMethod)
            .then((value) {
      value.fold((l) {
        checkOrder(orderId: orderId, paymentMethod: paymentMethod);
      }, (r) {
        log("22saasasasa");
        sentEmail(orderId: orderId, pointRewards: r.points,draw: r.draw);
      });
    }));
  }

  void removeFromCart(CartModel model) {
    cartService.removeFromCart(
      model: model,
    );
  }

  void changeCount(bool incress, CartModel model) {
    cartService.changeCount(
      incress: incress,
      model: model,
    );
  }

  void getWalletBalance({required int orderId}) {
    runFullLoadingFutureFunction(
        function: PaymentRepositories()
            .getWalletBalance(userId: storage.getUserId())
            .then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_check_connection'),
            messageType: MessageType.REJECTED);
      }, (r) {
        wallet.value = r.userBalance!;
        r.code == 1
            ? Get.defaultDialog(
                title: tr('key_Wallet_contains') +
                    wallet.value.toString() +
                    tr('key_AED'),
                titleStyle: const TextStyle(color: AppColors.mainYellowColor),
                content: Column(
                  children: [
                    Lottie.asset(
                      "images/wallet.json",
                      width: screenWidth(3),
                    ),
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_fill_wallet'),
                      textColor: AppColors.mainRedColor,
                    ),
                    wallet.value < cartService.subTotal.value.toInt()
                        ? CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: tr('key_wallet_not_contain_the_full_amount!'),
                            textColor: AppColors.mainRedColor,
                          )
                        : Column(
                            children: [
                              CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_Do_you_want_pay_from_the_wallet'),
                                textColor: AppColors.mainBlackColor,
                              ),
                              screenWidth(30).ph,
                              CustomButton(
                                  widthButton: 4,
                                  circularBorder: screenWidth(20),
                                  text: tr('key_Yes'),
                                  onPressed: () {
                                    walletUpdate(orderId: orderId);
                                  }),
                              screenWidth(30).ph,
                              CustomButton(
                                  widthButton: 4,
                                  circularBorder: screenWidth(20),
                                  text: tr('key_No'),
                                  onPressed: () {
                                    Get.back();
                                  }),
                            ],
                          ),
                  ],
                ))
            : Get.defaultDialog(
                title: tr('key_You_Do_not_have_wallet_yet'),
                titleStyle: const TextStyle(color: AppColors.mainRedColor),
                content: Column(
                  children: [
                    Lottie.asset(
                      "images/wallet.json",
                      width: screenWidth(3),
                    ),
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_fill_wallet'),
                      textColor: AppColors.mainRedColor,
                    )
                  ],
                ));
      });
    }));
  }

  void walletUpdate({required int orderId}) {
    runFullLoadingFutureFunction(
        function:
            PaymentRepositories().walletUpdate(orderId: orderId).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_check_connection'),
            messageType: MessageType.REJECTED);
      }, (r) {
        checkOrder(orderId: orderId, paymentMethod: "Wallet");
      });
    }));
  }

  void sentEmail({required int orderId, required double? pointRewards,required String? draw}) {
    runFullLoadingFutureFunction(
        function: PaymentRepositories()
            .sentEmailByOrder(orderId: orderId)
            .then((value) {
      value.fold((l) {
        sentEmail(orderId: orderId, pointRewards: pointRewards,draw: draw);
      }, (r) {
        if(draw!=null){
          CustomToast.showMessage(message: "${tr('wonEntryLottery')} '$draw'",messageType: MessageType.SUCCSESS);
        }
       else if (pointRewards != null) {
          CustomToast.showMessage(
              message: "$pointRewards points have been added to your account",
              messageType: MessageType.SUCCSESS);
        } else {
          CustomToast.showMessage(
              message: tr('key_Succsess_Payment'),
              messageType: MessageType.SUCCSESS);
        }
        cartService.clearCart();
        Get.off(OrdersView(
          currentlocation: currentlocation,
        ));
      });
    }));
  }

  // void checkout() {
  //   runFullLoadingFutureFunction(
  //       function: Future.delayed(Duration(seconds: 2)).then((value) {
  //     Get.off(CheckoutView());
  //   }));
  // }
}
