import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/food_sport_detalis_view/food-sport_detalis_view.dart';
import 'package:com.tad.cllllb/UI/views/shopping_view/shopping_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text_button.dart';
import '../../shared/custom_widgets/custom_text_field.dart';
import '../../shared/custom_widgets/virtical_divider.dart';
import '../sign_in_view/sign_in_view.dart';

class ShoppingView extends StatefulWidget {
  const ShoppingView({super.key, required this.currentlocation});
  final LocationData? currentlocation;

  @override
  State<ShoppingView> createState() => _ShoppingViewState();
}

class _ShoppingViewState extends State<ShoppingView> {
  late ShoppingController controller;
  @override
  void initState() {
    controller = Get.put(ShoppingController(currentlocation: widget.currentlocation));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        // appBar: PreferredSize(
        //   preferredSize: Size.fromHeight(screenWidth(6)),
        //   child: CustomAppBar(
        //     currentlocation: widget.currentlocation,
        //     drwerOnPressed: () {
        //       controller.key.currentState!.openDrawer();
        //     },
        //   ),
        // ),
        body: Form(
          key: controller.formKey,
          child: Obx(
            () {
              return controller.cartList.isEmpty
                  ? Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IconButton(
                            onPressed: () {
                              Get.back();
                            },
                            icon: Icon(
                              Icons.arrow_back_ios_new,
                              size: screenWidth(20),
                            ),
                          ),
                          CustomText(
                              textType: TextStyleType.CUSTOM,
                              text: tr("key_The_Cart_Is_Empty"),
                              textColor: AppColors.mainBlackColor),
                        ],
                      ),
                    )
                  : ListView(
                      children: [
                        screenHeight(80).ph,
                        Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                Get.back();
                              },
                              icon: Icon(
                                Icons.arrow_back_ios_new,
                                size: screenWidth(20),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                              child: CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_My_Cart'),
                                textColor: AppColors.mainBlackColor,
                                textAlign: TextAlign.start,
                                fontSize: screenWidth(15),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Obx(
                                () {
                                  return Column(
                                    children: controller.cartList.map(
                                      (e) {
                                        return Padding(
                                          padding: EdgeInsetsDirectional.symmetric(
                                              vertical: screenWidth(30)),
                                          child: Container(
                                            width: double.infinity,
                                            decoration: BoxDecoration(
                                              color: AppColors.mainWhiteColor,
                                              borderRadius: BorderRadius.circular(screenWidth(40)),
                                              border: Border.all(color: AppColors.mainGreyColor),
                                            ),
                                            child: Padding(
                                              padding: EdgeInsetsDirectional.symmetric(
                                                  vertical: screenWidth(20)),
                                              child: SizedBox(
                                                height: screenHeight(4.5),
                                                child: ListView(
                                                  shrinkWrap: true,
                                                  scrollDirection: Axis.horizontal,
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        CustomNetworkImage(
                                                          Uri.https(
                                                                      NetworkUtil.baseUrl,
                                                                      e.productsModel!.img!)
                                                                  .toString(),
                                                                   width: screenWidth(4),
                                                              height: screenHeight(9),
                                                              margin:const EdgeInsets.all(8.0) ,
                                                              onTap: ()=> Get.to(()=> FoodSportDetalisView(
                                                                    currentlocation:
                                                                        widget.currentlocation,
                                                                    productsModel:
                                                                        e.productsModel!),),

                                                        ),
                                                        // Padding(
                                                        //   padding: const EdgeInsets.all(8.0),
                                                        //   child: InkWell(
                                                        //     onTap: () {
                                                        //       Get.off(
                                                        //         FoodSportDetalisView(
                                                        //             currentlocation:
                                                        //                 widget.currentlocation,
                                                        //             productsModel:
                                                        //                 e.productsModel!),
                                                        //       );
                                                        //     },
                                                        //     child: CachedNetworkImage(
                                                        //       imageUrl: Uri.https(
                                                        //               NetworkUtil.baseUrl,
                                                        //               e.productsModel!.img!)
                                                        //           .toString(),
                                                        //       placeholder: (context, url) =>
                                                        //           Lottie.asset(
                                                        //               "images/download.json"),
                                                        //       errorWidget: (context, url, error) =>
                                                        //           const Icon(Icons.error),
                                                        //       width: screenWidth(4),
                                                        //       height: screenHeight(9),
                                                        //     ),
                                                        //   ),
                                                        // ),
                                                        Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment.start,
                                                          children: [
                                                            CustomText(
                                                              textType: TextStyleType.CUSTOM,
                                                              text: e.productsModel!.name!.length >
                                                                      25
                                                                  ? "${e.productsModel!.name!.substring(0, 25)}.."
                                                                  : e.productsModel!.name!,
                                                              textColor: AppColors.mainBlackColor,
                                                            ),
                                                            //screenWidth(20).ph,
                                                            Row(
                                                              children: [
                                                                CustomText(
                                                                  textType: TextStyleType.CUSTOM,
                                                                  text: tr('key_Price: '),
                                                                  textColor:
                                                                      AppColors.mainBlackColor,
                                                                ),
                                                                CustomText(
                                                                  textType: TextStyleType.CUSTOM,
                                                                  text: e.productsModel!.price
                                                                          .toString() +
                                                                      tr('key_AED'),
                                                                  textColor:
                                                                      AppColors.mainYellowColor,
                                                                ),
                                                              ],
                                                            ),
                                                            Row(
                                                              children: [
                                                                CustomText(
                                                                  textType: TextStyleType.CUSTOM,
                                                                  text: tr('key_Total: '),
                                                                  textColor:
                                                                      AppColors.mainBlackColor,
                                                                ),
                                                                CustomText(
                                                                  textType: TextStyleType.CUSTOM,
                                                                  text: e.totalItem.toString() +
                                                                      tr('key_AED'),
                                                                  textColor:
                                                                      AppColors.mainYellowColor,
                                                                ),
                                                              ],
                                                            ),
                                                            screenWidth(25).ph,
                                                            CustomText(
                                                              textType: TextStyleType.CUSTOM,
                                                              text: e.productsModel!.inProduct == 1
                                                                  ? tr('key_inside_the_club')
                                                                  : tr(
                                                                      'key_Inside/Outside_the_club'),
                                                              textColor: AppColors.mainRedColor,
                                                              fontSize: screenWidth(40),
                                                            ),
                                                            Row(
                                                              children: [
                                                                ElevatedButton(
                                                                  onPressed: e.count == 1
                                                                      ? null
                                                                      : () {
                                                                          controller.changeCount(
                                                                              false, e);
                                                                        },
                                                                  style: ElevatedButton.styleFrom(
                                                                    shape: const StadiumBorder(),
                                                                    backgroundColor:
                                                                        AppColors.mainYellowColor,
                                                                    disabledBackgroundColor:
                                                                        AppColors.mainGreyColor,
                                                                    fixedSize: Size(screenWidth(8),
                                                                        screenHeight(25)),
                                                                  ),
                                                                  child: const Text("-"),
                                                                ),
                                                                (screenWidth(45)).pw,
                                                                CustomText(
                                                                  text: "${e.count}",
                                                                  textType: TextStyleType.CUSTOM,
                                                                  textColor:
                                                                      AppColors.mainBlackColor,
                                                                  fontWeight: FontWeight.bold,
                                                                  fontSize: screenWidth(28),
                                                                ),
                                                                (screenWidth(50)).pw,
                                                                ElevatedButton(
                                                                  onPressed: () {
                                                                    controller.changeCount(true, e);
                                                                  },
                                                                  style: ElevatedButton.styleFrom(
                                                                    shape: const StadiumBorder(),
                                                                    backgroundColor:
                                                                        AppColors.mainYellowColor,
                                                                  ),
                                                                  child: const Text("+"),
                                                                ),
                                                              ],
                                                            ),
                                                          ],
                                                        ),
                                                        IconButton(
                                                          onPressed: () {
                                                            Get.defaultDialog(
                                                              title: tr(
                                                                  'key_Are_yousure_to_delete_the_product?'),
                                                              titleStyle: const TextStyle(
                                                                color: AppColors.mainYellowColor,
                                                              ),
                                                              content: Column(
                                                                children: [
                                                                  CustomButton(
                                                                    text: tr('key_Yes'),
                                                                    onPressed: () {
                                                                      controller.removeFromCart(e);
                                                                      Get.back();
                                                                    },
                                                                    widthButton: 4,
                                                                    circularBorder: screenWidth(10),
                                                                  ),
                                                                  screenWidth(20).ph,
                                                                  CustomButton(
                                                                    text: tr('key_No'),
                                                                    onPressed: () {
                                                                      Get.back();
                                                                    },
                                                                    widthButton: 4,
                                                                    circularBorder: screenWidth(10),
                                                                  ),
                                                                ],
                                                              ),
                                                            );
                                                            //controller.removeFromCart(e);
                                                          },
                                                          icon: const Icon(
                                                            Icons.delete,
                                                            color: AppColors.mainRedColor,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    ).toList(),
                                  );
                                },
                              ),
                              const VirticalDivider(
                                width: 1,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  CustomText(
                                    textType: TextStyleType.CUSTOM,
                                    fontSize: screenWidth(20),
                                    text: tr('key_Total: '),
                                    textColor: AppColors.mainBlackColor,
                                  ),
                                  Obx(
                                    () {
                                      return CustomText(
                                        textType: TextStyleType.CUSTOM,
                                        fontSize: screenWidth(20),
                                        text: cartService.subTotal.toString() + tr('key_AED'),
                                        textColor: AppColors.mainYellowColor,
                                      );
                                    },
                                  ),
                                ],
                              ),
                              screenWidth(30).ph,
                              Center(
                                child: CustomTextButton(
                                  colorText: AppColors.mainRedColor,
                                  onPressed: () {
                                    Get.defaultDialog(
                                      title: tr('key_Are_you_sure_to_clear_your_cart?'),
                                      titleStyle: const TextStyle(color: AppColors.mainYellowColor),
                                      content: Column(
                                        children: [
                                          CustomButton(
                                            text: tr('key_Yes'),
                                            onPressed: () {
                                              cartService.clearCart();
                                              Get.back();
                                            },
                                            widthButton: 4,
                                            circularBorder: screenWidth(10),
                                          ),
                                          screenWidth(20).ph,
                                          CustomButton(
                                            text: tr('key_No'),
                                            onPressed: () {
                                              Get.back();
                                            },
                                            widthButton: 4,
                                            circularBorder: screenWidth(10),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                  text: tr('key_Empty_Cart'),
                                  decoration: TextDecoration.underline,
                                  decorationThickness: 2,
                                ),
                              ),
                              CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_Payment'),
                                textColor: AppColors.mainBlackColor,
                                fontWeight: FontWeight.bold,
                                textAlign: TextAlign.start,
                              ),
                              screenWidth(20).ph,
                              CustomTextField(
                                hintText: tr('key_Name*'),
                                borderColor: AppColors.mainYellowColor,
                                circularSize: screenWidth(30),
                                contentPaddingLeft: screenWidth(10),
                                controller: controller.nameController,
                                validator: (value) {
                                  return value!.isEmpty ? tr('key_Please_Check_Name') : null;
                                },
                              ),
                              screenWidth(20).ph,
                              CustomTextField(
                                hintText: tr('key_Email*'),
                                borderColor: AppColors.mainYellowColor,
                                circularSize: screenWidth(30),
                                contentPaddingLeft: screenWidth(10),
                                controller: controller.emailController,
                                validator: (value) {
                                  return value!.isEmpty || !GetUtils.isEmail(value)
                                      ? tr('key_Please_Check_Email')
                                      : null;
                                },
                              ),
                              screenWidth(20).ph,
                              CustomTextField(
                                hintText: tr('key_Phone*'),
                                borderColor: AppColors.mainYellowColor,
                                circularSize: screenWidth(30),
                                contentPaddingLeft: screenWidth(10),
                                controller: controller.phoneController,
                                typeInput: TextInputType.phone,
                                validator: (value) {
                                  return value!.isEmpty || value.length<8? tr('key_Please_Check_Phone') : null;
                                },
                              ),
                              screenWidth(20).ph,
                              CustomTextField(
                                hintText: tr('key_Street*'),
                                borderColor: AppColors.mainYellowColor,
                                circularSize: screenWidth(30),
                                contentPaddingLeft: screenWidth(10),
                                controller: controller.streetController,
                                validator: (value) {
                                  return value!.isEmpty ? tr('key_Please_Check_Your_Street') : null;
                                },
                              ),
                              screenWidth(20).ph,
                              CustomTextField(
                                hintText: tr('key_City*'),
                                borderColor: AppColors.mainYellowColor,
                                circularSize: screenWidth(30),
                                contentPaddingLeft: screenWidth(10),
                                controller: controller.cityController,
                                validator: (value) {
                                  return value!.isEmpty ? tr('key_Please_Check_Your_City') : null;
                                },
                              ),
                              screenWidth(20).ph,
                              // Obx(
                              //   () {
                              //      print(controller.hidden.value);
                                 CustomTextField(
                                    onTap: () {
                                      showCountryPicker(
                                        context: context,
                                        onSelect: (Country country) {
                                          controller.countryController.text = country.name;
                                        },
                                      );
                                    },
                                    hintText: tr('key_Country*'),
                                    borderColor: AppColors.mainYellowColor,
                                    circularSize: screenWidth(30),
                                    contentPaddingLeft: screenWidth(10),
                                    controller: controller.countryController,
                                    validator: (value) {
                                      return value!.isEmpty
                                          ? tr('key_Please_Check_Your_Country')
                                          : null;
                                    },
                                  ),
                                
                              
                              screenWidth(20).ph,
                              // CustomTextField(
                              //   hintText: "Suburb",
                              //   borderColor: AppColors.mainYellowColor,
                              //   circularSize: screenWidth(30),
                              //   contentPaddingLeft: screenWidth(10),
                              //   controller: controller.suburbController,
                              // ),
                              // screenWidth(20).ph,
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      Get.defaultDialog(
                                        title: tr('key_Payment_Cash?'),
                                        titleStyle:
                                            const TextStyle(color: AppColors.mainYellowColor),
                                        content: Column(
                                          children: [
                                            CustomButton(
                                              widthButton: 4,
                                              circularBorder: screenWidth(20),
                                              text: tr('key_Yes'),
                                              onPressed: () {
                                                storage.getUserId() == 0
                                                    ? Get.defaultDialog(
                                                        title: tr('key_login_complete'),
                                                        titleStyle: const TextStyle(
                                                            color: AppColors.mainYellowColor),
                                                        content: Column(
                                                          children: [
                                                            CustomButton(
                                                              text: tr('key_Yes'),
                                                              onPressed: () async {
                                                                storage.setCartList([]);
                                                                storage.setFavoriteList([]);
                                                                storage.setFirstLanuch(true);
                                                                storage.setIsLoggedIN(false);
                                                                storage.globalSharedPrefs.remove(
                                                                    storage.PREF_TOKEN_INFO);
                                                                //storage.setTokenInfo(null);
                                                                await storage.setUserId(0);
                                                                storage.globalSharedPrefs
                                                                    .remove(storage.PREF_USER_NAME);

                                                                storage.setGoogle(false);
                                                                storage.globalSharedPrefs.remove(
                                                                    storage.PREF_MOBILE_NUMBER);
                                                                storage.globalSharedPrefs
                                                                    .remove(storage.PREF_EMAIL);
                                                                Get.offAll(const SignInView());
                                                              },
                                                              widthButton: 4,
                                                              circularBorder: screenWidth(10),
                                                            ),
                                                            screenWidth(20).ph,
                                                            CustomButton(
                                                              text: tr('key_No'),
                                                              onPressed: () {
                                                                Get.back();
                                                              },
                                                              widthButton: 4,
                                                              circularBorder: screenWidth(10),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    :controller.formKey.currentState!.validate()?
                                                    {   
                                                      Get.back(),
                                                       controller.checkVoucher(paymntMethod: "Cash")
                                                    }:null;
                                                    //payment(paymntMethod: "Cash");
                                              },
                                            ),
                                            screenWidth(30).ph,
                                            CustomButton(
                                              widthButton: 4,
                                              circularBorder: screenWidth(20),
                                              text: tr('key_No'),
                                              onPressed: () {
                                                Get.back();
                                              },
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                    child: Container(
                                      width: screenWidth(4),
                                      height: screenWidth(8),
                                      decoration: BoxDecoration(
                                        color: AppColors.mainYellowColor,
                                        borderRadius: BorderRadius.circular(screenWidth(20)),
                                      ),
                                      child: Transform.scale(
                                        scale: 0.5,
                                        child: SvgPicture.asset(
                                          "images/cash.svg",
                                        ),
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      storage.getUserId() == 0
                                          ? Get.defaultDialog(
                                              title: tr('key_login_complete'),
                                              titleStyle:
                                                  const TextStyle(color: AppColors.mainYellowColor),
                                              content: Column(
                                                children: [
                                                  CustomButton(
                                                    text: tr('key_Yes'),
                                                    onPressed: () async {
                                                      storage.setCartList([]);
                                                      storage.setFavoriteList([]);
                                                      storage.setFirstLanuch(true);
                                                      storage.setIsLoggedIN(false);
                                                      storage.globalSharedPrefs
                                                          .remove(storage.PREF_TOKEN_INFO);
                                                      //storage.setTokenInfo(null);
                                                      await storage.setUserId(0);
                                                      storage.globalSharedPrefs
                                                          .remove(storage.PREF_USER_NAME);

                                                      storage.setGoogle(false);
                                                      storage.globalSharedPrefs
                                                          .remove(storage.PREF_MOBILE_NUMBER);
                                                      storage.globalSharedPrefs
                                                          .remove(storage.PREF_EMAIL);

                                                      Get.offAll(() => const SignInView());
                                                    },
                                                    widthButton: 4,
                                                    circularBorder: screenWidth(10),
                                                  ),
                                                  screenWidth(20).ph,
                                                  CustomButton(
                                                    text: tr('key_No'),
                                                    onPressed: () {
                                                      Get.back();
                                                    },
                                                    widthButton: 4,
                                                    circularBorder: screenWidth(10),
                                                  ),
                                                ],
                                              ),
                                            )
                                          : controller.checkVoucher(paymntMethod: "Card");
                                    },
                                    child: Container(
                                      width: screenWidth(4),
                                      height: screenWidth(8),
                                      decoration: BoxDecoration(
                                        color: AppColors.mainYellowColor,
                                        borderRadius: BorderRadius.circular(screenWidth(20)),
                                      ),
                                      child: Transform.scale(
                                        scale: 0.5,
                                        child: SvgPicture.asset(
                                          "images/ic_visa.svg",
                                          color: AppColors.mainWhiteColor,
                                        ),
                                      ),
                                    ),
                                  ),
                                  // GetPlatform.isIOS
                                  //     ? SizedBox.shrink()
                                  //     :
                                  InkWell(
                                    onTap: () {
                                      storage.getUserId() == 0
                                          ? Get.defaultDialog(
                                              title: tr('key_login_complete'),
                                              titleStyle:
                                                  const TextStyle(color: AppColors.mainYellowColor),
                                              content: Column(
                                                children: [
                                                  CustomButton(
                                                    text: tr('key_Yes'),
                                                    onPressed: () async {
                                                      storage.setCartList([]);
                                                      storage.setFavoriteList([]);
                                                      storage.setFirstLanuch(true);
                                                      storage.setIsLoggedIN(false);
                                                      storage.globalSharedPrefs
                                                          .remove(storage.PREF_TOKEN_INFO);
                                                      //storage.setTokenInfo(null);
                                                      await storage.setUserId(0);
                                                      storage.globalSharedPrefs
                                                          .remove(storage.PREF_USER_NAME);

                                                      storage.setGoogle(false);
                                                      storage.globalSharedPrefs
                                                          .remove(storage.PREF_MOBILE_NUMBER);
                                                      storage.globalSharedPrefs
                                                          .remove(storage.PREF_EMAIL);
                                                      Get.offAll(const SignInView());
                                                    },
                                                    widthButton: 4,
                                                    circularBorder: screenWidth(10),
                                                  ),
                                                  screenWidth(20).ph,
                                                  CustomButton(
                                                    text: tr('key_No'),
                                                    onPressed: () {
                                                      Get.back();
                                                    },
                                                    widthButton: 4,
                                                    circularBorder: screenWidth(10),
                                                  ),
                                                ],
                                              ),
                                            )
                                          : controller.checkVoucher(paymntMethod: "Wallet");
                                    },
                                    child: Container(
                                      width: screenWidth(4),
                                      height: screenWidth(8),
                                      decoration: BoxDecoration(
                                        color: AppColors.mainYellowColor,
                                        borderRadius: BorderRadius.circular(screenWidth(20)),
                                      ),
                                      child: Transform.scale(
                                        scale: 0.5,
                                        child: SvgPicture.asset(
                                          "images/wallet.svg",
                                          color: AppColors.mainWhiteColor,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              screenWidth(20).ph,
                            ],
                          ),
                        ),
                      ],
                    );
            },
          ),
        ),
      ),
    );
  }
}
