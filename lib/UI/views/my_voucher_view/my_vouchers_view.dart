import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_app_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_drawer.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/lottery_view/widgets/lottery_loading.dart';
import 'package:com.tad.cllllb/UI/views/my_voucher_view/my_vouchers_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';

class MyVouchersView extends StatefulWidget {
  final LocationData? currentlocation;
  const MyVouchersView({super.key, required this.currentlocation});

  @override
  State<MyVouchersView> createState() => _MyVouchersViewState();
}

class _MyVouchersViewState extends State<MyVouchersView> {
  late MyVouchersController controller;
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    controller =
        Get.put(MyVouchersController(currentlocation: widget.currentlocation));
    _scrollController.addListener(_scrollListener);
  }

  void _scrollListener() async {
    if (controller.getMore.value) {
      if (controller.enableScrollListner.value &&
          _scrollController.position.maxScrollExtent ==
              _scrollController.offset &&
          !_scrollController.position.outOfRange) {
        controller.getMore.value = false;
        await controller.getMoreVouchers(page: controller.pageKey.value);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: controller.key,
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          controller: _scrollController,
          padding: const EdgeInsets.symmetric(horizontal: 15),
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
                CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: tr('myVouchers'),
                  textColor: AppColors.mainBlackColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                ),
              ],
            ),
            InkWell(
              onTap: () {
                Get.bottomSheet(
                  Container(
                    height: screenHeight(3),
                    decoration: BoxDecoration(
                        color: AppColors.mainWhiteColor,
                        borderRadius: BorderRadius.circular(30)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('key_Filter_By_Date'),
                          textColor: AppColors.mainYellowColor,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Obx(
                              () {
                                return InkWell(
                                  onTap: () async {
                                    controller.pickedStartDate =
                                        await showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime(2015),
                                      lastDate: DateTime(2100),
                                    );
                                    if (controller.pickedStartDate != null) {
                                      controller.startdate.value =
                                          DateFormat("yyyy-MM-dd").format(
                                              controller.pickedStartDate!);
                                    }
                                  },
                                  child: Container(
                                    width: 100,
                                    height: 40,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: AppColors.mainGrey2Color,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: controller.startdate.value,
                                      textColor: AppColors.mainBlackColor,
                                    ),
                                  ),
                                );
                              },
                            ),
                            const SizedBox(width: 30),
                            Obx(
                              () {
                                return InkWell(
                                  onTap: () async {
                                    controller.pickedEndDate =
                                        await showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime(2015),
                                      lastDate: DateTime(2100),
                                    );
                                    if (controller.pickedEndDate != null) {
                                      controller.enddate.value =
                                          DateFormat("yyyy-MM-dd").format(
                                              controller.pickedEndDate!);
                                    }
                                  },
                                  child: Container(
                                    width: 100,
                                    height: 40,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: AppColors.mainGrey2Color,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: controller.enddate.value,
                                      textColor: AppColors.mainBlackColor,
                                    ),
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 15),
                                child: CustomText(
                                  textType: TextStyleType.CUSTOM,
                                  text: tr('filterByDiscount'),
                                  textColor: AppColors.mainYellowColor,
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 5),
                                  child: CustomTextField(
                                    hintText: "",
                                    typeInput: TextInputType.number,
                                    controller: controller.discountController,
                                    contentPaddingLeft: screenWidth(10),
                                  ),
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsetsDirectional.only(end: 10),
                                child: CustomText(
                                  textType: TextStyleType.CUSTOM,
                                  text: "%",
                                  textColor: AppColors.mainBlackColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                        CustomButton(
                          text: tr('confirm'),
                          onPressed: () {
                            controller.filter();
                          },
                          widthButton: 2,
                          circularBorder: screenWidth(10),
                        ),
                      ],
                    ),
                  ),
                );
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset("images/filter.svg"),
                    screenWidth(30).pw,
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_Filter'),
                      textColor: AppColors.mainYellowColor,
                    ),
                  ],
                ),
              ),
            ),
            Obx(
              () {
                return controller.isVouchersLoading
                    ? const LotteryLoading()
                    : controller.vouchers.isEmpty
                        ? CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: tr('dontHaveVouchers'),
                            textColor: AppColors.mainYellowColor,
                          )
                        : controller.vouchers[0].message == null
                            ? customFaildConnection(onPressed: () {
                                controller.onInit();
                              },padding: 0)
                            : ListView.separated(
                                separatorBuilder: (context, index) =>
                                    const SizedBox(height: 8),
                                itemCount: controller.vouchers.length,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return Container(
                                    width: double.infinity,
                                    height: 180,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 10),
                                    decoration: BoxDecoration(
                                      color: AppColors.mainGrey2Color
                                          .withOpacity(0.5),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          controller.vouchers[index].title!
                                              .useCorrectEllipsis(),
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                            color: AppColors.mainBlackColor,
                                          ),
                                        ),
                                        Text(
                                          "${tr('startDate')}${DateFormat("dd-MM-yyyy").format(controller.vouchers[index].startDate!)}"
                                              .useCorrectEllipsis(),
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                            color: AppColors.mainGreyColor,
                                          ),
                                        ),
                                        Text(
                                          "${tr('endDate')}${DateFormat("dd-MM-yyyy").format(controller.vouchers[index].endDate!)}"
                                              .useCorrectEllipsis(),
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                            color: AppColors.mainGreyColor,
                                          ),
                                        ),
                                        // Text(
                                        //   "${tr('key_Price')}: ${controller.vouchers[index].points} ${tr('point')}"
                                        //       .useCorrectEllipsis(),
                                        //   overflow: TextOverflow.ellipsis,
                                        //   style: const TextStyle(
                                        //     color: AppColors.mainRedColor,
                                        //   ),
                                        // ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            Text(
                                              "${tr('willGetDiscount')}${controller.vouchers[index].couponValue} %",
                                              style: const TextStyle(
                                                color: AppColors.mainBlackColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                        controller.vouchers[index].isDeleted! ||
                                                controller
                                                    .vouchers[index].isEnded!
                                            ? Center(
                                                child: Text(
                                                  "${tr('vouchersInvalid')}",
                                                  style: const TextStyle(
                                                      color: AppColors
                                                          .mainRedColor),
                                                ),
                                              )
                                            : Center(
                                                child: CustomButton(
                                                  text: controller
                                                      .vouchers[index].code!,
                                                  onPressed: () {
                                                    Clipboard.setData(
                                                            ClipboardData(
                                                                text: controller
                                                                    .vouchers[
                                                                        index]
                                                                    .code!))
                                                        .then(
                                                      (value) {
                                                        ScaffoldMessenger.of(
                                                                context)
                                                            .showSnackBar(
                                                          SnackBar(
                                                            content: Text(tr(
                                                                'voucherCopied')),
                                                          ),
                                                        );
                                                      },
                                                    );
                                                  },
                                                  circularBorder:
                                                      screenWidth(10),
                                                  widthButton: 2,
                                                  heightButton: 10,
                                                ),
                                              )
                                      ],
                                    ),
                                  );
                                },
                              ).animate().scale(delay: 100.ms);
              },
            ),
            Obx(
              () {
                return controller.isMoreVouchersLoading
                    ? const SpinKitCircle(
                        color: AppColors.mainYellowColor,
                      )
                    : const SizedBox.shrink();
              },
            ),
          ],
        ),
      ),
    );
  }
}
