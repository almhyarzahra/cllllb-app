import 'dart:developer';

import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/UI/views/filter_my_vouchers_view/filter_my_vouchers_view.dart';
import 'package:com.tad.cllllb/core/data/models/apis/user_vouchers_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class MyVouchersController extends BaseController {
  MyVouchersController({
    this.currentlocation,
  }){
    //
  }
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  RxList<UserVouchersModel> vouchers =
      <UserVouchersModel>[UserVouchersModel()].obs;
  bool get isVouchersLoading => listType.contains(OperationType.myVouchers);
  bool get isMoreVouchersLoading =>
      listType.contains(OperationType.moreMyVouchers);
  RxInt pageKey = 2.obs;
  RxBool getMore = true.obs;
  RxBool enableScrollListner = true.obs;
 LocationData? currentlocation;
  RxString startdate = "Start Date".obs;
  RxString enddate = "End Date".obs;
  TextEditingController discountController = TextEditingController();
  DateTime? pickedStartDate;
  DateTime? pickedEndDate;

  @override
  void onInit() {
    super.onInit();
    getVouchers();
  }

  void getVouchers() {
    runLoadingFutureFunction(
      type: OperationType.myVouchers,
      function: UserRepository().getUserVouchers(page: 1).then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_check_connection'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (vouchers.isNotEmpty) {
                vouchers.clear();
              }
              vouchers.addAll(r);
              vouchers.map((element) {
                element.message = "l";
              }).toSet();
            },
          );
        },
      ),
    );
  }

  Future<void> getMoreVouchers({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
      type: OperationType.moreMyVouchers,
      function: UserRepository().getUserVouchers(page: page).then(
        (value) {
          value.fold((l) {
            pageKey.value = pageKey.value - 1;
          }, (r) {
            if (r.isEmpty) {
              enableScrollListner.value = false;
            } else {
              vouchers.addAll(r);
            }
          });
        },
      ),
    ).then((value) => getMore.value = true);
  }

  void filter() {
    runFullLoadingFutureFunction(
      function: UserRepository()
          .getUserVouchers(
        page: 1,
        startDate: startdate.value.isEmpty ? null : startdate.value,
        endDate: enddate.value.isEmpty ? null : enddate.value,
        discount: discountController.text.trim().isEmpty
            ? null
            : discountController.text.trim(),
      )
          .then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_try_again'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (r.isEmpty) {
                CustomToast.showMessage(
                    message: tr('noMatchingResults'),
                    messageType: MessageType.INFO);
              }
              else {
                Get.to(()=> FilterMyVouchersView(userVoucherModel: r));
              }
            },
          );
        },
      ),
    );
  }
}
