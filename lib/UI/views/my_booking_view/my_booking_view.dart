import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/utils.dart';
import 'my_booking_controller.dart';

class MyBookingView extends StatefulWidget {
  const MyBookingView({super.key, required this.currentlocation});
  final LocationData? currentlocation;

  @override
  State<MyBookingView> createState() => _MyBookingViewState();
}

class _MyBookingViewState extends State<MyBookingView> {
  late MyBookingController controller;
  @override
  void initState() {
    controller = Get.put(MyBookingController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            children: [
              CustomBar(
                text: tr('key_Booking'),
              ),
              Obx(
                () {
                  return controller.isBookingLoading
                      ? const SpinKitCircle(
                          color: AppColors.mainYellowColor,
                        )
                      : controller.myBooking.isEmpty
                          ? Padding(
                              padding: EdgeInsetsDirectional.only(
                                  top: screenHeight(3)),
                              child: CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_No_Booking_Available'),
                                textColor: AppColors.mainBlackColor,
                              ),
                            )
                          : controller.myBooking[0].message == null
                              ? customFaildConnection(
                                  onPressed: () {
                                    controller.onInit();
                                  },
                                )
                              : Column(
                                  children: controller.myBooking.map(
                                    (element) {
                                      return SizedBox(
                                        height: screenHeight(8),
                                        child: Card(
                                          child: Padding(
                                            padding:
                                                EdgeInsetsDirectional.symmetric(
                                                    horizontal:
                                                        screenWidth(20)),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                CustomText(
                                                  textType:
                                                      TextStyleType.CUSTOM,
                                                  text: tr('key_Book_number') +
                                                      element.id.toString(),
                                                  textColor:
                                                      AppColors.mainBlackColor,
                                                  fontSize: screenWidth(50),
                                                ),
                                                Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    CustomNetworkImage(
                                                      context.image(element
                                                          .gameClubField!
                                                          .image!),
                                                      width: screenWidth(5),
                                                      height: screenWidth(10),
                                                    ),
                                                    CustomText(
                                                      textType:
                                                          TextStyleType.CUSTOM,
                                                      text: element
                                                          .gameClubField!.name!,
                                                      textColor: AppColors
                                                          .mainBlackColor,
                                                    ),
                                                  ],
                                                ),
                                                Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    CustomText(
                                                      textType:
                                                          TextStyleType.CUSTOM,
                                                      text: element.date!,
                                                      textColor: AppColors
                                                          .mainYellowColor,
                                                    ),
                                                    screenWidth(20).ph,
                                                    CustomText(
                                                      fontSize: screenWidth(30),
                                                      textType:
                                                          TextStyleType.CUSTOM,
                                                      text: element.startTime! +
                                                          tr('key_to') +
                                                          element.endTime!,
                                                      textColor: AppColors
                                                          .mainBlackColor,
                                                    ),
                                                  ],
                                                ),
                                                IconButton(
                                                  onPressed: () {
                                                    Get.defaultDialog(
                                                      title: tr(
                                                          'key_sure_to_cancel_your_Booking'),
                                                      titleStyle: const TextStyle(
                                                          color: AppColors
                                                              .mainYellowColor),
                                                      content: Column(
                                                        children: [
                                                          CustomButton(
                                                            widthButton: 6,
                                                            circularBorder:
                                                                screenWidth(20),
                                                            text: tr('key_Yes'),
                                                            onPressed: () {
                                                              controller
                                                                  .cancelBooking(
                                                                      idBooking:
                                                                          element
                                                                              .id!);
                                                            },
                                                          ),
                                                          screenWidth(20).ph,
                                                          CustomButton(
                                                            widthButton: 6,
                                                            circularBorder:
                                                                screenWidth(20),
                                                            text: tr('key_No'),
                                                            onPressed: () {
                                                              Get.back();
                                                            },
                                                          )
                                                        ],
                                                      ),
                                                    );
                                                  },
                                                  icon: const Icon(
                                                    Icons.cancel,
                                                    color:
                                                        AppColors.mainRedColor,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  ).toList(),
                                );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
