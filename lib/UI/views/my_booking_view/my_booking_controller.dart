import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/data/models/apis/my_booking_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/booking_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../core/enums/request_status.dart';
import '../../../core/services/base_controller.dart';
import '../../../core/translation/app_translation.dart';

class MyBookingController extends BaseController {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<MyBookingModel> myBooking = <MyBookingModel>[MyBookingModel()].obs;
  bool get isBookingLoading => requestStatus.value == RequestStatus.LOADING;

  @override
  void onInit() {
    getUserBooking();
    super.onInit();
  }

  void getUserBooking() {
    runLoadingFutureFunction(
        function: BookingRepository().getUserBooking(userId: storage.getUserId()).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_check_connection'), messageType: MessageType.REJECTED);
      }, (r) {
        if (myBooking.isNotEmpty) {
          myBooking.clear();
        }
        myBooking.addAll(r);
        myBooking.map((element) {
          element.message = "l";
        }).toSet();
      });
    }));
  }

  void cancelBooking({required int idBooking}) {
    runFullLoadingFutureFunction(
        function: BookingRepository()
            .cancelBooking(idBooking: idBooking, now: formatHour(DateTime.now()))
            .then((value) {
      value.fold((l) {
        if (l == null) {
          cancelBooking(idBooking: idBooking);
        } else {
          CustomToast.showMessage(
              message: tr('key_Can_not_Cancle'), messageType: MessageType.REJECTED);
        }
        Get.back();
      }, (r) {
        CustomToast.showMessage(message: tr('key_success'), messageType: MessageType.SUCCSESS);
        Get.back();
        onInit();
      });
    }));
  }

  String formatHour(DateTime date) {
    return DateFormat('HH:mm:ss').format(date);
  }
}
