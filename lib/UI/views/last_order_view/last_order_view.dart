import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/data/models/apis/user_products_model.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';
import '../filter_last_order_view/filter_last_order_view.dart';
import 'last_order_controller.dart';

class LastOrderView extends StatefulWidget {
  const LastOrderView(
      {super.key, required this.productsUser, required this.currentlocation});
  final List<UserProductsModel> productsUser;
  final LocationData? currentlocation;

  @override
  State<LastOrderView> createState() => _LastOrderViewState();
}

class _LastOrderViewState extends State<LastOrderView> {
  late LastOrderController controller;
  @override
  void initState() {
    controller =
        Get.put(LastOrderController(currentlocation: widget.currentlocation));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        bottomNavigationBar:const CustomBottomNavigation(),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: widget.productsUser.isEmpty
            ? Center(
                child: CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: tr('key_No_Product'),
                  textColor: AppColors.mainYellowColor,
                ),
              )
            : ListView(
                children: [
                  Padding(
                    padding: EdgeInsetsDirectional.symmetric(
                        horizontal: screenWidth(20), vertical: screenWidth(20)),
                    child: InkWell(
                      onTap: () {
                        Get.off(
                          FilterLastOrderView(
                            currentlocation: widget.currentlocation,
                            productsUser: storage.getUserProductsList(),
                          ),
                        );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          IconButton(
                            onPressed: () {
                              Get.back();
                            },
                            icon: Icon(
                              Icons.arrow_back_ios_new,
                              size: screenWidth(20),
                            ),
                          ),
                          Row(
                            children: [
                              SvgPicture.asset("images/filter.svg"),
                              screenWidth(30).pw,
                              CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_Filter'),
                                textColor: AppColors.mainYellowColor,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Column(
                    children: widget.productsUser.map(
                      (e) {
                        return SizedBox(
                          width: double.infinity,
                          child: Padding(
                            padding: EdgeInsetsDirectional.symmetric(
                                horizontal: screenWidth(20)),
                            child: InkWell(
                              onTap: () {
                                controller.getProductsDetalis(
                                    productId: e.productId!);
                              },
                              child: Card(
                                elevation: 4,
                                child: Padding(
                                  padding: EdgeInsetsDirectional.symmetric(
                                      horizontal: screenWidth(40),
                                      vertical: screenWidth(40)),
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        e.club == null
                                            ? Container(
                                                width: screenWidth(7),
                                                height: screenHeight(10),
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                      color: AppColors
                                                          .mainYellowColor),
                                                  shape: BoxShape.circle,
                                                ),
                                                child: Image.asset(
                                                    "images/cllllb.png"),
                                              )
                                            : CustomNetworkImage(
                                                context
                                                    .image(e.club!.mainImage!),
                                                width: screenWidth(7),
                                                height: screenHeight(10),
                                                border: Border.all(
                                                    color: AppColors
                                                        .mainYellowColor),
                                                shape: BoxShape.circle,
                                              ),
                                        screenWidth(20).pw,
                                        Column(
                                          children: [
                                            e.club == null
                                                ? CustomText(
                                                    textType:
                                                        TextStyleType.CUSTOM,
                                                    text:
                                                        tr('key_Product_Appr'),
                                                    textColor: AppColors
                                                        .mainBlackColor,
                                                  )
                                                : CustomText(
                                                    textType:
                                                        TextStyleType.CUSTOM,
                                                    text:
                                                        "${e.club!.name}\n${e.club!.city}",
                                                    textColor: AppColors
                                                        .mainBlackColor,
                                                  ),
                                            CustomText(
                                              textType: TextStyleType.CUSTOM,
                                              text: e.date ?? '',
                                              textColor:
                                                  AppColors.mainYellowColor,
                                              fontSize: screenWidth(40),
                                            ),
                                          ],
                                        ),
                                        screenWidth(20).pw,
                                        CustomNetworkImage(
                                          context.image(e.img!),
                                          width: 40,
                                          height: 40,
                                        ),
                                        screenWidth(30).pw,
                                        CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text: e.productName ?? '',
                                          textColor: AppColors.mainBlackColor,
                                          fontSize: screenWidth(40),
                                        ),
                                        screenWidth(30).pw,
                                        CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text: e.price.toString() +
                                              tr('key_AED'),
                                          textColor: AppColors.mainYellowColor,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ).toList(),
                  )
                ],
              ),
      ),
    );
  }
}
