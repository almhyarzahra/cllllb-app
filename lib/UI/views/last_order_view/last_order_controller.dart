import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/data/repositories/club_repositories.dart';
import '../../../core/enums/message_type.dart';
import '../../../core/services/base_controller.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_toast.dart';
import '../food_sport_detalis_view/food-sport_detalis_view.dart';
import '../main_view/home_view/home_controller.dart';

class LastOrderController extends BaseController {
  LastOrderController({this.currentlocation});
   LocationData? currentlocation;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  @override
  void onClose() {
    Get.find<HomeController>().getProductsUser();
    super.onClose();
  }

  void getProductsDetalis({required int productId}) {
    runFullLoadingFutureFunction(
        function: ClubRepositories()
            .getProductsById(productId: productId)
            .then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_Please_Try_Again'), messageType: MessageType.REJECTED);
      }, (r) {
        Get.to(() => FoodSportDetalisView( currentlocation: currentlocation, productsModel: r));
      });
    }));
  }
}
