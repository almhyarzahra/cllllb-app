import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/payment_view/payment_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_book_bar.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_second_bar.dart';
import '../../shared/custom_widgets/virtical_divider.dart';
import '../../shared/utils.dart';
import '../table_detalis_view/table_detalis_controller.dart';

class PaymentView extends StatefulWidget {
  const PaymentView(
      {super.key,
      required this.nameClub,
      required this.address,
      required this.imageClub,
      required this.clubId,
      required this.clubType,
      required this.nameGame,
      required this.imageGame,
      required this.quantity,
      required this.gameFieldId,
      required this.price,
      required this.imageGameSelected,
      required this.bookId,
      required this.currentlocation});
  final String nameClub;
  final String address;
  final String imageClub;
  final int clubId;
  final String clubType;
  final String nameGame;
  final String imageGame;
  final int quantity;
  final int gameFieldId;
  final double price;
  final String imageGameSelected;
  final Map<String, int> bookId;
  final LocationData? currentlocation;

  @override
  State<PaymentView> createState() => _PaymentViewState();
}

class _PaymentViewState extends State<PaymentView> with WidgetsBindingObserver {
  late PaymentController controller;

  @override
  void initState() {
    controller = Get.put(PaymentController(
        bookId: widget.bookId,
        totalPrice: widget.price,
        currentlocation: widget.currentlocation));
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.paused) {
      Get.back();
      if (controller.bookId.isNotEmpty) {
        controller.bookingRetreat(idBooking: controller.idBooking);
      }
      Get.find<TableDetalisController>().onInit();
      Get.find<TableDetalisController>().bookId.clear();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: Obx(
          () {
            return controller.bookId.isEmpty
                ? Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        IconButton(
                          onPressed: () {
                            Get.back();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios_new,
                            size: screenWidth(20),
                          ),
                        ),
                        CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('key_No_Booking_Yet'),
                          textColor: AppColors.mainBlackColor,
                        ),
                      ],
                    ),
                  )
                : ListView(
                    children: [
                      screenHeight(80).ph,
                      CustomBookBar(
                        imageUrl: widget.imageClub,
                        imageName:
                            Uri.https(NetworkUtil.baseUrl, widget.imageClub)
                                .toString(),
                        text:
                            "${widget.nameClub}/ ${widget.address}/ ${widget.clubType}",
                      ),
                      CustomSecondBar(
                        svgName: widget.imageGame,
                        text1: widget.nameGame,
                        text2: "${widget.quantity} ${tr('key_QUANTITY')}",
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(screenWidth(12)),
                          child: Card(
                            elevation: 3,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CustomText(
                                    textType: TextStyleType.TITLE,
                                    text: tr('key_BOOKING_SUMMARY'),
                                    textColor: AppColors.mainBlackColor,
                                  ),
                                ),
                                screenWidth(50).ph,
                                const VirticalDivider(
                                  width: 1.2,
                                ),
                                CustomNetworkImage(
                                  context.image(widget.imageGameSelected),
                                  width: screenWidth(4),
                                  height: screenHeight(10),
                                  margin: const EdgeInsetsDirectional.only(
                                      start: 10, top: 3),
                                ),
                               
                                CustomText(
                                  textType: TextStyleType.CUSTOM,
                                  text: tr('key_Time_Slots'),
                                  textColor: AppColors.mainYellowColor,
                                ),
                                Padding(
                                  padding: EdgeInsetsDirectional.only(
                                      start: screenWidth(20)),
                                  child: SizedBox(
                                    height: screenHeight(45),
                                    child: ListView(
                                      shrinkWrap: true,
                                      scrollDirection: Axis.horizontal,
                                      children: controller.time.map(
                                        (element) {
                                          return CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: element,
                                            textColor: AppColors.mainBlackColor,
                                          );
                                        },
                                      ).toList(),
                                    ),
                                  ),
                                ),
                                screenWidth(20).ph,
                                Row(
                                  children: [
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: tr('key_Total_Price'),
                                      textColor: AppColors.mainYellowColor,
                                    ),
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: widget.price.toString() +
                                          tr('key_AED'),
                                      textColor: AppColors.mainBlackColor,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsetsDirectional.symmetric(
                            horizontal: screenWidth(20)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            screenWidth(20).ph,
                            CustomText(
                              textType: TextStyleType.CUSTOM,
                              text: tr('key_Payment'),
                              textColor: AppColors.mainBlackColor,
                              textAlign: TextAlign.start,
                            ),
                            screenWidth(20).ph,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                InkWell(
                                  onTap: () {
                                    Get.defaultDialog(
                                      title: tr('key_Payment_Cash?'),
                                      titleStyle: const TextStyle(
                                          color: AppColors.mainYellowColor),
                                      content: Column(
                                        children: [
                                          CustomButton(
                                            widthButton: 4,
                                            circularBorder: screenWidth(20),
                                            text: tr('key_Yes'),
                                            onPressed: () {
                                              controller.paymentCash();
                                            },
                                          ),
                                          screenWidth(30).ph,
                                          CustomButton(
                                            widthButton: 4,
                                            circularBorder: screenWidth(20),
                                            text: tr('key_No'),
                                            onPressed: () {
                                              Get.back();
                                            },
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: screenWidth(4),
                                    height: screenWidth(8),
                                    decoration: BoxDecoration(
                                      color: AppColors.mainYellowColor,
                                      borderRadius: BorderRadius.circular(
                                          screenWidth(20)),
                                    ),
                                    child: Transform.scale(
                                      scale: 0.5,
                                      child: SvgPicture.asset(
                                        "images/cash.svg",
                                        color: AppColors.mainWhiteColor,
                                      ),
                                    ),
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    //controller.payment(paymntMethod: "Wallet");
                                    controller.paymentCard();
                                  },
                                  child: Container(
                                    width: screenWidth(4),
                                    height: screenWidth(8),
                                    decoration: BoxDecoration(
                                      color: AppColors.mainYellowColor,
                                      borderRadius: BorderRadius.circular(
                                          screenWidth(20)),
                                    ),
                                    child: Transform.scale(
                                      scale: 0.5,
                                      child: SvgPicture.asset(
                                        "images/ic_visa.svg",
                                        color: AppColors.mainWhiteColor,
                                      ),
                                    ),
                                  ),
                                ),
                                // GetPlatform.isIOS
                                //     ? SizedBox.shrink()
                                //     :
                                InkWell(
                                  onTap: () {
                                    //controller.payment(paymntMethod: "Wallet");
                                    controller.getWalletBalance();
                                  },
                                  child: Container(
                                    width: screenWidth(4),
                                    height: screenWidth(8),
                                    decoration: BoxDecoration(
                                      color: AppColors.mainYellowColor,
                                      borderRadius: BorderRadius.circular(
                                          screenWidth(20)),
                                    ),
                                    child: Transform.scale(
                                      scale: 0.5,
                                      child: SvgPicture.asset(
                                        "images/wallet.svg",
                                        color: AppColors.mainWhiteColor,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      screenWidth(20).ph,
                      Obx(
                        () {
                          return controller.visibleSharePayment.value
                              ? Column(
                                  children: [
                                    CustomButton(
                                      circularBorder: screenWidth(20),
                                      widthButton: 3,
                                      heightButton: 10,
                                      text: tr('key_add_friend'),
                                      onPressed: () {
                                        controller.visibleCheckNumber.value =
                                            true;
                                        controller.addFormFeild();
                                      },
                                    ),
                                    Column(
                                      children: controller.formField,
                                    ),
                                    controller.amount.value != 0
                                        ? CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: tr('key_amount_pay') +
                                                " " +
                                                double.parse(controller
                                                        .amount.value
                                                        .toStringAsFixed(2))
                                                    .toString(),
                                            textColor: AppColors.mainBlackColor,
                                          )
                                        : const SizedBox.shrink(),
                                    controller.wrong.isEmpty
                                        ? const SizedBox.shrink()
                                        : Column(
                                            children: controller.wrong.map(
                                              (element) {
                                                return CustomText(
                                                  textType:
                                                      TextStyleType.CUSTOM,
                                                  text: element,
                                                  textColor:
                                                      AppColors.mainRedColor,
                                                );
                                              },
                                            ).toList(),
                                          ),
                                    controller.visibleCheckNumber.value
                                        ? Column(
                                            children: [
                                              CustomButton(
                                                circularBorder: screenWidth(20),
                                                widthButton: 3,
                                                heightButton: 10,
                                                text: tr('key_check'),
                                                onPressed: () {
                                                  controller.checkWalletUser();
                                                },
                                              ),
                                              CustomButton(
                                                  circularBorder:
                                                      screenWidth(20),
                                                  widthButton: 1.5,
                                                  heightButton: 10,
                                                  text: tr(
                                                      'key_approval_friends'),
                                                  onPressed: controller
                                                          .approvalFriends.value
                                                      ? () {
                                                          controller
                                                              .sendBookNotification();
                                                        }
                                                      : null),
                                              controller.enableTimer.value
                                                  ? CustomText(
                                                      textType:
                                                          TextStyleType.CUSTOM,
                                                      text:
                                                          tr('key_waiting_approval') +
                                                              controller
                                                                  .counter.value
                                                                  .toString(),
                                                      textColor: AppColors
                                                          .mainBlackColor,
                                                    )
                                                  : const SizedBox.shrink(),
                                              controller.userNotApproved
                                                      .isNotEmpty
                                                  ? Column(
                                                      children: controller
                                                          .userNotApproved
                                                          .map(
                                                        (element) {
                                                          return CustomText(
                                                            textType:
                                                                TextStyleType
                                                                    .CUSTOM,
                                                            text: element +
                                                                tr('key_didnt_agree'),
                                                            textColor: AppColors
                                                                .mainBlackColor,
                                                          );
                                                        },
                                                      ).toList(),
                                                    )
                                                  : const SizedBox.shrink(),
                                              CustomButton(
                                                  circularBorder:
                                                      screenWidth(20),
                                                  widthButton: 1.5,
                                                  heightButton: 10,
                                                  text: tr('key_pay'),
                                                  onPressed:
                                                      controller.enablePay.value
                                                          ? () {
                                                              controller
                                                                  .paySharing();
                                                            }
                                                          : null),
                                            ],
                                          )
                                        : const SizedBox.shrink()
                                  ],
                                )
                              : const SizedBox.shrink();
                        },
                      ),

                      // screenWidth(3.1).ph,
                      // CustomContact()
                    ],
                  );
          },
        ),
      ),
    );
  }
}
