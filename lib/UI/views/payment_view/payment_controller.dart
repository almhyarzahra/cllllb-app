import 'dart:async';
import 'dart:developer';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/main_view/profile_view/profile_controller.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:lottie/lottie.dart';
import '../../../core/data/models/apis/check_approved_model.dart';
import '../../../core/data/models/apis/check_wallet_model.dart';
import '../../../core/data/repositories/booking_repository.dart';
import '../../../core/data/repositories/payment_repositories.dart';
import '../../../core/enums/message_type.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/custom_widgets/custom_toast.dart';
import '../my_booking_view/my_booking_view.dart';
import '../shopping_view/stripe_payment/payment_manager.dart';
import '../table_detalis_view/table_detalis_controller.dart';

class PaymentController extends BaseController {
  PaymentController(
      {required Map<String, int> bookId, required double totalPrice, this.currentlocation}) {
    this.bookId.value = bookId;
    this.totalPrice.value = totalPrice;
  }
  RxList<CheckWalletModel> userSharing = <CheckWalletModel>[].obs;
  LocationData? currentlocation;
  RxList<String> time = <String>[].obs;
  RxList<int> idBooking = <int>[].obs;
  RxDouble totalPrice = 0.0.obs;
  RxMap<String, int> bookId = <String, int>{}.obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  RxDouble wallet = 0.0.obs;
  RxBool hidden = true.obs;
  RxList<TextEditingController> numberControllers = <TextEditingController>[].obs;
  RxList<Widget> formField = <Widget>[].obs;
  RxBool visibleSharePayment = false.obs;
  RxBool visibleCheckNumber = false.obs;
  RxList<String> wrong = <String>[].obs;
  RxDouble amount = 0.0.obs;
  RxList<int> userIDS = <int>[].obs;
  RxBool enablePay = false.obs;
  RxBool approvalFriends = false.obs;
  RxBool enableTimer = false.obs;

  RxList<int> code = <int>[].obs;
  Timer? timer;
  RxInt counter = 59.obs;
  List<String> phone = [];
  RxList<CheckApprovedModel> checkApproved = <CheckApprovedModel>[].obs;
  RxList<String> userNotApproved = <String>[].obs;
  @override
  void onInit() {
    if (bookId.isNotEmpty) {
      getTimeSlots();
    }
    super.onInit();
  }

  @override
  void onClose() {
    if (bookId.isNotEmpty) {
      bookingRetreat(idBooking: idBooking);
    }
    timer?.cancel();
    Get.find<TableDetalisController>().onInit();
    Get.find<TableDetalisController>().bookId.clear();
    super.onClose();
  }

  void addFormFeild() {
    Rx<TextEditingController> numController = TextEditingController().obs;
    numberControllers.add(numController.value);
    formField.add(Padding(
      padding:
          EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20), vertical: screenWidth(50)),
      child: CustomTextField(
        hintText: tr('key_Mobile_Number'),
        controller: numController.value,
        typeInput: TextInputType.phone,
        contentPaddingLeft: screenWidth(10),
      ),
    ));
  }

  void getTimeSlots() {
    for (var entry in bookId.entries) {
      time.add("/${entry.key} ");
      idBooking.add(entry.value);
    }
    log(idBooking.toString());
  }

  void bookingRetreat({required List<int> idBooking}) {
    runFullLoadingFutureFunction(
        function: BookingRepository().bookingRetreat(id: idBooking).then((value) {
      value.fold((l) {
        bookingRetreat(idBooking: idBooking);
      }, (r) {});
    }));
  }

  void getWalletBalance() {
    runFullLoadingFutureFunction(
        function: PaymentRepositories().getWalletBalance(userId: storage.getUserId()).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_check_connection'), messageType: MessageType.REJECTED);
      }, (r) {
        wallet.value = r.userBalance!;
        r.code == 1
            ? Get.defaultDialog(
                title: tr('key_Wallet_contains') + wallet.value.toString() + tr('key_AED'),
                titleStyle: const TextStyle(color: AppColors.mainYellowColor),
                content: Column(
                  children: [
                    Lottie.asset(
                      "images/wallet.json",
                      width: screenWidth(3),
                    ),
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_fill_wallet'),
                      textColor: AppColors.mainRedColor,
                    ),
                    wallet.value < totalPrice.value
                        ? CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: tr('key_wallet_not_contain_the_full_amount!'),
                            textColor: AppColors.mainRedColor,
                          )
                        : Column(
                            children: [
                              CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_Do_you_want_pay_from_the_wallet'),
                                textColor: AppColors.mainBlackColor,
                              ),
                              screenWidth(30).ph,
                              CustomButton(
                                  widthButton: 4,
                                  circularBorder: screenWidth(20),
                                  text: tr('key_Yes'),
                                  onPressed: () async {
                                    for (var entry in bookId.entries) {
                                      walletUpdate(bookId: entry.value, userIds: []);
                                    }
                                    sentEmail(bookingIds: idBooking);

                                    //  Get.back();
                                  }),
                              screenWidth(30).ph,
                              CustomButton(
                                  widthButton: 4,
                                  circularBorder: screenWidth(20),
                                  text: tr('key_No'),
                                  onPressed: () {
                                    Get.back();
                                  }),
                              screenWidth(30).ph,
                              // GetPlatform.isIOS
                              //     ? SizedBox.shrink()
                              //     :
                              CustomButton(
                                  //widthButton: 4,
                                  circularBorder: screenWidth(20),
                                  text: tr('key_share_payment_friend'),
                                  onPressed: () {
                                    visibleSharePayment.value = true;
                                    Get.back();
                                  }),
                            ],
                          ),
                  ],
                ))
            : Get.defaultDialog(
                title: tr('key_You_Do_not_have_wallet_yet'),
                titleStyle: const TextStyle(color: AppColors.mainRedColor),
                content: Lottie.asset(
                  "images/wallet.json",
                  width: screenWidth(3),
                ));
      });
    }));
  }

  void walletUpdate({required int bookId, required List<int> userIds}) {
    runFullLoadingFutureFunction(
        visible: false,
        function: BookingRepository()
            .bookingWalletUpdate(bookId: bookId, user_ids: userIds)
            .then((value) {
          value.fold((l) {
            walletUpdate(bookId: bookId, userIds: userIds);
          }, (r) {
            editBookingStatus(bookId: bookId, paymentMethod: 2);
          });
        }));
  }

  void editBookingStatus({required int bookId, required int paymentMethod}) {
    runFullLoadingFutureFunction(
        visible: false,
        function: BookingRepository()
            .editBookingStatus(bookId: bookId, paymentMethod: paymentMethod)
            .then((value) {
          value.fold((l) {
            editBookingStatus(bookId: bookId, paymentMethod: paymentMethod);
          }, (r) {});
        }));
  }

  void paymentCard() {
    PaymentManager.makePayment(totalPrice.value.toInt(), "AED").then((value) async {
      await Stripe.instance.presentPaymentSheet().then((value) {
        for (var entry in bookId.entries) {
          editBookingStatus(bookId: entry.value, paymentMethod: 3);
        }
        sentEmail(bookingIds: idBooking);
      });
    });
  }

  void paymentCash() {
    for (var entry in bookId.entries) {
      editBookingStatus(bookId: entry.value, paymentMethod: 1);
    }
    sentEmail(bookingIds: idBooking);
    // Get.back();
  }

  void sentEmail({
    required List<int> bookingIds,
  }) {
    log(bookingIds.toString());
    log(storage.getUserId().toString());
    runFullLoadingFutureFunction(
        function: BookingRepository()
            .sentEmailByBooking(bookingIds: bookingIds, userId: storage.getUserId())
            .then((value) {
      value.fold((l) {
        // this.bookId.clear();
        // CustomToast.showMessage(
        //     message: tr('key_Succsess_Payment'),
        //     messageType: MessageType.SUCCSESS);
        // Get.off(MyBookingView(
        //   currentlocation: currentlocation,
        // ));

        sentEmail(bookingIds: bookingIds);

        bookId.clear();
        // CustomToast.showMessage(
        //     message: tr('key_Succsess_Payment'), messageType: MessageType.SUCCSESS);
        // Get.off(MyBookingView(
        //   currentlocation: currentlocation,
        // ));
      }, (r) {
        bookId.clear();
        if(r.draw!=null){
          CustomToast.showMessage(message: "${tr('wonEntryLottery')} '${r.draw}'",messageType: MessageType.SUCCSESS);
        }
        else if (r.points != null) {
          CustomToast.showMessage(
              message: "${r.points} points have been added to your account",
              messageType: MessageType.SUCCSESS);
        } else {
          CustomToast.showMessage(
              message: tr('key_Succsess_Payment'),
              messageType: MessageType.SUCCSESS);
        }
        // CustomToast.showMessage(
        //     message: tr('key_Succsess_Payment'), messageType: MessageType.SUCCSESS);
       // Get.find<ProfileController>().onInit();
        Get.off(MyBookingView(
          currentlocation: currentlocation,
        ));
      });
    }));
  }

  void checkWalletUser() {
    if (userNotApproved.isNotEmpty) {
      userNotApproved.clear();
    }
    approvalFriends.value = false;
    enablePay.value = false;
    phone.clear();
    amount.value = 0.0;
    int count = 0;
    wrong.clear();
    code.clear();

    numberControllers.map((e) {
      if (e.text.trim().isNotEmpty) {
        phone.add(e.text.trim().toString());
        count++;
      }
    }).toSet();
    amount.value = totalPrice.value / (count + 1);
    runFullLoadingFutureFunction(
        function: BookingRepository()
            .checkWalletUsers(
                phone: phone,
                amount: double.parse(amount.value.toStringAsFixed(2)),
                user_id: storage.getUserId())
            .then((value) {
      value.fold((l) {
        if (l == null) {
          CustomToast.showMessage(message: tr('key_try_again'), messageType: MessageType.REJECTED);
        } else {
          CustomToast.showMessage(
              message: tr('key_wallet_not_contain_the_full_amount!'),
              messageType: MessageType.INFO);
        }
      }, (r) {
        if (userSharing.isNotEmpty) {
          userSharing.clear();
          userIDS.clear();
        }

        r.map((e) {
          code.add(e.code!);
          if (e.code == 3) {
            userSharing.add(e);
            userIDS.add(e.user!.id!);

            ///
          } else {
            numberControllers.map((element) {
              if (element.text.trim().toString() == e.phone) {
                if (e.code == 0) {
                  wrong.add("${e.phone} ${tr('key_is_wrong')}");
                  element.text = "";
                  count--;
                } else if (e.code == 1) {
                  wrong.add("${e.phone} ${tr('key_doesnt_wallet')}");
                  element.text = "";
                  count--;
                } else if (e.code == 2) {
                  wrong.add("${e.phone} ${tr('key_wallet_less')}");
                  element.text = "";
                  count--;
                }
                amount.value = totalPrice.value / (count + 1);
              }
            }).toSet();
          }
        }).toSet();
        if (!code.contains(0) && !code.contains(1) && !code.contains(2)) {
          approvalFriends.value = true;
        }
      });
    }));
  }

  void paySharing() {
    for (var entry in bookId.entries) {
      walletUpdate(bookId: entry.value, userIds: userIDS);
    }
    sentEmail(bookingIds: idBooking);
  }

  void sendBookNotification() {
    // List<String> phone = [];
    // numberControllers.map((e) {
    //   if (e.text.trim().isNotEmpty) {
    //     phone.add(e.text.trim().toString());
    //   }
    // }).toSet();
    runFullLoadingFutureFunction(
        function: BookingRepository()
            .sentBookNotification(
                phones: phone,
                amount: double.parse(amount.value.toStringAsFixed(2)),
                bookingIDS: idBooking)
            .then((value) {
      value.fold((l) {
        sendBookNotification();
      }, (r) {
        enableTimer.value = true;
        startTimer();
      });
    }));
  }

  void startTimer() {
    userNotApproved.clear();
    counter.value = 59;
    const duration = Duration(seconds: 1);
    timer = Timer.periodic(duration, (timer) {
      if (counter > 0) {
        counter.value--;
      } else {
        // checkApproved.map((element) {
        //   if (element.code == 0) {
        //     userNotApproved.add(element.phone!);
        //   }
        // }).toSet();
        // if (userNotApproved.isEmpty) {
        //   CustomToast.showMessage(
        //       message: tr('key_success'), messageType: MessageType.SUCCSESS);
        //   enablePay.value = true;
        //   enableTimer.value = false;
        //   approvalFriends.value = false;
        // }
        checkUserApproved();
        timer.cancel();
        enableTimer.value = false;
      }
    });
  }

  void checkUserApproved() {
    log(phone.toString());
    log(idBooking.toString());
    runFullLoadingFutureFunction(
        function: BookingRepository()
            .checkUserApproved(phones: phone, bookingIDS: idBooking)
            .then((value) {
      value.fold((l) {
        checkUserApproved();
        // CustomToast.showMessage(
        //     message: tr('key_try_again'), messageType: MessageType.REJECTED);
      }, (r) {
        if (checkApproved.isNotEmpty) {
          checkApproved.clear();
        }

        checkApproved.addAll(r);
        checkApproved.map((element) {
          if (element.code == 0) {
            userNotApproved.add(element.phone!);
          }
        }).toSet();
        if (userNotApproved.isEmpty) {
          CustomToast.showMessage(message: tr('key_success'), messageType: MessageType.SUCCSESS);
          enablePay.value = true;
          enableTimer.value = false;
          approvalFriends.value = false;
        }
      });
    }));
  }
}
