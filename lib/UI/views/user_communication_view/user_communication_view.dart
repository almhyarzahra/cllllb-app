import 'dart:async';
import 'dart:io';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/views/user_communication_view/user_communication_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../core/data/models/apis/looking_job_model.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:http/http.dart' as http;
// ignore: depend_on_referenced_packages
import 'package:path_provider/path_provider.dart';

class UserCommunicationView extends StatefulWidget {
  const UserCommunicationView({super.key, required this.currentlocation, required this.lookingJob});
  final LocationData? currentlocation;
  final LookingJobModel lookingJob;

  @override
  State<UserCommunicationView> createState() => _UserCommunicationViewState();
}

class _UserCommunicationViewState extends State<UserCommunicationView> {
  late UserCommunicationController controller;
  String? localPath;
  bool isLoading = true;
  @override
  void initState() {
    controller = Get.put(UserCommunicationController());
    downloadPDF(
      Uri.https(NetworkUtil.baseUrl, widget.lookingJob.cv!).toString(),
    ).then(
      (path) {
        setState(
          () {
            // print(path);
            localPath = path;
            isLoading = false;
          },
        );
      },
    );
    super.initState();
  }

  Future<String> downloadPDF(String url) async {
    var response = await http.get(Uri.parse(url));
    var dir = await getApplicationDocumentsDirectory();
    File file = File('${dir.path}/document.pdf');
    await file.writeAsBytes(response.bodyBytes);
    return file.path;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: isLoading
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                children: [
                  SizedBox(
                    height: screenHeight(1.5),
                    child: PDFView(
                      filePath: localPath,
                      enableSwipe: true,
                      //swipeHorizontal: true,
                      autoSpacing: false,
                      pageFling: false,
                    ),
                  ),
                  screenWidth(30).ph,
                  CustomButton(
                    text: tr('key_call_now'),
                    onPressed: widget.lookingJob.user!.phone == null
                        ? null
                        : () {
                            final Uri emailLaunchUri = Uri(
                              scheme: 'tel',
                              path: widget.lookingJob.user!.phone,
                            );
                            launchUrl(emailLaunchUri, mode: LaunchMode.externalApplication);
                          },
                    widthButton: 2,
                    heightButton: 10,
                    circularBorder: screenWidth(20),
                  ),
                  screenWidth(100).ph,
                  CustomButton(
                    text: tr('key_send_mail'),
                    onPressed: () {
                      String? encodeQueryParameters(Map<String, String> params) {
                        return params.entries
                            .map((MapEntry<String, String> e) =>
                                '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
                            .join('&');
                      }

                      final Uri emailLaunchUri = Uri(
                        scheme: 'mailto',
                        path: widget.lookingJob.user!.email,
                        query: encodeQueryParameters(
                          <String, String>{
                            'subject': 'Cllllb',
                          },
                        ),
                      );
                      launchUrl(emailLaunchUri, mode: LaunchMode.externalApplication);
                    },
                    widthButton: 2,
                    heightButton: 10,
                    circularBorder: screenWidth(20),
                  ),
                ],
              ),
      ),
    );
  }
}
