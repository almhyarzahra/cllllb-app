import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/all_clubs_view/all_clubs_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/data/models/apis/all_clubs_model.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../detalis_view/detalis_view.dart';
import '../filter_all_clubs_view/filter_all_clubs_view.dart';

class AllClubsView extends StatefulWidget {
  const AllClubsView(
      {super.key, required this.clubs, required this.currentlocation});
  final List<AllClubsModel> clubs;
  final LocationData? currentlocation;

  @override
  State<AllClubsView> createState() => _AllClubsViewState();
}

class _AllClubsViewState extends State<AllClubsView> {
  late AllClubsController controller;
  @override
  void initState() {
    controller = Get.put(AllClubsController(clubs: widget.clubs));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: controller.key,
         bottomNavigationBar:const CustomBottomNavigation(),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: widget.clubs.isEmpty
            ? Center(
                child: CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: tr('key_No_Clubs'),
                  textColor: AppColors.mainYellowColor,
                ),
              )
            : ListView(
                addAutomaticKeepAlives: true,
                children: [
                  Padding(
                    padding: EdgeInsetsDirectional.symmetric(
                        horizontal: screenWidth(20), vertical: screenWidth(20)),
                    child: InkWell(
                      onTap: () {
                        Get.off(
                          FilterAllClubsView(
                            currentlocation: widget.currentlocation,
                            clubsModel: storage.getAllClubsList(),
                          ),
                        );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          IconButton(
                            onPressed: () {
                              Get.back();
                            },
                            icon: Icon(
                              Icons.arrow_back_ios_new,
                              size: screenWidth(20),
                            ),
                          ),
                          Row(
                            children: [
                              SvgPicture.asset("images/filter.svg"),
                              screenWidth(30).pw,
                              CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_Filter'),
                                textColor: AppColors.mainYellowColor,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    // CustomTextField(
                    //   onChanged: (value) {
                    //     controller.goal.value = value;
                    //   },
                    //   hintText: "Search",
                    //   contentPaddingLeft: screenWidth(10),
                    //   controller: controller.searchController,
                    //   suffixIcon: Container(
                    //       height: screenHeight(25),
                    //       width: screenWidth(10),
                    //       decoration: BoxDecoration(
                    //           color: AppColors.mainYellowColor,
                    //           borderRadius: BorderRadius.circular(screenWidth(10))),
                    //       child: Icon(
                    //         Icons.search,
                    //         color: AppColors.mainWhiteColor,
                    //       )),
                    // ),
                  ),
                  Padding(
                    padding: EdgeInsetsDirectional.symmetric(
                        horizontal: screenWidth(20)),
                    child: CustomText(
                      textType: TextStyleType.TITLE,
                      text: tr('key_All_Clubs'),
                      textColor: AppColors.mainYellowColor,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  Container(
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("images/mask.png"),
                          fit: BoxFit.fill),
                    ),
                    child: Obx(
                      () {
                        return Column(
                          children: controller.search().map(
                            (e) {
                              return Padding(
                                padding: EdgeInsetsDirectional.symmetric(
                                    horizontal: screenWidth(50)),
                                child: InkWell(
                                  onTap: () {
                                    Get.to(
                                      DetalisView(
                                        currentlocation: widget.currentlocation,
                                        nameClub: e.name!,
                                        address: e.city!,
                                        clubId: e.id!,
                                        imageClub: e.mainImage!,
                                      ),
                                    );
                                  },
                                  child: Card(
                                    elevation: 4,
                                    child: Padding(
                                      padding: EdgeInsetsDirectional.symmetric(
                                          horizontal: screenWidth(20)),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          CustomNetworkImage(
                                            context.image(e.mainImage!),
                                            width: screenWidth(7),
                                            height: screenHeight(10),
                                            border: Border.all(
                                                color:
                                                    AppColors.mainYellowColor),
                                            shape: BoxShape.circle,
                                          ),
                                          Flexible(
                                            child: Column(
                                              children: [
                                                CustomText(
                                                  textType:
                                                      TextStyleType.CUSTOM,
                                                  fontSize: screenWidth(30),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  text: "${e.name}",
                                                  textColor:
                                                      AppColors.mainBlackColor,
                                                  textAlign: TextAlign.center,
                                                ),
                                                CustomText(
                                                  textType:
                                                      TextStyleType.CUSTOM,
                                                  fontSize: screenWidth(30),
                                                  text: e.city ?? '',
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  textColor:
                                                      AppColors.mainBlackColor,
                                                  textAlign: TextAlign.center,
                                                ),
                                                CustomText(
                                                  textType:
                                                      TextStyleType.CUSTOM,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  text:
                                                      "${tr('key_Type')} ${e.clubTypeName}",
                                                  textColor:
                                                      AppColors.mainYellowColor,
                                                  fontSize: screenWidth(35),
                                                ),
                                                CustomText(
                                                  textType:
                                                      TextStyleType.CUSTOM,
                                                  fontSize: screenWidth(35),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  text: e.distance == null
                                                      ? ""
                                                      : e.distance!.toInt() >
                                                              1000
                                                          ? tr('key_near') +
                                                              (e.distance!.toInt() /
                                                                      1000)
                                                                  .toString() +
                                                              tr('key_km')
                                                          : tr('key_near') +
                                                              e.distance!
                                                                  .toInt()
                                                                  .toString() +
                                                              tr('key_meter'),
                                                  textColor:
                                                      AppColors.mainYellowColor,
                                                  textAlign: TextAlign.center,
                                                )
                                              ],
                                            ),
                                          ),
                                          CustomNetworkImage(
                                            context.image(e.licenceImage!),
                                            width: screenWidth(5),
                                            height: screenWidth(8),
                                            radius: screenWidth(15),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ).toList(),
                        );
                      },
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
