import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/data/models/apis/all_clubs_model.dart';

class AllClubsController extends BaseController {
  AllClubsController({required List<AllClubsModel> clubs}) {
    this.clubs.value = clubs;
  }
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  TextEditingController searchController = TextEditingController();
  Rx<String> goal = "".obs;
  RxList<AllClubsModel> clubs = <AllClubsModel>[].obs;
  List<AllClubsModel> search() {
    RxList<AllClubsModel> results = <AllClubsModel>[].obs;
    if (goal.isEmpty) {
      results.value = clubs;
    } else {
      results.value = clubs
          .where((p0) => p0.name!.toLowerCase().contains(goal.toLowerCase()))
          .toList();
    }
    return results;
  }
}
