import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/main_view/main_view.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class WelcomeView extends StatefulWidget {
  final String addedPoints;
  final String invitationCode;
  final String pointWillTake;
  const WelcomeView(
      {super.key, required this.addedPoints, required this.invitationCode, required this.pointWillTake});

  @override
  State<WelcomeView> createState() => _WelcomeViewState();
}

class _WelcomeViewState extends State<WelcomeView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            width: double.infinity,
            height: screenHeight(2),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: AppColors.mainYellowColor,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(screenWidth(5)),
                bottomRight: Radius.circular(screenWidth(5)),
              ),
            ),
            child: Container(
              width: 200,
              height: 200,
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                color: AppColors.mainWhiteColor,
                shape: BoxShape.circle,
              ),
              child: Image.asset("images/confetti.png"),
            ),
          ),
          const SizedBox(height: 10),
          Text(
            tr('congratulations'),
            style: const TextStyle(
              fontSize: 23,
              fontWeight: FontWeight.bold,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Text(
              "${tr('youGot')}${widget.addedPoints} ${tr('rewardPointsRegister')}",
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 18,
              ),
            ),
          ),
          const Spacer(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Text(
              "${tr('inviteFriendsAndGet')}${widget.pointWillTake} ${tr('rewardPoints')}",
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 15,
              ),
            ),
          ),
          CustomButton(
            text: tr('inviteFreind'),
            circularBorder: screenWidth(20),
            onPressed: () {
              Clipboard.setData(ClipboardData(text: widget.invitationCode))
                  .then(
                (value) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      duration: const Duration(seconds: 5),
                      action: SnackBarAction(
                          label: "GO",
                          onPressed: () {
                            Get.offAll(const MainView(initialPage: 0,));
                          }),
                      content: Text(tr('copiedInvitionCode')),
                    ),
                  );
                },
              );
            },
          ),
          const SizedBox(height: 15),
        ],
      ),
    );
  }
}
