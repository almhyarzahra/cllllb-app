import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_app_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_drawer.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/titled_textfield.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/donate_points_view/donate_points_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class DonatePointsView extends StatefulWidget {
  final LocationData? currentlocation;
  final double userPoints;
  const DonatePointsView(
      {super.key, required this.currentlocation, required this.userPoints});

  @override
  State<DonatePointsView> createState() => _DonatePointsViewState();
}

class _DonatePointsViewState extends State<DonatePointsView> {
  late DonatePointsController controller;
  @override
  void initState() {
    super.initState();
    controller = Get.put(DonatePointsController());
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        key: controller.key,
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: Form(
          key: controller.formKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            Get.back();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios_new,
                            size: screenWidth(20),
                          ),
                        ),
                        CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('donatePoints'),
                          textColor: AppColors.mainBlackColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 22,
                        ),
                      ],
                    ),
                    Container(
                      height: 35.0,
                      padding:
                          const EdgeInsetsDirectional.symmetric(horizontal: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: AppColors.mainGrey2Color,
                      ),
                      child: Row(
                        children: [
                          Obx(() {
                            return Text(
                              "${controller.point}",
                              style: const TextStyle(
                                color: AppColors.mainBlackColor,
                              ),
                            );
                          }),
                          const SizedBox(
                            width: 6,
                          ),
                          SvgPicture.asset("images/coin.svg"),
                        ],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: SvgPicture.asset("images/gift.svg"),
                ),
                CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: tr('giftPointsFriends'),
                  textColor: AppColors.mainBlackColor,
                ),
                const SizedBox(height: 60),
                TitledTextField(
                  title: tr('key_Mobile_Number'),
                  child: CustomTextField(
                    textDirection: TextDirection.ltr,
                    prefixIcon: storage.getAppLanguage() == "en"
                        ? Padding(
                            padding: EdgeInsetsDirectional.symmetric(
                                vertical: screenWidth(40)),
                            child: Text(
                              "+971",
                              style: TextStyle(fontSize: screenWidth(20)),
                            ),
                          )
                        : null,
                    suffixIcon: storage.getAppLanguage() == "ar"
                        ? Padding(
                            padding: EdgeInsetsDirectional.symmetric(
                                vertical: screenWidth(40)),
                            child: Text(
                              "971+",
                              style: TextStyle(fontSize: screenWidth(20)),
                            ),
                          )
                        : null,
                    hintText: "",
                    typeInput: TextInputType.phone,
                    controller: controller.mobileNumberController,
                    contentPaddingLeft: screenWidth(15),
                    contentPaddingRight: screenWidth(15),
                    validator: (value) {
                      return value!.isEmpty ? tr('key_Check_Number') : null;
                    },
                  ),
                ),
                TitledTextField(
                  title: tr('numberPointsGift'),
                  child: CustomTextField(
                    hintText: "",
                    controller: controller.pointsController,
                    contentPaddingLeft: screenWidth(15),
                    typeInput: TextInputType.number,
                    validator: (value) {
                      return value!.isEmpty ||
                              double.parse(value) > storage.getUserPoint()
                          ? tr('checkNumberPoints')
                          : null;
                    },
                  ),
                ),
                const SizedBox(height: 30),
                CustomButton(
                  text: tr('key_Send'),
                  onPressed: () {
                    controller.sendPoints();
                  },
                  circularBorder: screenWidth(10),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
