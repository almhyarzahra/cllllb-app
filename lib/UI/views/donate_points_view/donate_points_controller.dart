import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/UI/views/main_view/profile_view/profile_controller.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DonatePointsController extends BaseController {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  TextEditingController mobileNumberController = TextEditingController();
  TextEditingController pointsController = TextEditingController();
  RxDouble point=storage.getUserPoint().obs;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  void sendPoints() {
    if (formKey.currentState!.validate()) {
      runFullLoadingFutureFunction(
        function: UserRepository()
            .giftPoints(
          reciverNumber:"971${mobileNumberController.text.trim().toString()}",
          points: double.parse(pointsController.text.trim()),
        )
            .then(
          (value) {
            value.fold(
              (l) {
                if (l == null) {
                  CustomToast.showMessage(
                      message: tr('key_try_again'),
                      messageType: MessageType.REJECTED);
                } else {
                  CustomToast.showMessage(
                      message: tr('checkTheNumber'),
                      messageType: MessageType.INFO);
                }
              },
              (r) {
                Get.back();
                storage.setUserPoint(storage.getUserPoint()-double.parse(pointsController.text.trim()));
                Get.find<ProfileController>().getUserInfo();
                CustomToast.showMessage(
                    message: tr('key_success'),
                    messageType: MessageType.SUCCSESS);
              },
            );
          },
        ),
      );
    }
  }
}
