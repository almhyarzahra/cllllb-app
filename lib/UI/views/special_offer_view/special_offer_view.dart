import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_app_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_drawer.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/special_offer_view/special_offer_controller.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';

class SpecialOfferView extends StatefulWidget {
  const SpecialOfferView(
      {super.key,
      required this.nameClub,
      required this.address,
      required this.imageClub,
      required this.clubId,
      required this.clubType,
      required this.currentlocation});
  final String nameClub;
  final String address;
  final String imageClub;
  final int clubId;
  final String clubType;
  final LocationData? currentlocation;

  @override
  State<SpecialOfferView> createState() => _SpecialOfferViewState();
}

class _SpecialOfferViewState extends State<SpecialOfferView> {
  SpecialOfferController controller = SpecialOfferController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.mainYellowColor,
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
           preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
         
        ),
        body: Center(
          child: CustomText(
            text: "COMING SOON",
            textType: TextStyleType.CUSTOM,
            fontSize: screenWidth(10),
          ),
        ),
        // ListView(
        //   children: [
        //     screenHeight(80).ph,
        //     CustomBookBar(
        //       imageUrl: widget.imageClub,
        //       imageName:
        //           Uri.https(NetworkUtil.baseUrl, widget.imageClub).toString(),
        //       text: widget.nameClub +
        //           "/ " +
        //           widget.address +
        //           "/ " +
        //           widget.clubType,
        //     ),
        //     Row(
        //       children: [
        //         Container(
        //           width: screenWidth(2.2),
        //           decoration: BoxDecoration(
        //               border: Border.all(color: AppColors.mainBlackColor),
        //               borderRadius: BorderRadius.circular(screenWidth(20))),
        //           child: Padding(
        //             padding: EdgeInsetsDirectional.symmetric(
        //                 horizontal: screenWidth(20)),
        //             child: Column(
        //               crossAxisAlignment: CrossAxisAlignment.start,
        //               children: [
        //                 Center(
        //                   child: Container(
        //                     width: screenWidth(7),
        //                     height: screenHeight(10),
        //                     decoration: BoxDecoration(
        //                         border:
        //                             Border.all(color: AppColors.mainYellowColor),
        //                         shape: BoxShape.circle,
        //                         image: DecorationImage(
        //                             fit: BoxFit.contain,
        //                             image: CachedNetworkImageProvider(Uri.https(
        //                                     NetworkUtil.baseUrl, widget.imageClub)
        //                                 .toString()))),
        //                   ),
        //                 ),
        //                 CustomText(
        //                   textType: TextStyleType.CUSTOM,
        //                   text: "${widget.nameClub}/${widget.address}",
        //                   textColor: AppColors.mainBlackColor,
        //                 ),
        //                 CustomText(
        //                   textType: TextStyleType.CUSTOM,
        //                   text: "Rent Tabel For 6 H",
        //                   textColor: AppColors.mainBlackColor,
        //                 ),
        //                 CustomText(
        //                   textType: TextStyleType.CUSTOM,
        //                   text: "150 AED",
        //                   textColor: AppColors.mainYellowColor,
        //                 ),
        //                 CustomText(
        //                   textType: TextStyleType.CUSTOM,
        //                   text: "Date : 11/11/2023",
        //                   textColor: AppColors.mainBlackColor,
        //                 ),
        //                 CustomText(
        //                   textType: TextStyleType.CUSTOM,
        //                   text: "Time : 5:00pm- 11:00pm",
        //                   textColor: AppColors.mainYellowColor,
        //                   fontSize: screenWidth(30),
        //                 ),
        //                 CustomButton(
        //                   text: "Copy Code : AL8870 ",
        //                   textSize: screenWidth(40),
        //                   heightButton: 15,
        //                   onPressed: () {},
        //                   circularBorder: screenWidth(10),
        //                 )
        //               ],
        //             ),
        //           ),
        //         ),
        //       ],
        //     )
        //   ],
        // ),
      ),
    );
  }
}
