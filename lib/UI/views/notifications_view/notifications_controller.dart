import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../core/data/models/apis/notif_model.dart';
import '../../../core/data/repositories/booking_repository.dart';
import '../../../core/data/repositories/notifictions_repositories.dart';
import '../../../core/enums/request_status.dart';
import '../../../core/utils/general_util.dart';

class NotificationsController extends BaseController {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<NotifModel> notifList = <NotifModel>[NotifModel()].obs;
  RxList<Notifications> notifications = <Notifications>[].obs;
  bool get isNotifLoading => requestStatus.value == RequestStatus.LOADING;
  @override
  void onInit() {
    notificationService.counter.value = 0;
    getAllNotifications();
    notificationService.notificationStream.stream.listen((event) {
      onInit();
    });
    super.onInit();
  }

  void getAllNotifications() {
    runLoadingFutureFunction(
        function: NotificationsRepositories()
            .getNotifications(userId: storage.getUserId())
            .then((value) {
      value.fold((l) {
        getAllNotifications();
      }, (r) {
        if (notifList.isNotEmpty) {
          notifList.clear();
          notifications.clear();
        }

        notifList.addAll(r);
        r.map((e) {
          e.notifications!.map((en) {
            notifications.add(en);
          }).toSet();
        }).toSet();
        notifications.sort(((a, b) {
          DateTime datetimeA =
              DateFormat("yyyy-MM-dd HH:mm:ss").parse(a.createdAt!);
          DateTime datetimeB =
              DateFormat("yyyy-MM-dd HH:mm:ss").parse(b.createdAt!);
          return datetimeB.compareTo(datetimeA);
        }));
        notifList.map((element) {
          element.message = "l";
        }).toSet();
      });
    }));
  }

  void approveBooking({required int notificationId}) {
    runFullLoadingFutureFunction(
        function: BookingRepository()
            .approveBooking(
                notificationId: notificationId, userId: storage.getUserId())
            .then((value) {
      value.fold((l) {
        approveBooking(notificationId: notificationId);
      }, (r) {
        if (r.code == 1) {
          CustomToast.showMessage(
              message: tr('key_success'), messageType: MessageType.SUCCSESS);
        } else if (r.code == 0) {
          CustomToast.showMessage(
              message: tr('key_booking_cancelled'),
              messageType: MessageType.INFO);
        }
      });
    }));
  }
}
