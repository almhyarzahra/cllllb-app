import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/notifications_view/notifications_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/virtical_divider.dart';

class NotificationsView extends StatefulWidget {
  const NotificationsView({super.key, required this.currentlocation});
  final LocationData? currentlocation;

  @override
  State<NotificationsView> createState() => _NotificationsViewState();
}

class _NotificationsViewState extends State<NotificationsView> {
  NotificationsController controller = Get.put(NotificationsController());
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        // appBar: PreferredSize(
        //   preferredSize: Size.fromHeight(screenWidth(6)),
        //   child: CustomAppBar(
        //     currentlocation: widget.currentlocation,
        //     drwerOnPressed: () {
        //       controller.key.currentState!.openDrawer();
        //     },
        //   ),
        // ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            children: [
              screenWidth(15).ph,
              Row(
                children: [
                  IconButton(
                    onPressed: () {
                      Get.back();
                    },
                    icon: Icon(
                      Icons.arrow_back_ios_new,
                      size: screenWidth(20),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                    child: CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_Notifications'),
                      textColor: AppColors.mainBlackColor,
                      textAlign: TextAlign.start,
                      fontSize: screenWidth(15),
                    ),
                  ),
                ],
              ),
              Obx(
                () {
                  return controller.isNotifLoading
                      ? const SpinKitCircle(
                          color: AppColors.mainYellowColor,
                        )
                      : controller.notifList.isEmpty
                          ? Center(
                              child: CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_No_Notifications_Yet'),
                                textColor: AppColors.mainYellowColor,
                              ),
                            )
                          : controller.notifList[0].message == null
                              ? customFaildConnection(
                                  onPressed: () {
                                    controller.onInit();
                                  },
                                )
                              : Column(
                                  children: controller.notifications.map(
                                    (element) {
                                      return Padding(
                                        padding: EdgeInsetsDirectional.symmetric(
                                            horizontal: screenWidth(20)),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            screenWidth(10).ph,
                                            CustomText(
                                              textType: TextStyleType.CUSTOM,
                                              text: element.title!,
                                              textColor: AppColors.mainBlackColor,
                                              fontWeight: FontWeight.bold,
                                              textAlign: TextAlign.start,
                                            ),
                                            screenWidth(20).ph,
                                            CustomText(
                                              textType: TextStyleType.CUSTOM,
                                              text: element.body!,
                                              textColor: AppColors.mainYellowColor,
                                              textAlign: TextAlign.start,
                                            ),
                                            element.type == 1
                                                ? Column(
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Expanded(
                                                            flex: 1,
                                                            child: CustomButton(
                                                              circularBorder: screenWidth(20),
                                                              heightButton: 10,
                                                              text: tr('key_ok'),
                                                              onPressed: () {
                                                                controller.approveBooking(
                                                                    notificationId: element.id!);
                                                              },
                                                            ),
                                                          ),
                                                          screenWidth(20).pw,
                                                          Expanded(
                                                            flex: 1,
                                                            child: CustomButton(
                                                              circularBorder: screenWidth(20),
                                                              heightButton: 10,
                                                              text: tr('key_exit'),
                                                              onPressed: () {},
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      screenWidth(10).ph,
                                                    ],
                                                  )
                                                : screenWidth(10).ph,
                                            const VirticalDivider(
                                              width: 1,
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                  ).toList(),
                                );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
