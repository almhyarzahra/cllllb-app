import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import '../../../shared/colors.dart';
import '../../../shared/utils.dart';

class MemberShipsShimmer extends StatelessWidget {
  const MemberShipsShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: ListView.builder(
        itemCount: 3,
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsetsDirectional.symmetric(
                horizontal: screenWidth(20), vertical: screenWidth(20)),
            child: Row(
              children: [
                Container(
                  width: screenWidth(2),
                  height: screenWidth(2.5),
                  decoration: BoxDecoration(
                      color: AppColors.mainOrangeColor,
                      borderRadius: BorderRadius.circular(screenWidth(20))),
                ),
                screenWidth(15).pw,
                Container(
                  width: screenWidth(3),
                  height: screenWidth(10),
                  decoration: BoxDecoration(
                      color: AppColors.mainOrangeColor,
                      borderRadius: BorderRadius.circular(screenWidth(20))),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
