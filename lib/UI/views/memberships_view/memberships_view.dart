import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_book_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/memberships_view/memberships_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:lottie/lottie.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text_button.dart';
import '../../shared/custom_widgets/custom_text_field.dart';
import '../food_sport_detalis_view/food-sport_detalis_view.dart';
import 'memberships_widgets/memberships_shimmer.dart';

class MemberShipsView extends StatefulWidget {
  const MemberShipsView(
      {super.key,
      required this.nameClub,
      required this.address,
      required this.imageClub,
      required this.clubId,
      required this.clubType,
      required this.currentlocation});
  final String nameClub;
  final String address;
  final String imageClub;
  final int clubId;
  final String clubType;
  final LocationData? currentlocation;

  @override
  State<MemberShipsView> createState() => _MemberShipsViewState();
}

class _MemberShipsViewState extends State<MemberShipsView> {
  late MembershipsController controller;
  ScrollController scrollController = ScrollController();
  @override
  void initState() {
    controller = Get.put(MembershipsController(clubId: widget.clubId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        bottomNavigationBar: BottomAppBar(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              CustomTextButton(
                text: tr('key_previous'),
                colorText: AppColors.mainRedColor,
                fontWeight: FontWeight.bold,
                onPressed: () {
                  if (controller.displayIndex.value - 12 >= 0) {
                    controller.displayIndex.value -= 12;
                    controller.resultsPage.value = controller.resultSearch
                        .sublist(controller.displayIndex.value, controller.displayIndex.value + 12);
                  }
                },
              ),
              CustomTextButton(
                text: tr('key_next'),
                colorText: AppColors.mainColorGreen,
                fontWeight: FontWeight.bold,
                fontSize: screenWidth(20),
                onPressed: () {
                  // print('Reached the end of ListView!');
                  if (controller.displayIndex.value + 24 < controller.resultSearch.length) {
                    controller.displayIndex.value += 12;
                    //print(controller.displayIndex.value);
                    controller.resultsPage.value = controller.resultSearch
                        .sublist(controller.displayIndex.value, controller.displayIndex.value + 12);
                    // _scrollController.animateTo(
                    //     _scrollController.position.minScrollExtent,
                    //     duration: Duration(milliseconds: 500),
                    //     curve: Curves.easeInOut);
                  } else {
                    controller.resultsPage.value = controller.resultSearch
                        .sublist(controller.displayIndex.value, controller.resultSearch.length);
                    // _scrollController.animateTo(
                    //     _scrollController.position.minScrollExtent,
                    //     duration: Duration(milliseconds: 10000),
                    //     curve: Curves.easeInOut);
                  }
                },
              ),
            ],
          ),
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            controller: scrollController,
            children: [
              screenHeight(80).ph,
              CustomBookBar(
                imageUrl: widget.imageClub,
                imageName: Uri.https(NetworkUtil.baseUrl, widget.imageClub).toString(),
                text: "${widget.nameClub}/ ${widget.address}/ ${widget.clubType}",
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                    child: CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_SUBSCRIPTIONS'),
                      fontSize: screenWidth(18),
                      textColor: AppColors.mainYellowColor,
                    ),
                  ),
                  screenHeight(50).ph,
                  Padding(
                    padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                    child: CustomTextField(
                      onChanged: (value) {
                        controller.goal.value = value;
                        controller.search();
                      },
                      hintText: tr('key_Search'),
                      contentPaddingLeft: screenWidth(10),
                      controller: controller.searchController,
                      suffixIcon: Container(
                        height: screenHeight(25),
                        width: screenWidth(10),
                        decoration: BoxDecoration(
                          color: AppColors.mainYellowColor,
                          borderRadius: BorderRadius.circular(screenWidth(10)),
                        ),
                        child: const Icon(
                          Icons.search,
                          color: AppColors.mainWhiteColor,
                        ),
                      ),
                    ),
                  ),
                  screenHeight(50).ph,
                  Obx(
                    () {
                      return controller.isSubscriptionsLoading
                          ? const MemberShipsShimmer()
                          : controller.productsSU.isEmpty
                              ? Center(
                                  child: CustomText(
                                    textType: TextStyleType.CUSTOM,
                                    text: tr('key_No_Subscriptions_Yet'),
                                    textColor: AppColors.mainBlackColor,
                                  ),
                                )
                              : controller.productsSU[0].message == null
                                  ? customFaildConnection(
                                      onPressed: () {
                                        controller.onInit();
                                      },
                                    )
                                  : ListView.builder(
                                      itemCount: controller.resultsPage.length,
                                      shrinkWrap: true,
                                      scrollDirection: Axis.vertical,
                                      physics: const NeverScrollableScrollPhysics(),
                                      itemBuilder: (BuildContext context, int index) {
                                        return Padding(
                                          padding: EdgeInsetsDirectional.symmetric(
                                              vertical: screenWidth(20),
                                              horizontal: screenWidth(20)),
                                          child: Row(
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  Get.to(
                                                    FoodSportDetalisView(
                                                      currentlocation: widget.currentlocation,
                                                      productsModel: controller.resultsPage[index],
                                                    ),
                                                  );
                                                },
                                                child: CachedNetworkImage(
                                                  imageUrl: Uri.https(NetworkUtil.baseUrl,
                                                          controller.resultsPage[index].img!)
                                                      .toString(),
                                                  placeholder: (context, url) =>
                                                      Lottie.asset("images/download.json"),
                                                  errorWidget: (context, url, error) =>
                                                      const Icon(Icons.error),
                                                  width: screenWidth(2.5),
                                                  height: screenHeight(6),
                                                ),
                                              ),
                                              screenWidth(15).pw,
                                              Flexible(
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    CustomText(
                                                      textType: TextStyleType.CUSTOM,
                                                      overflow: TextOverflow.ellipsis,
                                                      text: controller.resultsPage[index].name!,
                                                      fontSize: screenWidth(20),
                                                      textColor: AppColors.mainBlackColor,
                                                    ),
                                                    CustomText(
                                                      textType: TextStyleType.CUSTOM,
                                                      overflow: TextOverflow.ellipsis,
                                                      text: controller.resultsPage[index]
                                                          .productCategory![0].categoryInfo!.name!,
                                                      textColor: AppColors.mainYellowColor,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      },
                                    );
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
