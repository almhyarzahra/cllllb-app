import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/data/models/apis/all_products_model.dart';
import '../../../core/data/repositories/club_repositories.dart';
import '../../../core/enums/message_type.dart';
import '../../../core/enums/request_status.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_toast.dart';

class MembershipsController extends BaseController {
  MembershipsController({required int clubId}) {
    this.clubId.value = clubId;
  }
  RxInt clubId = 0.obs;
  TextEditingController searchController = TextEditingController();
  RxList<AllProductsModel> productsSU =
      <AllProductsModel>[AllProductsModel()].obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<AllProductsModel> resultSearch = <AllProductsModel>[].obs;
  RxList<AllProductsModel> resultsPage = <AllProductsModel>[].obs;
  Rx<String> goal = "".obs;
  RxInt displayIndex = 0.obs;
  bool get isSubscriptionsLoading =>
      requestStatus.value == RequestStatus.LOADING;

  @override
  void onInit() {
    getSubscriptions();
    super.onInit();
  }

  void getSubscriptions() {
    runLoadingFutureFunction(
        function: ClubRepositories()
            .getSpecialOffersApp(clubId: clubId.value, categoryId: 4)
            .then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_No_Internet_Connection'),
            messageType: MessageType.REJECTED);
      }, (r) {
        if (productsSU.isNotEmpty) {
          productsSU.clear();
        }
        productsSU.addAll(r);
        productsSU.map((element) {
          element.message = "l";
        }).toSet();
        resultSearch.value = productsSU;
        //print(resultSearch.length);
        if (resultSearch.length <= 12) {
          resultsPage.value = resultSearch;
        } else {
          resultsPage.value = resultSearch.sublist(0, 12);
        }
      });
    }));
  }

  List<AllProductsModel> search() {
    if (goal.isEmpty) {
      resultSearch.value = productsSU;
    } else {
      resultSearch.value = productsSU
          .where((p0) => p0.name!.toLowerCase().contains(goal.toLowerCase()))
          .toList();
    }
    if (resultSearch.length <= 12) {
      resultsPage.value = resultSearch;
    } else {
      resultsPage.value = resultSearch.sublist(0, 12);
    }
    return resultSearch;
  }
}
