import 'dart:developer';

import 'package:com.tad.cllllb/UI/views/vouchers_view/vouchers_controller.dart';
import 'package:com.tad.cllllb/core/data/models/apis/vouchers_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FilterVouchersController extends BaseController {
  FilterVouchersController({
    required List<VouchersModel> vouchersModel,
  }) {
    vouchers.value = vouchersModel;
  }
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<VouchersModel> vouchers = <VouchersModel>[VouchersModel()].obs;
  bool get isMoreVouchersLoading =>
      listType.contains(OperationType.filterMoreVouchers);
  RxInt pageKey = 2.obs;
  RxBool getMore = true.obs;
  RxBool enableScrollListner = true.obs;

  Future<void> getMoreVouchers({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
      type: OperationType.filterMoreVouchers,
      function: UserRepository()
          .getVouchers(
        page: page,
        startDate: Get.find<VouchersController>().startdate.value.isEmpty
            ? null
            : Get.find<VouchersController>().startdate.value,
        endDate: Get.find<VouchersController>().enddate.value.isEmpty
            ? null
            : Get.find<VouchersController>().enddate.value,
        price:
            Get.find<VouchersController>().pointController.text.trim().isEmpty
                ? null
                : Get.find<VouchersController>().pointController.text.trim(),
      )
          .then(
        (value) {
          value.fold((l) {
            pageKey.value = pageKey.value - 1;
          }, (r) {
            if (r.isEmpty) {
              enableScrollListner.value = false;
            } else {
              vouchers.addAll(r);
            }
          });
        },
      ),
    ).then((value) => getMore.value = true);
  }
}
