import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_app_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_drawer.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/filter_vouchers_view/filter_vouchers_controller.dart';
import 'package:com.tad.cllllb/UI/views/vouchers_view/vouchers_controller.dart';
import 'package:com.tad.cllllb/core/data/models/apis/vouchers_model.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class FilterVouchersView extends StatefulWidget {
  final List<VouchersModel> vouchersModel;
  const FilterVouchersView({super.key, required this.vouchersModel});

  @override
  State<FilterVouchersView> createState() => _FilterVouchersViewState();
}

class _FilterVouchersViewState extends State<FilterVouchersView> {
  late FilterVouchersController controller;
  final ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
    controller =
        Get.put(FilterVouchersController(vouchersModel: widget.vouchersModel));
  }

  void _scrollListener() async {
    if (controller.getMore.value) {
      if (controller.enableScrollListner.value &&
          _scrollController.position.maxScrollExtent ==
              _scrollController.offset &&
          !_scrollController.position.outOfRange) {
        controller.getMore.value = false;
        await controller.getMoreVouchers(page: controller.pageKey.value);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(
          currentlocation: Get.find<VouchersController>().currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: Get.find<VouchersController>().currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          controller: _scrollController,
          padding: const EdgeInsets.symmetric(horizontal: 15),
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
              ],
            ),
            Obx(() {
              return ListView.separated(
                separatorBuilder: (context, index) => const SizedBox(height: 8),
                itemCount: controller.vouchers.length,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Container(
                    width: double.infinity,
                    height: 180,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 10),
                    decoration: BoxDecoration(
                      color: AppColors.mainGrey2Color.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          controller.vouchers[index].title!
                              .useCorrectEllipsis(),
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: AppColors.mainBlackColor,
                          ),
                        ),
                        Text(
                          "${tr('startDate')}${DateFormat("dd-MM-yyyy").format(controller.vouchers[index].startDate!)}"
                              .useCorrectEllipsis(),
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            color: AppColors.mainGreyColor,
                          ),
                        ),
                        Text(
                          "${tr('endDate')}${DateFormat("dd-MM-yyyy").format(controller.vouchers[index].endDate!)}"
                              .useCorrectEllipsis(),
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            color: AppColors.mainGreyColor,
                          ),
                        ),
                        Text(
                          "${tr('key_Price')}: ${controller.vouchers[index].points} ${tr('point')}"
                              .useCorrectEllipsis(),
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            color: AppColors.mainRedColor,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              "${tr('willGetDiscount')}${controller.vouchers[index].couponValue} %",
                              style: const TextStyle(
                                color: AppColors.mainBlackColor,
                              ),
                            ),
                            if (!controller
                                .vouchers[index].userAlreadyPurchased!)
                              CustomButton(
                                text: tr('buy'),
                                onPressed: () {
                                  Get.find<VouchersController>().buyingVoucher(
                                      couponId: controller.vouchers[index].id!,
                                      points:
                                          controller.vouchers[index].points!,
                                      allow: controller.vouchers[index]
                                          .userHasEnoughPoints!);
                                },
                                circularBorder: screenWidth(10),
                                widthButton: 6,
                                heightButton: 10,
                              ),
                            if (controller
                                .vouchers[index].userAlreadyPurchased!)
                              CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('ownThisVoucher'),
                                textColor: AppColors.mainRedColor,
                              ),
                          ],
                        ),
                      ],
                    ),
                  );
                },
              ).animate().scale(delay: 100.ms);
            }),
            Obx(
              () {
                return controller.isMoreVouchersLoading
                    ? const SpinKitCircle(
                        color: AppColors.mainYellowColor,
                      )
                    : const SizedBox.shrink();
              },
            ),
          ],
        ),
      ),
    );
  }
}
