import 'dart:developer';

import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/data/models/apis/all_products_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/club_repositories.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class SearchAllProductsController extends BaseController {
  SearchAllProductsController({this.currentlocation,}){
        //this.allProducts.value = allProducts;
  }
   LocationData? currentlocation;
   
  TextEditingController searchController = TextEditingController();
  RxBool enableSearch = false.obs;
  RxList<AllProductsModel> allProducts =
      <AllProductsModel>[AllProductsModel()].obs;
  RxString goal = "".obs;
  RxBool getMore = true.obs;
  RxInt pageKey = 2.obs;
  bool get isProductLoading =>
      listType.contains(OperationType.searchAllProducts);
  bool get isMoreSearchLoading => listType.contains(OperationType.moreSearch);

  void getProducts({required int page}) {
    runLoadingFutureFunction(
      type: OperationType.searchAllProducts,
      function: ClubRepositories()
          .searchAllProducts(page: page, name: goal.value)
          .then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                message: tr('key_No_Internet_Connection'),
                messageType: MessageType.REJECTED,
              );
            },
            (r) {
              if (allProducts.isNotEmpty) {
                allProducts.clear();
              }
              allProducts.addAll(r);
              allProducts.map((element) {
                element.message = "l";
              }).toSet();
            },
          );
        },
      ),
    );
  }

  Future<void> getMoreProducts({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
      type: OperationType.moreSearch,
      function: ClubRepositories()
          .searchAllProducts(page: page, name: goal.value)
          .then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                message: tr('key_No_Internet_Connection'),
                messageType: MessageType.REJECTED,
              );
              pageKey.value = pageKey.value - 1;
            },
            (r) {
              if (r.isEmpty) {
                pageKey.value = pageKey.value - 1;
              } else {
                allProducts.addAll(r);
              }
            },
          );
        },
      ),
    ).then((value) => getMore.value = true);
  }
   void addTofavorite({required AllProductsModel productsModel}) {
    if (favoriteService.getCartModel(productsModel) == null) {
      favoriteService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Favorite'), messageType: MessageType.SUCCSESS);
          });
    } else {
      CustomToast.showMessage(
          message: productsModel.name! + tr('key_already_in_your_favourites'),
          messageType: MessageType.INFO);
    }
  }

  void addToCart({required AllProductsModel productsModel}) {
    if (currentlocation == null) {
      CustomToast.showMessage(
          message: tr('key_Please_turn_on_location_feature'), messageType: MessageType.INFO);
    } else if (productsModel.inProduct == 1 &&
        calculateDistance(
                currentlocation!.latitude!,
                currentlocation!.longitude!,
                double.parse(productsModel.club!.latitude!),
                double.parse(productsModel.club!.longtitude!)) >
            100.0) {
      CustomToast.showMessage(message: tr('key_within_100_metres'), messageType: MessageType.INFO);
    } else {
      cartService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Card'), messageType: MessageType.SUCCSESS);
          });
    }
  }
  double calculateDistance(double lat1, double lng1, double lat2, double lng2) {
    double distance = Geolocator.distanceBetween(lat1, lng1, lat2, lng2);
    return distance;
  }
}
