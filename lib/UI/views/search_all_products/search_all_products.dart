import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/food_sport_detalis_view/food-sport_detalis_view.dart';
import 'package:com.tad.cllllb/UI/views/search_all_products/search_all_products_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class SearchAllProducts extends StatefulWidget {
  final LocationData? currentlocation;
  const SearchAllProducts({super.key, required this.currentlocation});

  @override
  State<SearchAllProducts> createState() => _SearchAllProductsState();
}

class _SearchAllProductsState extends State<SearchAllProducts> {
  late SearchAllProductsController controller;
  final ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    super.initState();
    controller = Get.put(
        SearchAllProductsController(currentlocation: widget.currentlocation));

    _scrollController.addListener(_scrollListener);
  }

  void _scrollListener() async {
    if (controller.allProducts.isNotEmpty) {
      if (controller.getMore.value) {
        if (_scrollController.position.maxScrollExtent ==
                _scrollController.offset &&
            !_scrollController.position.outOfRange) {
          controller.getMore.value = false;
          await controller.getMoreProducts(page: controller.pageKey.value);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: CustomScrollView(
          controller: _scrollController,
          slivers: [
            SliverAppBar(
              pinned: true,
              backgroundColor: AppColors.mainGrey4Color,
              leading: IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: const Icon(
                    Icons.arrow_back,
                    color: AppColors.mainBlackColor,
                  )),
              title: CustomTextField(
                hintText: tr('key_Search'),
                controller: controller.searchController,
                contentPaddingLeft: screenWidth(10),
                onChanged: (value) {
                  if (value.isNotEmpty) {
                    controller.goal.value = value;
                    controller.getProducts(page: 1);
                    controller.enableSearch.value = true;
                  } else {
                    controller.enableSearch.value = false;
                  }
                },
              ),
            ),
            SliverToBoxAdapter(
              child: Obx(
                () {
                  return !controller.enableSearch.value
                      ? const SizedBox.shrink()
                      : controller.isProductLoading
                          ? const SpinKitCircle(
                              color: AppColors.mainYellowColor,
                            )
                          : controller.allProducts.isEmpty
                              ? CustomText(
                                  textType: TextStyleType.CUSTOM,
                                  text: tr('noProductsMatchingSearch'),
                                  textColor: AppColors.mainBlackColor,
                                )
                              : controller.allProducts[0].message == null
                                  ? customFaildConnection(
                                      onPressed: () {
                                        controller.getProducts(page: 1);
                                      },
                                    )
                                  : GridView.count(
                                      childAspectRatio: 0.6,
                                      crossAxisCount: 2,
                                      crossAxisSpacing: 0,
                                      shrinkWrap: true,
                                      physics: const BouncingScrollPhysics(),
                                      children: controller.allProducts.map(
                                        (e) {
                                          return Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Card(
                                              elevation: 4,
                                              child: SizedBox(
                                                width: screenWidth(2.2),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        screenWidth(40).pw,
                                                        CustomNetworkImage(
                                                          context.image(
                                                              e.club!.mainImage!),
                                                          width: screenWidth(8.7),
                                                          height:
                                                              screenHeight(10),
                                                          border: Border.all(
                                                              color: AppColors
                                                                  .mainYellowColor),
                                                          shape: BoxShape.circle,
                                                        ),
                                                        screenWidth(40).pw,
                                                        Flexible(
                                                          child: Column(
                                                            children: [
                                                              CustomText(
                                                                textType:
                                                                    TextStyleType
                                                                        .CUSTOM,
                                                                text:
                                                                    "${e.club!.name}",
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                textColor: AppColors
                                                                    .mainBlackColor,
                                                                fontSize:
                                                                    screenWidth(
                                                                        25),
                                                              ),
                                                              CustomText(
                                                                textType:
                                                                    TextStyleType
                                                                        .CUSTOM,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                text:
                                                                    "${e.club!.city}",
                                                                textColor: AppColors
                                                                    .mainYellowColor,
                                                                fontSize:
                                                                    screenWidth(
                                                                        25),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    CustomText(
                                                      textType:
                                                          TextStyleType.CUSTOM,
                                                      text: e.name!,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      textColor: AppColors
                                                          .mainBlackColor,
                                                    ),
                                                    screenWidth(30).ph,
                                                    CustomNetworkImage(
                                                      context.image(e.img!),
                                                      width: screenWidth(1),
                                                      height: screenHeight(7),
                                                      onTap: () => Get.to(
                                                        FoodSportDetalisView(
                                                          currentlocation: widget
                                                              .currentlocation,
                                                          productsModel: e,
                                                        ),
                                                      ),
                                                    ),
                                                    screenWidth(25).ph,
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        InkWell(
                                                          onTap: () {
                                                            controller
                                                                .addTofavorite(
                                                                    productsModel:
                                                                        e);
                                                          },
                                                          child: SizedBox(
                                                            height:
                                                                screenWidth(10),
                                                            width:
                                                                screenWidth(10),
                                                            child: ClipRRect(
                                                              child: Card(
                                                                shape: RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                                18)),
                                                                elevation: 4,
                                                                child: Center(
                                                                  child: SvgPicture
                                                                      .asset(
                                                                          "images/like.svg"),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        CustomText(
                                                          textType: TextStyleType
                                                              .CUSTOM,
                                                          textColor: AppColors
                                                              .mainYellowColor,
                                                          text:
                                                              e.price.toString() +
                                                                  tr('key_AED'),
                                                        ),
                                                        InkWell(
                                                          onTap: () {
                                                            controller.addToCart(
                                                                productsModel: e);
                                                          },
                                                          child: SizedBox(
                                                            height:
                                                                screenWidth(10),
                                                            width:
                                                                screenWidth(10),
                                                            child: ClipRRect(
                                                              child: Card(
                                                                shape:
                                                                    RoundedRectangleBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              18),
                                                                ),
                                                                elevation: 4,
                                                                child: Center(
                                                                  child: SvgPicture
                                                                      .asset(
                                                                          "images/shopping-cart.svg"),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      ).toList(),
                                    );
                },
              ),
            ),
            SliverToBoxAdapter(
              child: Obx(
                () {
                  return controller.isMoreSearchLoading
                      ? const SpinKitCircle(
                          color: AppColors.mainYellowColor,
                        )
                      : const SizedBox.shrink();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
