import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/sign_in_view/sign_in_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../main_view/main_view.dart';
import '../personal_information_view/personal_information_view.dart';

class SignInView extends StatefulWidget {
  const SignInView({super.key});

  @override
  State<SignInView> createState() => _SignInViewState();
}

class _SignInViewState extends State<SignInView> {
  late SignInController controller;
  @override
  void initState() {
    controller = Get.put(SignInController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Form(
          key: controller.formKey,
          child: Padding(
            padding:
                EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
            child: ListView(
              children: [
                Image.asset(
                  "images/cllllb.png",
                  height: screenHeight(5),
                ),
                screenHeight(50).ph,
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(screenWidth(15)),
                    border: Border.all(color: AppColors.mainYellowColor),
                  ),
                  child: Padding(
                    padding: EdgeInsetsDirectional.symmetric(
                        horizontal: screenWidth(30)),
                    child: Column(
                      children: [
                        screenHeight(20).ph,
                        CustomText(
                          textType: TextStyleType.SUBTITLE,
                          textColor: AppColors.mainGreyColor,
                          text: tr('key_signIn_or_createAccount'),
                        ),
                        screenHeight(50).ph,
                        CustomText(
                          textType: TextStyleType.SMALL,
                          textColor: AppColors.mainGreyColor,
                          text: tr('key_choose_signIn_options'),
                        ),
                        screenHeight(50).ph,
                        Padding(
                          padding: EdgeInsetsDirectional.symmetric(
                              horizontal: screenWidth(20)),
                          child: Row(
                            children: [
                              CustomText(
                                textType: TextStyleType.CUSTOM,
                                textColor: AppColors.mainBlackColor,
                                fontSize: screenWidth(20),
                                text: tr('key_Mobile_Number'),
                              ),
                            ],
                          ),
                        ),
                        CustomTextField(
                          textDirection: TextDirection.ltr,
                          prefixIcon: storage.getAppLanguage() == "en"
                              ? Padding(
                                  padding: EdgeInsetsDirectional.symmetric(
                                      horizontal: screenWidth(20),
                                      vertical: screenWidth(40)),
                                  child: Text(
                                    "+971",
                                    style: TextStyle(
                                      fontSize: screenWidth(20),
                                    ),
                                  ),
                                )
                              : null,
                          suffixIcon: storage.getAppLanguage() == "ar"
                              ? Padding(
                                  padding: EdgeInsetsDirectional.symmetric(
                                      vertical: screenWidth(40)),
                                  child: Text(
                                    "971+",
                                    style: TextStyle(
                                      fontSize: screenWidth(20),
                                    ),
                                  ),
                                )
                              : null,
                          typeInput: TextInputType.phone,
                          hintText: "",
                          contentPaddingLeft: screenWidth(10),
                          contentPaddingRight: screenWidth(10),
                          controller: controller.mobileNumberController,
                          validator: (value) {
                            return value!.isEmpty
                                ? tr('key_Check_Number')
                                : null;
                          },
                        ),
                        screenHeight(50).ph,
                        CustomButton(
                          circularBorder: screenWidth(15),
                          heightButton: 8,
                          onPressed: () async {
                            controller.loginOtp();
                          },
                          text: tr('key_logIn'),
                          backgroundColor: AppColors.mainYellowColor,
                        ),
                        screenHeight(30).ph,
                        CustomButton(
                          text: tr('key_google'),
                          svgName: "ic_google1",
                          //colorSvg: AppColors.mainRedColor,
                          textSize: screenWidth(35),
                          textColor: AppColors.mainBlackColor,
                          backgroundColor: AppColors.mainWhiteColor,
                          borderColor: AppColors.mainRedColor,
                          onPressed: () {
                            controller.googleLogin();
                          },
                          widthButton: 2.5,
                          circularBorder: screenWidth(15),
                        ),
                        screenHeight(30).ph,
                      ],
                    ),
                  ),
                ),
                screenHeight(50).ph,
                CustomTextButton(
                  onPressed: () async {
                    await storage.setCartList([]);
                    await storage.setFavoriteList([]);
                    storage.setFirstLanuch(true);
                    storage.setIsLoggedIN(false);
                    storage.globalSharedPrefs.remove(storage.PREF_TOKEN_INFO);
                    //storage.setTokenInfo(null);
                    await storage.setUserId(0);
                    storage.globalSharedPrefs.remove(storage.PREF_USER_NAME);

                    storage.setGoogle(false);
                    storage.globalSharedPrefs
                        .remove(storage.PREF_MOBILE_NUMBER);
                    storage.globalSharedPrefs.remove(storage.PREF_EMAIL);

                    Get.offAll(() => const MainView(initialPage: 0,));
                  },
                  text: tr('key_continue_visitor'),
                  colorText: AppColors.mainYellowColor,
                  decoration: TextDecoration.underline,
                  decorationThickness: 3,
                ),
                screenHeight(50).ph,
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(screenWidth(15)),
                    border: Border.all(color: AppColors.mainYellowColor),
                  ),
                  child: Padding(
                    padding: EdgeInsetsDirectional.symmetric(
                        horizontal: screenWidth(30)),
                    child: Column(
                      children: [
                        screenHeight(50).ph,
                        CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('key_Don\'t_have_account'),
                          textColor: AppColors.mainBlackColor,
                        ),
                        screenHeight(50).ph,
                        CustomButton(
                          text: tr('key_signUp'),
                          onPressed: () {
                            Get.to(const PersonalInformationView());
                          },
                          circularBorder: screenWidth(15),
                          heightButton: 8,
                        ),
                        screenHeight(50).ph,
                      ],
                    ),
                  ),
                ),
                screenWidth(20).ph,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
