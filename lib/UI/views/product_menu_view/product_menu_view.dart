import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/product_menu_view/product_menu_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_book_bar.dart';
import '../../shared/custom_widgets/custom_card_sport.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text_button.dart';
import '../../shared/custom_widgets/custom_text_field.dart';
import '../../shared/utils.dart';
import '../category_view/category_view.dart';
import '../food_sport_detalis_view/food-sport_detalis_view.dart';

class ProductMenuView extends StatefulWidget {
  const ProductMenuView(
      {super.key,
      required this.currentlocation,
      required this.menuId,
      required this.nameClub,
      required this.address,
      required this.imageClub,
      required this.clubType});
  final LocationData? currentlocation;
  final int menuId;
  final String nameClub;
  final String address;
  final String imageClub;
  final String clubType;

  @override
  State<ProductMenuView> createState() => _ProductMenuViewState();
}

class _ProductMenuViewState extends State<ProductMenuView> {
  late ProductMenuController controller;
  ScrollController scrollController = ScrollController();
  @override
  void initState() {
    controller = Get.put(
        ProductMenuController(menuId: widget.menuId, currentlocation: widget.currentlocation));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: BottomAppBar(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              CustomTextButton(
                text: tr('key_previous'),
                colorText: AppColors.mainRedColor,
                fontWeight: FontWeight.bold,
                onPressed: () {
                  if (controller.displayIndex.value - 3 >= 0) {
                    controller.displayIndex.value -= 3;
                    controller.resultsPage.value = controller.resultSearch
                        .sublist(controller.displayIndex.value, controller.displayIndex.value + 3);
                  }
                },
              ),
              CustomTextButton(
                text: tr('key_next'),
                colorText: AppColors.mainColorGreen,
                fontWeight: FontWeight.bold,
                fontSize: screenWidth(20),
                onPressed: () {
                  //print('Reached the end of ListView!');
                  if (controller.displayIndex.value + 6 < controller.resultSearch.length) {
                    controller.displayIndex.value += 3;
                   // print(controller.displayIndex.value);
                    controller.resultsPage.value = controller.resultSearch
                        .sublist(controller.displayIndex.value, controller.displayIndex.value + 3);
                    // _scrollController.animateTo(
                    //     _scrollController.position.minScrollExtent,
                    //     duration: Duration(milliseconds: 500),
                    //     curve: Curves.easeInOut);
                  } else {
                    controller.resultsPage.value = controller.resultSearch
                        .sublist(controller.displayIndex.value, controller.resultSearch.length);
                    // _scrollController.animateTo(
                    //     _scrollController.position.minScrollExtent,
                    //     duration: Duration(milliseconds: 10000),
                    //     curve: Curves.easeInOut);
                  }
                },
              ),
            ],
          ),
        ),
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
          
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            controller: scrollController,
            children: [
              screenHeight(80).ph,
              CustomBookBar(
                imageUrl: widget.imageClub,
                imageName: Uri.https(NetworkUtil.baseUrl, widget.imageClub).toString(),
                text:"${widget.nameClub}/ ${widget.address}/ ${widget.clubType}"
                ,
              ),
              screenHeight(50).ph,
              Obx(
                () {
                  return controller.isCategoriesLoading
                      ? const SpinKitCircle(
                          color: AppColors.mainYellowColor,
                        )
                      :controller.categories[0].message == null
                          ? customFaildConnection(onPressed: () {
                              controller.onInit();
                            })
                          : 
                      Column(
                          children: [
                            Padding(
                              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(10)),
                              child: DropdownButton(
                                isExpanded: true,
                                hint: CustomText(
                                  text: tr('key_Filter'),
                                  textType: TextStyleType.CUSTOM,
                                  textColor: AppColors.mainYellowColor,
                                ),
                                items: controller.categories.map(
                                  (e) {
                                    return DropdownMenuItem(
                                      value: e,
                                      child: Text(
                                        e.name!,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      
                                    );
                                  },
                                ).toList(),
                                onChanged: (v) {
                                  Get.to(
                                    CategoryView(
                                      categoryModel: v!,
                                      currentlocation: widget.currentlocation,
                                      address: widget.address,
                                      clubType: widget.clubType,
                                      imageClub: widget.imageClub,
                                      nameClub: widget.nameClub,
                                    ),
                                  );
                                },
                              ),
                            ),
                            screenHeight(50).ph,
                            Padding(
                              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                              child: CustomTextField(
                                onChanged: (value) {
                                  controller.goal.value = value;
                                  controller.search();
                                },
                                hintText: tr('key_Search'),
                                contentPaddingLeft: screenWidth(10),
                                controller: controller.searchController,
                                suffixIcon: Container(
                                  height: screenHeight(25),
                                  width: screenWidth(10),
                                  decoration: BoxDecoration(
                                    color: AppColors.mainYellowColor,
                                    borderRadius: BorderRadius.circular(screenWidth(10)),
                                  ),
                                  child:const Icon(
                                    Icons.search,
                                    color: AppColors.mainWhiteColor,
                                  ),
                                ),
                              ),
                            ),
                            screenHeight(50).ph,
                            Card(
                              elevation: 4,
                              shadowColor: AppColors.mainYellowColor,
                              child: SizedBox(
                                width: double.infinity,
                                height: screenHeight(10),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: tr('key_subscription_menu'),
                                      textColor: AppColors.mainYellowColor,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: tr('sub_meals'),
                                      textColor: AppColors.mainBlackColor,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            screenHeight(50).ph,
                            Column(
                              // crossAxisAlignment: CrossAxisAlignment.start,
                              children: controller.resultsPage.map(
                                (element) {
                                  return Column(
                                    // crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          Get.to(
                                            CategoryView(
                                              categoryModel: element,
                                              currentlocation: widget.currentlocation,
                                              address: widget.address,
                                              clubType: widget.clubType,
                                              imageClub: widget.imageClub,
                                              nameClub: widget.nameClub,
                                            ),
                                          );
                                        },
                                        child: Container(
                                          width: double.infinity,
                                          height: screenHeight(25),
                                          color: AppColors.mainYellowColor,
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Padding(
                                                padding: EdgeInsetsDirectional.symmetric(
                                                    horizontal: screenWidth(20)),
                                                child: CustomText(
                                                  textType: TextStyleType.CUSTOM,
                                                  text: element.name!,
                                                  overflow: TextOverflow.ellipsis,
                                                  textAlign: TextAlign.start,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      element.menuProduct!.isEmpty
                                          ? Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: CustomText(
                                                textType: TextStyleType.CUSTOM,
                                                text: tr("key_no_products"),
                                                textColor: AppColors.mainBlackColor,
                                              ),
                                            )
                                          : SizedBox(
                                              height: screenHeight(2.3),
                                              child: ListView.builder(
                                                shrinkWrap: true,
                                                scrollDirection: Axis.horizontal,
                                                itemCount: element.menuProduct!.length <= 5
                                                    ? element.menuProduct!.length
                                                    : 5,
                                                itemBuilder: (BuildContext context, int index) {
                                                  return Padding(
                                                    padding: EdgeInsetsDirectional.symmetric(
                                                        horizontal: screenWidth(50)),
                                                    child: CustomCardStore(
                                                      rewardPoint: element
                                                                .menuProduct![index].product!.pointsValue,
                                                      onPressedImage: () {
                                                        Get.to(
                                                          FoodSportDetalisView(
                                                            currentlocation: widget.currentlocation,
                                                            productsModel: element
                                                                .menuProduct![index].product!,
                                                          ),
                                                        );
                                                      },
                                                      onPressedLike: () {
                                                        controller.addTofavorite(
                                                            productsModel: element
                                                                .menuProduct![index].product!);
                                                      },
                                                      onPressedCart: () {
                                                        controller.addToCart(
                                                            productsModel: element
                                                                .menuProduct![index].product!);
                                                      },
                                                      imageUrl: Uri.https(
                                                              NetworkUtil.baseUrl,
                                                              element.menuProduct![index].product!
                                                                  .img!)
                                                          .toString(),
                                                      textBunch: element.menuProduct![index]
                                                                  .product!.club !=
                                                              null
                                                          ? tr('Club :') +
                                                              element.menuProduct![index].product!
                                                                  .club!.name!
                                                          : "",
                                                      textItem: element
                                                          .menuProduct![index].product!.name!,
                                                      textLength: element.name!,
                                                      textPrice: element
                                                              .menuProduct![index].product!.price!
                                                              .toString() +
                                                          tr('key_AED'),
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                    ],
                                  );
                                },
                              ).toList(),
                            ),
                          ].animate(interval: 50.ms).scale(delay: 100.ms),
                        );
                },
              )
            ].animate(interval: 50.ms).scale(delay: 100.ms),
          ),
        ),
      ),
    );
  }
}
