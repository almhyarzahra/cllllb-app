import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/food_sport_detalis_view/food_sport_detalis_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/data/models/apis/all_products_model.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_book_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';

class FoodSportDetalisView extends StatefulWidget {
  const FoodSportDetalisView({
    super.key,
    required this.productsModel,
    required this.currentlocation,
  });
  final AllProductsModel productsModel;
  final LocationData? currentlocation;

  @override
  State<FoodSportDetalisView> createState() => _FoodSportDetalisViewState();
}

class _FoodSportDetalisViewState extends State<FoodSportDetalisView> {
  late FoodSportDetalisController controller;
  @override
  void initState() {
    controller = FoodSportDetalisController(
        productsModel: widget.productsModel,
        currentlocation: widget.currentlocation);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
         bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          children: [
            screenHeight(80).ph,
            widget.productsModel.club == null
                ? Padding(
                    padding: EdgeInsetsDirectional.symmetric(
                        horizontal: screenWidth(20)),
                    child: Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            Get.back();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios_new,
                            size: screenWidth(20),
                          ),
                        ),
                        CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('key_Products_From_App'),
                          textColor: AppColors.mainBlackColor,
                          fontSize: screenWidth(15),
                          fontWeight: FontWeight.bold,
                        ),
                      ],
                    ),
                  )
                : CustomBookBar(
                    imageUrl: widget.productsModel.club!.mainImage!,
                    imageName: Uri.https(NetworkUtil.baseUrl,
                            widget.productsModel.club!.mainImage!)
                        .toString(),
                    text:
                        "${widget.productsModel.club!.name!}/ ${widget.productsModel.club!.city!}/ ${widget.productsModel.clubTypeName!}",
                  ),
            Padding(
              padding:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: widget.productsModel.name!,
                    textColor: AppColors.mainYellowColor,
                    fontSize: screenWidth(15),
                  ),
                  Center(
                    child: CustomNetworkImage(
                      context.image(widget.productsModel.img!),
                      width: screenWidth(1),
                      height: screenHeight(3),
                      radius: screenWidth(30),
                    ),
                    // ClipRRect(
                    //   borderRadius: BorderRadius.circular(screenWidth(30)),
                    //   child: CachedNetworkImage(
                    //     imageUrl:
                    //         Uri.https(NetworkUtil.baseUrl, widget.productsModel.img!).toString(),
                    //     placeholder: (context, url) => Lottie.asset("images/download.json"),
                    //     errorWidget: (context, url, error) => const Icon(Icons.error),
                    //     width: screenWidth(1),
                    //     height: screenHeight(3),
                    //     fit: BoxFit.fill,
                    //   ),
                    // ),
                  ),
                  screenHeight(20).ph,
                  Column(
                    children: [
                      CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: widget.productsModel.productCategory!.isEmpty
                            ? ""
                            : widget.productsModel.productCategory![0]
                                .categoriesName!.name!,
                        textColor: AppColors.mainBlackColor,
                        fontSize: screenWidth(20),
                      ),
                      CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: widget.productsModel.club == null
                            ? ""
                            : tr('Club :') + widget.productsModel.club!.name!,
                        textColor: AppColors.mainGrey3Color,
                      ),
                    ],
                  ),
                  screenWidth(10).pw,
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: tr('key_Price') +
                        ":" +
                        widget.productsModel.price.toString() +
                        tr('key_AED'),
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(15),
                    textColor: AppColors.mainYellowColor,
                  ),
                  if (widget.productsModel.pointsValue != null)
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: "+${widget.productsModel.pointsValue} ${tr('rewardPoints')}",
                      overflow: TextOverflow.ellipsis,
                      fontSize: 15,
                      textColor: AppColors.mainYellowColor,
                    ),
                  screenWidth(15).ph,
                  widget.productsModel.description == null
                      ? const SizedBox.shrink()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(
                              textType: TextStyleType.CUSTOM,
                              text: tr('key_Description:'),
                              textColor: AppColors.mainYellowColor,
                              fontWeight: FontWeight.bold,
                            ),
                            CustomText(
                              textType: TextStyleType.CUSTOM,
                              text: widget.productsModel.description!,
                              textColor: AppColors.mainBlackColor,
                              textAlign: TextAlign.start,
                            ),
                          ],
                        ),
                  screenWidth(20).ph,
                  Obx(
                    () {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            onPressed: controller.count.value == 1
                                ? null
                                : () {
                                    controller.changeCount(false);
                                  },
                            style: ElevatedButton.styleFrom(
                              shape: const StadiumBorder(),
                              backgroundColor: AppColors.mainYellowColor,
                              disabledBackgroundColor: AppColors.mainGreyColor,
                              fixedSize: Size(screenWidth(8), screenHeight(25)),
                            ),
                            child: const Text("-"),
                          ),
                          (screenWidth(45)).pw,
                          CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: "${controller.count}",
                            textColor: AppColors.mainBlackColor,
                            fontWeight: FontWeight.bold,
                            fontSize: screenWidth(28),
                          ),
                          (screenWidth(50)).pw,
                          ElevatedButton(
                            onPressed: () {
                              controller.changeCount(true);
                            },
                            style: ElevatedButton.styleFrom(
                              shape: const StadiumBorder(),
                              backgroundColor: AppColors.mainYellowColor,
                            ),
                            child: const Text("+"),
                          ),
                        ],
                      );
                    },
                  ),
                  screenWidth(10).ph,
                  Center(
                    child: CustomButton(
                      circularBorder: screenWidth(20),
                      text: tr('key_ADD_TO_CARD'),
                      onPressed: () {
                        controller.addToCart();
                        // Get.to(CardView(
                        //   imageName: "widget.imageName",
                        //   title: "widget.title",
                        // ));
                      },
                      //widthButton: 2.5,
                    ),
                  ),
                ],
              ),
            ),
            //  screenWidth(18).ph,
            // CustomContact()
          ],
        ),
      ),
    );
  }
}
