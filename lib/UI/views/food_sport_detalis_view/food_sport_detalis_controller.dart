import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/data/models/apis/all_products_model.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/general_util.dart';

class FoodSportDetalisController extends BaseController {
  FoodSportDetalisController({required this.productsModel, required this.currentlocation});
  LocationData? currentlocation;
  AllProductsModel productsModel = AllProductsModel();
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxInt count = 1.obs;
  void changeCount(bool incress) {
    if (incress) {
      count++;
    } else {
      if (count > 1) count--;
    }
  }

  void addToCart() {
    if (productsModel.club == null) {
      cartService.addToCart(
          model: productsModel,
          count: count.value,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Card'), messageType: MessageType.SUCCSESS);
          });
    } else if (currentlocation == null) {
      CustomToast.showMessage(
          message: tr('key_Please_turn_on_location_feature'), messageType: MessageType.INFO);
    } else if (productsModel.inProduct == 1 &&
        calculateDistance(
                currentlocation!.latitude!,
                currentlocation!.longitude!,
                double.parse(productsModel.club!.latitude!),
                double.parse(productsModel.club!.longtitude!)) >
            100.0) {
      CustomToast.showMessage(message: tr('key_within_100_metres'), messageType: MessageType.INFO);
    } else {
      cartService.addToCart(
          model: productsModel,
          count: count.value,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Card'), messageType: MessageType.SUCCSESS);
          });
    }
  }

  double calculateDistance(double lat1, double lng1, double lat2, double lng2) {
    double distance = Geolocator.distanceBetween(lat1, lng1, lat2, lng2);
    return distance;
  }
}
