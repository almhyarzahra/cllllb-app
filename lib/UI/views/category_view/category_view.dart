import 'package:com.tad.cllllb/UI/views/category_view/category_controller.dart';
import 'package:com.tad.cllllb/core/data/models/apis/category_model.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_book_bar.dart';
import '../../shared/custom_widgets/custom_card_sport.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/custom_widgets/custom_text_button.dart';
import '../../shared/custom_widgets/custom_text_field.dart';
import '../../shared/utils.dart';
import '../food_sport_detalis_view/food-sport_detalis_view.dart';

class CategoryView extends StatefulWidget {
  const CategoryView(
      {super.key,
      required this.currentlocation,
      required this.categoryModel,
      required this.nameClub,
      required this.address,
      required this.imageClub,
      required this.clubType});
  final LocationData? currentlocation;
  final CategoryModel categoryModel;
  final String nameClub;
  final String address;
  final String imageClub;
  final String clubType;

  @override
  State<CategoryView> createState() => _CategoryViewState();
}

class _CategoryViewState extends State<CategoryView> {
  ScrollController scrollController = ScrollController();
  late CategoryController controller;
  @override
  void initState() {
    controller = Get.put(CategoryController(
        currentlocation: widget.currentlocation,
        menuProduct: widget.categoryModel.menuProduct!));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: BottomAppBar(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              CustomTextButton(
                text: tr('key_previous'),
                colorText: AppColors.mainRedColor,
                fontWeight: FontWeight.bold,
                onPressed: () {
                  if (controller.displayIndex.value - 12 >= 0) {
                    controller.displayIndex.value -= 12;
                    controller.resultsPage.value = controller.resultSearch
                        .sublist(controller.displayIndex.value,
                            controller.displayIndex.value + 12);
                  }
                },
              ),
              CustomTextButton(
                text: tr('key_next'),
                colorText: AppColors.mainColorGreen,
                fontWeight: FontWeight.bold,
                fontSize: screenWidth(20),
                onPressed: () {
                  //  print('Reached the end of ListView!');
                  if (controller.displayIndex.value + 24 <
                      controller.resultSearch.length) {
                    controller.displayIndex.value += 12;
                    // print(controller.displayIndex.value);
                    controller.resultsPage.value = controller.resultSearch
                        .sublist(controller.displayIndex.value,
                            controller.displayIndex.value + 12);
                    // _scrollController.animateTo(
                    //     _scrollController.position.minScrollExtent,
                    //     duration: Duration(milliseconds: 500),
                    //     curve: Curves.easeInOut);
                  } else {
                    controller.resultsPage.value = controller.resultSearch
                        .sublist(controller.displayIndex.value,
                            controller.resultSearch.length);
                    // _scrollController.animateTo(
                    //     _scrollController.position.minScrollExtent,
                    //     duration: Duration(milliseconds: 10000),
                    //     curve: Curves.easeInOut);
                  }
                },
              ),
            ],
          ),
        ),
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          controller: scrollController,
          children: [
            screenHeight(80).ph,
            CustomBookBar(
              imageUrl: widget.imageClub,
              imageName:
                  Uri.https(NetworkUtil.baseUrl, widget.imageClub).toString(),
              text: "${widget.nameClub}/ ${widget.address}/ ${widget.clubType}",
            ),
            screenHeight(50).ph,
            Padding(
              padding:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
              child: CustomTextField(
                onChanged: (value) {
                  controller.goal.value = value;
                  controller.search();
                },
                hintText: tr('key_Search'),
                contentPaddingLeft: screenWidth(10),
                controller: controller.searchController,
                suffixIcon: Container(
                  height: screenHeight(25),
                  width: screenWidth(10),
                  decoration: BoxDecoration(
                    color: AppColors.mainYellowColor,
                    borderRadius: BorderRadius.circular(screenWidth(10)),
                  ),
                  child: const Icon(
                    Icons.search,
                    color: AppColors.mainWhiteColor,
                  ),
                ),
              ),
            ),
            screenHeight(50).ph,
            Container(
              width: double.infinity,
              height: screenHeight(25),
              color: AppColors.mainYellowColor,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsetsDirectional.symmetric(
                        horizontal: screenWidth(20)),
                    child: CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: widget.categoryModel.name!,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.start,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            screenHeight(40).ph,
            widget.categoryModel.menuProduct!.isEmpty
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr("key_no_products"),
                      textColor: AppColors.mainBlackColor,
                    ),
                  )
                : Obx(
                    () {
                      return GridView.count(
                        childAspectRatio: 0.6,
                        crossAxisCount: 2,
                        crossAxisSpacing: 0,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        children: controller.resultsPage
                            .map(
                              (element) {
                                return CustomCardStore(
                                  rewardPoint: element.product!.pointsValue,
                                  onPressedImage: () {
                                    Get.to(
                                      FoodSportDetalisView(
                                        currentlocation: widget.currentlocation,
                                        productsModel: element.product!,
                                      ),
                                    );
                                  },
                                  onPressedLike: () {
                                    controller.addTofavorite(
                                        productsModel: element.product!);
                                  },
                                  onPressedCart: () {
                                    controller.addToCart(
                                        productsModel: element.product!);
                                  },
                                  imageUrl: Uri.https(NetworkUtil.baseUrl,
                                          element.product!.img!)
                                      .toString(),
                                  textBunch: element.product!.club?.name != null
                                      ? tr('Club :') +
                                          element.product!.club!.name!
                                      : "",
                                  textItem: element.product!.name!,
                                  textLength: widget.categoryModel.name!,
                                  textPrice: element.product!.price.toString() +
                                      tr('key_AED'),
                                );
                              },
                            )
                            .toList()
                            .animate(interval: 50.ms)
                            .scale(delay: 100.ms),
                      );
                    },
                  )
          ].animate(interval: 50.ms).scale(delay: 100.ms),
        ),
      ),
    );
  }
}
