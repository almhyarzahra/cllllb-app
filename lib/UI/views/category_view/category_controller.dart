import 'dart:developer';

import 'package:com.tad.cllllb/core/data/models/apis/category_model.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/data/models/apis/all_products_model.dart';
import '../../../core/enums/message_type.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/general_util.dart';
import '../../shared/custom_widgets/custom_toast.dart';

class CategoryController extends BaseController {
  CategoryController({this.currentlocation, required List<MenuProduct> menuProduct}) {
    this.menuProduct.value = menuProduct;
  }
  RxList<MenuProduct> menuProduct = <MenuProduct>[].obs;
  LocationData? currentlocation;
  Rx<String> goal = "".obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  TextEditingController searchController = TextEditingController();
  RxList<MenuProduct> resultSearch = <MenuProduct>[].obs;
  RxList<MenuProduct> resultsPage = <MenuProduct>[].obs;
  RxInt displayIndex = 0.obs;

  @override
  void onInit() {
    resultSearch.value = menuProduct;
    log(resultSearch.length.toString());
    if (resultSearch.length <= 12) {
      resultsPage.value = resultSearch;
    } else {
      resultsPage.value = resultSearch.sublist(0, 12);
    }
    super.onInit();
  }

  List<MenuProduct> search() {
    if (goal.isEmpty) {
      resultSearch.value = menuProduct;
    } else {
      resultSearch.value = menuProduct
          .where((p0) => p0.product!.name!.toLowerCase().contains(goal.toLowerCase()))
          .toList();
    }
    if (resultSearch.length <= 3) {
      resultsPage.value = resultSearch;
    } else {
      resultsPage.value = resultSearch.sublist(0, 3);
    }
    return resultSearch;
  }

  void addTofavorite({required AllProductsModel productsModel}) {
    if (favoriteService.getCartModel(productsModel) == null) {
      favoriteService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Favorite'), messageType: MessageType.SUCCSESS);
          });
    } else {
      CustomToast.showMessage(
          message: productsModel.name! + tr('key_already_in_your_favourites'),
          messageType: MessageType.INFO);
    }
  }

  void addToCart({required AllProductsModel productsModel}) {
    if (productsModel.club == null) {
      cartService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Card'), messageType: MessageType.SUCCSESS);
          });
    } else if (currentlocation == null) {
      CustomToast.showMessage(
          message: tr('key_Please_turn_on_location_feature'), messageType: MessageType.INFO);
    } else if (productsModel.inProduct == 1 &&
        calculateDistance(
                currentlocation!.latitude!,
                currentlocation!.longitude!,
                double.parse(productsModel.club!.latitude!),
                double.parse(productsModel.club!.longtitude!)) >
            100.0) {
      CustomToast.showMessage(message: tr('key_within_100_metres'), messageType: MessageType.INFO);
    } else {
      cartService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Card'), messageType: MessageType.SUCCSESS);
          });
    }
  }

  double calculateDistance(double lat1, double lng1, double lat2, double lng2) {
    double distance = Geolocator.distanceBetween(lat1, lng1, lat2, lng2);
    return distance;
  }
}
