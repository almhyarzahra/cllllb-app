import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/data/models/apis/news_model.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';
import '../news_detalis_view/news_detalis_view.dart';
import 'all_news_controller.dart';

class AllNewsView extends StatefulWidget {
  const AllNewsView(
      {super.key, required this.currentlocation, required this.news});
  final LocationData? currentlocation;
  final List<NewsModel> news;

  @override
  State<AllNewsView> createState() => _AllNewsViewState();
}

class _AllNewsViewState extends State<AllNewsView> {
  late AllNewsController controller;
  @override
  void initState() {
    controller = Get.put(AllNewsController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
         bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          children: widget.news.map(
            (e) {
              return Card(
                elevation: 4,
                child: Padding(
                  padding: EdgeInsetsDirectional.symmetric(
                      vertical: screenWidth(20)),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsetsDirectional.symmetric(
                            horizontal: screenWidth(20)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            CustomNetworkImage(
                              context.image(e.image!),
                              width: screenWidth(1.8),
                              height: screenHeight(7.2),
                              radius: screenWidth(20),
                            ),
                            CustomNetworkImage(
                              context.image(e.club!.mainImage!),
                              width: screenWidth(4),
                              height: screenHeight(6),
                              border:
                                  Border.all(color: AppColors.mainYellowColor),
                              shape: BoxShape.circle,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsetsDirectional.symmetric(
                            horizontal: screenWidth(20)),
                        child: CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: e.title!,
                          textColor: AppColors.mainGreyColor,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          CustomButton(
                            text: tr('key_news_detalis'),
                            onPressed: () {
                              Get.to(
                                NewsDetalisView(
                                  currentlocation: widget.currentlocation,
                                  news: e,
                                ),
                              );
                            },
                            textSize: screenWidth(35),
                            circularBorder: screenWidth(20),
                            widthButton: 3,
                            heightButton: 15,
                          ),
                          screenWidth(20).pw,
                        ],
                      ),
                    ],
                  ),
                ),
              );
            },
          ).toList(),
        ),
      ),
    );
  }
}
