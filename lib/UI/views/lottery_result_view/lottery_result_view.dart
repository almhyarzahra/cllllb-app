import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_app_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_drawer.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/lottery_result_view/lottery_result_controller.dart';
import 'package:com.tad.cllllb/UI/views/lottery_result_view/widgets/result_loading.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';

class LotteryResultView extends StatefulWidget {
  final LocationData? currentlocation;
  const LotteryResultView({super.key, required this.currentlocation});

  @override
  State<LotteryResultView> createState() => _LotteryResultViewState();
}

class _LotteryResultViewState extends State<LotteryResultView> {
  late LotterResultController controller;
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    controller = Get.put(LotterResultController(currentlocation: widget.currentlocation));
    _scrollController.addListener(_scrollListener);
  }

  void _scrollListener() async {
    if (controller.getMore.value) {
      if (controller.enableScrollListner.value &&
          _scrollController.position.maxScrollExtent ==
              _scrollController.offset &&
          !_scrollController.position.outOfRange) {
        controller.getMore.value = false;
        await controller.getMoreLottary(page: controller.pageKey.value);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: controller.key,
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          controller: _scrollController,
          padding: const EdgeInsets.symmetric(horizontal: 15),
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
                CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: tr('lotteryResults'),
                  textColor: AppColors.mainBlackColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                ),
              ],
            ),
            SvgPicture.asset("images/winner.svg"),
            InkWell(
              onTap: () {
                Get.bottomSheet(
                  Container(
                    height: screenHeight(4),
                    decoration: BoxDecoration(
                        color: AppColors.mainWhiteColor,
                        borderRadius: BorderRadius.circular(30)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CustomText(
                              textType: TextStyleType.CUSTOM,
                              text: tr('key_Filter_By_Date'),
                              textColor: AppColors.mainYellowColor,
                            ),
                            const SizedBox(width: 30),
                            Obx(
                              () {
                                return InkWell(
                                  onTap: () async {
                                    controller.pickedDate =
                                        await showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime(2015),
                                      lastDate: DateTime(2100),
                                    );
                                    if (controller.pickedDate != null) {
                                      controller.date.value =
                                          DateFormat("yyyy-MM-dd")
                                              .format(controller.pickedDate!);
                                    }
                                  },
                                  child: Container(
                                    width: 100,
                                    height: 40,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: AppColors.mainGrey2Color,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: controller.date.value,
                                      textColor: AppColors.mainBlackColor,
                                    ),
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                        Obx(() {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('myLottery'),
                                textColor: AppColors.mainBlackColor,
                              ),
                              Checkbox(
                                value: controller.myLottery.value,
                                onChanged: ((value) {
                                  controller.myLottery.value = value!;
                                }),
                              ),
                            ],
                          );
                        }),
                        CustomButton(
                          text: tr('confirm'),
                          onPressed: () {
                            controller.filter();
                          },
                          widthButton: 2,
                          circularBorder: screenWidth(10),
                        ),
                      ],
                    ),
                  ),
                );
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset("images/filter.svg"),
                    screenWidth(30).pw,
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_Filter'),
                      textColor: AppColors.mainYellowColor,
                    ),
                  ],
                ),
              ),
            ),
            Obx(() {
              return controller.isResultLoading
                  ? const ResultLoading()
                  : controller.lotteries.isEmpty
                      ? CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('noResult'),
                          textColor: AppColors.mainYellowColor,
                        )
                      : controller.lotteries[0].message == null
                          ? customFaildConnection(onPressed: () {
                              controller.onInit();
                            },padding: 0)
                          : ListView.separated(
                              separatorBuilder: (context, index) =>
                                  const SizedBox(height: 8),
                              itemCount: controller.lotteries.length,
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                return Container(
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    color: AppColors.mainGrey2Color
                                        .withOpacity(0.5),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        // Center(
                                        //     child: SvgPicture.asset(
                                        //         "images/discount.svg")),
                                      Row(
                                          children: [
                                            Expanded(
                                              child: CustomText(
                                                textType: TextStyleType.CUSTOM,
                                                textAlign: TextAlign.start,
                                                text: controller
                                                    .lotteries[index].title!
                                                    .useCorrectEllipsis(),
                                                overflow: TextOverflow.ellipsis,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16,
                                                textColor:
                                                    AppColors.mainBlackColor,
                                              ),
                                            ),
                                            CustomNetworkImage(
                                              context.image(controller
                                                      .lotteries[index]
                                                      .club!
                                                      .logoImage ??
                                                  ""),
                                              width: 40,
                                              height: 40,
                                              shape: BoxShape.circle,
                                            ),
                                          ],
                                        ),

                                        Text(
                                          controller
                                              .lotteries[index].description!
                                              .useCorrectEllipsis(),
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                          style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: AppColors.mainBlackColor,
                                          ),
                                        ),
                                        CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text:
                                              "${tr('clubName')}${controller.lotteries[index].club!.name ?? "Cllllb"}",
                                          textColor: AppColors.mainBlackColor,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        // Text(
                                        //   "${tr('winnerName')}${controller.lotteries[index].winner}"
                                        //       .useCorrectEllipsis(),
                                        //   overflow: TextOverflow.ellipsis,
                                        //   style: const TextStyle(
                                        //     fontSize: 13,
                                        //   ),
                                        // ),
                                        Text(
                                                "${tr('startDate')} ${controller.lotteries[index].createdAt}",
                                                style: TextStyle(
                                                    color:
                                                        AppColors.mainGreyColor,
                                                    fontSize: screenWidth(30)),
                                              ),
                                        Text(
                                          "${tr('lotteryDate')}${DateFormat("dd-MM-yyyy").format(controller.lotteries[index].exipresAt!)}"
                                              .useCorrectEllipsis(),
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                            fontSize: 13,
                                          ),
                                        ),
                                        Text(
                                          tr('awards'),
                                          style: const TextStyle(
                                            color: AppColors.mainBlackColor,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        ListView.separated(
                                          separatorBuilder: (context, index) =>
                                              const SizedBox(height: 6),
                                          shrinkWrap: true,
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          itemCount: controller
                                              .lotteries[index].prizes!.length,
                                          itemBuilder: (context, myindex) {
                                            return Row(
                                              children: [
                                                Expanded(
                                                  child: CustomText(
                                                    textType:
                                                        TextStyleType.CUSTOM,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    textAlign: TextAlign.start,
                                                    text: controller
                                                        .lotteries[index]
                                                        .prizes![myindex]
                                                        .prizeName!,
                                                    textColor: AppColors
                                                        .mainBlackColor,
                                                  ),
                                                ),
                                                GestureDetector(
                                                  onTap: () {
                                                    controller
                                                        .lotteries[index]
                                                        .prizes![myindex].winners!.isEmpty?
                                                        CustomToast.showMessage(message: tr('noWinners'),messageType: MessageType.INFO)
                                                      :
                                                    Get.bottomSheet(
                                                      Container(
                                                        decoration:
                                                            const BoxDecoration(
                                                          color: Colors.white,
                                                          borderRadius:
                                                              BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    10),
                                                            topRight:
                                                                Radius.circular(
                                                                    10),
                                                          ),
                                                        ),
                                                        child:
                                                            ListView.separated(
                                                          separatorBuilder: (context,
                                                                  suberIndex) =>
                                                              const SizedBox(
                                                                  height: 6),
                                                          itemCount: controller
                                                              .lotteries[index]
                                                              .prizes![myindex]
                                                              .winners!
                                                              .length,
                                                          itemBuilder: (context,
                                                              suberIndex) {
                                                            return Padding(
                                                              padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 5),
                                                              child: Row(
                                                                children: [
                                                                  CustomNetworkImage(
                                                                    context.image(controller
                                                                            .lotteries[
                                                                                index]
                                                                            .prizes![
                                                                                myindex]
                                                                            .winners![
                                                                                suberIndex]
                                                                            .image ??
                                                                        ""),
                                                                    width: 50,
                                                                    height: 50,
                                                                    shape: BoxShape
                                                                        .circle,
                                                                  ),
                                                                  const SizedBox(width: 20,),
                                                                  Expanded(
                                                                    child:
                                                                        CustomText(
                                                                          textAlign: TextAlign.start,
                                                                      textType:
                                                                          TextStyleType
                                                                              .CUSTOM,
                                                                      text: controller
                                                                          .lotteries[
                                                                              index]
                                                                          .prizes![
                                                                              myindex]
                                                                          .winners![
                                                                              suberIndex]
                                                                          .name!,
                                                                      textColor:
                                                                          AppColors
                                                                              .mainBlackColor,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                            );
                                                          },
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                  child: Container(
                                                    width: 100,
                                                    height: 30,
                                                    alignment: Alignment.center,
                                                    decoration: BoxDecoration(
                                                      color: AppColors
                                                          .mainYellowColor,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                    ),
                                                    child: CustomText(
                                                        textType: TextStyleType
                                                            .CUSTOM,
                                                        text: tr('winners')),
                                                  ),
                                                ),
                                              ],
                                            );
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ).animate().scale(delay: 100.ms);
            }),
            Obx(
              () {
                return controller.isMoreResultLoading
                    ? const SpinKitCircle(
                        color: AppColors.mainYellowColor,
                      )
                    : const SizedBox.shrink();
              },
            ),
          ],
        ),
      ),
    );
  }
}
