import 'dart:developer';

import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/UI/views/filter_lottery_result_view/filter_lottery_result_view.dart';
import 'package:com.tad.cllllb/core/data/models/apis/lottery_result_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class LotterResultController extends BaseController {
  LotterResultController({
     this.currentlocation,
  }){
    //
  }
  LocationData? currentlocation;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  RxList<LotteryResultModel> lotteries =
      <LotteryResultModel>[LotteryResultModel()].obs;
  bool get isResultLoading => listType.contains(OperationType.result);
  bool get isMoreResultLoading => listType.contains(OperationType.moreResult);
  RxInt pageKey = 2.obs;
  RxBool getMore = true.obs;
  RxBool enableScrollListner = true.obs;

  RxString date = "".obs;
  DateTime? pickedDate;
  RxBool myLottery = false.obs;

  @override
  void onInit() {
    super.onInit();
    getLotteryResult();
  }

  void getLotteryResult() {
    runLoadingFutureFunction(
      type: OperationType.result,
      function: UserRepository().getLotteryResult(page: 1).then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_check_connection'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (lotteries.isNotEmpty) {
                lotteries.clear();
              }
              lotteries.addAll(r);
              lotteries.map((element) {
                element.message = "l";
              }).toSet();
            },
          );
        },
      ),
    );
  }

  Future<void> getMoreLottary({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
        type: OperationType.moreResult,
        function: UserRepository().getLotteryResult(page: page).then((value) {
          value.fold((l) {
            pageKey.value = pageKey.value - 1;
          }, (r) {
            if (r.isEmpty) {
              enableScrollListner.value = false;
            } else {
              lotteries.addAll(r);
            }
          });
        })).then((value) => getMore.value = true);
  }

  void filter() {
    runFullLoadingFutureFunction(
      function: UserRepository()
          .getLotteryResult(
        page: 1,
        date: date.value.isEmpty ? null : date.value,
        winner: myLottery.value,
      )
          .then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_try_again'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (r.isEmpty) {
                CustomToast.showMessage(
                    message: tr('noMatchingResults'),
                    messageType: MessageType.INFO);
              }
              else{
                Get.to(()=> FilterLotteryResult(lotteryResultModel: r));
              }
            },
          );
        },
      ),
    );
  }
}
