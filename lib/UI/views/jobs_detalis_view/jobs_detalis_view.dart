import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/core/data/models/apis/jobs_model.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';
import '../sign_in_view/sign_in_view.dart';
import 'jobs_detalis_controller.dart';

class JobsDetalisView extends StatefulWidget {
  const JobsDetalisView(
      {super.key, required this.currentlocation, required this.job});
  final LocationData? currentlocation;
  final JobsModel job;

  @override
  State<JobsDetalisView> createState() => _JobsDetalisViewState();
}

class _JobsDetalisViewState extends State<JobsDetalisView> {
  late JobsDetalisController controller;
  @override
  void initState() {
    controller = Get.put(JobsDetalisController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
         bottomNavigationBar:const CustomBottomNavigation(),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
              ],
            ),
            CustomNetworkImage(
              context.image(widget.job.club!.mainImage!),
              width: screenWidth(5),
              height: screenHeight(8),
              boxFit: BoxFit.contain,
              border: Border.all(color: AppColors.mainYellowColor),
              shape: BoxShape.circle,
            ),
            screenWidth(20).ph,
            Padding(
              padding:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(15)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: widget.job.club!.name!,
                    textColor: AppColors.mainBlackColor,
                    fontSize: screenWidth(25),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: widget.job.club!.city!,
                    textColor: AppColors.mainYellowColor,
                    fontSize: screenWidth(25),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: widget.job.distance == null
                        ? ""
                        : widget.job.distance!.toInt() > 1000
                            ? tr('key_near') +
                                (widget.job.distance!.toInt() / 1000)
                                    .toString() +
                                tr('key_km')
                            : tr('key_near') +
                                widget.job.distance!.toInt().toString() +
                                tr('key_meter'),
                    textColor: AppColors.mainYellowColor,
                    fontSize: screenWidth(25),
                  ),
                  screenWidth(20).ph,
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: tr('key_job_title'),
                    textColor: AppColors.mainYellowColor,
                    fontSize: screenWidth(25),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: widget.job.jobTitle!,
                    textColor: AppColors.mainBlackColor,
                    fontSize: screenWidth(25),
                  ),
                  screenWidth(20).ph,
                  CustomText(
                    textAlign: TextAlign.start,
                    textType: TextStyleType.CUSTOM,
                    text: widget.job.jobDescription!,
                    textColor: AppColors.mainBlackColor,
                    fontSize: screenWidth(25),
                  ),
                  screenWidth(20).ph,
                  Row(
                    children: [
                      CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: "reference No: ",
                        textColor: AppColors.mainYellowColor,
                        fontSize: screenWidth(25),
                      ),
                      CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: "Job${widget.job.id}",
                        textColor: AppColors.mainBlackColor,
                        fontSize: screenWidth(25),
                      ),
                    ],
                  ),
                  screenWidth(20).ph,
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: tr('key_dead_line'),
                    textColor: AppColors.mainGreyColor,
                    fontSize: screenWidth(25),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: widget.job.announcementEndDate!,
                    textColor: AppColors.mainYellowColor,
                    fontSize: screenWidth(25),
                  ),
                ],
              ),
            ),
            screenWidth(10).ph,
            CustomText(
              textType: TextStyleType.CUSTOM,
              text: tr('key_expected_salary'),
              textColor: AppColors.mainYellowColor,
              fontSize: screenWidth(20),
            ),
            CustomText(
              textType: TextStyleType.CUSTOM,
              text: widget.job.expectSalary! + tr('key_AED'),
              textColor: AppColors.mainYellowColor,
              fontSize: screenWidth(20),
            ),
            screenWidth(10).ph,
            Obx(
              () {
                return Padding(
                  padding: EdgeInsetsDirectional.symmetric(
                      horizontal: screenWidth(10)),
                  child: CustomButton(
                    backgroundColor: AppColors.mainBlueColor,
                    onPressed: () {
                      storage.getUserId() == 0
                          ? Get.defaultDialog(
                              title: tr('key_login_complete'),
                              titleStyle: const TextStyle(
                                  color: AppColors.mainYellowColor),
                              content: Column(
                                children: [
                                  CustomButton(
                                    text: tr('key_Yes'),
                                    onPressed: () async {
                                      storage.setCartList([]);
                                      storage.setFavoriteList([]);
                                      storage.setFirstLanuch(true);
                                      storage.setIsLoggedIN(false);
                                      storage.globalSharedPrefs
                                          .remove(storage.PREF_TOKEN_INFO);
                                      //storage.setTokenInfo(null);
                                      await storage.setUserId(0);
                                      storage.globalSharedPrefs
                                          .remove(storage.PREF_USER_NAME);

                                      storage.setGoogle(false);
                                      storage.globalSharedPrefs
                                          .remove(storage.PREF_MOBILE_NUMBER);
                                      storage.globalSharedPrefs
                                          .remove(storage.PREF_EMAIL);

                                      Get.offAll(() => const SignInView());
                                    },
                                    widthButton: 4,
                                    circularBorder: screenWidth(10),
                                  ),
                                  screenWidth(20).ph,
                                  CustomButton(
                                    text: tr('key_No'),
                                    onPressed: () {
                                      Get.back();
                                    },
                                    widthButton: 4,
                                    circularBorder: screenWidth(10),
                                  ),
                                ],
                              ),
                            )
                          : controller.selectFile();
                    },
                    circularBorder: screenWidth(20),
                    text: controller.visible.value
                        ? tr('key_edit_cv')
                        : tr('key_cv'),
                  ),
                );
              },
            ),
            screenWidth(10).ph,
            Obx(
              () {
                return Padding(
                  padding: EdgeInsetsDirectional.symmetric(
                      horizontal: screenWidth(10)),
                  child: CustomButton(
                    circularBorder: screenWidth(20),
                    text: tr('key_apply_now'),
                    onPressed: controller.visible.value
                        ? () {
                            controller.uploadPDF(jobId: widget.job.id!);
                          }
                        : null,
                  ),
                );
              },
            ),
            screenWidth(10).ph,
          ],
        ),
      ),
    );
  }
}
