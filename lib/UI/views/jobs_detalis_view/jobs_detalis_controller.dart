import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/data/repositories/club_repositories.dart';

class JobsDetalisController extends BaseController {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  FilePickerResult? result ;
  RxBool visible = false.obs;

  void selectFile() async {
    result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf'],
    );
    if (result != null) {
      visible.value = true;
    }
  }

  void uploadPDF({required int jobId}) {
    runFullLoadingFutureFunction(
        function: ClubRepositories()
            .applyJob(
                userId: storage.getUserId(),
                jobId: jobId,
                cv: result!.paths[0]!)
            .then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_try_again'), messageType: MessageType.REJECTED);
      }, (r) {
        CustomToast.showMessage(
            message: tr('key_success'), messageType: MessageType.SUCCSESS);
        Get.back();
      });
    }));
  }
}
