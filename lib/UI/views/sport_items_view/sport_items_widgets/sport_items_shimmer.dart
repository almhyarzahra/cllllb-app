import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import '../../../shared/colors.dart';
import '../../../shared/utils.dart';

class SportItemsShimmer extends StatelessWidget {
  const SportItemsShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    List<String> list = ["1", "1", "1", "1"];
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: GridView.count(
        childAspectRatio: 0.7,
        crossAxisCount: 2,
        crossAxisSpacing: 0,
        shrinkWrap: true,
        physics:const NeverScrollableScrollPhysics(),
        children: list.map((element) {
          return Padding(
            padding: EdgeInsetsDirectional.symmetric(
                horizontal: screenWidth(40), vertical: screenWidth(40)),
            child: Container(
              width: screenWidth(2),
              height: screenWidth(2.5),
              decoration: BoxDecoration(
                  color: AppColors.mainOrangeColor,
                  borderRadius: BorderRadius.circular(screenWidth(20))),
            ),
          );
        }).toList(),
      ),
    );
  }
}
