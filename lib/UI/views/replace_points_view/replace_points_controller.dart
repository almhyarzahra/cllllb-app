import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/UI/views/main_view/profile_view/profile_controller.dart';
import 'package:com.tad.cllllb/core/data/models/apis/replace_points_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ReplacePointsController extends BaseController {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  Rx<ReplacePointsModel> replacePoints = ReplacePointsModel().obs;
  bool get isMoneyLoading => listType.contains(OperationType.money);

  TextEditingController pointController = TextEditingController();
  RxDouble point=storage.getUserPoint().obs;
  RxString aed = "".obs;

  @override
  void onInit() {
    super.onInit();
    getPointMoney();
  }

  void getPointMoney() {
    runLoadingFutureFunction(
      type: OperationType.money,
      function: UserRepository().getPointMoney().then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_check_connection'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              replacePoints.value = r;
              aed.value = r.money.toString();
              pointController.text = r.pointsNedded.toString();
            },
          );
        },
      ),
    );
  }

  void calcPoints(String value) {
    double result = 0.0;
    if (value.isEmpty) {
      aed.value = "0";
    } else {
      result = (double.parse(value) * replacePoints.value.money!) /
          replacePoints.value.pointsNedded!;
      aed.value = result.toStringAsFixed(2);
    }
  }

  void replacing() {
    if (formKey.currentState!.validate()) {
      runFullLoadingFutureFunction(
        function: UserRepository()
            .replaceUserPoints(
                pointsCounts: double.parse(pointController.text.trim()))
            .then(
          (value) {
            value.fold(
              (l) {
                if (l == 400) {
                  CustomToast.showMessage(
                      message:  "${tr('minimumPoints')}${replacePoints.value.minValue} ${tr('point')}",
                      messageType: MessageType.INFO);
                } else {
                  CustomToast.showMessage(
                      message: tr("Try Agin"),
                      messageType: MessageType.REJECTED);
                }
              },
              (r) {
                Get.back();
                storage.setUserPoint(storage.getUserPoint() +
                    double.parse(pointController.text.trim()));
                Get.find<ProfileController>().getUserInfo();
                CustomToast.showMessage(
                    message: tr('key_success'),
                    messageType: MessageType.SUCCSESS);
              },
            );
          },
        ),
      );
    }
  }
}
