import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_app_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_drawer.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/titled_textfield.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/replace_points_view/replace_points_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class ReplacePointsView extends StatefulWidget {
  final LocationData? currentlocation;
  const ReplacePointsView({super.key, required this.currentlocation});

  @override
  State<ReplacePointsView> createState() => _ReplacePointsViewState();
}

class _ReplacePointsViewState extends State<ReplacePointsView> {
  late ReplacePointsController controller;
  @override
  void initState() {
    super.initState();
    controller = Get.put(ReplacePointsController());
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: controller.key,
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: Obx(
          () {
            return controller.isMoneyLoading
                ? const SpinKitCircle(
                    color: AppColors.mainYellowColor,
                  )
                : controller.replacePoints.value.money == null
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          customFaildConnection(onPressed: () {
                            controller.onInit();
                          }),
                        ],
                      )
                    : Form(
                        key: controller.formKey,
                        child: ListView(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    IconButton(
                                      onPressed: () {
                                        Get.back();
                                      },
                                      icon: Icon(
                                        Icons.arrow_back_ios_new,
                                        size: screenWidth(20),
                                      ),
                                    ),
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: tr('replacePoints'),
                                      textColor: AppColors.mainBlackColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 22,
                                    ),
                                  ],
                                ),
                                Container(
                                  height: 35.0,
                                  padding:
                                      const EdgeInsetsDirectional.symmetric(
                                          horizontal: 5),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: AppColors.mainGrey2Color,
                                  ),
                                  child: Row(
                                    children: [
                                      Obx(() {
                                        return Text(
                                          "${controller.point}",
                                          style: const TextStyle(
                                            color: AppColors.mainBlackColor,
                                          ),
                                        );
                                      }),
                                      const SizedBox(
                                        width: 6,
                                      ),
                                      SvgPicture.asset("images/coin.svg"),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            CustomText(
                              textType: TextStyleType.CUSTOM,
                              text:
                                  "${tr('exchangeYourPoints')}\n${tr('forEvery')} ${controller.replacePoints.value.pointsNedded} ${tr('point')} ${tr('willBeAdded')}${controller.replacePoints.value.money} ${tr('key_AED')} ${tr('toYourWallet')}",
                              textColor: AppColors.mainBlackColor,
                            ),
                            screenWidth(20).ph,
                            CustomText(
                              textType: TextStyleType.CUSTOM,
                              text:
                                  "${tr('minimumPoints')}${controller.replacePoints.value.minValue} ${tr('point')}",
                              textColor: AppColors.mainRedColor,
                            ),
                            screenWidth(20).ph,
                            TitledTextField(
                              title: tr('numberPointsReplace'),
                              child: CustomTextField(
                                hintText: "",
                                controller: controller.pointController,
                                contentPaddingLeft: screenWidth(15),
                                typeInput: TextInputType.number,
                                onChanged: (value) {
                                  controller.calcPoints(value);
                                },
                                validator: (value) {
                                  return value!.isEmpty ||
                                          double.parse(value) >
                                              storage.getUserPoint()
                                      ? tr('checkNumberPoints')
                                      : null;
                                },
                              ),
                            ),
                            CustomText(
                              textType: TextStyleType.CUSTOM,
                              text: "${controller.aed} ${tr('key_AED')}",
                              textColor: AppColors.mainYellowColor,
                            ),
                            screenHeight(5).ph,
                            CustomButton(
                              text: tr('replacing'),
                              circularBorder: screenWidth(10),
                              onPressed: () {
                                controller.replacing();
                              },
                            )
                          ],
                        ),
                      );
          },
        ),
      ),
    );
  }
}
