import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_app_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_drawer.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/cllllb_offers_view/cllllb_offers_controller.dart';
import 'package:com.tad.cllllb/UI/views/food_sport_detalis_view/food-sport_detalis_view.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class CllllbOffersView extends StatefulWidget {
  final LocationData? currentlocation;
  const CllllbOffersView({super.key, required this.currentlocation});

  @override
  State<CllllbOffersView> createState() => _CllllbOffersViewState();
}

class _CllllbOffersViewState extends State<CllllbOffersView> {
  late CllllbOffersController controller;

  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    controller = Get.put(CllllbOffersController(currentlocation: widget.currentlocation));
     scrollController.addListener(_scrollListener);
  }

   void _scrollListener() async {
    if (controller.getMore.value) {
      if (controller.enableScrollListner.value &&
          scrollController.position.maxScrollExtent ==
              scrollController.offset &&
          !scrollController.position.outOfRange) {
        controller.getMore.value = false;
        await controller.getMoreProducts(page: controller.pageKey.value);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: controller.key,
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          controller: scrollController,
          //padding: const EdgeInsets.symmetric(horizontal: 15),
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
                CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: tr('cllllbOffers'),
                  textColor: AppColors.mainBlackColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                ),
              ],
            ),
            Obx((){
              return controller.isOffersLoading
                  ? const SpinKitCircle(color: AppColors.mainYellowColor)
                  : controller.products.isEmpty
                      ? CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('key_No_Products_Yet'),
                          textColor: AppColors.mainYellowColor,
                        )
                      : controller.products[0].message == null
                          ? customFaildConnection(onPressed: () {
                              controller.onInit();
                            })
                          :GridView.count(
                                childAspectRatio: 0.55,
                                crossAxisCount: 2,
                                crossAxisSpacing: 2,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                children: controller.products.map(
                                  (e) {
                                    return Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Card(
                                        elevation: 4,
                                        child: SizedBox(
                                          width: screenWidth(2.2),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Row(
                                                children: [
                                                  screenWidth(40).pw,
                                                  CustomNetworkImage(
                                                    context.image(
                                                        e.club!.mainImage!),
                                                    width: screenWidth(8.7),
                                                    height: screenHeight(10),
                                                    border: Border.all(
                                                        color: AppColors
                                                            .mainYellowColor),
                                                    shape: BoxShape.circle,
                                                  ),
                                                  screenWidth(40).pw,
                                                  Flexible(
                                                    child: Column(
                                                      children: [
                                                        CustomText(
                                                          textType:
                                                              TextStyleType
                                                                  .CUSTOM,
                                                          text:
                                                              "${e.club!.name}",
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          textColor: AppColors
                                                              .mainBlackColor,
                                                          fontSize:
                                                              screenWidth(25),
                                                        ),
                                                        CustomText(
                                                          textType:
                                                              TextStyleType
                                                                  .CUSTOM,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          text:
                                                              "${e.club!.city}",
                                                          textColor: AppColors
                                                              .mainYellowColor,
                                                          fontSize:
                                                              screenWidth(25),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              CustomText(
                                                textType: TextStyleType.CUSTOM,
                                                text: e.name!,
                                                overflow: TextOverflow.ellipsis,
                                                textColor:
                                                    AppColors.mainBlackColor,
                                              ),
                                              if (e.pointsValue != null)
                                                CustomText(
                                                  textType:
                                                      TextStyleType.CUSTOM,
                                                  text:
                                                      "+${e.pointsValue} ${tr('rewardPoints')}",
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  fontSize: 12,
                                                  textColor:
                                                      AppColors.mainYellowColor,
                                                ),
                                              if (e.pointsValue == null)
                                                const SizedBox(height: 13),
                                              screenWidth(30).ph,
                                              CustomNetworkImage(
                                                context.image(e.img!),
                                                width: screenWidth(1),
                                                height: screenHeight(7),
                                                onTap: () => Get.to(
                                                  FoodSportDetalisView(
                                                    currentlocation:
                                                        widget.currentlocation,
                                                    productsModel: e,
                                                  ),
                                                ),
                                              ),
                                              screenWidth(25).ph,
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  InkWell(
                                                    onTap: () {
                                                      controller.addTofavorite(
                                                          productsModel: e);
                                                    },
                                                    child: SizedBox(
                                                      height: screenWidth(10),
                                                      width: screenWidth(10),
                                                      child: ClipRRect(
                                                        child: Card(
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          18)),
                                                          elevation: 4,
                                                          child: Center(
                                                            child: SvgPicture.asset(
                                                                "images/like.svg"),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  CustomText(
                                                    textType:
                                                        TextStyleType.CUSTOM,
                                                    textColor: AppColors
                                                        .mainYellowColor,
                                                    text: e.price.toString() +
                                                        tr('key_AED'),
                                                  ),
                                                  InkWell(
                                                    onTap: () {
                                                      controller.addToCart(
                                                          productsModel: e);
                                                    },
                                                    child: SizedBox(
                                                      height: screenWidth(10),
                                                      width: screenWidth(10),
                                                      child: ClipRRect(
                                                        child: Card(
                                                          shape:
                                                              RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18),
                                                          ),
                                                          elevation: 4,
                                                          child: Center(
                                                            child: SvgPicture.asset(
                                                                "images/shopping-cart.svg"),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ).toList().animate(interval: 50.ms).scale(delay: 100.ms),
                              );
            }),

            Obx(
              () {
                return controller.isMoreOffersLoading
                    ? const SpinKitCircle(
                        color: AppColors.mainYellowColor,
                      )
                    : const SizedBox.shrink();
              },
            ),
          ],
        ),
      ),
    );
  }
}
