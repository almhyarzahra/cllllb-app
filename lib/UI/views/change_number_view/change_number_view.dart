import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/UI/views/change_number_view/change_number_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';

class ChangeNumberView extends StatefulWidget {
  const ChangeNumberView({super.key, required this.currentlocation});
  final LocationData? currentlocation;

  @override
  State<ChangeNumberView> createState() => _ChangeNumberViewState();
}

class _ChangeNumberViewState extends State<ChangeNumberView> {
  ChangeNumberController controller = ChangeNumberController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      drawer: CustomDrawer(
        currentlocation: widget.currentlocation,
      ),
      key: controller.key,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(screenWidth(6)),
        child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            }),
      ),
      body: Column(
        children: [
          CustomBar(
            text: tr('key_Change_Number'),
          ),
          Padding(
            padding: EdgeInsetsDirectional.symmetric(
                horizontal: screenWidth(30), vertical: screenWidth(8)),
            child: Column(
              children: [
                CustomTextField(
                    hintText: tr('key_Old_Number'),
                    typeInput: TextInputType.phone,
                    contentPaddingLeft: screenWidth(30),
                    widthBorder: 2,
                    colorHintText: AppColors.mainBlackColor,
                    borderColor: AppColors.mainYellowColor,
                    circularSize: screenWidth(35),
                    controller: controller.oldNumberController),
                screenHeight(50).ph,
                CustomTextField(
                    hintText: tr('key_New_Number'),
                    typeInput: TextInputType.phone,
                    contentPaddingLeft: screenWidth(30),
                    widthBorder: 2,
                    colorHintText: AppColors.mainBlackColor,
                    borderColor: AppColors.mainYellowColor,
                    circularSize: screenWidth(35),
                    controller: controller.newNumberController),
              ],
            ),
          ),
          CustomButton(
            text: tr('key_SUBMIT'),
            onPressed: () {},
            circularBorder: screenWidth(20),
            widthButton: 2.3,
            heightButton: 8,
            textFontWight: FontWeight.bold,
          )
        ],
      ),
    ));
  }
}
