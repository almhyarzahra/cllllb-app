import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';

class ChangeNumberController extends BaseController {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  TextEditingController oldNumberController = TextEditingController();
  TextEditingController newNumberController = TextEditingController();
}
