import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_app_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_drawer.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/lottery_view/lottery_controller.dart';
import 'package:com.tad.cllllb/core/data/models/apis/lottery_model.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';

import 'filter_lottery_controller.dart';

class FilterLotteryView extends StatefulWidget {
  final LocationData? currentlocation;
  final List<LotteryModel> lotteries;
  const FilterLotteryView({
    super.key,
    required this.currentlocation,
    required this.lotteries,
  });

  @override
  State<FilterLotteryView> createState() => _FilterLotteryViewState();
}

class _FilterLotteryViewState extends State<FilterLotteryView> {
  late FilterLotteryController controller;
  final ScrollController _scrollController = ScrollController();

  void _scrollListener() async {
    if (controller.getMore.value) {
      if (controller.enableScrollListner.value &&
          _scrollController.position.maxScrollExtent ==
              _scrollController.offset &&
          !_scrollController.position.outOfRange) {
        controller.getMore.value = false;
        await controller.getMoreLottary(page: controller.pageKey.value);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
    controller = Get.put(FilterLotteryController(
      currentlocation: widget.currentlocation,
      lotteriesModelModel: widget.lotteries,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          controller: _scrollController,
          padding: const EdgeInsets.symmetric(horizontal: 15),
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
              ],
            ),
            Obx(() {
              return ListView.separated(
                separatorBuilder: (context, index) => const SizedBox(height: 8),
                itemCount: controller.lotteries.length,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Container(
                    width: double.infinity,
                    height: 170,
                    padding: const EdgeInsetsDirectional.only(end: 10),
                    decoration: BoxDecoration(
                      color: AppColors.mainGrey2Color.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          "images/winner.svg",
                          width: 60,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                controller.lotteries[index].title!,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: screenWidth(20),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                controller.lotteries[index].description!,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                style: TextStyle(
                                  fontSize: screenWidth(30),
                                ),
                              ),
                              Text(
                                   "${tr('startDate')} ${controller.lotteries[index].createdAt}",
                                          style: TextStyle(
                                             color:
                                                  AppColors.mainGreyColor,
                                             fontSize: screenWidth(30)),
                                              ),
                              Text(
                                "${tr('lotteryDate')} ${DateFormat("dd-MM-yyyy").format(controller.lotteries[index].expiresAt!)}",
                                style: TextStyle(
                                    color: AppColors.mainGreyColor,
                                    fontSize: screenWidth(30)),
                              ),
                              Text(
                                "${tr('entryFee')} ${controller.lotteries[index].pointsValue} ${tr('point')}",
                                style: TextStyle(
                                    color: AppColors.mainRedColor,
                                    fontSize: screenWidth(30)),
                              ),
                              if (!controller.lotteries[index].ended!)
                                Row(
                                 // mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Expanded(
                                                      child: Text(
                                                          "${tr('chancesOfWinning')} ${controller.lotteries[index].chances??""}",
                                                        overflow:
                                                            TextOverflow.ellipsis,
                                                        style: const TextStyle(color: AppColors.mainYellowColor),
                                                      ),
                                                    ),
                                    CustomButton(
                                      text: controller.lotteries[index].user!
                                          ? tr('joined')
                                          : tr('joining'),
                                      widthButton: 5,
                                      textSize: 12,
                                      circularBorder: screenWidth(10),
                                      heightButton: 10,
                                      onPressed:
                                          controller.lotteries[index].user!
                                              ? null
                                              : () {
                                                  Get.find<LotteryController>()
                                                      .enterLottery(
                                                          drawId: controller
                                                              .lotteries[index]
                                                              .id!,
                                                          points: controller
                                                              .lotteries[index]
                                                              .pointsValue!);
                                                },
                                    ),
                                  ],
                                ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ).animate().scale(delay: 100.ms);
            }),
            Obx(
              () {
                return controller.isMoreLotteryLoading
                    ? const SpinKitCircle(
                        color: AppColors.mainYellowColor,
                      )
                    : const SizedBox.shrink();
              },
            ),
          ],
        ),
      ),
    );
  }
}
