import 'dart:developer';

import 'package:com.tad.cllllb/UI/views/lottery_view/lottery_controller.dart';
import 'package:com.tad.cllllb/core/data/models/apis/lottery_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class FilterLotteryController extends BaseController {
  FilterLotteryController({
    this.currentlocation,
    required List<LotteryModel> lotteriesModelModel,
  }) {
    lotteries.value = lotteriesModelModel;
  }
  LocationData? currentlocation;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<LotteryModel> lotteries = <LotteryModel>[LotteryModel()].obs;
  bool get isMoreLotteryLoading =>
      listType.contains(OperationType.filterLottery);
  RxInt pageKey = 2.obs;
  RxBool getMore = true.obs;
  RxBool enableScrollListner = true.obs;

  Future<void> getMoreLottary({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
        type: OperationType.filterLottery,
        function: UserRepository()
            .getLottery(
                page: page,
                date: Get.find<LotteryController>().date.value.isEmpty
                    ? null
                    : Get.find<LotteryController>().date.value,
                points: Get.find<LotteryController>()
                        .pointController
                        .text
                        .trim()
                        .isEmpty
                    ? null
                    : Get.find<LotteryController>().pointController.text.trim(),
                ended: Get.find<LotteryController>().finished.value)
            .then((value) {
          value.fold((l) {
            pageKey.value = pageKey.value - 1;
          }, (r) {
            if (r.isEmpty) {
              enableScrollListner.value = false;
            } else {
              lotteries.addAll(r);
            }
          });
        })).then((value) => getMore.value = true);
  }
}
