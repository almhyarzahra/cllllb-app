import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_button.dart';
import 'package:com.tad.cllllb/UI/views/payment_view/payment_view.dart';
import 'package:com.tad.cllllb/UI/views/table_detalis_view/table_detalis_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:lottie/lottie.dart';
import 'package:table_calendar/table_calendar.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_book_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_second_bar.dart';
import '../../shared/utils.dart';
import '../sign_in_view/sign_in_view.dart';

class TableDetalisView extends StatefulWidget {
  const TableDetalisView(
      {super.key,
      required this.nameClub,
      required this.address,
      required this.imageClub,
      required this.clubId,
      required this.clubType,
      required this.nameGame,
      required this.imageGame,
      required this.quantity,
      required this.gameFieldId,
      required this.price,
      required this.imageGameSelected,
      required this.currentlocation,
      required this.bookingType});
  final String nameClub;
  final String address;
  final String imageClub;
  final int clubId;
  final String clubType;
  final String nameGame;
  final String imageGame;
  final int quantity;
  final int gameFieldId;
  final double price;
  final int bookingType;
  final String imageGameSelected;
  final LocationData? currentlocation;

  @override
  State<TableDetalisView> createState() => _TableDetalisViewState();
}

class _TableDetalisViewState extends State<TableDetalisView> with WidgetsBindingObserver {
  late TableDetalisController controller;
  @override
  void initState() {
    controller = Get.put(TableDetalisController(
      gameFieldId: widget.gameFieldId,
      bookingType: widget.bookingType,
      imageGameSelected: widget.imageGameSelected,
      currentlocation: widget.currentlocation,
      address: widget.address,
      clubId: widget.clubId,
      clubType: widget.clubType,
      imageClub: widget.imageClub,
      imageGame: widget.imageGame,
      nameClub: widget.nameClub,
      nameGame: widget.nameGame,
      quantity: widget.quantity,
    ));
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.paused) {
      if (controller.bookId.isNotEmpty) {
        for (var entry in controller.bookId.entries) {
          controller.bookingRetreat(idBooking: [entry.value]);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            children: [
              screenHeight(80).ph,
              CustomBookBar(
                imageUrl: widget.imageClub,
                imageName: Uri.https(NetworkUtil.baseUrl, widget.imageClub).toString(),
                text: "${widget.nameClub}/ ${widget.address}/ ${widget.clubType}",
              ),
              CustomSecondBar(
                svgName: widget.imageGame,
                text1: widget.nameGame,
                text2: widget.quantity.toString() + tr('key_QUANTITY2'),
              ),
              widget.bookingType == 2
                  ? Obx(
                      () {
                        return controller.isShotsLoading
                            ? const SpinKitCircle(
                                color: AppColors.mainYellowColor,
                              )
                            : controller.shots.value.withWeapon == 1 &&
                                    controller.shots.value.gameWeapon!.isEmpty
                                ? Center(
                                    child: CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: tr('key_no_weapon'),
                                      textColor: AppColors.mainYellowColor,
                                    ),
                                  )
                                : Padding(
                                    padding: EdgeInsetsDirectional.symmetric(
                                        horizontal: screenWidth(20)),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        controller.shots.value.gameWeapon!.isEmpty
                                            ? const SizedBox.shrink()
                                            : Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  CustomText(
                                                    textType: TextStyleType.CUSTOM,
                                                    text: tr('key_select_weapon'),
                                                    textColor: AppColors.mainYellowColor,
                                                  ),
                                                  SizedBox(
                                                    height: screenHeight(15),
                                                    child: ListView.builder(
                                                      shrinkWrap: true,
                                                      scrollDirection: Axis.horizontal,
                                                      itemCount:
                                                          controller.shots.value.gameWeapon!.length,
                                                      itemBuilder:
                                                          (BuildContext context, int index) {
                                                        return Padding(
                                                          padding: EdgeInsetsDirectional.symmetric(
                                                              horizontal: screenWidth(40)),
                                                          child: Obx(
                                                            () {
                                                              return InkWell(
                                                                onTap: () {
                                                                  controller.idWeapon.value =
                                                                      controller.shots.value
                                                                          .gameWeapon![index].id!;
                                                                  controller.numberStrikes.value =
                                                                      controller.shots.value
                                                                          .gameWeapon![index].shot!;
                                                                  controller.timeStrikes.value =
                                                                      controller.shots.value
                                                                          .gameWeapon![index].time!;
                                                                  controller.priceShots.value =
                                                                      controller
                                                                          .shots
                                                                          .value
                                                                          .gameWeapon![index]
                                                                          .price!;
                                                                  controller.minStrikes.value =
                                                                      controller
                                                                          .numberStrikes.value;
                                                                  controller.minTime.value =
                                                                      controller.timeStrikes.value;
                                                                  controller.minPrice.value =
                                                                      controller.priceShots.value;
                                                                  controller.getTimeShots(
                                                                      date: controller.date.value);
                                                                },
                                                                child: Container(
                                                                  decoration: BoxDecoration(
                                                                    color: controller
                                                                                .shots
                                                                                .value
                                                                                .gameWeapon![index]
                                                                                .id ==
                                                                            controller
                                                                                .idWeapon.value
                                                                        ? AppColors.mainYellowColor
                                                                            .withOpacity(0.5)
                                                                        : AppColors.mainWhiteColor,
                                                                    border: Border.all(
                                                                        color: AppColors
                                                                            .mainGreyColor),
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            screenWidth(25)),
                                                                  ),
                                                                  // width: screenWidth(5),
                                                                  // height: screenHeight(8),
                                                                  child: CachedNetworkImage(
                                                                    imageUrl: Uri.https(
                                                                            NetworkUtil.baseUrl,
                                                                            controller
                                                                                .shots
                                                                                .value
                                                                                .gameWeapon![index]
                                                                                .image!)
                                                                        .toString(),
                                                                    placeholder: (context, url) =>
                                                                        Lottie.asset(
                                                                            "images/download.json"),
                                                                    errorWidget:
                                                                        (context, url, error) =>
                                                                            const Icon(Icons.error),
                                                                    // width: screenWidth(1),
                                                                    // height: screenHeight(7),
                                                                    fit: BoxFit.fill,
                                                                  ),
                                                                ),
                                                              );
                                                            },
                                                          ),
                                                        );
                                                      },
                                                    ),
                                                  ),
                                                  screenWidth(40).ph,
                                                ],
                                              ),
                                        CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text: tr('key_select_strikes'),
                                          textColor: AppColors.mainYellowColor,
                                        ),
                                        Obx(
                                          () {
                                            return Row(
                                              children: [
                                                ElevatedButton(
                                                  onPressed: controller.numberStrikes.value ==
                                                          controller.minStrikes.value
                                                      ? null
                                                      : () {
                                                          controller.numberStrikes.value -=
                                                              controller.minStrikes.value;
                                                          controller.timeStrikes.value -=
                                                              controller.minTime.value;
                                                          controller.priceShots.value -=
                                                              controller.minPrice.value;
                                                          controller.getTimeShots(
                                                              date: controller.date.value);
                                                        },
                                                  style: ElevatedButton.styleFrom(
                                                    shape: const StadiumBorder(),
                                                    backgroundColor: AppColors.mainYellowColor,
                                                    disabledBackgroundColor:
                                                        AppColors.mainGreyColor,
                                                    fixedSize:
                                                        Size(screenWidth(8), screenHeight(25)),
                                                  ),
                                                  child: const Text("-"),
                                                ),
                                                (screenWidth(45)).pw,
                                                CustomText(
                                                  text: "${controller.numberStrikes.value}",
                                                  textType: TextStyleType.CUSTOM,
                                                  textColor: AppColors.mainBlackColor,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: screenWidth(28),
                                                ),
                                                (screenWidth(50)).pw,
                                                ElevatedButton(
                                                  onPressed: () {
                                                    controller.numberStrikes.value +=
                                                        controller.minStrikes.value;
                                                    controller.timeStrikes.value +=
                                                        controller.minTime.value;
                                                    controller.priceShots.value +=
                                                        controller.minPrice.value;
                                                    controller.getTimeShots(
                                                        date: controller.date.value);
                                                  },
                                                  style: ElevatedButton.styleFrom(
                                                    shape: const StadiumBorder(),
                                                    backgroundColor: AppColors.mainYellowColor,
                                                  ),
                                                  child: const Text("+"),
                                                ),
                                              ],
                                            );
                                          },
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            CustomText(
                                              textType: TextStyleType.CUSTOM,
                                              text: tr('key_time_finish_strikes'),
                                              textColor: AppColors.mainYellowColor,
                                            ),
                                            CustomText(
                                              textType: TextStyleType.CUSTOM,
                                              text: tr('key_Price'),
                                              textColor: AppColors.mainYellowColor,
                                            ),
                                          ],
                                        ),
                                        Obx(
                                          () {
                                            return Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Container(
                                                  width: screenWidth(5),
                                                  height: screenWidth(10),
                                                  decoration: BoxDecoration(
                                                    color: AppColors.mainColorGreen,
                                                    borderRadius:
                                                        BorderRadius.circular(screenWidth(25)),
                                                  ),
                                                  child: Center(
                                                    child: CustomText(
                                                      textType: TextStyleType.CUSTOM,
                                                      text:
                                                          controller.timeStrikes.value.toString() +
                                                              tr('key_min'),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  width: screenWidth(4.5),
                                                  height: screenWidth(10),
                                                  decoration: BoxDecoration(
                                                    color: AppColors.mainColorGreen,
                                                    borderRadius:
                                                        BorderRadius.circular(screenWidth(25)),
                                                  ),
                                                  child: Center(
                                                    child: CustomText(
                                                        textType: TextStyleType.CUSTOM,
                                                        text:
                                                            controller.priceShots.value.toString() +
                                                                tr('key_AED')),
                                                  ),
                                                ),
                                              ],
                                            );
                                          },
                                        ),
                                      ],
                                    ),
                                  );
                      },
                    )
                  : const SizedBox.shrink(),
              Obx(
                () {
                  log(controller.date.toString());
                  return Padding(
                    padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                    child: TableCalendar(
                      availableGestures: AvailableGestures.horizontalSwipe,
                      selectedDayPredicate: (day) {
                        return isSameDay(controller.date.value, day);
                      },
                      headerStyle: HeaderStyle(
                        formatButtonVisible: true,
                        titleCentered: true,
                        formatButtonShowsNext: false,
                        formatButtonDecoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        formatButtonTextStyle: const TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      calendarStyle: const CalendarStyle(
                        selectedDecoration: BoxDecoration(
                          color: AppColors.mainYellowColor,
                          shape: BoxShape.circle,
                        ),
                      ),
                      onDaySelected: (selectedDay, focusedDay) {
                        controller.selectDay.value = true;
                        controller.date.value = selectedDay;
                        if (widget.bookingType == 2) {
                          controller.getTimeShots(date: selectedDay);
                        } else {
                          controller.getGameTime(
                            date: selectedDay,
                          );
                        }
                      },
                      availableCalendarFormats: const {
                        CalendarFormat.month: '',
                      },
                      firstDay: DateTime.utc(
                          DateTime.now().year, DateTime.now().month, DateTime.now().day),
                      lastDay: DateTime.now().add(const Duration(days: 90)),
                      focusedDay: DateTime.now(),
                    ),
                  );
                },
              ),
              Obx(
                () {
                  return controller.shots.value.withWeapon == 1 &&
                          controller.shots.value.gameWeapon!.isEmpty
                      ? const SizedBox.shrink()
                      : controller.isGameTimeLoading || controller.gameTime[0].message == null
                          ? const SpinKitCircle(
                              color: AppColors.mainYellowColor,
                            )
                          :
                          // controller.gameTime[0].message == null
                          //     ? customFaildConnection(onPressed: () {
                          //         controller.onInit();
                          //       })
                          //     :
                          Column(
                              children: [
                                widget.bookingType == 2
                                    ? const SizedBox.shrink()
                                    : CustomTextButton(
                                        onPressed: () {
                                          if (storage.getUserId() == 0) {
                                            Get.defaultDialog(
                                              title: tr('key_login_complete'),
                                              titleStyle:
                                                  const TextStyle(color: AppColors.mainYellowColor),
                                              content: Column(
                                                children: [
                                                  CustomButton(
                                                    text: tr('key_Yes'),
                                                    onPressed: () async {
                                                      storage.setCartList([]);
                                                      storage.setFavoriteList([]);
                                                      storage.setFirstLanuch(true);
                                                      storage.setIsLoggedIN(false);
                                                      storage.globalSharedPrefs
                                                          .remove(storage.PREF_TOKEN_INFO);
                                                      //storage.setTokenInfo(null);
                                                      await storage.setUserId(0);
                                                      storage.globalSharedPrefs
                                                          .remove(storage.PREF_USER_NAME);

                                                      storage.setGoogle(false);
                                                      storage.globalSharedPrefs
                                                          .remove(storage.PREF_MOBILE_NUMBER);
                                                      storage.globalSharedPrefs
                                                          .remove(storage.PREF_EMAIL);

                                                      Get.offAll(() => const SignInView());
                                                    },
                                                    widthButton: 4,
                                                    circularBorder: screenWidth(10),
                                                  ),
                                                  screenWidth(20).ph,
                                                  CustomButton(
                                                    text: tr('key_No'),
                                                    onPressed: () {
                                                      Get.back();
                                                    },
                                                    widthButton: 4,
                                                    circularBorder: screenWidth(10),
                                                  ),
                                                ],
                                              ),
                                            );
                                          } else {
                                            controller.selectDay.value == true
                                                ? controller.selectAllDay(price: widget.price)
                                                : null;
                                          }
                                        },
                                        text: tr('key_Select_All_Day'),
                                        colorText: AppColors.mainYellowColor,
                                      ),
                                Padding(
                                  padding:
                                      EdgeInsetsDirectional.symmetric(horizontal: screenWidth(50)),
                                  child: GridView.count(
                                    crossAxisCount: 4,
                                    crossAxisSpacing: 8,
                                    shrinkWrap: true,
                                    physics: const NeverScrollableScrollPhysics(),
                                    children: controller.gameTime.map(
                                      (element) {
                                        return Padding(
                                          padding: EdgeInsetsDirectional.symmetric(
                                              vertical: screenWidth(20)),
                                          child: ListTile(
                                            onTap: element.booking == "not_available"
                                                ? null
                                                : () {
                                                    if (storage.getUserId() == 0) {
                                                      Get.defaultDialog(
                                                        title: tr('key_login_complete'),
                                                        titleStyle: const TextStyle(
                                                            color: AppColors.mainYellowColor),
                                                        content: Column(
                                                          children: [
                                                            CustomButton(
                                                              text: tr('key_Yes'),
                                                              onPressed: () async {
                                                                storage.setCartList([]);
                                                                storage.setFavoriteList([]);
                                                                storage.setFirstLanuch(true);
                                                                storage.setIsLoggedIN(false);
                                                                storage.globalSharedPrefs.remove(
                                                                    storage.PREF_TOKEN_INFO);
                                                                //storage.setTokenInfo(null);
                                                                await storage.setUserId(0);
                                                                storage.globalSharedPrefs
                                                                    .remove(storage.PREF_USER_NAME);

                                                                storage.setGoogle(false);
                                                                storage.globalSharedPrefs.remove(
                                                                    storage.PREF_MOBILE_NUMBER);
                                                                storage.globalSharedPrefs
                                                                    .remove(storage.PREF_EMAIL);
                                                                Get.offAll(const SignInView());
                                                              },
                                                              widthButton: 4,
                                                              circularBorder: screenWidth(10),
                                                            ),
                                                            screenWidth(20).ph,
                                                            CustomButton(
                                                              text: tr('key_No'),
                                                              onPressed: () {
                                                                Get.back();
                                                              },
                                                              widthButton: 4,
                                                              circularBorder: screenWidth(10),
                                                            ),
                                                          ],
                                                        ),
                                                      );
                                                    } else {
                                                      if (widget.bookingType == 2) {
                                                        controller.bookingShots(
                                                            startTime: element.time!,
                                                            total: controller.priceShots.value);
                                                      } else {
                                                        controller.addToBook(element, widget.price);
                                                      }
                                                    }
                                                  },
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(screenWidth(20)),
                                            ),
                                            tileColor: element.color!.value,
                                            title: Padding(
                                              padding: EdgeInsetsDirectional.only(
                                                  bottom: screenWidth(50)),
                                              child: CustomText(
                                                textType: TextStyleType.CUSTOM,
                                                text: element.time!,
                                                fontSize: screenWidth(30),
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    ).toList(),
                                  ),
                                ),
                                widget.bookingType == 2
                                    ? const SizedBox.shrink()
                                    : CustomButton(
                                        circularBorder: screenWidth(20),
                                        text: tr('key_Make_an_Appointment'),
                                        onPressed: () {
                                          Get.to(
                                            PaymentView(
                                              currentlocation: widget.currentlocation,
                                              bookId: controller.bookId,
                                              imageGameSelected: widget.imageGameSelected,
                                              address: widget.address,
                                              clubId: widget.clubId,
                                              clubType: widget.clubType,
                                              gameFieldId: widget.gameFieldId,
                                              imageClub: widget.imageClub,
                                              imageGame: widget.imageGame,
                                              nameClub: widget.nameClub,
                                              nameGame: widget.nameGame,
                                              price: controller.totalPrice.value,
                                              quantity: widget.quantity,
                                            ),
                                          );
                                          controller.totalPrice.value = 0.0;
                                          //controller.bookId.clear();
                                        },
                                      ),
                                screenWidth(20).ph
                              ],
                            );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
