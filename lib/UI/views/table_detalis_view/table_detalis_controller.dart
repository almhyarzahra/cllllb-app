import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import '../../../core/data/models/apis/game_time_model.dart';
import '../../../core/data/models/apis/shots_model.dart';
import '../../../core/data/repositories/booking_repository.dart';
import '../../../core/translation/app_translation.dart';
import '../payment_view/payment_view.dart';

class TableDetalisController extends BaseController {
  TableDetalisController({
    required int gameFieldId,
    required int bookingType,
    this.currentlocation,
    required String this.imageGameSelected,
    required String this.address,
    required int this.clubId,
    required String this.clubType,
    required String this.imageClub,
    required String this.imageGame,
    required String this.nameClub,
    required String this.nameGame,
    required int this.quantity,
  }) {
    this.gameFieldId.value = gameFieldId;
    this.bookingType.value = bookingType;
  }
  int? quantity;
  String? nameGame;
  String? nameClub;
  String? imageGame;
  String? imageClub;
  String? clubType;
  int? clubId;
  String? address;
  String? imageGameSelected;
  RxInt bookingType = 0.obs;
  RxInt gameFieldId = 0.obs;
  LocationData? currentlocation;
  RxList<GameTimeModel> gameTime = <GameTimeModel>[GameTimeModel()].obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  bool get isGameTimeLoading => listType.contains(OperationType.GAME_TIME);
  bool get isShotsLoading => listType.contains(OperationType.SHOTS);

  Rx<DateTime> date = DateTime.now().obs;
  Rx<int> count = 0.obs;
  Rx<bool> selectDay = true.obs;
  RxMap<String, int> bookId = <String, int>{}.obs;
  RxDouble totalPrice = 0.0.obs;
  Rx<ShotsModel> shots = ShotsModel().obs;
  RxInt numberStrikes = 0.obs;
  RxInt timeStrikes = 0.obs;
  RxInt minStrikes = 0.obs;
  RxInt minTime = 0.obs;
  RxDouble priceShots = 0.0.obs;
  RxDouble minPrice = 0.0.obs;
  RxInt idWeapon = 0.obs;

  @override
  Future<void> onInit() async {
    if (bookingType.value == 2) {
      getShots();
    } else {
      getGameTime(date: date.value);
    }

    super.onInit();
  }

  @override
  void onClose() {
    if (bookId.isNotEmpty) {
      for (var entry in bookId.entries) {
        bookingRetreat(idBooking: [entry.value]);
      }
    }
    super.onClose();
  }

  void getTimeShots({required DateTime date}) {
    runLoadingFutureFunction(
        type: OperationType.GAME_TIME,
        function: BookingRepository()
            .getGameTimeShots(
                date: formatDate(date),
                gameFieldId: gameFieldId.value,
                hourNow: DateFormat("yyyy-MM-dd").format(date) ==
                        DateFormat("yyyy-MM-dd").format(DateTime.now())
                    ? formatHour(DateTime.now())
                    : formatHour(date),
                time: timeStrikes.value)
            .then((value) {
          value.fold((l) {
            getTimeShots(date: date);
          }, (r) {
            if (gameTime.isNotEmpty) {
              gameTime.clear();
            }

            gameTime.addAll(r);

            gameTime.map((element) {
              element.booking == "not_available"
                  ? element.color!.value = AppColors.mainRedColor
                  : element.color!.value = AppColors.mainColorGreen;
              element.message = "l";
            }).toSet();
          });
        }));
  }

  void getGameTime({
    required DateTime date,
  }) {
    runLoadingFutureFunction(
        type: OperationType.GAME_TIME,
        function: BookingRepository()
            .getGameTime(
                date: formatDate(date),
                gameFieldId: gameFieldId.value,
                hourNow: DateFormat("yyyy-MM-dd").format(date) ==
                        DateFormat("yyyy-MM-dd").format(DateTime.now())
                    ? formatHour(DateTime.now())
                    : formatHour(date))
            .then((value) {
          value.fold((l) {
            getGameTime(date: date);
          }, (r) {
            if (gameTime.isNotEmpty) {
              gameTime.clear();
            }

            gameTime.addAll(r);

            gameTime.map((element) {
              element.booking == "not_available"
                  ? element.color!.value = AppColors.mainRedColor
                  : element.color!.value = AppColors.mainColorGreen;
              element.message = "l";
            }).toSet();
          });
        }));
  }

  String formatDate(DateTime date) {
    return DateFormat("yyyy-MM-dd").format(date);
  }

  String formatHour(DateTime date) {
    return DateFormat('HH:mm:ss').format(date);
  }

  void selectAllDay({required double price}) {
    gameTime.map((element) {
      if (element.booking == "not_available") {
        count.value++;

        return;
      }
    }).toSet();
    if (count.value == 0) {
      runFullLoadingFutureFunction(
          function: BookingRepository()
              .booking(
                  userId: storage.getUserId().toString(),
                  gameClubFieldId: gameFieldId.value.toString(),
                  startTime: gameTime[0].time!,
                  endTime: addHalfHourToTime(gameTime.last.time!, 30),
                  date: DateFormat("yyyy-MM-dd").format(date.value),
                  total: price * gameTime.length)
              .then((value) {
        value.fold((l) {
          CustomToast.showMessage(
              message: tr('key_check_connection2'), messageType: MessageType.REJECTED);
        }, (r) {
          if (r.code == 0) {
            CustomToast.showMessage(message: r.success!, messageType: MessageType.INFO);
            onInit();
          } else if (r.code == 1) {
            selectDay.value = false;
            totalPrice.value += price * gameTime.length;
            bookId["${tr('all_day')} ${gameTime[0].time} ${tr('to')} ${addHalfHourToTime(gameTime.last.time!, 30)}"] =
                r.bookId!;
            gameTime.map((element) {
              element.color!.value = AppColors.mainYellowColor;
            }).toSet();
          }
        });
      }));
    } else {
      CustomToast.showMessage(
          message: tr('key_Can_not_Select_All_Day'), messageType: MessageType.REJECTED);
      count.value = 0;
    }
  }

  void booking({required String startTime, required double total}) {
    runFullLoadingFutureFunction(
        function: BookingRepository()
            .booking(
                userId: storage.getUserId().toString(),
                gameClubFieldId: gameFieldId.value.toString(),
                startTime: startTime,
                endTime: addHalfHourToTime(startTime, 30),
                date: DateFormat("yyyy-MM-dd").format(date.value),
                total: total)
            .then((value) {
      value.fold((l) {
        booking(startTime: startTime, total: total);
      }, (r) {
        if (r.code == 0) {
          CustomToast.showMessage(
              message: tr('key_The_Time ') + startTime + tr('key_is_not_available'),
              messageType: MessageType.INFO);
          onInit();
        } else if (r.code == 1) {
          bookId["$startTime ${tr('to')} ${addHalfHourToTime(startTime, 30)}"] = r.bookId!;
          totalPrice.value += total;
        }
      });
    }));
  }

  String addHalfHourToTime(String time, int duration) {
    DateTime time1 = DateFormat('HH:mm:ss').parse(time);
    DateTime newTime = time1.add(Duration(minutes: duration));
    return DateFormat('HH:mm:ss').format(newTime);
  }

  void addToBook(GameTimeModel element, double price) {
    element.color!.value == AppColors.mainYellowColor
        ? {
            element.color!.value = AppColors.mainColorGreen,
            bookingRetreat(idBooking: [bookId[element.time]!]),
            bookId.remove(element.time),
            totalPrice.value -= price
          }
        : {
            element.color!.value = AppColors.mainYellowColor,
            booking(startTime: element.time!, total: price),
          };
  }

  void bookingRetreat({required List<int> idBooking}) {
    runFullLoadingFutureFunction(
        function: BookingRepository().bookingRetreat(id: idBooking).then((value) {
      value.fold((l) {
        bookingRetreat(idBooking: idBooking);
      }, (r) {});
    }));
  }

  Future<void> getShots() async {
    runLoadingFutureFunction(
        type: OperationType.SHOTS,
        function: BookingRepository().getShots(gameId: gameFieldId.value).then((value) {
          value.fold((l) {
            getShots();
          }, (r) {
            shots.value = r;
            numberStrikes.value = r.shots == null && r.gameWeapon!.isNotEmpty
                ? r.gameWeapon![0].shot ?? 0
                : r.shots ?? 0;
            timeStrikes.value = r.time == null && r.gameWeapon!.isNotEmpty
                ? r.gameWeapon![0].time ?? 0
                : r.time ?? 0;
            priceShots.value = r.shots == null && r.gameWeapon!.isNotEmpty
                ? r.gameWeapon![0].price ?? 0.0
                : r.price ?? 0.0;
            minStrikes.value = numberStrikes.value;
            minTime.value = timeStrikes.value;
            minPrice.value = priceShots.value;
            if (r.gameWeapon!.isNotEmpty) {
              idWeapon.value = r.gameWeapon![0].id ?? 0;
            }
            getTimeShots(date: date.value);
          });
        }));
  }

  void bookingShots({required String startTime, required double total}) {
    runFullLoadingFutureFunction(
        function: BookingRepository()
            .booking(
                userId: storage.getUserId().toString(),
                gameClubFieldId: gameFieldId.value.toString(),
                startTime: startTime,
                endTime: addHalfHourToTime(startTime, timeStrikes.value),
                date: DateFormat("yyyy-MM-dd").format(date.value),
                total: total)
            .then((value) {
      value.fold((l) {
        booking(startTime: startTime, total: total);
      }, (r) {
        if (r.code == 0) {
          CustomToast.showMessage(
              message: tr('key_The_Time ') + startTime + tr('key_is_not_available'),
              messageType: MessageType.INFO);
          onInit();
        } else if (r.code == 1) {
          bookId["$startTime ${tr('to')} ${addHalfHourToTime(startTime, timeStrikes.value)}"] =
              r.bookId!;
          totalPrice.value += total;
          Get.to(PaymentView(
            currentlocation: currentlocation,
            bookId: bookId,
            imageGameSelected: imageGameSelected!,
            address: address!,
            clubId: clubId!,
            clubType: clubType!,
            gameFieldId: gameFieldId.value,
            imageClub: imageClub!,
            imageGame: imageGame!,
            nameClub: nameClub!,
            nameGame: nameGame!,
            price: totalPrice.value,
            quantity: quantity!,
          ));
          totalPrice.value = 0.0;
        }
      });
    }));
  }
}
