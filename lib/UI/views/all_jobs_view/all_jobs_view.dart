import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/core/data/models/apis/jobs_model.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text_field.dart';
import '../../shared/utils.dart';
import '../jobs_detalis_view/jobs_detalis_view.dart';
import 'all_jobs_controller.dart';

class AllJobsView extends StatefulWidget {
  const AllJobsView(
      {super.key, required this.currentlocation, required this.jobs});
  final LocationData? currentlocation;
  final List<JobsModel> jobs;

  @override
  State<AllJobsView> createState() => _AllJobsViewState();
}

class _AllJobsViewState extends State<AllJobsView> {
  late AllJobsController controller;
  @override
  void initState() {
    controller = Get.put(AllJobsController(jobs: widget.jobs));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
         bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
              ],
            ),
            Padding(
              padding:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
              child: CustomTextField(
                onChanged: (value) {
                  controller.goal.value = value;
                },
                hintText: tr('key_Search'),
                contentPaddingLeft: screenWidth(10),
                controller: controller.searchController,
                suffixIcon: Container(
                  height: screenHeight(25),
                  width: screenWidth(10),
                  decoration: BoxDecoration(
                    color: AppColors.mainYellowColor,
                    borderRadius: BorderRadius.circular(screenWidth(10)),
                  ),
                  child: const Icon(
                    Icons.search,
                    color: AppColors.mainWhiteColor,
                  ),
                ),
              ),
            ),
            Obx(
              () {
                return Column(
                  children: controller.search().map(
                    (e) {
                      return Card(
                        elevation: 4,
                        child: Padding(
                          padding: EdgeInsetsDirectional.symmetric(
                              vertical: screenWidth(20)),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    CustomNetworkImage(
                                      context.image(e.club!.mainImage!),
                                      width: screenWidth(7),
                                      height: screenHeight(10),
                                      border: Border.all(
                                          color: AppColors.mainYellowColor),
                                      shape: BoxShape.circle,
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: e.club!.name!,
                                      fontSize: screenWidth(30),
                                      overflow: TextOverflow.ellipsis,
                                      textColor: AppColors.mainBlackColor,
                                    ),
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: e.club!.city!,
                                      overflow: TextOverflow.ellipsis,
                                      textColor: AppColors.mainYellowColor,
                                      fontSize: screenWidth(30),
                                    ),
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: e.distance == null
                                          ? ""
                                          : e.distance!.toInt() > 1000
                                              ? tr('key_near') +
                                                  (e.distance!.toInt() / 1000)
                                                      .toString() +
                                                  tr('key_km')
                                              : tr('key_near') +
                                                  e.distance!
                                                      .toInt()
                                                      .toString() +
                                                  tr('key_meter'),
                                      textColor: AppColors.mainYellowColor,
                                      fontSize: screenWidth(30),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    Row(
                                      children: [
                                        CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text: tr('key_job_title'),
                                          textColor: AppColors.mainYellowColor,
                                          fontSize: screenWidth(30),
                                        ),
                                        Flexible(
                                          child: CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: e.jobTitle!,
                                            textColor: AppColors.mainBlackColor,
                                            overflow: TextOverflow.ellipsis,
                                            fontSize: screenWidth(30),
                                          ),
                                        ),
                                      ],
                                    ),
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: e.jobDescription!,
                                      textColor: AppColors.mainBlackColor,
                                      overflow: TextOverflow.ellipsis,
                                      fontSize: screenWidth(30),
                                    ),
                                    Row(
                                      children: [
                                        CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text: "reference No: ",
                                          textColor: AppColors.mainBlackColor,
                                          fontSize: screenWidth(30),
                                        ),
                                        Flexible(
                                          child: CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: "Job${e.id}",
                                            textColor: AppColors.mainBlackColor,
                                            fontSize: screenWidth(30),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Flexible(
                                          child: CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: tr('key_dead_line'),
                                            textColor: AppColors.mainGreyColor,
                                            fontSize: screenWidth(30),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                        Flexible(
                                          child: CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: e.announcementEndDate!,
                                            textColor:
                                                AppColors.mainYellowColor,
                                            fontSize: screenWidth(30),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: tr('key_expected_salary'),
                                      textColor: AppColors.mainYellowColor,
                                      fontSize: screenWidth(42),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: e.expectSalary! + tr('key_AED'),
                                      textColor: AppColors.mainYellowColor,
                                      fontSize: screenWidth(42),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    CustomButton(
                                      text: tr('key_apply_now'),
                                      onPressed: () {
                                        Get.to(
                                          JobsDetalisView(
                                            currentlocation:
                                                widget.currentlocation,
                                            job: e,
                                          ),
                                        );
                                      },
                                      heightButton: 15,
                                      textSize: screenWidth(60),
                                      widthButton: 6,
                                      circularBorder: screenWidth(20),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ).toList(),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
