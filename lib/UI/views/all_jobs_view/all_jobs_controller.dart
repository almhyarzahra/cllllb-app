import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/data/models/apis/jobs_model.dart';

class AllJobsController extends BaseController {
  AllJobsController({required List<JobsModel> jobs}) {
    this.jobs.value = jobs;
  }
  RxList<JobsModel> jobs = <JobsModel>[].obs;
  Rx<String> goal = "".obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  TextEditingController searchController = TextEditingController();


  List<JobsModel> search() {
    RxList<JobsModel> results = <JobsModel>[].obs;
    if (goal.isEmpty) {
      results.value = jobs;
    } else {
      results.value = jobs
          .where((p0) => p0.jobTitle!.toLowerCase().contains(goal.toLowerCase()))
          .toList();
    }
    return results;
  }
}
