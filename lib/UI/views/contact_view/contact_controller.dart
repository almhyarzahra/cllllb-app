import 'dart:async';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ContactController extends BaseController {
  ContactController({
    required double latitude,
    required double longtitude,
    required int clubId,
  }) {
    this.latitude.value = latitude;
    this.longtitude.value = longtitude;
    this.clubId.value = clubId;
  }
  RxInt clubId = 0.obs;
  RxDouble latitude = 0.0.obs;
  RxDouble longtitude = 0.0.obs;
  RxSet<Marker> markers = <Marker>{}.obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  final Completer<GoogleMapController> mapController =
      Completer<GoogleMapController>();

  Set<Marker> getMarker() {
    Marker myMarker = Marker(
      markerId: MarkerId(clubId.value.toString()),
      position: LatLng(latitude.value, longtitude.value),
    );
    markers.add(myMarker);
    return markers;
  }

  CameraPosition getKGooglePlex() {
    CameraPosition kGooglePlex = CameraPosition(
      target: LatLng(latitude.value, longtitude.value),
      zoom: 14.4746,
    );
    return kGooglePlex;
  }

  // Future<BitmapDescriptor> loadCustomMarker() async {
  //   return await BitmapDescriptor.fromAssetImage(
  //     ImageConfiguration(size: Size(48, 48)),
  //     'assets/custom_marker.png',
  //   );
  // }
}
