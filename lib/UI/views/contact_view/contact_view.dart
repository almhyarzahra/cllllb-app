import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/views/contact_view/contact_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_book_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';

class ContactView extends StatefulWidget {
  const ContactView(
      {super.key,
      required this.nameClub,
      required this.address,
      required this.imageClub,
      required this.clubId,
      required this.latitude,
      required this.longtitude,
      required this.phone,
      required this.email,
      required this.clubType,
      required this.currentlocation});
  final String nameClub;
  final String address;
  final String imageClub;
  final int clubId;
  final double latitude;
  final double longtitude;
  final String phone;
  final String email;
  final String clubType;
  final LocationData? currentlocation;

  @override
  State<ContactView> createState() => _ContactViewState();
}

class _ContactViewState extends State<ContactView> {
  late ContactController controller;
  @override
  void initState() {
    controller = Get.put(
      ContactController(
          latitude: widget.latitude, longtitude: widget.longtitude, clubId: widget.clubId),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            screenHeight(80).ph,
            CustomBookBar(
              imageUrl: widget.imageClub,
              imageName: Uri.https(NetworkUtil.baseUrl, widget.imageClub).toString(),
              text: "${widget.nameClub}/ ${widget.address}/ ${widget.clubType}",
            ),
            SizedBox(
              height: screenHeight(3),
              child: GoogleMap(
                mapType: MapType.normal,
                initialCameraPosition: controller.getKGooglePlex(),
                onMapCreated: (GoogleMapController googlecontroller) {
                  controller.mapController.complete(googlecontroller);
                },
                markers: controller.getMarker(),
              ),
            ),
            Padding(
              padding: EdgeInsetsDirectional.symmetric(
                  horizontal: screenWidth(10), vertical: screenWidth(10)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SelectableText(tr('key_phone: ') + widget.phone + "\n"),
                  SelectableText(tr('Email: ') + widget.email + "\n"),
                  // CustomText(
                  //   textType: TextStyleType.CUSTOM,
                  //   text: "phone: ${widget.phone}\n",
                  //   textColor: AppColors.mainBlackColor,
                  // ),
                  // CustomText(
                  //   textType: TextStyleType.CUSTOM,
                  //   text: "Email: ${widget.email}\n",
                  //   textColor: AppColors.mainBlackColor,
                  // ),
                  // CustomText(
                  //   textType: TextStyleType.CUSTOM,
                  //   text: "whatsapp : 056 1534995\n",
                  //   textColor: AppColors.mainBlackColor,
                  // ),
                ],
              ),
            ),
            //Spacer(),
            // CustomContact()
          ],
        ),
      ),
    );
  }
}
