import 'dart:convert';
import 'dart:developer';

import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/UI/views/main_view/main_view.dart';
import 'package:com.tad.cllllb/UI/views/welcome_view/welcome_view.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
// ignore: depend_on_referenced_packages
import 'package:path/path.dart' as path;

import '../../../core/data/models/apis/all_game_model.dart';
import '../../../core/data/repositories/user_repository.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../verification_view/verification_view.dart';

class PersonalInformationController extends BaseController {
  PersonalInformationController(String? invitationCode) {
    qrCode = invitationCode;
  }
  String? qrCode;
  TextEditingController mobileNumberController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController invitationCode = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  Rx<AllGameModel> allGameModel = AllGameModel().obs;
  RxList<bool> isChecked = <bool>[].obs;
  RxList<int>? idGame = <int>[].obs;
  final ImagePicker picker = ImagePicker();
  XFile? image;
  XFile? image1;
  CroppedFile? croppedImage;
  RxInt code = 0.obs;
  RxBool getMore = true.obs;
  RxBool enableScrollListner = true.obs;
  String? base64Image;
  RxBool isSelected = false.obs;
  RxInt pageKey = 2.obs;

  bool get isGameLoading => listType.contains(OperationType.g);
  bool get isMoreGamesLoading => listType.contains(OperationType.moreG);
  @override
  void onInit() {
    super.onInit();
    invitationCode.text = qrCode ?? "";
    getAllGame();
  }

  void addIdGame({required int id}) {
    if (idGame!.contains(id)) {
      idGame!.remove(id);
    } else {
      idGame!.add(id);
    }
    //print(idGame);
  }

  void getAllGame() {
    runLoadingFutureFunction(
      type: OperationType.g,
      function: UserRepository().getAllGame(page: 1).then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_No_Internet_Please Refresh'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              allGameModel.value = r;
              // isChecked.clear();
              r.data!.map((e) {
                isChecked.add(false);
              }).toSet();
            },
          );
        },
      ),
    );
  }

  Future<void> getMoreGames({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
        type: OperationType.moreG,
        function: UserRepository().getAllGame(page: page).then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: tr('key_No_Internet_Please Refresh'),
                messageType: MessageType.REJECTED);
            pageKey.value = pageKey.value - 1;
          }, (r) {
            if (r.data!.isEmpty) {
              enableScrollListner.value = false;
            } else {
              allGameModel.value.data!.addAll(r.data!);
              r.data!.map((e) {
                isChecked.add(false);
              }).toSet();
            }
          });
        })).then((value) => getMore.value = true);
  }

  void selectImageFromCamera() async {
    isSelected.value = false;

    image = await picker.pickImage(source: ImageSource.camera);
    croppedImage = await ImageCropper().cropImage(
      sourcePath: image!.path,
      cropStyle: CropStyle.rectangle,
      compressFormat: ImageCompressFormat.jpg,
      compressQuality: 100,
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: tr('key_Cropper'),
            toolbarColor: AppColors.mainYellowColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
      ],
    );
    if (croppedImage != null) {
      image1 = XFile(path.absolute(croppedImage!.path));
      isSelected.value = true;
      imageToBase64();
    } else if (image != null) {
      image1 = image;
      isSelected.value = true;
      imageToBase64();
    }

    Get.back();
  }

  void selectImageFromGallery() async {
    isSelected.value = false;

    image = await picker.pickImage(source: ImageSource.gallery);
    croppedImage = await ImageCropper().cropImage(
      sourcePath: image!.path,
      cropStyle: CropStyle.rectangle,
      compressFormat: ImageCompressFormat.jpg,
      compressQuality: 100,
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: tr('key_Cropper'),
            toolbarColor: AppColors.mainYellowColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
      ],
    );
    if (croppedImage != null) {
      image1 = XFile(path.absolute(croppedImage!.path));
      isSelected.value = true;
      imageToBase64();
    } else if (image != null) {
      image1 = image;
      isSelected.value = true;
      imageToBase64();
    }
    Get.back();
  }

  void imageToBase64() async {
    if (image1 != null) {
      final bytes = await image1!.readAsBytes();
      base64Image = base64.encode(bytes);
    }
  }

  void otp() {
    if (formKey.currentState!.validate()) {
      runFullLoadingFutureFunction(
          function: UserRepository().getCodeRandom().then((value) {
        value.fold((l) {
          CustomToast.showMessage(
              message: tr('key_check_connection'),
              messageType: MessageType.REJECTED);
        }, (r) {
          code.value = r.code!;
          // print(code.value);
          runFullLoadingFutureFunction(
              function: UserRepository()
                  .otp(
                      code: "971${mobileNumberController.text.trim()}" ==
                              "971559039837"
                          ? "000000"
                          : code.value.toString(),
                      phone:
                          "971${mobileNumberController.text.trim().toString()}")
                  .then((value) {
            value.fold((l) {
              CustomToast.showMessage(
                  message: tr('key_try_again'),
                  messageType: MessageType.REJECTED);
              // Get.to(VerificationView(
              //   code:
              //       "971${mobileNumberController.text.trim()}" == "971559039837"
              //           ? 000000
              //           : 000000,
              //   //code.value,
              //   type: "register",
              // ));
            }, (r) {
              Get.to(VerificationView(
                code:
                    "971${mobileNumberController.text.trim()}" == "971559039837"
                        ? 000000
                        : code.value,
                type: "register",
              ));
            });
          }));
        });
      }));
    }
  }

  void register() {
    if (formKey.currentState!.validate()) {
      runFullLoadingFutureFunction(
          function: UserRepository()
              .register(
                  name: nameController.text.trim().toString(),
                  email: emailController.text.trim().toString(),
                  phone: "971${mobileNumberController.text.trim().toString()}",
                  password: "0962245305",
                  password_confirmation: "0962245305",
                  game_id: idGame,
                  image: base64Image,
                  referenceCode: invitationCode.text.trim().isEmpty
                      ? null
                      : invitationCode.text.trim())
              .then((value) {
        value.fold((l) {
          if (l == null) {
            CustomToast.showMessage(
                message: tr('key_No_Internet_Connection'),
                messageType: MessageType.REJECTED);
          } else {
            CustomToast.showMessage(
                message: tr('key_Please_Check_Information'),
                messageType: MessageType.REJECTED);
          }
        }, (r) {
          CustomToast.showMessage(
              message: tr('key_success'), messageType: MessageType.SUCCSESS);
          storage.setTokenInfo(r.token!);
          storage.setUserId(r.userId!);
          storage.setUserName(r.userName!);
          storage.setEmail(r.userEmail!);
          storage.setmobileNumber(r.userPhone!);
          if (r.addedPoints != null) {
            Get.offAll(WelcomeView(
              addedPoints: r.addedPoints.toString(),
              invitationCode: r.referenceCode!,
              pointWillTake: r.pointsWillTake!.toString(),
            ));
          } else {
            Get.offAll( const MainView(initialPage: 0));
          }
        });
      }));
    }
  }
}
