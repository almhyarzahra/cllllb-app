import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/personal_information_view/personal_information_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:com.tad.cllllb/core/utils/network_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

import '../../../core/translation/app_translation.dart';

class PersonalInformationView extends StatefulWidget {
  final String? invitationCode;
  const PersonalInformationView({super.key, this.invitationCode});

  @override
  State<PersonalInformationView> createState() =>
      _PersonalInformationViewState();
}

class _PersonalInformationViewState extends State<PersonalInformationView> {
  late PersonalInformationController controller;
  final ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    controller = Get.put(PersonalInformationController(widget.invitationCode));
    _scrollController.addListener(_scrollListener);
    super.initState();
  }

  void _scrollListener() async {
    if (controller.getMore.value) {
      if (controller.enableScrollListner.value &&
          _scrollController.position.maxScrollExtent ==
              _scrollController.offset &&
          !_scrollController.position.outOfRange) {
        controller.getMore.value = false;
        await controller.getMoreGames(page: controller.pageKey.value);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Form(
          key: controller.formKey,
          child: Padding(
            padding:
                EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
            child: RefreshIndicator(
              onRefresh: () async {
                controller.onInit();
              },
              child: ListView(
                children: [
                  Row(
                    children: [
                      IconButton(
                        onPressed: () {
                          Get.back();
                        },
                        icon: Icon(
                          Icons.arrow_back_ios_new,
                          size: screenWidth(20),
                        ),
                      ),
                      screenWidth(10).pw,
                      Image.asset(
                        "images/cllllb.png",
                        height: screenHeight(5),
                      ),
                    ],
                  ),
                  screenHeight(50).ph,
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: tr('key_enter_information'),
                    textColor: AppColors.mainYellowColor,
                    fontSize: screenWidth(20),
                  ),
                  screenHeight(50).ph,
                  InkWell(
                    onTap: () {
                      Get.defaultDialog(
                        title: tr('key_Please_Select'),
                        content: Column(
                          children: [
                            CustomButton(
                              text: tr('key_Camera'),
                              onPressed: () {
                                controller.selectImageFromCamera();
                              },
                              widthButton: 3,
                              circularBorder: screenWidth(20),
                            ),
                            screenWidth(20).ph,
                            CustomButton(
                              text: tr('key_Gallery'),
                              onPressed: () {
                                controller.selectImageFromGallery();
                              },
                              widthButton: 3,
                              circularBorder: screenWidth(20),
                            ),
                          ],
                        ),
                      );
                    },
                    child: Obx(
                      () {
                        return controller.isSelected.value
                            ? Container(
                                height: screenHeight(6),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: AppColors.mainYellowColor),
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    fit: BoxFit.contain,
                                    image: FileImage(
                                      File(controller.image1!.path),
                                    ),
                                  ),
                                ),
                                child: controller.isSelected.isFalse
                                    ? Icon(
                                        Icons.camera_alt_rounded,
                                        size: screenWidth(5),
                                        color: AppColors.mainWhiteColor,
                                      )
                                    : null,
                              )
                            : CircleAvatar(
                                radius: 60,
                                backgroundColor: AppColors.mainYellowColor,
                                // backgroundImage: controller.image != null
                                //     ? FileImage(File(controller.image!.path))
                                //     : null,
                                child: controller.image == null
                                    ? Icon(
                                        Icons.camera_alt_rounded,
                                        size: screenWidth(5),
                                        color: AppColors.mainWhiteColor,
                                      )
                                    : null,
                              );
                      },
                    ),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: tr('key_Select_Your_Image'),
                    textColor: AppColors.mainBlackColor,
                  ),
                  screenWidth(10).ph,
                  CustomTextField(
                    hintText: tr('key_your_Name'),
                    controller: controller.nameController,
                    contentPaddingLeft: screenWidth(10),
                    validator: (value) {
                      return value!.isEmpty
                          ? tr('key_Please_Check_Name')
                          : null;
                    },
                  ),
                  screenWidth(20).ph,
                  CustomTextField(
                    textDirection: TextDirection.ltr,
                    prefixIcon: storage.getAppLanguage() == "en"
                        ? Padding(
                            padding: EdgeInsetsDirectional.symmetric(
                                vertical: screenWidth(40)),
                            child: Text(
                              "+971",
                              style: TextStyle(fontSize: screenWidth(20)),
                            ),
                          )
                        : null,
                    suffixIcon: storage.getAppLanguage() == "ar"
                        ? Padding(
                            padding: EdgeInsetsDirectional.symmetric(
                                vertical: screenWidth(40)),
                            child: Text(
                              "971+",
                              style: TextStyle(fontSize: screenWidth(20)),
                            ),
                          )
                        : null,
                    hintText: tr('key_Mobile_Number'),
                    typeInput: TextInputType.phone,
                    controller: controller.mobileNumberController,
                    contentPaddingRight: screenWidth(10),
                    contentPaddingLeft: screenWidth(15),
                    validator: (value) {
                      return value!.isEmpty ? tr('key_Check_Number') : null;
                    },
                  ),
                  screenWidth(20).ph,
                  CustomTextField(
                    hintText: tr('key_Email') + "  example@gmail.com",
                    controller: controller.emailController,
                    contentPaddingLeft: screenWidth(10),
                    validator: (value) {
                      return value!.isEmpty || !GetUtils.isEmail(value.trim())
                          ? tr('key_Please_Check_Email')
                          : null;
                    },
                  ),
                  screenWidth(20).ph,
                  CustomTextField(
                    hintText: tr('invitationCode'),
                    controller: controller.invitationCode,
                    contentPaddingLeft: screenWidth(10),
                  ),
                  Obx(
                    () {
                      return controller.isGameLoading
                          ? const SpinKitCircle(
                              color: AppColors.mainYellowColor,
                            )
                          : controller.allGameModel.value.data!.isEmpty
                              ? const SizedBox.shrink()
                              : controller.allGameModel.value.data![0].id ==
                                      null
                                  ? customFaildConnection(
                                      onPressed: () {
                                        controller.getAllGame();
                                      },
                                    )
                                  : GestureDetector(
                                      onTap: () {
                                        Get.bottomSheet(
                                          CustomScrollView(
                                            controller: _scrollController,
                                            slivers: [
                                              SliverToBoxAdapter(child: Obx(() {
                                                return Column(
                                                  children: controller
                                                      .allGameModel.value.data!
                                                      .map((e) {
                                                    return Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 10),
                                                      child: Row(
                                                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: [
                                                          // CustomNetworkImage(
                                                          //   context.image(
                                                          //       e.bannerImage!),
                                                          //   // key:
                                                          //   //     ValueKey<int>(
                                                          //   //         e.id!),
                                                          //   width: 30,
                                                          //   height: 30,
                                                          //   radius: 0,
                                                          //   memCacheWidth: 100,
                                                          //   memCacheHeight: 80,
                                                          // ),
                                                          CachedNetworkImage(
                                                            imageUrl: Uri.https(
                                                                    NetworkUtil
                                                                        .baseUrl,
                                                                    e.bannerImage!)
                                                                .toString(),
                                                            placeholder: (context,
                                                                    url) =>
                                                                Image.asset(
                                                                    'images/cllllb.png'),
                                                            errorWidget: (context,
                                                                    url,
                                                                    error) =>
                                                                Image.asset(
                                                                    'images/cllllb.png'),
                                                            width: 30,
                                                            height: 30,
                                                            memCacheWidth: 100,
                                                            memCacheHeight: 80,
                                                          ),
                                                          screenWidth(20).pw,
                                                          Expanded(
                                                            child: Text(
                                                              e.name!,
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                            ),
                                                          ),
                                                          Checkbox(
                                                            focusColor: AppColors
                                                                .mainYellowColor,
                                                            value: controller
                                                                    .isChecked[
                                                                controller
                                                                    .allGameModel
                                                                    .value
                                                                    .data!
                                                                    .indexOf(
                                                                        e)],
                                                            onChanged: (value) {
                                                              controller
                                                                      .isChecked[
                                                                  controller
                                                                      .allGameModel
                                                                      .value
                                                                      .data!
                                                                      .indexOf(
                                                                          e)] = value!;
                                                              controller
                                                                  .addIdGame(
                                                                      id: e
                                                                          .id!);
                                                            },
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  }).toList(),
                                                );
                                              })),
                                              SliverToBoxAdapter(
                                                child: Obx(
                                                  () {
                                                    return controller
                                                            .isMoreGamesLoading
                                                        ? const SpinKitCircle(
                                                            color: AppColors
                                                                .mainYellowColor,
                                                          )
                                                        : const SizedBox
                                                            .shrink();
                                                  },
                                                ),
                                              ),
                                            ],
                                          ),
                                          backgroundColor:
                                              AppColors.mainWhiteColor,
                                        );
                                      },
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Container(
                                            width: 150,
                                            height: 30,
                                            decoration: const BoxDecoration(
                                              border: Border(
                                                  bottom: BorderSide(
                                                      color: AppColors
                                                          .mainYellowColor)),
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                CustomText(
                                                  text: tr('key_SELECT_GAME'),
                                                  textType:
                                                      TextStyleType.CUSTOM,
                                                  textColor:
                                                      AppColors.mainBlackColor,
                                                ),
                                                const Icon(Icons.arrow_downward)
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                      //  :DropdownButton(
                      //     isExpanded: true,
                      //     hint: CustomText(
                      //       text: tr('key_Select_Game'),
                      //       textType: TextStyleType.CUSTOM,
                      //       textColor: AppColors.mainBlackColor,
                      //     ),
                      //     items: controller.allGameModel.map(
                      //       (e) {
                      //         return DropdownMenuItem(
                      //           value: e.id,
                      //           child: Obx(
                      //             () {
                      //               return Row(
                      //                 // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //                 children: [
                      //                   CustomNetworkImage(
                      //                     context.image(e.bannerImage!),
                      //                     width: 30,
                      //                     height: 30,
                      //                   ),
                      //                   // CachedNetworkImage(
                      //                   //   imageUrl: Uri.https(
                      //                   //           NetworkUtil.baseUrl,
                      //                   //           e.bannerImage!)
                      //                   //       .toString(),
                      //                   //   placeholder: (context, url) =>
                      //                   //       Lottie.asset(
                      //                   //           "images/download.json"),
                      //                   //   errorWidget:
                      //                   //       (context, url, error) =>
                      //                   //           const Icon(Icons.error),
                      //                   //   width: screenWidth(10),
                      //                   // ),
                      //                   screenWidth(20).pw,
                      //                   Expanded(
                      //                     child: Text(
                      //                       e.name!,
                      //                       overflow: TextOverflow.ellipsis,
                      //                     ),
                      //                   ),
                      //                   Checkbox(
                      //                     focusColor:
                      //                         AppColors.mainYellowColor,
                      //                     value: controller.isChecked[
                      //                         controller.allGameModel
                      //                             .indexOf(e)],
                      //                     onChanged: (value) {
                      //                       controller.isChecked[controller
                      //                           .allGameModel
                      //                           .indexOf(e)] = value!;
                      //                       controller.addIdGame(id: e.id!);
                      //                     },
                      //                   ),
                      //                 ],
                      //               );
                      //             },
                      //           ),
                      //         );
                      //       },
                      //     ).toList(),
                      //     onChanged: (_) {},
                      //   );
                    },
                  ),
                  screenWidth(10).ph,
                  CustomButton(
                    text: tr('key_continue'),
                    onPressed: () {
                      controller.otp();
                      // Get.to(MainView());
                    },
                    circularBorder: screenWidth(10),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
