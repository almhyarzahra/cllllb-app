import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/data/models/apis/news_model.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_book_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';
import 'news_detalis_controller.dart';

class NewsDetalisView extends StatefulWidget {
  const NewsDetalisView(
      {super.key, required this.currentlocation, required this.news});
  final LocationData? currentlocation;
  final NewsModel news;

  @override
  State<NewsDetalisView> createState() => _NewsDetalisViewState();
}

class _NewsDetalisViewState extends State<NewsDetalisView> {
  late NewsDetalisController controller;
  @override
  void initState() {
    controller = Get.put(NewsDetalisController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
         bottomNavigationBar:const CustomBottomNavigation(),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          children: [
            CustomBookBar(
              imageUrl: widget.news.club!.mainImage!,
              imageName:
                  Uri.https(NetworkUtil.baseUrl, widget.news.club!.mainImage!)
                      .toString(),
              text:
                  "${widget.news.club!.name!}/ ${widget.news.club!.city!}/ ${widget.news.clubTypeName!}",
            ),
            screenWidth(20).ph,
            CustomText(
              textType: TextStyleType.CUSTOM,
              text: widget.news.title!,
              textColor: AppColors.mainYellowColor,
              fontWeight: FontWeight.bold,
            ),
            screenWidth(30).ph,
            CustomNetworkImage(
              context.image(widget.news.image!),
              width: double.infinity,
              height: screenHeight(4.2),
              radius: screenWidth(20),
              margin:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
            ),
            screenWidth(20).ph,
            Padding(
              padding:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
              child: CustomText(
                textAlign: TextAlign.start,
                textType: TextStyleType.CUSTOM,
                text: widget.news.content!,
                textColor: AppColors.mainGreyColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
