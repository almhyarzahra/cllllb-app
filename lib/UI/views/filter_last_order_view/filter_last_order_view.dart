import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/data/models/apis/user_products_model.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';
import '../last_order_view/last_order_view.dart';
import 'filter_last_order_controller.dart';
import 'package:intl/intl.dart';

class FilterLastOrderView extends StatefulWidget {
  const FilterLastOrderView({super.key, required this.productsUser, required this.currentlocation});
  final List<UserProductsModel> productsUser;
  final LocationData? currentlocation;

  @override
  State<FilterLastOrderView> createState() => _FilterLastOrderViewState();
}

class _FilterLastOrderViewState extends State<FilterLastOrderView> {
  late FilterLastOrderController controller;

  @override
  void initState() {
    controller = Get.put(FilterLastOrderController(productsUser: widget.productsUser));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsetsDirectional.symmetric(
                  horizontal: screenWidth(75), vertical: screenWidth(25)),
              child: Card(
                elevation: 4,
                child: Column(
                  children: [
                    Obx(
                      () {
                        return Padding(
                          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(10)),
                          child: DropdownButton(
                            hint: Padding(
                              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(8)),
                              child: CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_Filter_By_Clubs'),
                                textColor: AppColors.mainYellowColor,
                              ),
                            ),
                            items: controller.clubs.map(
                              (element) {
                                return DropdownMenuItem(
                                  value: element,
                                  child: Obx(
                                    () {
                                      return Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: element,
                                            textColor: AppColors.mainBlackColor,
                                          ),
                                          Checkbox(
                                            value: controller.isCheckedClub[
                                                controller.clubs.toList().indexOf(element)],
                                            onChanged: (value) {
                                              controller.isCheckedClub[controller.clubs
                                                  .toList()
                                                  .indexOf(element)] = value!;
                                              controller.addClubToFilter(nameClub: element);
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                );
                              },
                            ).toList(),
                            onChanged: (value) {},
                          ),
                        );
                      },
                    ),
                    screenWidth(30).ph,
                    Obx(
                      () {
                        return Padding(
                          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(10)),
                          child: DropdownButton(
                            hint: Padding(
                              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(5)),
                              child: CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_Filter_By_Type'),
                                textColor: AppColors.mainYellowColor,
                              ),
                            ),
                            items: controller.type.map(
                              (element) {
                                return DropdownMenuItem(
                                  value: element,
                                  child: Obx(
                                    () {
                                      return Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: element,
                                            textColor: AppColors.mainBlackColor,
                                          ),
                                          Checkbox(
                                            value: controller.isCheckedType[
                                                controller.type.toList().indexOf(element)],
                                            onChanged: (value) {
                                              controller.isCheckedType[controller.type
                                                  .toList()
                                                  .indexOf(element)] = value!;
                                              controller.addTypeToFilter(type: element);
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                );
                              },
                            ).toList(),
                            onChanged: (value) {},
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
            screenWidth(30).ph,
            Padding(
              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
              child: Card(
                elevation: 4,
                child: Column(
                  children: [
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_Filter_By_Date'),
                      textColor: AppColors.mainYellowColor,
                      fontSize: screenWidth(22),
                      fontWeight: FontWeight.bold,
                    ),
                    screenWidth(30).ph,
                    Padding(
                      padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                      child: CustomTextField(
                        contentPaddingLeft: screenWidth(10),
                        onTap: () async {
                          controller.pickedFirstDate = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(2015),
                            lastDate: DateTime(2100),
                          );
                          if (controller.pickedFirstDate != null) {
                            controller.firstDateController.text =
                                DateFormat("MM/dd/yyyy").format(controller.pickedFirstDate!);
                          }
                        },
                        hintText: tr('key_First_Date'),
                        controller: controller.firstDateController,
                      ),
                    ),
                    screenWidth(30).ph,
                    Padding(
                      padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                      child: CustomTextField(
                        contentPaddingLeft: screenWidth(10),
                        onTap: () async {
                          controller.pickedLastDate = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(2015),
                            lastDate: DateTime(2100),
                          );
                          if (controller.pickedLastDate != null) {
                            controller.lastDateController.text =
                                DateFormat("MM/dd/yyyy").format(controller.pickedLastDate!);
                          }
                        },
                        hintText: tr('key_Last_Date'),
                        controller: controller.lastDateController,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('key_All_Date'),
                          textColor: AppColors.mainBlackColor,
                        ),
                        Obx(
                          () {
                            return Checkbox(
                              value: controller.isCheckedAllDate.value,
                              onChanged: (value) {
                                controller.isCheckedAllDate.value = value!;
                              },
                            );
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            screenWidth(20).ph,
            Padding(
              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
              child: CustomButton(
                circularBorder: screenWidth(20),
                text: tr('key_Finish'),
                onPressed: () async {
                  await controller.filter();

                  Get.off(
                    LastOrderView(
                        currentlocation: widget.currentlocation,
                        productsUser: controller.productsUserChanged),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
