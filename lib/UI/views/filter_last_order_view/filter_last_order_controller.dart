import 'package:com.tad.cllllb/UI/views/main_view/home_view/home_controller.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../core/data/models/apis/user_products_model.dart';

class FilterLastOrderController extends BaseController {
  FilterLastOrderController({required List<UserProductsModel> productsUser}) {
    this.productsUser.value = productsUser;
    //this.productsUserChanged.value = productsUser;
  }
  TextEditingController firstDateController = TextEditingController();
  TextEditingController lastDateController = TextEditingController();

  RxList<UserProductsModel> productsUser = <UserProductsModel>[].obs;
  RxList<UserProductsModel> productsUserChanged = <UserProductsModel>[].obs;
  RxList<UserProductsModel> test = <UserProductsModel>[].obs;
  RxList<UserProductsModel> test1 = <UserProductsModel>[].obs;
  RxList<UserProductsModel> test2 = <UserProductsModel>[].obs;

  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxSet<String> clubs = <String>{}.obs;
  RxList<bool> isCheckedClub = <bool>[].obs;
  RxList<String> finalNameClub = <String>["All Clubs"].obs;

  RxSet<String> type = <String>{}.obs;
  RxList<bool> isCheckedType = <bool>[].obs;
  RxList<String> finalType = <String>["All Type"].obs;

  RxBool isCheckedAllDate = true.obs;
  DateTime? pickedFirstDate;
  DateTime? pickedLastDate;

  @override
  void onClose() {
    Get.find<HomeController>().getProductsUser();
    super.onClose();
  }

  @override
  void onInit() {
    getClubs();
    productsUserChanged = productsUser;
    super.onInit();
  }

  void getClubs() {
    productsUser.map((element) {
      if (element.club != null) {
        clubs.add(element.club!.name!);
      }
      element.category!.map((e) {
        type.add(e.name!);
      }).toSet();
    }).toSet();

    type.map((element) {
      isCheckedType.add(false);
    }).toSet();
    type.add("All Type");
    isCheckedType.add(true);

    clubs.map((element) {
      isCheckedClub.add(false);
    }).toSet();
    clubs.add("All Clubs");

    isCheckedClub.add(true);
  }

  void addClubToFilter({required String nameClub}) {
    if (finalNameClub.contains(nameClub)) {
      finalNameClub.remove(nameClub);
    } else {
      finalNameClub.add(nameClub);
    }
    //print(finalNameClub);
  }

  void addTypeToFilter({required String type}) {
    if (finalType.contains(type)) {
      finalType.remove(type);
    } else {
      finalType.add(type);
    }
    //print(finalType);
  }

  Future<void> filter() async {
    if (!finalNameClub.contains("All Clubs")) {
      finalNameClub.map((e) {
        productsUserChanged.map((element) {
          if (element.club != null && element.club!.name == e) {
            test2.add(element);
          }
        }).toSet();
      }).toSet();
      productsUserChanged = test2;
    }
    if (!finalType.contains("All Type")) {
      finalType.map((el) {
        productsUserChanged.map((element) {
          element.category!.map((e) {
            if (e.name == el) test.add(element);
          }).toSet();
        }).toSet();
      }).toSet();
      productsUserChanged = test;
    }
    if (isCheckedAllDate.value == false &&
        firstDateController.text.isNotEmpty &&
        lastDateController.text.isNotEmpty) {
      productsUserChanged.map((element) {
        if (DateFormat("MM/dd/yyyy")
                .parse(element.date!)
                .isAfter(DateFormat("MM/dd/yyyy").parse(firstDateController.text)) &&
            DateFormat("MM/dd/yyyy")
                .parse(element.date!)
                .isBefore(DateFormat("MM/dd/yyyy").parse(lastDateController.text))) {
          test1.add(element);
        }
      }).toSet();
      productsUserChanged = test1;
    }
  }
}
