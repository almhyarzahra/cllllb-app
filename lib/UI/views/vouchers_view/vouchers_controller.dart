import 'dart:developer';

import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/filter_vouchers_view/filter_vouchers_view.dart';
import 'package:com.tad.cllllb/UI/views/main_view/profile_view/profile_controller.dart';
import 'package:com.tad.cllllb/core/data/models/apis/vouchers_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class VouchersController extends BaseController {
  VouchersController({
    this.currentlocation,
  }) {
    //
  }
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  RxList<VouchersModel> vouchers = <VouchersModel>[VouchersModel()].obs;
  bool get isVouchersLoading => listType.contains(OperationType.vouchers);
  bool get isMoreVouchersLoading =>
      listType.contains(OperationType.moreVouchers);
  RxInt pageKey = 2.obs;
  RxBool getMore = true.obs;
  RxBool enableScrollListner = true.obs;

  LocationData? currentlocation;

  TextEditingController pointController = TextEditingController();

  RxString startdate = "Start Date".obs;
  RxString enddate = "End Date".obs;
  RxBool myVouchers = true.obs;

  DateTime? pickedStartDate;
  DateTime? pickedEndDate;

  @override
  void onInit() {
    super.onInit();
    getVouchers();
  }

  void getVouchers() {
    runLoadingFutureFunction(
      type: OperationType.vouchers,
      function: UserRepository().getVouchers(page: 1).then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_check_connection'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (vouchers.isNotEmpty) {
                vouchers.clear();
              }
              vouchers.addAll(r);
              vouchers.map((element) {
                element.message = "l";
              }).toSet();
            },
          );
        },
      ),
    );
  }

  Future<void> getMoreVouchers({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
      type: OperationType.moreVouchers,
      function: UserRepository().getVouchers(page: page).then(
        (value) {
          value.fold((l) {
            pageKey.value = pageKey.value - 1;
          }, (r) {
            if (r.isEmpty) {
              enableScrollListner.value = false;
            } else {
              vouchers.addAll(r);
            }
          });
        },
      ),
    ).then((value) => getMore.value = true);
  }

  void buyingVoucher(
      {required bool allow, required int couponId, required double points}) {
    if (!allow) {
      CustomToast.showMessage(
          message: tr('dontHavePoint'), messageType: MessageType.INFO);
    } else {
      Get.defaultDialog(
        title: tr('key_Are_You_Sure'),
        titleStyle: const TextStyle(color: AppColors.mainYellowColor),
        content: Column(
          children: [
            CustomButton(
              text: tr('key_Yes'),
              onPressed: () {
                buying(couponId: couponId, points: points);
              },
              widthButton: 4,
              circularBorder: screenWidth(10),
            ),
            screenWidth(20).ph,
            CustomButton(
              text: tr('key_No'),
              onPressed: () {
                Get.back();
              },
              widthButton: 4,
              circularBorder: screenWidth(10),
            ),
          ],
        ),
      );
    }
  }

  void buying({required int couponId, required double points}) {
    runFullLoadingFutureFunction(
      function: UserRepository().buyVoucher(couponId: couponId).then(
        (value) {
          value.fold(
            (l) {
              if (l == 422) {
                CustomToast.showMessage(
                    message: tr('voucherNotAvailable'),
                    messageType: MessageType.INFO);
              } else {
                CustomToast.showMessage(
                    message: tr('key_try_again'),
                    messageType: MessageType.REJECTED);
              }
            },
            (r) {
              onInit();
              Get.back();
              Get.back();
              Get.find<ProfileController>().getUserInfo();
              storage.setUserPoint(storage.getUserPoint() - points);
              CustomToast.showMessage(
                  message: tr('key_success'),
                  messageType: MessageType.SUCCSESS);
            },
          );
        },
      ),
    );
  }

  void filter() {
    runFullLoadingFutureFunction(
      function: UserRepository()
          .getVouchers(
        page: 1,
        startDate: startdate.value.isEmpty ? null : startdate.value,
        endDate: enddate.value.isEmpty ? null : enddate.value,
        price: pointController.text.trim().isEmpty
            ? null
            : pointController.text.trim(),
      )
          .then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_try_again'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (r.isEmpty) {
                CustomToast.showMessage(
                    message: tr('noMatchingResults'),
                    messageType: MessageType.INFO);
              } else {
                Get.to(() => FilterVouchersView(vouchersModel: r));
              }
            },
          );
        },
      ),
    );
  }
}
