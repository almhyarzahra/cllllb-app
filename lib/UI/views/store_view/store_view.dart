import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/sport_items_view/sport_items_view.dart';
import 'package:com.tad.cllllb/UI/views/store_view/store_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_book_bar.dart';
import '../../shared/custom_widgets/custom_card_store.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';
import '../memberships_view/memberships_view.dart';

class StoreView extends StatefulWidget {
  const StoreView(
      {super.key,
      required this.nameClub,
      required this.address,
      required this.imageClub,
      required this.clubId,
      required this.clubType,
      required this.currentlocation});
  final String nameClub;
  final String address;
  final String imageClub;
  final int clubId;
  final String clubType;
  final LocationData? currentlocation;

  @override
  State<StoreView> createState() => _StoreViewState();
}

class _StoreViewState extends State<StoreView> {
  late StoreController controller;

  @override
  void initState() {
    controller = Get.put(StoreController(
      clubId: widget.clubId,
      currentlocation: widget.currentlocation,
      address: widget.address,
      clubType: widget.clubType,
      imageClub: widget.imageClub,
      nameClub: widget.nameClub,
    ));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          children: [
            screenHeight(80).ph,
            CustomBookBar(
              imageUrl: widget.imageClub,
              imageName: Uri.https(NetworkUtil.baseUrl, widget.imageClub).toString(),
              text: "${widget.nameClub}/ ${widget.address}/ ${widget.clubType}",
            ),
            screenHeight(40).ph,
            Obx(() {
              return controller.isStoreLoading
                  ? const SpinKitCircle(
                      color: AppColors.mainYellowColor,
                    )
                  : controller.store.isEmpty
                      ? CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('key_no_element_yet'),
                          textColor: AppColors.mainBlackColor,
                        )
                      : controller.store[0].message == null
                          ? customFaildConnection(onPressed: () {
                              controller.onInit();
                            })
                          : GridView.count(
                              childAspectRatio: 0.7,
                              crossAxisCount: 2,
                              crossAxisSpacing: 0,
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              children: controller.store.map((element) {
                                return CustomCardStore(
                                  imageName: element.image!,
                                  text: element.code == 1
                                      ? tr('key_SPORT_ITEMS')
                                      : element.code == 2
                                          ? tr('key_FOOD&DRINKS')
                                          : element.code == 3
                                              ? tr('key_SUBSCRIPTIONS')
                                              : element.code == 4
                                                  ? tr('key_news')
                                                  : tr('key_jobs'),
                                  onPressed: () {
                                    if (element.code == 1) {
                                      Get.to(
                                        SportItemsView(
                                          currentlocation: widget.currentlocation,
                                          clubType: widget.clubType,
                                          nameClub: widget.nameClub,
                                          address: widget.address,
                                          imageClub: widget.imageClub,
                                          clubId: widget.clubId,
                                        ),
                                      );
                                    } else if (element.code == 2) {
                                      controller.checkMenu();
                                    } else if (element.code == 3) {
                                      Get.to(
                                        MemberShipsView(
                                          currentlocation: widget.currentlocation,
                                          clubType: widget.clubType,
                                          nameClub: widget.nameClub,
                                          address: widget.address,
                                          imageClub: widget.imageClub,
                                          clubId: widget.clubId,
                                        ),
                                      );
                                    } else if (element.code == 4) {
                                      controller.getClubNews();
                                    } else if (element.code == 5) {
                                      controller.getClubJobs();
                                    }
                                  },
                                );
                              }).toList(),
                            );
            }),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceAround,
            //   children: [
            //     CustomCardStore(
            //       onPressed: () {
            //         Get.to(
            //           SportItemsView(
            //             currentlocation: widget.currentlocation,
            //             clubType: widget.clubType,
            //             nameClub: widget.nameClub,
            //             address: widget.address,
            //             imageClub: widget.imageClub,
            //             clubId: widget.clubId,
            //           ),
            //         );
            //       },
            //       imageName: "ic_sport",
            //       text: tr('key_SPORT_ITEMS'),
            //     ),
            //     CustomCardStore(
            //       onPressed: () {
            //         controller.checkMenu();
            //       },
            //       imageName: "ic_food",
            //       text: tr('key_FOOD&DRINKS'),
            //     ),
            //   ],
            // ),
            // screenHeight(40).ph,
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceAround,
            //   children: [
            //     CustomCardStore(
            //       onPressed: () {
            //         Get.to(
            //           MemberShipsView(
            //             currentlocation: widget.currentlocation,
            //             clubType: widget.clubType,
            //             nameClub: widget.nameClub,
            //             address: widget.address,
            //             imageClub: widget.imageClub,
            //             clubId: widget.clubId,
            //           ),
            //         );
            //       },
            //       imageName: "ic_memberships",
            //       text: tr('key_SUBSCRIPTIONS'),
            //     ),
            //     CustomCardStore(
            //       onPressed: () {
            //         controller.getClubNews();
            //       },
            //       imageName: "news",
            //       text: tr('key_news'),
            //     ),
            //   ],
            // ),
            // screenHeight(40).ph,
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceAround,
            //   children: [
            //     CustomCardStore(
            //       onPressed: () {
            //         controller.getClubJobs();
            //       },
            //       //distans: 20,
            //       imageName: "jobs",
            //       text: tr('key_jobs'),
            //     ),
            //   ],
            // ),
            // screenHeight(40).ph,
          ],
        ),
      ),
    );
  }
}
