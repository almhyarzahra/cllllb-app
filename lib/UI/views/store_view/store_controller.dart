import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/UI/views/product_menu_view/product_menu_view.dart';
import 'package:com.tad.cllllb/core/data/repositories/club_repositories.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/data/models/apis/menu_model.dart';
import '../../../core/data/models/apis/store_model.dart';
import '../../../core/enums/request_status.dart';
import '../../../core/services/base_controller.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/utils.dart';
import '../all_jobs_view/all_jobs_view.dart';
import '../all_news_view/all_news_view.dart';

class StoreController extends BaseController {
  StoreController(
      {required int clubId,
      this.currentlocation,
      required String address,
      clubType,
      imageClub,
      nameClub}) {
    this.clubId.value = clubId;
    this.address.value = address;
    this.clubType.value = clubType;
    this.imageClub.value = imageClub;
    this.nameClub.value = nameClub;
  }
  RxString address = "".obs;
  RxString clubType = "".obs;
  RxString imageClub = "".obs;
  RxString nameClub = "".obs;
  LocationData? currentlocation;
  RxInt clubId = 0.obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<MenuModel> menus = <MenuModel>[].obs;
  RxList<StoreModel> store = <StoreModel>[StoreModel()].obs;

  bool get isStoreLoading => requestStatus.value == RequestStatus.LOADING;

  @override
  void onInit() {
    getStore();
    super.onInit();
  }

  void getStore() {
    runLoadingFutureFunction(
        function: ClubRepositories().getClubStore(clubId: clubId.value).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_check_connection'), messageType: MessageType.REJECTED);
      }, (r) {
        if (store.isNotEmpty) {
          store.clear();
        }
        store.addAll(r);
        store.map((element) {
          element.message = "l";
        }).toSet();
      });
    }));
  }

  void getClubNews() {
    runFullLoadingFutureFunction(
      function: ClubRepositories().getNewsClub(clubId: clubId.value).then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_try_again'), messageType: MessageType.REJECTED);
            },
            (r) {
              if (r.isEmpty) {
                CustomToast.showMessage(message: tr('key_no_ads'), messageType: MessageType.INFO);
              } else {
                Get.to(
                  AllNewsView(
                    currentlocation: currentlocation,
                    news: r,
                  ),
                );
              }
            },
          );
        },
      ),
    );
  }

  void getClubJobs() {
    runFullLoadingFutureFunction(
        function: ClubRepositories().getJobsClub(clubId: clubId.value).then((value) {
      value.fold((l) {
        CustomToast.showMessage(message: tr('key_try_again'), messageType: MessageType.REJECTED);
      }, (r) {
        if (r.isEmpty) {
          CustomToast.showMessage(message: tr('key_no_jobs'), messageType: MessageType.INFO);
        } else {
          Get.to(AllJobsView(
            currentlocation: currentlocation,
            jobs: r,
          ));
        }
      });
    }));
  }

  void checkMenu() {
    runFullLoadingFutureFunction(
        function: ClubRepositories().checkMenu(clubId: clubId.value).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_check_connection'), messageType: MessageType.REJECTED);
      }, (r) {
        if (r.code == 0) {
          CustomToast.showMessage(message: tr('key_not_available'), messageType: MessageType.INFO);
        } else if (r.code == 1) {
          getMenu().then((value) => Get.to(ProductMenuView(
                address: address.value,
                clubType: clubType.value,
                imageClub: imageClub.value,
                nameClub: nameClub.value,
                currentlocation: currentlocation,
                menuId: menus[0].id!,
              )));
        } else if (r.code == 2) {
          getMenu().then((value) {
            Get.bottomSheet(
              Container(
                height: screenHeight(1.5),
                decoration: BoxDecoration(
                  color: AppColors.mainWhiteColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(screenWidth(15)),
                    topRight: Radius.circular(screenWidth(15)),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsetsDirectional.symmetric(
                      vertical: screenWidth(20), horizontal: screenWidth(20)),
                  child: ListView(
                    children: [
                      CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: tr('key_menus'),
                        textColor: AppColors.mainYellowColor,
                        fontSize: screenWidth(20),
                        fontWeight: FontWeight.bold,
                        textAlign: TextAlign.start,
                      ),
                      Column(
                        children: menus.map((element) {
                          return InkWell(
                            onTap: () {
                              Get.to(ProductMenuView(
                                address: address.value,
                                clubType: clubType.value,
                                imageClub: imageClub.value,
                                nameClub: nameClub.value,
                                currentlocation: currentlocation,
                                menuId: element.id!,
                              ));
                            },
                            child: Card(
                              elevation: 4,
                              color: AppColors.mainGrey2Color.withOpacity(0.5),
                              shadowColor: AppColors.mainYellowColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(screenWidth(20)),
                              ),
                              child: Padding(
                                padding: EdgeInsetsDirectional.symmetric(
                                    vertical: screenWidth(20), horizontal: screenWidth(20)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Flexible(
                                      child: CustomText(
                                        textType: TextStyleType.CUSTOM,
                                        text: element.name!,
                                        textColor: AppColors.mainBlackColor,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                   const Icon(
                                      Icons.food_bank_outlined,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }).toList(),
                      )
                    ].animate(interval: 100.ms).scale(delay: 100.ms),
                  ),
                ),
              ),
            );
          });
        }
      });
    }));
  }

  Future<void> getMenu() async {
    await runFullLoadingFutureFunction(
        function: ClubRepositories().getMenus(clubId: clubId.value).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_check_connection'), messageType: MessageType.REJECTED);
      }, (r) async {
        if (menus.isNotEmpty) {
          menus.clear();
        }
        menus.addAll(r);
      });
    }));
  }
}
