import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_app_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_drawer.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/cllllb_offers_view/cllllb_offers_view.dart';
import 'package:com.tad.cllllb/UI/views/donate_points_view/donate_points_view.dart';
import 'package:com.tad.cllllb/UI/views/lottery_result_view/lottery_result_view.dart';
import 'package:com.tad.cllllb/UI/views/lottery_view/lottery_view.dart';
import 'package:com.tad.cllllb/UI/views/my_voucher_view/my_vouchers_view.dart';
import 'package:com.tad.cllllb/UI/views/replace_points_view/replace_points_view.dart';
import 'package:com.tad.cllllb/UI/views/rewards_view/rewards_controller.dart';
import 'package:com.tad.cllllb/UI/views/rewards_view/widgets/rewards_card.dart';
import 'package:com.tad.cllllb/UI/views/vouchers_view/vouchers_view.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class RewardsView extends StatefulWidget {
  final LocationData? currentlocation;
  final double userPoints;
  const RewardsView(
      {super.key, required this.currentlocation, required this.userPoints});

  @override
  State<RewardsView> createState() => _RewardsViewState();
}

class _RewardsViewState extends State<RewardsView> {
  late RewardsController controller;

  @override
  void initState() {
    super.initState();
    controller = Get.put(RewardsController());
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: controller.key,
       // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
                CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: tr('howUsePoints'),
                  textColor: AppColors.mainBlackColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                ),
              ],
            ),
            RewardsCard(
              onTap: () {
                Get.to(
                  () => VouchersView(currentlocation: widget.currentlocation),
                );
              },
              icon: "images/vouchers.svg",
              title: tr('vouchers'),
            ),
            RewardsCard(
              onTap: () {
                Get.to(
                  () => MyVouchersView(currentlocation: widget.currentlocation),
                );
              },
              icon: "images/vouchers.svg",
              title: tr('myVouchers'),
            ),
            RewardsCard(
              onTap: () {
                Get.to(
                  () => ReplacePointsView(
                      currentlocation: widget.currentlocation),
                );
              },
              icon: "images/replace_points.svg",
              title: tr('replacePoints'),
            ),
            RewardsCard(
              onTap: () {
                Get.to(
                  () => LotteryView(currentlocation: widget.currentlocation),
                );
              },
              icon: "images/lottery.svg",
              title: tr('lottery'),
            ),
            RewardsCard(
              onTap: () {
                Get.to(
                  LotteryResultView(currentlocation: widget.currentlocation),
                );
              },
              icon: "images/winner.svg",
              title: tr('lotteryResults'),
            ),
            RewardsCard(
              onTap: () {
                Get.to(
                  () =>
                      CllllbOffersView(currentlocation: widget.currentlocation),
                );
              },
              icon: "images/club_offers.svg",
              title: tr('cllllbOffers'),
            ),
            RewardsCard(
              onTap: () {
                Get.to(
                  () => DonatePointsView(
                    currentlocation: widget.currentlocation,
                    userPoints: widget.userPoints,
                  ),
                );
              },
              widthIcon: 30,
              icon: "images/gift.svg",
              title: tr('donatePoints'),
            ),
          ].animate(interval: 50.ms).scale(delay: 100.ms),
        ),
      ),
    );
  }
}
