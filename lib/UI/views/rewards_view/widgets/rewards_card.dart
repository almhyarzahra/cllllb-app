import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RewardsCard extends StatelessWidget {
  final String icon;
  final String title;
  final void Function() onTap;
  final double? widthIcon;
  const RewardsCard(
      {super.key,
      required this.icon,
      required this.title,
      required this.onTap,
      this.widthIcon});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        height: 77,
        margin: const EdgeInsets.only(bottom: 5),
        padding: const EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          color: AppColors.mainGrey2Color.withOpacity(0.5),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          children: [
            Container(
              width: 60,
              height: 60,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: AppColors.mainYellowColor,
                borderRadius: BorderRadius.circular(10),
              ),
              child: SvgPicture.asset(
                icon,
                color: AppColors.mainWhiteColor,
                width: widthIcon,
              ),
            ),
            const SizedBox(width: 20),
            CustomText(
              textType: TextStyleType.CUSTOM,
              text: title,
              textColor: AppColors.mainBlackColor,
              fontSize: 20,
            )
          ],
        ),
      ),
    );
  }
}
