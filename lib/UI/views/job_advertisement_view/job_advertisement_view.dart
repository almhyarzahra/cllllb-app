import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_button.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/data/models/apis/user_info_model.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/custom_widgets/custom_text_field.dart';
import '../../shared/utils.dart';
import '../photo_view/photo_view.dart';
import 'job_advertisement_controller.dart';

class JobAdvertisementView extends StatefulWidget {
  const JobAdvertisementView({super.key, required this.currentlocation, required this.userInfo});
  final LocationData? currentlocation;
  final UserInfoModel userInfo;

  @override
  State<JobAdvertisementView> createState() => _JobAdvertisementViewState();
}

class _JobAdvertisementViewState extends State<JobAdvertisementView> {
  late JobAdvertisementController controller;
  @override
  void initState() {
    controller = Get.put(JobAdvertisementController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        bottomNavigationBar:const CustomBottomNavigation(),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: Obx(
          () {
            return controller.isUserInfoLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : controller.viewCV.value
                    ? Column(
                        children: [
                          SizedBox(
                            height: screenHeight(1.5),
                            child: PDFView(
                              filePath: controller.result!.paths[0]!,
                              enableSwipe: true,
                              //swipeHorizontal: true,
                              autoSpacing: false,
                              pageFling: false,
                            ),
                          ),
                          screenWidth(30).ph,
                          CustomButton(
                            text: tr('key_ok'),
                            onPressed: () {
                              controller.viewCV.value = false;
                            },
                            widthButton: 2,
                            heightButton: 10,
                            circularBorder: screenWidth(20),
                          ),
                          screenWidth(100).ph,
                          CustomButton(
                            text: tr('key_exit'),
                            onPressed: () {
                              controller.viewCV.value = false;
                              controller.visible.value = false;
                              controller.result = null;
                            },
                            widthButton: 2,
                            heightButton: 10,
                            circularBorder: screenWidth(20),
                          ),
                        ],
                      )
                    : Form(
                        key: controller.formKey,
                        child: ListView(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                IconButton(
                                  onPressed: () {
                                    Get.back();
                                  },
                                  icon: Icon(
                                    Icons.arrow_back_ios_new,
                                    size: screenWidth(20),
                                  ),
                                ),
                                CustomTextButton(
                                  onPressed: () {
                                    Get.defaultDialog(
                                      title: tr('key_Are_You_Sure'),
                                      titleStyle: const TextStyle(color: AppColors.mainYellowColor),
                                      content: Column(
                                        children: [
                                          CustomButton(
                                            text: tr('key_Yes'),
                                            onPressed: () {
                                              controller.deleteLookingJob();
                                            },
                                            widthButton: 4,
                                            circularBorder: screenWidth(10),
                                          ),
                                          screenWidth(20).ph,
                                          CustomButton(
                                            text: tr('key_No'),
                                            onPressed: () {
                                              Get.back();
                                            },
                                            widthButton: 4,
                                            circularBorder: screenWidth(10),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                  text: tr('key_delete_ad'),
                                  colorText: AppColors.mainRedColor,
                                  decoration: TextDecoration.underline,
                                  decorationThickness: 2,
                                ),
                              ],
                            ),
                            InkWell(
                              onTap: () {
                                Get.to(
                                  PhotoWidgetView(
                                    imageUrl: [widget.userInfo.userInfo!.image ?? ""],
                                  ),
                                );
                              },
                              child: Container(
                                width: screenWidth(5),
                                height: screenHeight(8),
                                decoration: BoxDecoration(
                                  border: Border.all(color: AppColors.mainYellowColor),
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    fit: BoxFit.contain,
                                    image: CachedNetworkImageProvider(Uri.https(NetworkUtil.baseUrl,
                                            widget.userInfo.userInfo!.image ?? "")
                                        .toString()),
                                  ),
                                ),
                              ),
                            ),
                            screenWidth(20).ph,
                            Padding(
                              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(15)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  CustomText(
                                    textType: TextStyleType.CUSTOM,
                                    text: widget.userInfo.userInfo!.name!,
                                    textColor: AppColors.mainBlackColor,
                                  ),
                                  screenWidth(20).ph,
                                  Row(
                                    children: [
                                      CustomText(
                                        textType: TextStyleType.CUSTOM,
                                        text: tr('key_number'),
                                        textColor: AppColors.mainYellowColor,
                                      ),
                                      Flexible(
                                        child: CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text: widget.userInfo.userInfo!.phone ??
                                              tr('key_not_found'),
                                          textColor: AppColors.mainGreyColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                  screenWidth(20).ph,
                                  Row(
                                    children: [
                                      CustomText(
                                        textType: TextStyleType.CUSTOM,
                                        text: tr('key_mail'),
                                        textColor: AppColors.mainYellowColor,
                                      ),
                                      Flexible(
                                        child: CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: widget.userInfo.userInfo!.email!,
                                            textColor: AppColors.mainGreyColor,
                                            overflow: TextOverflow.ellipsis),
                                      ),
                                    ],
                                  ),
                                  screenWidth(25).ph,
                                  CustomTextField(
                                    hintText: tr('key_job_title'),
                                    controller: controller.jobController,
                                    contentPaddingLeft: screenWidth(10),
                                    validator: (value) {
                                      return value!.isEmpty ? tr('key_field_required') : null;
                                    },
                                  ),
                                  screenWidth(25).ph,
                                  CustomTextField(
                                    hintText: tr('key_years_experience'),
                                    controller: controller.yearsController,
                                    contentPaddingLeft: screenWidth(10),
                                    validator: (value) {
                                      return value!.isEmpty ? tr('key_field_required') : null;
                                    },
                                  ),
                                  screenWidth(25).ph,
                                  CustomText(
                                    textType: TextStyleType.CUSTOM,
                                    text: tr('key_skills'),
                                    textColor: AppColors.mainYellowColor,
                                  ),
                                  CustomTextField(
                                    hintText: "",
                                    controller: controller.skillsController,
                                    maxLines: 5,
                                    maxLength: 150,
                                    contentPaddingLeft: screenWidth(20),
                                    contentPaddingTop: screenWidth(20),
                                    contentPaddingBottom: screenWidth(20),
                                    contentPaddingRight: screenWidth(20),
                                  ),
                                  screenWidth(25).ph,
                                  CustomText(
                                    textType: TextStyleType.CUSTOM,
                                    text: tr('key_educations'),
                                    textColor: AppColors.mainYellowColor,
                                  ),
                                  CustomTextField(
                                    hintText: "",
                                    controller: controller.educationsController,
                                    maxLines: 5,
                                    maxLength: 150,
                                    contentPaddingLeft: screenWidth(20),
                                    contentPaddingTop: screenWidth(20),
                                    contentPaddingBottom: screenWidth(20),
                                    contentPaddingRight: screenWidth(20),
                                  ),
                                  screenWidth(25).ph,
                                  Center(
                                    child: CustomButton(
                                      text: tr('key_upload_cv'),
                                      onPressed: () {
                                        controller.selectFile();
                                      },
                                      widthButton: 2.2,
                                      heightButton: 9,
                                      circularBorder: screenWidth(20),
                                    ),
                                  ),
                                  screenWidth(25).ph,
                                  Center(
                                    child: CustomButton(
                                      text: tr('key_apply_now'),
                                      onPressed: controller.visible.value
                                          ? () {
                                              controller.editLookingJob.value
                                                  ? controller.editingLookingJob()
                                                  : controller.addLookingJob();
                                            }
                                          : null,
                                      widthButton: 3,
                                      heightButton: 9,
                                      circularBorder: screenWidth(20),
                                    ),
                                  ),
                                  screenWidth(25).ph,
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
          },
        ),
      ),
    );
  }
}
