import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/data/repositories/club_repositories.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/enums/request_status.dart';

class JobAdvertisementController extends BaseController {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  TextEditingController jobController = TextEditingController();
  TextEditingController skillsController = TextEditingController();
  TextEditingController educationsController = TextEditingController();
  TextEditingController yearsController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  RxInt jobId = 0.obs;
  FilePickerResult? result ;
  RxBool visible = false.obs;
  RxBool viewCV = false.obs;
  RxBool editLookingJob = false.obs;

  bool get isUserInfoLoading => requestStatus.value == RequestStatus.LOADING;
  @override
  void onInit() {
    getUserLookingJob();
    super.onInit();
  }

  void selectFile() async {
    result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf'],
    );
    if (result != null) {
      visible.value = true;
      viewCV.value = true;
    }
  }

  void getUserLookingJob() {
    runLoadingFutureFunction(
        function: ClubRepositories().getUserLookingJob(userId: storage.getUserId()).then((value) {
      value.fold((l) {
        if (l == null) {
          getUserLookingJob();
        }
        editLookingJob.value = false;
      }, (r) {
        editLookingJob.value = true;
        jobId.value = r.id!;
        jobController.text = r.jobTitle!;
        skillsController.text = r.skills ?? "";
        yearsController.text = r.yearExperince!;
        educationsController.text = r.education ?? "";
      });
    }));
  }

  void editingLookingJob() {
    if (formKey.currentState!.validate()) {
      runFullLoadingFutureFunction(
          function: ClubRepositories()
              .editingLookingJob(
                  jobTitle: jobController.text.trim().toString(),
                  jobId: jobId.value,
                  yearsExperince: yearsController.text.trim().toString(),
                  cv: result!.paths[0]!,
                  education: educationsController.text.isEmpty
                      ? null
                      : educationsController.text.trim().toString(),
                  skills: skillsController.text.isEmpty
                      ? null
                      : skillsController.text.trim().toString())
              .then((value) {
        value.fold((l) {
          editingLookingJob();
        }, (r) {
          CustomToast.showMessage(message: tr('key_success'), messageType: MessageType.SUCCSESS);
          Get.back();
        });
      }));
    }
  }

  void addLookingJob() {
    if (formKey.currentState!.validate()) {
      runFullLoadingFutureFunction(
          function: ClubRepositories()
              .addLookingJob(
                  jobTitle: jobController.text.trim().toString(),
                  userId: storage.getUserId(),
                  yearsExperince: yearsController.text.trim().toString(),
                  cv: result!.paths[0]!,
                  education: educationsController.text.isEmpty
                      ? null
                      : educationsController.text.trim().toString(),
                  skills: skillsController.text.isEmpty
                      ? null
                      : skillsController.text.trim().toString())
              .then((value) {
        value.fold((l) {
          addLookingJob();
        }, (r) {
          CustomToast.showMessage(message: tr('key_success'), messageType: MessageType.SUCCSESS);
          Get.back();
        });
      }));
    }
  }

  void deleteLookingJob() {
    runFullLoadingFutureFunction(
        function: ClubRepositories().deleteLookingJob(user_id: storage.getUserId()).then((value) {
      value.fold((l) {
        if (l == null) {
          CustomToast.showMessage(message: tr('key_try_again'), messageType: MessageType.REJECTED);
          Get.back();
        } else {
          CustomToast.showMessage(message: tr('key_dont_ad'), messageType: MessageType.INFO);
          Get.back();
        }
      }, (r) {
        CustomToast.showMessage(message: tr('key_success'), messageType: MessageType.SUCCSESS);
        Get.back();
        Get.back();
      });
    }));
  }
}
