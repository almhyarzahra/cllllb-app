import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:photo_view/photo_view.dart';

import '../../../core/utils/network_util.dart';

class PhotoWidgetView extends StatefulWidget {
  const PhotoWidgetView({super.key, required this.imageUrl});
  final List<String> imageUrl;

  @override
  State<PhotoWidgetView> createState() => _PhotoWidgetViewState();
}

class _PhotoWidgetViewState extends State<PhotoWidgetView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.mainWhiteColor,
        body: PageView.builder(
          itemCount: widget.imageUrl.length,
          itemBuilder: (context, index) {
            return PhotoView(
              backgroundDecoration:
                  const BoxDecoration(color: AppColors.mainWhiteColor),
              imageProvider: NetworkImage(
                  Uri.https(NetworkUtil.baseUrl, widget.imageUrl[index])
                      .toString()),
              loadingBuilder: (context, event) => Center(
                child: Lottie.asset("images/download.json"),
              ),
            );
          },
        ),
        // Center(
        //     child: PhotoView(
        //   backgroundDecoration: BoxDecoration(color: AppColors.mainWhiteColor),
        //   imageProvider: NetworkImage(
        //       Uri.https(NetworkUtil.baseUrl, widget.imageUrl).toString()),
        //   loadingBuilder: (context, event) => Center(
        //     child: Lottie.asset("images/download.json"),
        //   ),
        // )),
      ),
    );
  }
}
