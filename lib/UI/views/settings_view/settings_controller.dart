import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingsController extends BaseController {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxBool enableNotification = storage.getEnableNotifications().obs;

  void switchNotification(bool value) async {
    if (value) {
      storage.setEnableNotifications(true);
    } else {
      storage.setEnableNotifications(false);
    }
  }
}
