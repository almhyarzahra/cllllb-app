import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/views/settings_view/settings_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../app/my_app.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/utils.dart';

class SettingsView extends StatefulWidget {
  const SettingsView({super.key, required this.currentlocation});
  final LocationData? currentlocation;

  @override
  State<SettingsView> createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView> {
  SettingsController controller = SettingsController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
              currentlocation: widget.currentlocation,
              drwerOnPressed: () {
                controller.key.currentState!.openDrawer();
              }),
        ),
        body: Column(
          children: [
            CustomBar(
              text: tr('key_Settings'),
            ),
            Padding(
              padding: EdgeInsetsDirectional.symmetric(
                  vertical: screenWidth(10), horizontal: screenWidth(10)),
              child: Column(
                children: [
                  Row(
                    children: [
                      InkWell(
                        onTap: () {
                          Get.defaultDialog(
                            title: tr("Change_Language"),
                            content: Column(
                              children: [
                                TextButton(
                                    onPressed: () {
                                      storage.setAppLanguage('en');
                                      Get.updateLocale(getLocal());
                                      Get.back();
                                    },
                                    child: const Text('English')),
                                TextButton(
                                    onPressed: () {
                                      storage.setAppLanguage('ar');
                                      Get.updateLocale(getLocal());
                                      Get.back();
                                    },
                                    child: const Text('العربية')),
                              ],
                            ),
                          );
                        },
                        child: SvgPicture.asset("images/languge.svg"),
                      ),
                      screenWidth(20).pw,
                      CustomText(
                        textType: TextStyleType.SUBTITLE,
                        text: storage.getAppLanguage() == 'ar'
                            ? "اللغة     : العربية"
                            : "Language     : English",
                        textColor: AppColors.mainBlackColor,
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      SvgPicture.asset("images/notification.svg"),
                      screenWidth(15).pw,
                      CustomText(
                        textType: TextStyleType.SUBTITLE,
                        text: tr('key_Notification :'),
                        textColor: AppColors.mainBlackColor,
                      ),
                      screenWidth(20).pw,
                      Obx(
                        () {
                          return Switch(
                            activeColor: AppColors.mainYellowColor,
                            value: controller.enableNotification.value,
                            onChanged: (value) {
                              //  print(value);
                              controller.enableNotification.value =
                                  !controller.enableNotification.value;
                              controller.switchNotification(value);
                            },
                          );
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
