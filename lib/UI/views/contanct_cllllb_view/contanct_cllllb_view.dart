import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/UI/views/contanct_cllllb_view/contanct_cllllb_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';

class ContanctCllllbView extends StatefulWidget {
  const ContanctCllllbView({super.key, required this.currentlocation});
  final LocationData? currentlocation;

  @override
  State<ContanctCllllbView> createState() => _ContanctCllllbViewState();
}

class _ContanctCllllbViewState extends State<ContanctCllllbView> {
  ContanctCllllbController controller = ContanctCllllbController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: Form(
          key: controller.formKey,
          child: ListView(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomBar(
                text: tr('key_Contanct_Cllllb'),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: Column(
                  children: [
                    CustomText(
                      textType: TextStyleType.TITLE,
                      text: tr('communicateApplicationAd'),
                      textColor: AppColors.mainBlackColor,
                      textAlign: TextAlign.start,
                    ),
                    screenWidth(20).ph,
                    CustomTextField(
                      hintText: tr("key_Email*"),
                      controller: controller.emailController,
                      contentPaddingLeft: screenWidth(10),
                      validator: (value) {
                        return value!.isEmpty || !GetUtils.isEmail(value.trim())
                            ? tr('key_Please_Check_Email')
                            : null;
                      },
                    ),
                    screenWidth(20).ph,
                    CustomTextField(
                      hintText: tr('key_Mobile_Number'),
                      contentPaddingLeft: screenWidth(15),
                      typeInput: TextInputType.phone,
                      controller: controller.mobileNumberController,
                    ),
                    screenWidth(20).ph,
                    CustomTextField(
                      hintText: tr('title'),
                      controller: controller.titleController,
                      contentPaddingLeft: screenWidth(15),
                      validator: (value) {
                        return value!.isEmpty ? tr('key_field_required') : null;
                      },
                    ),
                    screenWidth(20).ph,
                    CustomTextField(
                      hintText: tr('description'),
                      maxLines: 6,
                      controller: controller.descriptionController,
                      contentPaddingLeft: screenWidth(15),
                      contentPaddingTop: screenWidth(10),
                      validator: (value) {
                        return value!.isEmpty ? tr('key_field_required') : null;
                      },
                    ),
                    screenWidth(20).ph,
                    InkWell(
                      onTap: () {
                        controller.selectImageFromGallery();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              SvgPicture.asset("images/attach.svg"),
                              const SizedBox(width: 10),
                              CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('attachPicture'),
                                textColor: AppColors.mainYellowColor,
                              ),
                            ],
                          ),
                          Obx(() {
                            return controller.enableImage.value
                                ? Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: AppColors.mainColorGreen,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: const Text("1"),
                                  )
                                : const SizedBox.shrink();
                          })
                        ],
                      ),
                    ),
                    screenWidth(20).ph,
                    CustomButton(
                      text: tr('key_Send'),
                      circularBorder: screenWidth(10),
                      onPressed: () {
                        controller.sendToAdminstritor();
                      },
                    ),
                  ],
                ),
              )
              // Padding(
              //   padding: EdgeInsetsDirectional.only(top: screenWidth(20), start: screenWidth(20)),
              //   child: CustomText(
              //     textType: TextStyleType.CUSTOM,
              //     fontSize: screenWidth(18),
              //     textColor: AppColors.mainBlackColor,
              //     text: tr('key_World_Club_Portal_L.L.C'),
              //   ),
              // ),
              // Image.asset(
              //   "images/map.png",
              //   fit: BoxFit.fill,
              // ),
              // Padding(
              //   padding: EdgeInsetsDirectional.symmetric(
              //     horizontal: screenWidth(10),
              //     vertical: screenWidth(10),
              //   ),
              //   child: Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       CustomText(
              //         textType: TextStyleType.CUSTOM,
              //         text: tr('key_phone_04 123 4567'),
              //         textColor: AppColors.mainBlackColor,
              //       ),
              //       CustomText(
              //         textType: TextStyleType.CUSTOM,
              //         text: tr('key_info@cllllb.com'),
              //         textColor: AppColors.mainBlackColor,
              //       ),
              //       CustomText(
              //         textType: TextStyleType.CUSTOM,
              //         text: tr('key_whatsapp'),
              //         textColor: AppColors.mainBlackColor,
              //       ),
              //     ],
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
