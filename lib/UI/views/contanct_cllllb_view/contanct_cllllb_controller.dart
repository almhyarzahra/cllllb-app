import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class ContanctCllllbController extends BaseController {
  TextEditingController emailController =
      TextEditingController(text: storage.getEmail() ?? "");
  TextEditingController mobileNumberController =
      TextEditingController(text: storage.getmobileNumber() ?? "");
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final ImagePicker picker = ImagePicker();
  XFile? image;
  RxBool enableImage=false.obs;

  void selectImageFromGallery() async {
    image = await picker.pickImage(source: ImageSource.gallery);
    if(image!=null){
      enableImage.value=true;
    }
    else{
      enableImage.value=false;
    }
  }

  void sendToAdminstritor() {
    if (formKey.currentState!.validate()) {
      runFullLoadingFutureFunction(
        function: UserRepository()
            .addPostToSupport(
          title: titleController.text.trim(),
          description: descriptionController.text.trim(),
          email: emailController.text.trim(),
          image: image == null ? null : image!.path,
          phone: mobileNumberController.text.trim().isEmpty
              ? null
              : mobileNumberController.text.trim(),
        )
            .then(
          (value) {
            value.fold(
              (l) {
                if (l == null) {
                  CustomToast.showMessage(
                    message: tr('key_No_Internet_Connection'),
                    messageType: MessageType.REJECTED,
                  );
                } else {
                  CustomToast.showMessage(
                    message: tr('tryAgainAfterMinute'),
                    messageType: MessageType.INFO,
                  );
                }
              },
              (r) {
                Get.back();
                CustomToast.showMessage(
                  message: tr('key_success'),
                  messageType: MessageType.SUCCSESS,
                );
              },
            );
          },
        ),
      );
    }
  }
}
