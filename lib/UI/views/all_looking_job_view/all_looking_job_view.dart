import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/data/models/apis/looking_job_model.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/custom_widgets/custom_text_field.dart';
import '../../shared/utils.dart';
import '../user_job_details_view/user_job_details_view.dart';
import 'all_looking_job_controller.dart';

class AllLookingJobView extends StatefulWidget {
  const AllLookingJobView({super.key, required this.currentlocation, required this.lookingJob});
  final LocationData? currentlocation;
  final List<LookingJobModel> lookingJob;

  @override
  State<AllLookingJobView> createState() => _AllLookingJobViewState();
}

class _AllLookingJobViewState extends State<AllLookingJobView> {
  late AllLookingJobController controller;
  @override
  void initState() {
    controller = Get.put(AllLookingJobController(lookingJob: widget.lookingJob));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
         bottomNavigationBar:const CustomBottomNavigation(),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
              child: CustomTextField(
                onChanged: (value) {
                  controller.goal.value = value;
                },
                hintText: tr('key_Search'),
                contentPaddingLeft: screenWidth(10),
                controller: controller.searchController,
                suffixIcon: Container(
                  height: screenHeight(25),
                  width: screenWidth(10),
                  decoration: BoxDecoration(
                      color: AppColors.mainYellowColor,
                      borderRadius: BorderRadius.circular(screenWidth(10))),
                  child: const Icon(
                    Icons.search,
                    color: AppColors.mainWhiteColor,
                  ),
                ),
              ),
            ),
            Obx(
              () {
                return Column(
                  children: controller.search().map(
                    (e) {
                      return Card(
                        elevation: 4,
                        child: Padding(
                          padding: EdgeInsetsDirectional.symmetric(vertical: screenWidth(20)),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    Container(
                                      width: screenWidth(7),
                                      height: screenHeight(10),
                                      decoration: BoxDecoration(
                                        border: Border.all(color: AppColors.mainYellowColor),
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                          fit: BoxFit.contain,
                                          image: CachedNetworkImageProvider(
                                              Uri.https(NetworkUtil.baseUrl, e.user!.image ?? "")
                                                  .toString()),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: e.user!.name!,
                                      fontSize: screenWidth(30),
                                      overflow: TextOverflow.ellipsis,
                                      textColor: AppColors.mainBlackColor,
                                    ),
                                    Row(
                                      children: [
                                        CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text: tr('key_job_title'),
                                          textColor: AppColors.mainYellowColor,
                                          fontSize: screenWidth(30),
                                        ),
                                        Flexible(
                                          child: CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: e.jobTitle!,
                                            textColor: AppColors.mainBlackColor,
                                            overflow: TextOverflow.ellipsis,
                                            fontSize: screenWidth(30),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text: "${e.yearExperince!} ",
                                          textColor: AppColors.mainYellowColor,
                                          fontSize: screenWidth(30),
                                        ),
                                        Flexible(
                                          child: CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: tr('key_years_experience'),
                                            textColor: AppColors.mainBlackColor,
                                            overflow: TextOverflow.ellipsis,
                                            fontSize: screenWidth(30),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text: tr('key_number'),
                                          textColor: AppColors.mainYellowColor,
                                          fontSize: screenWidth(35),
                                        ),
                                        Flexible(
                                          child: CustomText(
                                              textType: TextStyleType.CUSTOM,
                                              text: e.user!.phone ?? tr('key_not_found'),
                                              textColor: AppColors.mainGreyColor,
                                              fontSize: screenWidth(35),
                                              overflow: TextOverflow.ellipsis),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text: tr('key_mail'),
                                          textColor: AppColors.mainYellowColor,
                                          fontSize: screenWidth(35),
                                        ),
                                        Flexible(
                                          child: CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: e.user!.email!,
                                            textColor: AppColors.mainGreyColor,
                                            fontSize: screenWidth(35),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    Obx(
                                      () {
                                        return e.id != controller.lectureId.value
                                            ? CustomButton(
                                                text: tr('key_download_cv'),
                                                onPressed: () {
                                                  controller.lectureId.value = e.id!;
                                                  controller.downloadCV(
                                                    url: Uri.https(NetworkUtil.baseUrl, e.cv!)
                                                        .toString(),
                                                    name: e.user!.name!,
                                                  );
                                                },
                                                heightButton: 15,
                                                textSize: screenWidth(50),
                                                circularBorder: screenWidth(20),
                                              )
                                            : Column(
                                                children: [
                                                  const CircularProgressIndicator(),
                                                  CustomText(
                                                    textColor: AppColors.mainBlackColor,
                                                    fontSize: screenWidth(35),
                                                    textType: TextStyleType.CUSTOM,
                                                    text: controller.progress.value.toString(),
                                                  ),
                                                ],
                                              );
                                      },
                                    ),
                                    CustomButton(
                                      text: tr('key_details'),
                                      onPressed: () {
                                        Get.to(
                                          UserJobDetailsView(
                                            currentlocation: widget.currentlocation,
                                            lookingJob: e,
                                          ),
                                        );
                                      },
                                      heightButton: 15,
                                      widthButton: 6,
                                      textSize: screenWidth(50),
                                      circularBorder: screenWidth(20),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ).toList(),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
