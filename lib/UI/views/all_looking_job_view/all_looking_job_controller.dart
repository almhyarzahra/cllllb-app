import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_file_downloader/flutter_file_downloader.dart';
import '../../../core/data/models/apis/looking_job_model.dart';
import '../../../core/enums/message_type.dart';
import '../../shared/custom_widgets/custom_toast.dart';

class AllLookingJobController extends BaseController {
  AllLookingJobController({required List<LookingJobModel> lookingJob}) {
    this.lookingJob.value = lookingJob;
  }
  RxList<LookingJobModel> lookingJob = <LookingJobModel>[].obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  Rx<String> goal = "".obs;
  TextEditingController searchController = TextEditingController();
  RxDouble progress = 0.0.obs;
  RxInt lectureId = (-1).obs;
  List<LookingJobModel> search() {
    RxList<LookingJobModel> results = <LookingJobModel>[].obs;
    if (goal.isEmpty) {
      results.value = lookingJob;
    } else {
      results.value = lookingJob
          .where(
              (p0) => p0.jobTitle!.toLowerCase().contains(goal.toLowerCase()))
          .toList();
    }
    return results;
  }

  void downloadCV({required String url, required String name}) {
    FileDownloader.downloadFile(
        url: url,
        name: name,
        notificationType: NotificationType.all,
        onProgress: (String? fileName, double progress) {
          this.progress.value = progress;
        },
        onDownloadCompleted: (String path) {
          CustomToast.showMessage(
              message: tr('key_succsess_download'),
              messageType: MessageType.SUCCSESS);
          lectureId.value = -1;
        },
        onDownloadError: (String error) {
          CustomToast.showMessage(
              message: "Error $error" , messageType: MessageType.REJECTED);
          lectureId.value = -1;
        });
  }
}
