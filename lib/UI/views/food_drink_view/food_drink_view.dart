import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/food_drink_view/food_drink_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import '../../../core/translation/app_translation.dart';
import 'package:location/location.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_book_bar.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/custom_widgets/custom_card_sport.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/custom_widgets/custom_text_button.dart';
import '../../shared/custom_widgets/custom_text_field.dart';
import '../food_sport_detalis_view/food-sport_detalis_view.dart';
import '../sport_items_view/sport_items_widgets/sport_items_shimmer.dart';

class FoodDrinkView extends StatefulWidget {
  const FoodDrinkView(
      {super.key,
      required this.nameClub,
      required this.address,
      required this.imageClub,
      required this.clubId,
      required this.categoryId,
      required this.clubType,
      required this.currentlocation});
  final String? nameClub;
  final String? address;
  final String? imageClub;
  final int clubId;
  final int categoryId;
  final String? clubType;
  final LocationData? currentlocation;

  @override
  State<FoodDrinkView> createState() => _FoodDrinkViewState();
}

class _FoodDrinkViewState extends State<FoodDrinkView> {
  late FoodDrinkController controller;

  ScrollController scrollController = ScrollController();
  @override
  void initState() {
    controller = Get.put(FoodDrinkController(
        clubId: widget.clubId,
        categoryId: widget.categoryId,
        currentlocation: widget.currentlocation));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        bottomNavigationBar: BottomAppBar(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              CustomTextButton(
                text: tr('key_previous'),
                colorText: AppColors.mainRedColor,
                fontWeight: FontWeight.bold,
                onPressed: () {
                  if (controller.displayIndex.value - 12 >= 0) {
                    controller.displayIndex.value -= 12;
                    controller.resultsPage.value = controller.resultSearch
                        .sublist(controller.displayIndex.value, controller.displayIndex.value + 12);
                  }
                },
              ),
              CustomTextButton(
                text: tr('key_next'),
                colorText: AppColors.mainColorGreen,
                fontWeight: FontWeight.bold,
                fontSize: screenWidth(20),
                onPressed: () {
                  //print('Reached the end of ListView!');
                  if (controller.displayIndex.value + 24 < controller.resultSearch.length) {
                    controller.displayIndex.value += 12;
                    //print(controller.displayIndex.value);
                    controller.resultsPage.value = controller.resultSearch
                        .sublist(controller.displayIndex.value, controller.displayIndex.value + 12);
                    // _scrollController.animateTo(
                    //     _scrollController.position.minScrollExtent,
                    //     duration: Duration(milliseconds: 500),
                    //     curve: Curves.easeInOut);
                  } else {
                    controller.resultsPage.value = controller.resultSearch
                        .sublist(controller.displayIndex.value, controller.resultSearch.length);
                    // _scrollController.animateTo(
                    //     _scrollController.position.minScrollExtent,
                    //     duration: Duration(milliseconds: 10000),
                    //     curve: Curves.easeInOut);
                  }
                },
              ),
            ],
          ),
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            controller: scrollController,
            children: [
              screenHeight(80).ph,
              widget.nameClub == null
                  ? Row(
                      children: [
                        IconButton(
                            onPressed: () {
                              Get.back();
                            },
                            icon: Icon(
                              Icons.arrow_back_ios_new,
                              size: screenWidth(20),
                            )),
                        screenWidth(20).pw,
                        storage.getAppLanguage() == 'ar'
                            ? const CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: "منتجات Cllllb على الانترنت",
                                textColor: AppColors.mainBlackColor,
                              )
                            : Image.asset("images/cllllb_text.png"),
                      ],
                    )
                  : CustomBookBar(
                      imageUrl: widget.imageClub!,
                      imageName: Uri.https(NetworkUtil.baseUrl, widget.imageClub!).toString(),
                      text: "${widget.nameClub!}/ ${widget.address!}/ ${widget.clubType!}",
                    ),
              screenHeight(50).ph,
              InkWell(
                onTap: () {
                  Get.bottomSheet(
                    Container(
                      decoration: BoxDecoration(
                        color: AppColors.mainWhiteColor,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(screenWidth(8)),
                          topRight: Radius.circular(screenWidth(8)),
                        ),
                      ),
                      child: Padding(
                        padding: EdgeInsetsDirectional.only(top: screenHeight(30)),
                        child: Padding(
                          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                          child: ListView(
                            children: [
                              CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_filter_price'),
                                textColor: AppColors.mainYellowColor,
                                textAlign: TextAlign.start,
                                fontWeight: FontWeight.bold,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: CustomTextField(
                                      hintText: tr('key_lowest_price'),
                                      controller: controller.lowestPriceController,
                                      contentPaddingLeft: screenWidth(10),
                                      typeInput: TextInputType.number,
                                    ),
                                  ),
                                  screenWidth(50).pw,
                                  Expanded(
                                    flex: 1,
                                    child: CustomTextField(
                                      hintText: tr('key_highest_price'),
                                      controller: controller.highestPriceController,
                                      contentPaddingLeft: screenWidth(10),
                                      typeInput: TextInputType.number,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CustomText(
                                    textType: TextStyleType.CUSTOM,
                                    text: tr('key_all_price'),
                                    textColor: AppColors.mainBlackColor,
                                  ),
                                  Obx(
                                    () {
                                      return Checkbox(
                                        value: controller.isCheckedAllPrice.value,
                                        onChanged: (value) {
                                          controller.isCheckedAllPrice.value = value!;
                                        },
                                      );
                                    },
                                  ),
                                ],
                              ),
                              screenWidth(12).ph,
                              Obx(
                                () {
                                  return DropdownButton(
                                      hint: Padding(
                                        padding: EdgeInsetsDirectional.symmetric(
                                            horizontal: screenWidth(5)),
                                        child: CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text: tr('key_filter_delivery'),
                                          textColor: AppColors.mainYellowColor,
                                        ),
                                      ),
                                      items: controller.delivary.map(
                                        (element) {
                                          return DropdownMenuItem(
                                            value: element,
                                            child: Obx(
                                              () {
                                                return Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    CustomText(
                                                      textType: TextStyleType.CUSTOM,
                                                      text: element,
                                                      textColor: AppColors.mainBlackColor,
                                                    ),
                                                    Checkbox(
                                                      value: controller.isCheckedDelivary[controller
                                                          .delivary
                                                          .toList()
                                                          .indexOf(element)],
                                                      onChanged: (value) {
                                                        controller.isCheckedDelivary[controller
                                                            .delivary
                                                            .toList()
                                                            .indexOf(element)] = value!;
                                                        controller.addDelivaryToFilter(
                                                            delivary: element);
                                                      },
                                                    ),
                                                  ],
                                                );
                                              },
                                            ),
                                          );
                                        },
                                      ).toList(),
                                      onChanged: (value) {});
                                },
                              ),
                              screenWidth(20).ph,
                              Padding(
                                padding:
                                    EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                                child: CustomButton(
                                  circularBorder: screenWidth(20),
                                  text: tr('key_Finish'),
                                  onPressed: () async {
                                    controller.filter();
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset("images/filter.svg"),
                    screenWidth(30).pw,
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_Filter'),
                      textColor: AppColors.mainYellowColor,
                    ),
                  ],
                ),
              ),
              screenHeight(50).ph,
              Padding(
                padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                child: CustomTextField(
                  onChanged: (value) {
                    controller.goal.value = value;
                    controller.search();
                  },
                  hintText: tr('key_Search'),
                  contentPaddingLeft: screenWidth(10),
                  controller: controller.searchController,
                  suffixIcon: Container(
                    height: screenHeight(25),
                    width: screenWidth(10),
                    decoration: BoxDecoration(
                        color: AppColors.mainYellowColor,
                        borderRadius: BorderRadius.circular(screenWidth(10))),
                    child: const Icon(
                      Icons.search,
                      color: AppColors.mainWhiteColor,
                    ),
                  ),
                ),
              ),
              screenHeight(50).ph,
              Obx(
                () {
                  return controller.isFoodDrinkLoading
                      ? const SportItemsShimmer()
                      : controller.productsFD.isEmpty
                          ? Center(
                              child: CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_No_Products_Yet'),
                                textColor: AppColors.mainBlackColor,
                              ),
                            )
                          : controller.productsFD[0].message == null
                              ? customFaildConnection(
                                  onPressed: () {
                                    controller.onInit();
                                  },
                                )
                              : GridView.count(
                                  childAspectRatio: 0.6,
                                  crossAxisCount: 2,
                                  crossAxisSpacing: 0,
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  children: controller.resultsPage.map(
                                    (element) {
                                      return CustomCardStore(
                                        rewardPoint: element.pointsValue,
                                        onPressedImage: () {
                                          Get.to(
                                            FoodSportDetalisView(
                                              currentlocation: widget.currentlocation,
                                              productsModel: element,
                                            ),
                                          );
                                        },
                                        onPressedLike: () {
                                          controller.addTofavorite(productsModel: element);
                                        },
                                        onPressedCart: () {
                                          controller.addToCart(productsModel: element);
                                        },
                                        imageUrl:
                                            Uri.https(NetworkUtil.baseUrl, element.img!).toString(),
                                        textBunch: element.club?.name != null
                                            ? tr('Club :') + element.club!.name!
                                            : "",
                                        textItem: element.name!,
                                        textLength: element.productCategory!.isEmpty
                                            ? ""
                                            : element.productCategory![0].categoryInfo!.name!,
                                        textPrice: element.price.toString() + tr('key_AED'),
                                      );
                                    },
                                  ).toList(),
                                );
                },
              ),

              //screenWidth(3).ph,
              // Align(
              //   alignment: Alignment.topLeft,
              //   child: Padding(
              //     padding: const EdgeInsets.all(8.0),
              //     child: CustomText(
              //       fontSize: screenWidth(20),
              //       textType: TextStyleType.CUSTOM,
              //       text: "SPONSOR",
              //       textColor: AppColors.mainYellowColor,
              //     ),
              //   ),
              // ),
              //CustomSpon(),
              //CustomContact()
            ],
          ),
        ),
      ),
    );
  }
}
