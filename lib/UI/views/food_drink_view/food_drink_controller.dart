import 'dart:developer';

import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/data/models/apis/all_products_model.dart';
import '../../../core/data/models/cart_model.dart';
import '../../../core/data/repositories/club_repositories.dart';
import '../../../core/enums/message_type.dart';
import '../../../core/enums/request_status.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_toast.dart';

class FoodDrinkController extends BaseController {
  FoodDrinkController({required int clubId, required int categoryId, this.currentlocation}) {
    this.clubId.value = clubId;
    this.categoryId.value = categoryId;
  }
  List<CartModel> get favoriteList => favoriteService.favoriteList;
  LocationData? currentlocation;
  RxInt clubId = 0.obs;
  RxInt categoryId = 0.obs;
  RxBool isCheckedAllPrice = true.obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  TextEditingController searchController = TextEditingController();
  TextEditingController lowestPriceController = TextEditingController();
  TextEditingController highestPriceController = TextEditingController();

  RxList<AllProductsModel> productsFD = <AllProductsModel>[AllProductsModel()].obs;
  RxList<AllProductsModel> test = <AllProductsModel>[].obs;
  RxList<AllProductsModel> test1 = <AllProductsModel>[].obs;
  RxInt displayIndex = 0.obs;

  Rx<String> goal = "".obs;
  RxList<String> delivary = <String>[
    tr('key_inside_the_club'),
    tr('key_Inside/Outside_the_club'),
  ].obs;
  RxList<bool> isCheckedDelivary = <bool>[false, true].obs;
  RxList<String> finalDelivary = <String>[tr('key_Inside/Outside_the_club')].obs;
  RxList<AllProductsModel> resultSearch = <AllProductsModel>[].obs;
  RxList<AllProductsModel> resultsPage = <AllProductsModel>[].obs;
  bool get isFoodDrinkLoading => requestStatus.value == RequestStatus.LOADING;
  @override
  void onInit() {
    getFoodDrink();
    super.onInit();
  }

  void getFoodDrink() {
    runLoadingFutureFunction(
        function: ClubRepositories()
            .getSpecialOffersApp(clubId: clubId.value, categoryId: categoryId.value)
            .then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_No_Internet_Connection'), messageType: MessageType.REJECTED);
      }, (r) {
        if (productsFD.isNotEmpty) {
          productsFD.clear();
        }
        productsFD.addAll(r);
        productsFD.map((element) {
          element.message = "l";
        }).toSet();
        resultSearch.value = productsFD;
        //print(resultSearch.length);
        if (resultSearch.length <= 12) {
          resultsPage.value = resultSearch;
        } else {
          resultsPage.value = resultSearch.sublist(0, 12);
        }
        storage.setAllProductsList(r);
      });
    }));
  }

  List<AllProductsModel> search() {
    if (goal.isEmpty) {
      resultSearch.value = productsFD;
    } else {
      resultSearch.value =
          productsFD.where((p0) => p0.name!.toLowerCase().contains(goal.toLowerCase())).toList();
    }
    if (resultSearch.length <= 12) {
      resultsPage.value = resultSearch;
    } else {
      resultsPage.value = resultSearch.sublist(0, 12);
    }
    return resultSearch;
  }

  void addTofavorite({required AllProductsModel productsModel}) {
    if (favoriteService.getCartModel(productsModel) == null) {
      favoriteService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Favorite'), messageType: MessageType.SUCCSESS);
          });
    } else {
      CustomToast.showMessage(
          message: productsModel.name! + tr('key_already_in_your_favourites'),
          messageType: MessageType.INFO);
    }
  }

  void addToCart({required AllProductsModel productsModel}) {
    if (productsModel.club == null) {
      cartService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Card'), messageType: MessageType.SUCCSESS);
          });
    } else if (currentlocation == null) {
      CustomToast.showMessage(
          message: tr('key_Please_turn_on_location_feature'), messageType: MessageType.INFO);
    } else if (productsModel.inProduct == 1 &&
        calculateDistance(
                currentlocation!.latitude!,
                currentlocation!.longitude!,
                double.parse(productsModel.club!.latitude!),
                double.parse(productsModel.club!.longtitude!)) >
            100.0) {
      CustomToast.showMessage(message: tr('key_within_100_metres'), messageType: MessageType.INFO);
    } else {
      cartService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Card'), messageType: MessageType.SUCCSESS);
          });
    }
  }

  double calculateDistance(double lat1, double lng1, double lat2, double lng2) {
    double distance = Geolocator.distanceBetween(lat1, lng1, lat2, lng2);
    return distance;
  }

  void addDelivaryToFilter({required String delivary}) {
    if (finalDelivary.contains(delivary)) {
      finalDelivary.remove(delivary);
    } else
      {finalDelivary.add(delivary);}
    log(finalDelivary.toString());
  }

  void filter() {
    RxList<AllProductsModel> productsChangedModel = storage.getAllProductsList().obs;
    if (test.isNotEmpty) test.clear();
    if (test1.isNotEmpty) test1.clear();
    if (finalDelivary.contains(tr('key_inside_the_club'))) {
      productsChangedModel.map((element) {
        if (element.inProduct == 1) test.add(element);
      }).toSet();
      productsChangedModel = test;
    }
    if (isCheckedAllPrice .isFalse &&
        lowestPriceController.text.isNotEmpty &&
        highestPriceController.text.isNotEmpty) {
      productsChangedModel.map((element) {
        if (element.price! >= double.parse(lowestPriceController.text.trim().toString()) &&
            element.price! <= double.parse(highestPriceController.text.trim().toString())) {
          test1.add(element);
        }
      }).toSet();
      productsChangedModel = test1;
    }
    resultsPage.value = productsChangedModel;
    Get.back();
  }
}
