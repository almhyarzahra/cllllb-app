import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/terms_condition_view/terms_condition_controller.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';

class TermsConditionView extends StatefulWidget {
  const TermsConditionView({super.key, required this.currentlocation});
  final LocationData? currentlocation;

  @override
  State<TermsConditionView> createState() => _TermsConditionViewState();
}

class _TermsConditionViewState extends State<TermsConditionView> {
  TermsConditionController controller = TermsConditionController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
          
        ),
        body: ListView(
          children: [
            CustomBar(
              text: tr('key_Terms_Condition'),
            ),
            Padding(
              padding: EdgeInsetsDirectional.symmetric(
                  horizontal: screenWidth(15), vertical: screenWidth(15)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    textAlign: TextAlign.start,
                    text: tr('key_WORLD_CLUB'),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    textAlign: TextAlign.start,
                    text: tr('key_Use_Of_The_Site'),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    textAlign: TextAlign.start,
                    text: tr('key_And_The_WORLD'),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    textAlign: TextAlign.start,
                    text: tr('key_Laws_And_Regulations '),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    textAlign: TextAlign.start,
                    text: tr('key_Right_To_Change'),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    textAlign: TextAlign.start,
                    text: tr('key_Of_The_Site'),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    textAlign: TextAlign.start,
                    text: tr('key_Terms_&_Conditions'),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    textAlign: TextAlign.start,
                    text: tr('key_Your_Use_Of_The Site'),
                  ),
                ],
              ),
            ),
            // CustomContact()
          ],
        ),
      ),
    );
  }
}
