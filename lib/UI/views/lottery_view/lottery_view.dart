import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_app_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_drawer.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/lottery_view/lottery_controller.dart';
import 'package:com.tad.cllllb/UI/views/lottery_view/widgets/lottery_loading.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';

class LotteryView extends StatefulWidget {
  final LocationData? currentlocation;
  const LotteryView({super.key, required this.currentlocation});

  @override
  State<LotteryView> createState() => _LotteryViewState();
}

class _LotteryViewState extends State<LotteryView> {
  late LotteryController controller;
  final ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
    controller = Get.put(LotteryController(currentlocation: widget.currentlocation));
  }

  void _scrollListener() async {
    if (controller.getMore.value) {
      if (controller.enableScrollListner.value &&
          _scrollController.position.maxScrollExtent ==
              _scrollController.offset &&
          !_scrollController.position.outOfRange) {
        controller.getMore.value = false;
        await controller.getMoreLottary(page: controller.pageKey.value);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: controller.key,
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          controller: _scrollController,
          padding: const EdgeInsets.symmetric(horizontal: 15),
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
                CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: tr('lottery'),
                  textColor: AppColors.mainBlackColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                ),
              ],
            ),
            InkWell(
              onTap: () {
                Get.bottomSheet(
                  Container(
                    decoration: BoxDecoration(
                        color: AppColors.mainWhiteColor,
                        borderRadius: BorderRadius.circular(30)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CustomText(
                              textType: TextStyleType.CUSTOM,
                              text: tr('key_Filter_By_Date'),
                              textColor: AppColors.mainYellowColor,
                            ),
                            const SizedBox(width: 30),
                            Obx(
                              () {
                                return InkWell(
                                  onTap: () async {
                                    controller.pickedDate =
                                        await showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime(2015),
                                      lastDate: DateTime(2100),
                                    );
                                    if (controller.pickedDate != null) {
                                      controller.date.value =
                                          DateFormat("yyyy-MM-dd")
                                              .format(controller.pickedDate!);
                                    }
                                  },
                                  child: Container(
                                    width: 100,
                                    height: 40,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: AppColors.mainGrey2Color,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: controller.date.value,
                                      textColor: AppColors.mainBlackColor,
                                    ),
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 15),
                                child: CustomText(
                                  textType: TextStyleType.CUSTOM,
                                  text: tr('filterByPoints'),
                                  textColor: AppColors.mainYellowColor,
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 5),
                                  child: CustomTextField(
                                    hintText: "",
                                    typeInput: TextInputType.number,
                                    controller: controller.pointController,
                                    contentPaddingLeft: screenWidth(10),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 10),
                        CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('lotteryStatus'),
                          textColor: AppColors.mainYellowColor,
                        ),
                        Obx(() {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Row(
                                children: [
                                  CustomText(
                                    textType: TextStyleType.CUSTOM,
                                    text: tr('finished'),
                                    textColor: AppColors.mainBlackColor,
                                  ),
                                  Checkbox(
                                    value: controller.finished.value,
                                    onChanged: ((value) {
                                      controller.finished.value = value!;
                                      controller.notFinished.value = !value;
                                    }),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  CustomText(
                                    textType: TextStyleType.CUSTOM,
                                    text: tr('notFinished'),
                                    textColor: AppColors.mainBlackColor,
                                  ),
                                  Checkbox(
                                    value: controller.notFinished.value,
                                    onChanged: ((value) {
                                      controller.notFinished.value = value!;
                                      controller.finished.value = !value;
                                    }),
                                  )
                                ],
                              ),
                            ],
                          );
                        }),
                        CustomButton(
                          text: tr('confirm'),
                          onPressed: () {
                            controller.filter();
                          },
                          widthButton: 2,
                          circularBorder: screenWidth(10),
                        ),
                      ],
                    ),
                  ),
                );
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset("images/filter.svg"),
                    screenWidth(30).pw,
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_Filter'),
                      textColor: AppColors.mainYellowColor,
                    ),
                  ],
                ),
              ),
            ),
            Obx(
              () {
                return controller.isLotteryLoading
                    ? const LotteryLoading()
                    : controller.lotteries.isEmpty
                        ? CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: tr('noLottery'),
                            textColor: AppColors.mainYellowColor,
                          )
                        : controller.lotteries[0].message == null
                            ? customFaildConnection(onPressed: () {
                                controller.onInit();
                              },padding: 0)
                            : ListView.separated(
                                separatorBuilder: (context, index) =>
                                    const SizedBox(height: 8),
                                itemCount: controller.lotteries.length,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return Container(
                                    width: double.infinity,
                                   // height: 170,
                                    padding: const EdgeInsetsDirectional.only(
                                        end: 10),
                                    decoration: BoxDecoration(
                                      color: AppColors.mainGrey2Color
                                          .withOpacity(0.5),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Row(
                                      children: [
                                        SvgPicture.asset(
                                          "images/winner.svg",
                                          width: 60,
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                controller
                                                    .lotteries[index].title!,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  fontSize: screenWidth(20),
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              Text(
                                                controller.lotteries[index]
                                                    .description!,
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                                style: TextStyle(
                                                  fontSize: screenWidth(30),
                                                ),
                                              ),
                                              Text(
                                                "${tr('startDate')} ${controller.lotteries[index].createdAt}",
                                                style: TextStyle(
                                                    color:
                                                        AppColors.mainGreyColor,
                                                    fontSize: screenWidth(30)),
                                              ),
                                              Text(
                                                "${tr('lotteryDate')} ${DateFormat("dd-MM-yyyy").format(controller.lotteries[index].expiresAt!)}",
                                                style: TextStyle(
                                                    color:
                                                        AppColors.mainGreyColor,
                                                    fontSize: screenWidth(30)),
                                              ),
                                              
                                              Text(
                                                "${tr('entryFee')} ${controller.lotteries[index].pointsValue} ${tr('point')}",
                                                style: TextStyle(
                                                    color:
                                                        AppColors.mainRedColor,
                                                    fontSize: screenWidth(30)),
                                              ),
                                              if (!controller
                                                  .lotteries[index].ended!)
                                                Row(
                                                  // mainAxisAlignment:
                                                  //     MainAxisAlignment.end,
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                          "${tr('chancesOfWinning')} ${controller.lotteries[index].chances??""}",
                                                        overflow:
                                                            TextOverflow.ellipsis,
                                                        style: const TextStyle(color: AppColors.mainYellowColor),
                                                      ),
                                                    ),
                                                    CustomButton(
                                                      text: controller
                                                              .lotteries[index]
                                                              .user!
                                                          ? tr('joined')
                                                          : tr('joining'),
                                                      widthButton: 5,
                                                      textSize: 12,
                                                      circularBorder:
                                                          screenWidth(10),
                                                      heightButton: 10,
                                                      onPressed: controller
                                                              .lotteries[index]
                                                              .user!
                                                          ? null
                                                          : () {
                                                              controller.enterLottery(
                                                                  drawId: controller
                                                                      .lotteries[
                                                                          index]
                                                                      .id!,
                                                                  points: controller
                                                                      .lotteries[
                                                                          index]
                                                                      .pointsValue!);
                                                            },
                                                    ),
                                                  ],
                                                ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              ).animate().scale(delay: 100.ms);
              },
            ),
            Obx(
              () {
                return controller.isMoreLotteryLoading
                    ? const SpinKitCircle(
                        color: AppColors.mainYellowColor,
                      )
                    : const SizedBox.shrink();
              },
            ),
          ],
        ),
      ),
    );
  }
}
