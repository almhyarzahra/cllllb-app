import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LotteryLoading extends StatelessWidget {
  const LotteryLoading({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: ListView.separated(
        separatorBuilder: (context, index) => const SizedBox(height: 8),
        itemCount: 6,
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          return Container(
            width: double.infinity,
            height: 170,
            decoration: BoxDecoration(
              color: AppColors.mainGrey2Color.withOpacity(0.5),
              borderRadius: BorderRadius.circular(10),
            ),
          );
        },
      ),
    );
  }
}
