import 'dart:developer';

import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/UI/views/filter_lottery_view/filter_lottery_view.dart';
import 'package:com.tad.cllllb/UI/views/main_view/profile_view/profile_controller.dart';
import 'package:com.tad.cllllb/core/data/models/apis/lottery_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class LotteryController extends BaseController {
  LotteryController({
    this.currentlocation,
  }) {
    //
  }
  LocationData? currentlocation;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<LotteryModel> lotteries = <LotteryModel>[LotteryModel()].obs;
  bool get isLotteryLoading => listType.contains(OperationType.lottery);
  bool get isMoreLotteryLoading => listType.contains(OperationType.moreLottery);
  RxInt pageKey = 2.obs;
  RxBool getMore = true.obs;
  RxBool enableScrollListner = true.obs;

  RxString date = "".obs;
  DateTime? pickedDate;
  RxBool finished = true.obs;
  RxBool notFinished = false.obs;
  TextEditingController pointController = TextEditingController();
  @override
  void onInit() {
    super.onInit();
    getLottery();
  }

  void getLottery() {
    runLoadingFutureFunction(
      type: OperationType.lottery,
      function: UserRepository().getLottery(page: 1).then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_check_connection'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (lotteries.isNotEmpty) {
                lotteries.clear();
              }
              lotteries.addAll(r);
              lotteries.map((element) {
                element.message = "l";
              }).toSet();
            },
          );
        },
      ),
    );
  }

  Future<void> getMoreLottary({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
        type: OperationType.moreLottery,
        function: UserRepository().getLottery(page: page).then((value) {
          value.fold((l) {
            pageKey.value = pageKey.value - 1;
          }, (r) {
            if (r.isEmpty) {
              enableScrollListner.value = false;
            } else {
              lotteries.addAll(r);
            }
          });
        })).then((value) => getMore.value = true);
  }

  void enterLottery({required double points, required int drawId}) {
    if (storage.getUserPoint() < points) {
      CustomToast.showMessage(
          message: tr('checkNumberPoints'), messageType: MessageType.INFO);
    } else {
      runFullLoadingFutureFunction(
        function: UserRepository().enterLottery(drawId: drawId).then(
          (value) {
            value.fold(
              (l) {
                CustomToast.showMessage(
                    message: tr('key_check_connection'),
                    messageType: MessageType.REJECTED);
              },
              (r) {
                Get.back();
                Get.find<ProfileController>().getUserInfo();
                storage.setUserPoint(storage.getUserPoint() - points);
                CustomToast.showMessage(
                    message: tr('key_success'),
                    messageType: MessageType.SUCCSESS);
              },
            );
          },
        ),
      );
    }
  }

  void filter() {
    runFullLoadingFutureFunction(
      function: UserRepository()
          .getLottery(
        page: 1,
        date: date.value.isEmpty ? null : date.value,
        points: pointController.text.trim().isEmpty
            ? null
            : pointController.text.trim(),
        ended: finished.value,
      )
          .then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_try_again'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (r.isEmpty) {
                CustomToast.showMessage(
                    message: tr('noMatchingResults'),
                    messageType: MessageType.INFO);
              } else {
                Get.to(
                  () => FilterLotteryView(
                    currentlocation: currentlocation,
                    lotteries: r,
                  ),
                );
              }
            },
          );
        },
      ),
    );
  }
}
