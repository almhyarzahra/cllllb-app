import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_book_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/subscription_view/subscription_view.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:com.tad.cllllb/core/utils/network_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:lottie/lottie.dart';

import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';
import 'coach_controller.dart';

class CoachView extends StatefulWidget {
  const CoachView(
      {super.key,
      required this.currentlocation,
      required this.imageClub,
      required this.nameClub,
      required this.address,
      required this.clubType,
      required this.fieldId});
  final LocationData? currentlocation;
  final String imageClub;
  final String nameClub;
  final String address;
  final String clubType;
  final int fieldId;

  @override
  State<CoachView> createState() => _CoachViewState();
}

class _CoachViewState extends State<CoachView> {
  late CoachController controller;
  @override
  void initState() {
    controller = Get.put(CoachController(fieldId: widget.fieldId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
         bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            children: [
              screenHeight(80).ph,
              CustomBookBar(
                imageUrl: widget.imageClub,
                imageName: Uri.https(NetworkUtil.baseUrl, widget.imageClub).toString(),
                text: "${widget.nameClub}/ ${widget.address}/ ${widget.clubType}",
              ),
              screenHeight(80).ph,
              Padding(
                padding: EdgeInsetsDirectional.only(start: screenWidth(20)),
                child: CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: tr('key_choose_coach'),
                  textAlign: TextAlign.start,
                  textColor: AppColors.mainYellowColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
              screenHeight(80).ph,
              Obx(() {
                return controller.isCoachLoading
                    ? const SpinKitCircle(
                        color: AppColors.mainYellowColor,
                      )
                    : GridView.count(
                        childAspectRatio: 1,
                        crossAxisCount: 2,
                        crossAxisSpacing: 0,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        children: controller.coachModel
                            .map((element) {
                              return InkWell(
                                onTap: () {
                                  Get.to(SubscriptionView(
                                    address: widget.address,
                                    clubType: widget.clubType,
                                    currentlocation: widget.currentlocation,
                                    imageClub: widget.imageClub,
                                    nameClub: widget.nameClub,
                                    fieldId: widget.fieldId,
                                    coachId: element.id,
                                  ));
                                },
                                child: Card(
                                  elevation: 4,
                                  shadowColor: AppColors.mainYellowColor,
                                  child: Column(
                                    children: [
                                      CachedNetworkImage(
                                        imageUrl: Uri.https(NetworkUtil.baseUrl, element.image!)
                                            .toString(),
                                        placeholder: (context, url) =>
                                            Lottie.asset("images/download.json"),
                                        errorWidget: (context, url, error) =>
                                            const Icon(Icons.error),
                                        width: double.infinity,
                                        height: screenHeight(6),
                                        fit: BoxFit.cover,
                                      ),
                                      screenWidth(30).ph,
                                      Flexible(
                                        child: CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text: element.name!,
                                          textColor: AppColors.mainBlackColor,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            })
                            .toList()
                            .animate(interval: 50.ms)
                            .scale(delay: 100.ms),
                      );
              })
            ].animate(interval: 50.ms).scale(delay: 100.ms),
          ),
        ),
      ),
    );
  }
}
