import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/data/repositories/booking_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/data/models/apis/coach_model.dart';
import '../../../core/enums/request_status.dart';
import '../../../core/translation/app_translation.dart';

class CoachController extends BaseController {
  CoachController({required int fieldId}) {
    this.fieldId.value = fieldId;
  }
  RxInt fieldId = 0.obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<CoachModel> coachModel = <CoachModel>[CoachModel()].obs;

  bool get isCoachLoading => requestStatus.value == RequestStatus.LOADING;

  @override
  void onInit() {
    getCoach();
    super.onInit();
  }

  void getCoach() {
    runLoadingFutureFunction(
        function: BookingRepository().getGameCoach(fieldId: fieldId.value).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_No_Internet_Connection'), messageType: MessageType.REJECTED);
      }, (r) {
        if (coachModel.isNotEmpty) {
          coachModel.clear();
        }

        coachModel.addAll(r);
        coachModel.map((element) {
          element.message = "l";
        }).toSet();
      });
    }));
  }
}
