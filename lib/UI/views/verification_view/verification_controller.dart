import 'package:com.tad.cllllb/UI/views/sign_in_view/sign_in_controller.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/enums/message_type.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_toast.dart';
import '../main_view/profile_view/profile_controller.dart';
import '../personal_information_view/personal_information_controller.dart';

class VerificationController extends BaseController {
  VerificationController({required int code, required String type}) {
    this.code.value = code;
    this.type.value = type;
  }
  RxInt code = 0.obs;
  RxString type = "".obs;
  TextEditingController numberController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  void login() {
    //print(code.value);
    if (formKey.currentState!.validate()) {
      if (code.value == int.parse(numberController.text)) {
        if (type.value == "login") {
          Get.back();
          Get.find<SignInController>().login();
        } else if (type.value == "register") {
          Get.back();
          Get.find<PersonalInformationController>().register();
        } else if (type.value == "edit") {
          Get.back();
          Get.find<ProfileController>().editProfile();
        }
        // runFullLoadingFutureFunction(
        //     function:
        //         UserRepository().login(phone: phoneNumber.value).then((value) {
        //   value.fold((l) {
        //     if (l == null) {
        //       CustomToast.showMessage(
        //           message: tr('key_check_connection'),
        //           messageType: MessageType.REJECTED);
        //     }
        //   }, (r) async {
        //     CustomToast.showMessage(
        //         message: tr('key_success'), messageType: MessageType.SUCCSESS);
        //     storage.setTokenInfo(r.token);
        //     storage.setEmail(r.userEmail);
        //     storage.setmobileNumber(r.userPhone);
        //     await storage.setUserId(r.userId);
        //     storage.setUserName(r.userName);
        //     Get.offAll(() => MainView());
        //   });
        // }));
      } else {
        CustomToast.showMessage(
            message: tr('key_code_wrong'), messageType: MessageType.REJECTED);
      }
    }
  }
}
