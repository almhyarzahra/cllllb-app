import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/verification_view/verification_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../../shared/utils.dart';

class VerificationView extends StatefulWidget {
  const VerificationView({super.key, required this.code, required this.type});
  final int code;
  final String type;

  @override
  State<VerificationView> createState() => _VerificationViewState();
}

class _VerificationViewState extends State<VerificationView> {
  late VerificationController controller;
  @override
  void initState() {
    controller = Get.put(VerificationController(code: widget.code, type: widget.type));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Form(
          key: controller.formKey,
          child: Column(
            children: [
              Center(
                child: Image.asset(
                  "images/cllllb.png",
                  height: screenHeight(5),
                ),
              ),
              Padding(
                padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(screenWidth(15)),
                    border: Border.all(color: AppColors.mainYellowColor),
                  ),
                  child: Padding(
                    padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(30)),
                    child: Column(
                      children: [
                        screenHeight(20).ph,
                        CustomText(
                          textType: TextStyleType.CUSTOM,
                          fontSize: screenWidth(20),
                          text: tr('key_PIN_has_been_sent'),
                          textColor: AppColors.mainBlackColor,
                        ),
                        screenHeight(15).ph,
                        PinCodeTextField(
                          keyboardType: TextInputType.number,
                          appContext: context,
                          controller: controller.numberController,
                          length: 6,
                          cursorHeight: screenWidth(15),
                          enableActiveFill: true,
                          textStyle: TextStyle(fontSize: screenWidth(20)),
                          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                          pinTheme: PinTheme(
                            shape: PinCodeFieldShape.box,
                            fieldWidth: screenWidth(8),
                            fieldHeight: screenWidth(8),
                            inactiveColor: Colors.grey,
                            selectedColor: Colors.lightBlue,
                            activeColor: Colors.blue,
                            selectedFillColor: AppColors.mainYellowColor,
                            inactiveFillColor: Colors.grey.shade100,
                            borderWidth: 1,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          validator: (value) {
                            return value!.isEmpty || value.length < 6
                                ? tr('key_Please_Enter_Verification_Number')
                                : null;
                          },
                          onChanged: ((value) {}),
                        ),
                        screenHeight(10).ph,
                        CustomButton(
                          text: tr('key_continue'),
                          onPressed: () {
                            controller.login();
                          },
                          circularBorder: screenWidth(10),
                          widthButton: 2,
                          heightButton: 10,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
