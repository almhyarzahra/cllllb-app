import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/book_now_view/book_now_controller.dart';
import 'package:com.tad.cllllb/UI/views/book_now_view/book_now_shimmer/players_shimmer.dart';
import 'package:com.tad.cllllb/UI/views/main_view/home_view/home_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_book_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_second_bar.dart';
import '../../shared/utils.dart';
import '../table_detalis_view/table_detalis_view.dart';
import 'book_now_shimmer/game_field_shimmer.dart';

class BookNowView extends StatefulWidget {
  const BookNowView(
      {super.key,
      required this.nameClub,
      required this.address,
      required this.imageClub,
      required this.clubId,
      required this.clubType,
      required this.gameId,
      required this.nameGame,
      required this.imageGame,
      required this.quantity,
      required this.currentlocation,
      required this.bookingType});
  final String nameClub;
  final String address;
  final String imageClub;
  final int clubId;
  final String clubType;
  final int gameId;
  final String nameGame;
  final String imageGame;
  final int quantity;
  final int bookingType;
  final LocationData? currentlocation;
  @override
  State<BookNowView> createState() => _BookNowViewState();
}

class _BookNowViewState extends State<BookNowView> {
  late BookNowController controller;

  @override
  void initState() {
    controller = Get.put(BookNowController(
        gameId: widget.gameId,
        address: widget.address,
        clupType: widget.clubType,
        imageClub: widget.imageClub,
        nameClub: widget.nameClub,
        currentlocation: widget.currentlocation));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
         bottomNavigationBar:const CustomBottomNavigation(),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            children: [
              screenHeight(80).ph,
              CustomBookBar(
                imageUrl: widget.imageClub,
                imageName:
                    Uri.https(NetworkUtil.baseUrl, widget.imageClub).toString(),
                text:
                    "${widget.nameClub}/ ${widget.address}/ ${widget.clubType}",
              ),
              CustomSecondBar(
                svgName: widget.imageGame,
                text1: widget.nameGame,
                text2: widget.quantity.toString() + tr('key_QUANTITY2'),
              ),
              widget.bookingType == 3
                  ? const SizedBox.shrink()
                  : Padding(
                      padding: EdgeInsetsDirectional.symmetric(
                          horizontal: screenWidth(20)),
                      child: Row(
                        children: [
                          CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: tr('key_PLAYERS_IN_THE_HALL '),
                            textColor: AppColors.mainGreyColor,
                            fontSize: screenWidth(22),
                          ),
                          CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: tr('key_NOW'),
                            textColor: AppColors.mainRedColor,
                            fontSize: screenWidth(22),
                          ),
                        ],
                      ),
                    ),
              widget.bookingType == 3
                  ? const SizedBox.shrink()
                  : Obx(
                      () {
                        return controller.isPlayersLoading
                            ? const PlayersShimmer()
                            : controller.players.isEmpty
                                ? CustomText(
                                    textType: TextStyleType.CUSTOM,
                                    text: tr('key_No_Players_Now'),
                                    textColor: AppColors.mainYellowColor,
                                  )
                                : controller.gameField[0].message == null
                                    ? customFaildConnection(
                                        onPressed: () {
                                          controller.getPlayersNow();
                                        },
                                      )
                                    : SizedBox(
                                        height: screenHeight(5),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: ListView(
                                            shrinkWrap: true,
                                            scrollDirection: Axis.horizontal,
                                            children: controller.players.map(
                                              (element) {
                                                return Padding(
                                                  padding: EdgeInsetsDirectional
                                                      .symmetric(
                                                          horizontal:
                                                              screenWidth(30)),
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        width: screenWidth(7),
                                                        height:
                                                            screenHeight(10),
                                                        decoration:
                                                            BoxDecoration(
                                                          border: Border.all(
                                                              color: AppColors
                                                                  .mainYellowColor),
                                                          shape:
                                                              BoxShape.circle,
                                                          image:
                                                              DecorationImage(
                                                            fit: BoxFit.contain,
                                                            image:
                                                                CachedNetworkImageProvider(
                                                              element.user!
                                                                          .image ==
                                                                      null
                                                                  ? ""
                                                                  : Uri.https(
                                                                          NetworkUtil
                                                                              .baseUrl,
                                                                          element
                                                                              .user!
                                                                              .image!)
                                                                      .toString(),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      CustomText(
                                                        textType: TextStyleType
                                                            .CUSTOM,
                                                        text: element
                                                            .field!.name!,
                                                        textColor: AppColors
                                                            .mainYellowColor,
                                                      ),
                                                    ],
                                                  ),
                                                );
                                              },
                                            ).toList(),
                                          ),
                                        ),
                                      );
                      },
                    ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: widget.bookingType == 3
                      ? tr('key_new_subscription')
                      : tr('key_NEW_BOOKING'),
                  textColor: AppColors.mainBlackColor,
                  textAlign: TextAlign.start,
                ),
              ),
              Obx(
                () {
                  return controller.isGameFieldLoading
                      ? const GameFieldShimmer()
                      : controller.gameField[0].message == null
                          ? customFaildConnection(
                              onPressed: () {
                                controller.getFieldGame();
                              },
                            )
                          : Padding(
                              padding: EdgeInsetsDirectional.symmetric(
                                  vertical: screenWidth(15)),
                              child: GridView.count(
                                //padding: EdgeInsetsDirectional.only(start: 20),
                                crossAxisCount: 2,
                                crossAxisSpacing: 1,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                children: controller.gameField.map(
                                  (e) {
                                    return InkWell(
                                      onTap: () {
                                        widget.bookingType == 3
                                            ? controller.checkCoach(
                                                fieldId: e.id!)
                                            : Get.to(
                                                TableDetalisView(
                                                  bookingType:
                                                      widget.bookingType,
                                                  currentlocation:
                                                      Get.find<HomeController>()
                                                          .currentlocation,
                                                  imageGameSelected: e.image!,
                                                  price: e.price ?? 0.0,
                                                  gameFieldId: e.id!,
                                                  imageGame: widget.imageGame,
                                                  nameGame: widget.nameGame,
                                                  quantity: widget.quantity,
                                                  address: widget.address,
                                                  clubId: widget.clubId,
                                                  clubType: widget.clubType,
                                                  imageClub: widget.imageClub,
                                                  nameClub: widget.nameClub,
                                                ),
                                              );
                                      },
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(
                                            screenWidth(8)),
                                        child: Padding(
                                          padding:
                                              EdgeInsetsDirectional.symmetric(
                                                  horizontal: screenWidth(50)),
                                          child: Card(
                                            elevation: 3,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                screenWidth(25).ph,
                                                Center(
                                                  child: CustomText(
                                                    textType:
                                                        TextStyleType.CUSTOM,
                                                        overflow: TextOverflow.ellipsis,
                                                    fontSize: screenWidth(20),
                                                    text: e.name!,
                                                    textColor: AppColors
                                                        .mainBlackColor,
                                                  ),
                                                ),
                                                Center(
                                                    child: CustomNetworkImage(
                                                  context.image(e.image!),
                                                  width: 70,
                                                  height: 70,
                                                )),
                                                if (e.pointsValue != null)
                                                  Center(
                                                    child: CustomText(
                                                      textType:
                                                          TextStyleType.CUSTOM,
                                                      text:
                                                          "+${e.pointsValue} ${tr('rewardPoints')}",
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      fontSize: 10,
                                                      textColor: AppColors
                                                          .mainYellowColor,
                                                    ),
                                                  ),
                                                widget.bookingType == 3
                                                    ? const SizedBox.shrink()
                                                    : Padding(
                                                        padding: EdgeInsetsDirectional
                                                            .only(
                                                                start:
                                                                    screenWidth(
                                                                        25)),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            CustomText(
                                                              textType:
                                                                  TextStyleType
                                                                      .CUSTOM,
                                                              text: tr(
                                                                  'key_Price'),
                                                              textColor: AppColors
                                                                  .mainYellowColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                            CustomText(
                                                                textType:
                                                                    TextStyleType
                                                                        .CUSTOM,
                                                                fontSize:
                                                                    screenWidth(
                                                                        35),
                                                                text: e.price
                                                                        .toString() +
                                                                    tr(
                                                                        'key_AED_Half_hours'),
                                                                textColor: AppColors
                                                                    .mainYellowColor),
                                                          ],
                                                        ),
                                                      ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ).toList(),
                              ),
                            );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
