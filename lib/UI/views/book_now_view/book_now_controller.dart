import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import '../../../core/data/models/apis/game_field_model.dart';
import '../../../core/data/models/apis/players_model.dart';
import '../../../core/data/repositories/booking_repository.dart';
import '../../../core/translation/app_translation.dart';
import '../coach_view/coach_view.dart';
import '../subscription_view/subscription_view.dart';

class BookNowController extends BaseController {
  BookNowController(
      {required int gameId,
      required String address,
      required String clupType,
      required String imageClub,
      required String nameClub,
      this.currentlocation}) {
    this.gameId.value = gameId;
    this.address.value = address;
    this.clupType.value = clupType;
    this.imageClub.value = imageClub;
    this.nameClub.value = nameClub;
  }
  RxString address = "".obs;
  RxString clupType = "".obs;
  RxString imageClub = "".obs;
  RxString nameClub = "".obs;

  LocationData? currentlocation;
  RxInt gameId = 0.obs;
  RxList<GameFieldModel> gameField = <GameFieldModel>[GameFieldModel()].obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  Rx<DateTime> date = DateTime.now().obs;
  RxList<PlayersModel> players = <PlayersModel>[PlayersModel()].obs;
  bool get isGameFieldLoading => listType.contains(OperationType.GAME_FIELD);
  bool get isPlayersLoading => listType.contains(OperationType.PLAYERS);

  @override
  void onInit() {
    getFieldGame();
    getPlayersNow();
    super.onInit();
  }

  void getPlayersNow() {
    runLoadingFutureFunction(
        type: OperationType.PLAYERS,
        function: BookingRepository()
            .getPlayersNow(gameId: gameId.value, hourNow: formatHour(date.value))
            .then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: tr('key_No_Internet_Connection'), messageType: MessageType.REJECTED);
          }, (r) {
            if (players.isNotEmpty) {
              players.clear();
            }

            players.addAll(r);
            players.map((element) {
              element.message = "l";
            }).toSet();
          });
        }));
  }

  void getFieldGame() {
    runLoadingFutureFunction(
        type: OperationType.GAME_FIELD,
        function: BookingRepository().getFieldGame(gameId: gameId.value).then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: tr('key_No_Internet_Connection'), messageType: MessageType.REJECTED);
          }, (r) {
            if (gameField.isNotEmpty) {
              gameField.clear();
            }

            gameField.addAll(r);
            gameField.map((element) {
              element.message = "l";
            }).toSet();
          });
        }));
  }

  String formatHour(DateTime date) {
    return DateFormat('HH:mm:ss').format(date);
  }

  void checkCoach({required int fieldId}) {
    runFullLoadingFutureFunction(
        function: BookingRepository().checkCoach(fieldId: fieldId).then((value) {
      value.fold((l) {
        CustomToast.showMessage(message: tr('key_try_again'), messageType: MessageType.REJECTED);
      }, (r) {
        if (r.code == 0) {
          Get.to(CoachView(
            fieldId: fieldId,
            address: address.value,
            clubType: clupType.value,
            currentlocation: currentlocation,
            imageClub: imageClub.value,
            nameClub: nameClub.value,
          ));
        } else if (r.code == 1) {
          Get.to(SubscriptionView(
            fieldId:fieldId ,
            address: address.value,
            clubType: clupType.value,
            currentlocation: currentlocation,
            imageClub: imageClub.value,
            nameClub: nameClub.value,
          ));
        }
      });
    }));
  }
}
