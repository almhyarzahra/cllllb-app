import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import '../../../shared/colors.dart';
import '../../../shared/utils.dart';

class GameFieldShimmer extends StatelessWidget {
  const GameFieldShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: Padding(
        padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: screenWidth(2.5),
              height: screenWidth(2),
              decoration: BoxDecoration(
                color: AppColors.mainOrangeColor,
                borderRadius: BorderRadius.circular(screenWidth(35)),
              ),
            ),
            Container(
              width: screenWidth(2.5),
              height: screenWidth(2),
              decoration: BoxDecoration(
                color: AppColors.mainOrangeColor,
                borderRadius: BorderRadius.circular(screenWidth(35)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
