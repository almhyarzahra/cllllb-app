import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import '../../../shared/colors.dart';
import '../../../shared/utils.dart';

class PlayersShimmer extends StatelessWidget {
  const PlayersShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: SizedBox(
        height: screenHeight(5),
        child: Center(
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: 4,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: EdgeInsetsDirectional.symmetric(
                    horizontal: screenWidth(30)),
                child: Container(
                  width: screenWidth(6),
                  height: screenHeight(10),
                  decoration: const BoxDecoration(
                    color: AppColors.mainOrangeColor,
                    shape: BoxShape.circle,
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
