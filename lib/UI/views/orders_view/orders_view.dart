import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_drawer.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/orders_view/orders_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_bar.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/custom_widgets/virtical_divider.dart';
import '../../shared/utils.dart';

class OrdersView extends StatefulWidget {
  const OrdersView({super.key, required this.currentlocation});
  final LocationData? currentlocation;

  @override
  State<OrdersView> createState() => _OrdersViewState();
}

class _OrdersViewState extends State<OrdersView> {
  late OrdersController controller;
  @override
  void initState() {
    controller = Get.put(OrdersController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            children: [
              CustomBar(
                text: tr('key_Orders'),
              ),
              Obx(
                () {
                  return controller.isOrdersLoading
                      ? const SpinKitCircle(
                          color: AppColors.mainYellowColor,
                        )
                      : controller.userOrders.isEmpty
                          ? Padding(
                              padding: EdgeInsetsDirectional.only(top: screenHeight(3)),
                              child: CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_No_Order_Available'),
                                textColor: AppColors.mainBlackColor,
                              ),
                            )
                          : controller.userOrders[0].message == null
                              ? customFaildConnection(
                                  onPressed: () {
                                    controller.onInit();
                                  },
                                )
                              : Column(
                                  children: controller.userOrders.map(
                                    (element) {
                                      return InkWell(
                                        onTap: () {
                                          Get.defaultDialog(
                                            title: tr('key_Order') +
                                                "${controller.userOrders.indexOf(element) + 1}",
                                            titleStyle: const TextStyle(
                                                color: AppColors.mainYellowColor,
                                                fontWeight: FontWeight.bold),
                                            content: Column(
                                              children: [
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                  children: [
                                                    CustomText(
                                                      textType: TextStyleType.CUSTOM,
                                                      text: tr('key_Product'),
                                                      textColor: AppColors.mainBlueColor,
                                                    ),
                                                    CustomText(
                                                      textType: TextStyleType.CUSTOM,
                                                      text: tr('key_Quantity'),
                                                      textColor: AppColors.mainBlueColor,
                                                    ),
                                                    CustomText(
                                                      textType: TextStyleType.CUSTOM,
                                                      text: tr('key_Price'),
                                                      textColor: AppColors.mainBlueColor,
                                                    ),
                                                  ],
                                                ),
                                                Column(
                                                  children: element.orderProducts!.map(
                                                    (e) {
                                                      return Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment.spaceAround,
                                                        children: [
                                                          Column(
                                                            children: [
                                                              CustomText(
                                                                textType: TextStyleType.CUSTOM,
                                                                text: e.productName!.length > 8
                                                                    ? e.productName!.substring(0, 8)
                                                                    : e.productName!,
                                                                fontSize: screenWidth(40),
                                                                textColor: AppColors.mainBlackColor,
                                                              ),
                                                            ],
                                                          ),
                                                          CustomText(
                                                            fontSize: screenWidth(40),
                                                            textType: TextStyleType.CUSTOM,
                                                            text: "${e.quantity}x${e.price}",
                                                            textColor: AppColors.mainBlackColor,
                                                          ),
                                                          CustomText(
                                                            textType: TextStyleType.CUSTOM,
                                                            text: e.finalPrice.toString(),
                                                            textColor: AppColors.mainBlackColor,
                                                          ),
                                                        ],
                                                      );
                                                    },
                                                  ).toList(),
                                                ),
                                                screenWidth(20).ph,
                                                const VirticalDivider(
                                                  width: 1,
                                                ),
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    CustomText(
                                                      textType: TextStyleType.CUSTOM,
                                                      text: tr('key_Total'),
                                                      textColor: AppColors.mainBlueColor,
                                                    ),
                                                    CustomText(
                                                      textType: TextStyleType.CUSTOM,
                                                      text: element.orderPrice.toString() +
                                                          tr('key_AED'),
                                                      textColor: AppColors.mainYellowColor,
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                          );
                                        },
                                        child: SizedBox(
                                          height: screenHeight(10),
                                          child: Card(
                                            child: Padding(
                                              padding: EdgeInsetsDirectional.symmetric(
                                                  horizontal: screenWidth(20)),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  CustomText(
                                                    fontSize: screenWidth(50),
                                                    textType: TextStyleType.CUSTOM,
                                                    text: tr('key_Order_number') +
                                                        element.id.toString(),
                                                    textColor: AppColors.mainBlackColor,
                                                  ),
                                                  CustomText(
                                                    textType: TextStyleType.CUSTOM,
                                                    text: element.status!.name == "Pending"
                                                        ? tr('key_Incomplete')
                                                        : element.status!.name == "Cancel"
                                                            ? tr('key_Cancel')
                                                            : tr('key_Complete'),
                                                    textColor: element.status!.name == "Pending"
                                                        ? AppColors.mainGrey2Color
                                                        : element.status!.name == "Cancel"
                                                            ? AppColors.mainRedColor
                                                            : AppColors.mainColorGreen,
                                                  ),
                                                  element.status!.name == "Cancel"
                                                      ? const SizedBox.shrink()
                                                      : IconButton(
                                                          onPressed: () {
                                                            Get.defaultDialog(
                                                              title: tr(
                                                                  'key_Are_you_sure_to_cancel_your_Order'),
                                                              titleStyle: const TextStyle(
                                                                  color: AppColors.mainYellowColor),
                                                              content: Column(
                                                                children: [
                                                                  CustomButton(
                                                                    widthButton: 6,
                                                                    circularBorder: screenWidth(20),
                                                                    text: tr('key_Yes'),
                                                                    onPressed: () {
                                                                      controller.cancelOrder(
                                                                          orderId: element.id!);
                                                                    },
                                                                  ),
                                                                  screenWidth(20).ph,
                                                                  CustomButton(
                                                                    widthButton: 6,
                                                                    circularBorder: screenWidth(20),
                                                                    text: tr('key_No'),
                                                                    onPressed: () {
                                                                      Get.back();
                                                                    },
                                                                  ),
                                                                ],
                                                              ),
                                                            );
                                                          },
                                                          icon: const Icon(
                                                            Icons.cancel,
                                                            color: AppColors.mainRedColor,
                                                          ),
                                                        ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  ).toList(),
                                );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
