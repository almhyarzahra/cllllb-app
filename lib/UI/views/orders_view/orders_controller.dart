import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/data/models/apis/user_orders_model.dart';
import '../../../core/data/repositories/payment_repositories.dart';
import '../../../core/enums/request_status.dart';
import '../../../core/translation/app_translation.dart';

class OrdersController extends BaseController {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<UserOrdersModel> userOrders = <UserOrdersModel>[UserOrdersModel()].obs;
  bool get isOrdersLoading => requestStatus.value == RequestStatus.LOADING;

  @override
  void onInit() {
    getUserOrders();
    super.onInit();
  }

  void getUserOrders() {
    runLoadingFutureFunction(
        function: UserRepository().getUserOrders(userId: storage.getUserId()).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_No_Internet_Connection'), messageType: MessageType.REJECTED);
      }, (r) {
        if (userOrders.isNotEmpty) {
          userOrders.clear();
        }
        userOrders.addAll(r);
        userOrders.map((element) {
          element.message = "l";
        }).toSet();
      });
    }));
  }

  void cancelOrder({required int orderId}) {
    runFullLoadingFutureFunction(
        function: PaymentRepositories().cancelOrder(orderId: orderId).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_Please_Try_Again'), messageType: MessageType.REJECTED);
        Get.back();
      }, (r) {
        CustomToast.showMessage(message: tr("key_success"), messageType: MessageType.SUCCSESS);
        Get.back();
        onInit();
      });
    }));
  }
}
