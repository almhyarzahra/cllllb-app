import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/custom_widgets/custom_text_button.dart';
import '../../shared/utils.dart';
import '../food_sport_detalis_view/food-sport_detalis_view.dart';
import 'favorite_controller.dart';

class FavoriteView extends StatefulWidget {
  const FavoriteView({super.key, required this.currentlocation});
  final LocationData? currentlocation;

  @override
  State<FavoriteView> createState() => _FavoriteViewState();
}

class _FavoriteViewState extends State<FavoriteView> {
  FavoriteController controller = FavoriteController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        // appBar: PreferredSize(
        //    preferredSize: Size.fromHeight(screenWidth(6)),
        //   child: CustomAppBar(
        //     currentlocation: widget.currentlocation,
        //     drwerOnPressed: () {
        //       controller.key.currentState!.openDrawer();
        //     },
        //   ),

        // ),
        body: Obx(
          () {
            return controller.favoriteList.isEmpty
                ? Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        IconButton(
                          onPressed: () {
                            Get.back();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios_new,
                            size: screenWidth(20),
                          ),
                        ),
                        CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('key_The_Favorite_Is_Empty'),
                          textColor: AppColors.mainBlackColor,
                        ),
                      ],
                    ),
                  )
                : ListView(
                    children: [
                      screenHeight(80).ph,
                      Padding(
                        padding: EdgeInsetsDirectional.symmetric(
                            horizontal: screenWidth(30)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                IconButton(
                                  onPressed: () {
                                    Get.back();
                                  },
                                  icon: Icon(Icons.arrow_back_ios_new,
                                      size: screenWidth(20)),
                                ),
                                CustomText(
                                  textType: TextStyleType.CUSTOM,
                                  text: tr('key_My_Favorite_Products'),
                                  textColor: AppColors.mainBlackColor,
                                  textAlign: TextAlign.start,
                                  fontSize: screenWidth(20),
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                            CustomTextButton(
                              colorText: AppColors.mainRedColor,
                              onPressed: () {
                                Get.defaultDialog(
                                  title:
                                      tr('key_you_sure_to_clear_your_favorite'),
                                  titleStyle: const TextStyle(
                                      color: AppColors.mainYellowColor),
                                  content: Column(
                                    children: [
                                      CustomButton(
                                        text: tr('key_Yes'),
                                        onPressed: () {
                                          favoriteService.clearCart();
                                          Get.back();
                                        },
                                        widthButton: 4,
                                        circularBorder: screenWidth(10),
                                      ),
                                      screenWidth(20).ph,
                                      CustomButton(
                                        text: tr('key_No'),
                                        onPressed: () {
                                          Get.back();
                                        },
                                        widthButton: 4,
                                        circularBorder: screenWidth(10),
                                      ),
                                    ],
                                  ),
                                );
                              },
                              text: tr('key_Empty_Favorite'),
                              decoration: TextDecoration.underline,
                              decorationThickness: 2,
                              fontSize: screenWidth(40),
                            ),
                          ],
                        ),
                      ),
                      GridView.count(
                        childAspectRatio: 0.68,
                        crossAxisCount: 2,
                        crossAxisSpacing: 0,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        children: controller.favoriteList.map(
                          (e) {
                            return InkWell(
                              onTap: () {
                                Get.to(
                                  FoodSportDetalisView(
                                    currentlocation: widget.currentlocation,
                                    productsModel: e.productsModel!,
                                  ),
                                );
                              },
                              child: Column(
                                children: [
                                  ClipRRect(
                                    borderRadius:
                                        BorderRadius.circular(screenWidth(10)),
                                    child: SizedBox(
                                      height: screenHeight(2.9),
                                      width: screenWidth(2),
                                      child: Card(
                                        elevation: 3,
                                        child: Column(
                                          children: [
                                            CustomNetworkImage(
                                              context
                                                  .image(e.productsModel!.img!),
                                              width: screenWidth(1),
                                              height: screenHeight(6),
                                            ),

                                            screenWidth(20).ph,
                                            //Spacer(),
                                            CustomText(
                                              textType: TextStyleType.CUSTOM,
                                              text: e.productsModel!.name!,
                                              textColor:
                                                  AppColors.mainBlackColor,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            CustomText(
                                              textType: TextStyleType.CUSTOM,
                                              fontSize: screenWidth(40),
                                              text: e.productsModel!
                                                      .productCategory!.isEmpty
                                                  ? ""
                                                  : e
                                                      .productsModel!
                                                      .productCategory![0]
                                                      .categoriesName!
                                                      .name!,
                                              textColor:
                                                  AppColors.mainGrey3Color,
                                            ),
                                            screenWidth(35).ph,
                                            CustomText(
                                              textType: TextStyleType.CUSTOM,
                                              fontSize: screenWidth(30),
                                              fontWeight: FontWeight.bold,
                                              text: e.productsModel!.price
                                                      .toString() +
                                                  tr('key_AED'),
                                              textColor:
                                                  AppColors.mainYellowColor,
                                            ),
                                            CustomText(
                                              overflow: TextOverflow.ellipsis,
                                              textType: TextStyleType.CUSTOM,
                                              fontSize: screenWidth(30),
                                              text:
                                                  e.productsModel!.club?.name !=
                                                          null
                                                      ? tr('key_Club') +
                                                          e.productsModel!.club!
                                                              .name!
                                                      : "",
                                              textColor:
                                                  AppColors.mainBlackColor,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ).toList(),
                      )
                    ],
                  );
          },
        ),
      ),
    );
  }
}
