import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import '../../../core/data/models/cart_model.dart';

class FavoriteController extends BaseController{
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  List<CartModel> get favoriteList => favoriteService.favoriteList;


}