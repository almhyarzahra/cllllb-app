import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/data/models/apis/club_info_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/club_repositories.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/request_status.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/translation/app_translation.dart';

class DetalisController extends BaseController {
  DetalisController({required int clubId}) {
    this.clubId.value = clubId;
  }
  RxInt clubId = 0.obs;
  Rx<ClubInfoModel> clubInfo = ClubInfoModel().obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController reportController = TextEditingController();
  TextEditingController commentController = TextEditingController();

  RxDouble rating = 1.0.obs;

  bool get isClubInfoLoading => requestStatus.value == RequestStatus.LOADING;

  @override
  void onInit() {
    getClubInfo();
    super.onInit();
  }

  void getClubInfo() {
    runLoadingFutureFunction(
        function: ClubRepositories().getClubInfo(clubId: clubId.value).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_No_Internet_Connection'), messageType: MessageType.REJECTED);
      }, (r) {
        clubInfo.value = r;
      });
    }));
  }

  void addComment() {
    runFullLoadingFutureFunction(
        function: ClubRepositories()
            .addReviews(
                rating: rating.value.toInt(),
                userId: storage.getUserId(),
                clubId: clubId.value,
                description:
                    commentController.text.isEmpty ? null : commentController.text.toString())
            .then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_Please_Try_Again'), messageType: MessageType.REJECTED);
      }, (r) {
        CustomToast.showMessage(message: tr('key_success'), messageType: MessageType.SUCCSESS);
        commentController.text = "";
        Get.back();
        onInit();
      });
    }));
  }

  void addReport() {
    if (formKey.currentState!.validate()) {
      runFullLoadingFutureFunction(
          function: ClubRepositories()
              .addReport(
                  message: reportController.text.toString(),
                  userId: storage.getUserId(),
                  clubId: clubId.value)
              .then((value) {
        value.fold((l) {
          CustomToast.showMessage(
              message: tr('key_Please_Try_Again'), messageType: MessageType.REJECTED);
        }, (r) {
          CustomToast.showMessage(message: tr('key_success'), messageType: MessageType.SUCCSESS);
          reportController.text = "";
        });
      }));
    }
  }
}
