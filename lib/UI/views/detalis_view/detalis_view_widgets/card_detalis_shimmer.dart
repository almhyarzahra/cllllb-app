import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import '../../../shared/colors.dart';
import '../../../shared/custom_widgets/custom_card_detalis.dart';
import '../../../shared/utils.dart';

class CardDetalisShimmer extends StatelessWidget {
  const CardDetalisShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: Padding(
        padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(40)),
        child: Container(
          decoration: BoxDecoration(
            //color: AppColors.mainGrey2Color,
            borderRadius: BorderRadius.circular(screenWidth(10)),
          ),
          child: Padding(
            padding: EdgeInsetsDirectional.symmetric(
                horizontal: screenWidth(18), vertical: screenWidth(20)),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomCardDetalis(
                      onPressed: () {},
                      svgName: "book_game",
                      text: "BOOK GAME",
                    ),
                    CustomCardDetalis(
                      onPressed: () {},
                      svgName: "ic_store",
                      text: "STORE",
                    ),
                  ],
                ),
                screenWidth(30).ph,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomCardDetalis(
                      onPressed: () {},
                      svgName: "special_offer",
                      text: "SPECIAL OFFER",
                    ),
                    CustomCardDetalis(
                      onPressed: () {},
                      svgName: "contact",
                      text: "CONTACT",
                      prefixDistance: 30,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
