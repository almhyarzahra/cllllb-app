import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import '../../../shared/colors.dart';
import '../../../shared/utils.dart';

class ButtonShimmer extends StatelessWidget {
  const ButtonShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: Padding(
        padding: const EdgeInsetsDirectional.all(8.0),
        child: Container(
          height: screenHeight(15),
          width: screenWidth(3),
          color: AppColors.mainOrangeColor,
        ),
      ),
    );
  }
}
