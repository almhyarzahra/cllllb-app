import 'package:carousel_slider/carousel_slider.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/UI/views/detalis_view/detalis_controller.dart';
import 'package:com.tad.cllllb/UI/views/store_view/store_view.dart';
import 'package:com.tad.cllllb/UI/views/video_view/video_view.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:com.tad.cllllb/core/utils/network_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_book_bar.dart';
import '../../shared/custom_widgets/custom_card_detalis.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';
import '../book_game_view/book_game_view.dart';
import '../contact_view/contact_view.dart';
import '../main_view/home_view/home_view_shimmer/advertisements_shimmer.dart';
import '../photo_view/photo_view.dart';
import '../sign_in_view/sign_in_view.dart';
import '../special_offer_view/special_offer_view.dart';
import 'detalis_view_widgets/button_shimmer.dart';
import 'detalis_view_widgets/card_detalis_shimmer.dart';

class DetalisView extends StatefulWidget {
  const DetalisView(
      {super.key,
      required this.nameClub,
      required this.address,
      required this.imageClub,
      required this.clubId,
      required this.currentlocation});
  final String nameClub;
  final String address;
  final String imageClub;
  final int clubId;
  final LocationData? currentlocation;

  @override
  State<DetalisView> createState() => _DetalisViewState();
}

class _DetalisViewState extends State<DetalisView>
    with AutomaticKeepAliveClientMixin {
  late DetalisController controller;
  @override
  void initState() {
    controller = Get.put(DetalisController(clubId: widget.clubId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(
      child: Scaffold(
         bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: Form(
            key: controller.formKey,
            child: ListView(
              children: [
                screenHeight(80).ph,
                Obx(
                  () {
                    return controller.isClubInfoLoading
                        ? const SpinKitCircle(
                            color: AppColors.mainYellowColor,
                          )
                        : controller.clubInfo.value.name == null
                            ? const SizedBox.shrink()
                            : CustomBookBar(
                                imageUrl: widget.imageClub,
                                imageName: Uri.https(
                                        NetworkUtil.baseUrl, widget.imageClub)
                                    .toString(),
                                text:
                                    "${widget.nameClub}/ ${widget.address}/ ${controller.clubInfo.value.clubType!.name!}",
                              );
                  },
                ),
                screenHeight(70).ph,
                Obx(
                  () {
                    return controller.isClubInfoLoading
                        ? const AdvertisementsShimmer()
                        : controller.clubInfo.value.name == null
                            ? customFaildConnection(
                                onPressed: () {
                                  controller.onInit();
                                },
                              )
                            : controller.clubInfo.value.images!.isEmpty
                                ? CustomText(
                                    textType: TextStyleType.CUSTOM,
                                    text: tr('key_No_image_Yet'),
                                    textColor: AppColors.mainBlackColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: screenWidth(20),
                                  )
                                : CarouselSlider.builder(
                                    itemCount: controller
                                        .clubInfo.value.images!.length,
                                    options: CarouselOptions(
                                        reverse: false,
                                        autoPlay: true,
                                        height: screenWidth(1.7),
                                        enlargeCenterPage: true,
                                        viewportFraction: 0.9,
                                        enlargeStrategy:
                                            CenterPageEnlargeStrategy.scale,
                                        enlargeFactor: 0.25,
                                        onPageChanged: (index, reason) {}),
                                    itemBuilder: (context, index, realIndex) {
                                      return CustomNetworkImage(
                                        context.image(controller
                                            .clubInfo.value.images![index]),
                                        width: screenWidth(1),
                                        height: screenHeight(2),
                                        radius: screenWidth(50),
                                        onTap: () => Get.to(
                                          () => PhotoWidgetView(
                                            imageUrl: controller
                                                .clubInfo.value.images!,
                                          ),
                                        ),
                                      );
                                    },
                                  );
                  },
                ),
                Obx(
                  () {
                    return IconButton(
                      onPressed: controller.clubInfo.value.clubVideo == null
                          ? null
                          : () {
                              Get.to(
                                VideoView(
                                    videoURL: Uri.https(
                                            NetworkUtil.baseUrl,
                                            controller
                                                .clubInfo.value.clubVideo!)
                                        .toString()),
                              );
                            },
                      icon: Icon(
                        Icons.video_collection_outlined,
                        size: screenWidth(10),
                        color: AppColors.mainYellowColor,
                      ),
                    );
                  },
                ),
                screenHeight(40).ph,
                Obx(
                  () {
                    return controller.isClubInfoLoading
                        ? const CardDetalisShimmer()
                        : controller.clubInfo.value.name == null
                            ? const SizedBox.shrink()
                            : Padding(
                                padding: EdgeInsetsDirectional.symmetric(
                                    horizontal: screenWidth(40)),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: AppColors.mainGrey4Color,
                                    borderRadius:
                                        BorderRadius.circular(screenWidth(10)),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsetsDirectional.symmetric(
                                        horizontal: screenWidth(18),
                                        vertical: screenWidth(20)),
                                    child: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            CustomCardDetalis(
                                              onPressed: () {
                                                Get.to(
                                                  BookGameView(
                                                    currentlocation:
                                                        widget.currentlocation,
                                                    clubType: controller
                                                        .clubInfo
                                                        .value
                                                        .clubType!
                                                        .name!,
                                                    nameClub: widget.nameClub,
                                                    address: widget.address,
                                                    imageClub: widget.imageClub,
                                                    clubId: widget.clubId,
                                                  ),
                                                );
                                              },
                                              svgName: "book_game",
                                              text: tr('key_BOOK_GAME'),
                                            ),
                                            CustomCardDetalis(
                                              onPressed: () {
                                                Get.to(
                                                  StoreView(
                                                    currentlocation:
                                                        widget.currentlocation,
                                                    clubType: controller
                                                        .clubInfo
                                                        .value
                                                        .clubType!
                                                        .name!,
                                                    nameClub: widget.nameClub,
                                                    address: widget.address,
                                                    imageClub: widget.imageClub,
                                                    clubId: widget.clubId,
                                                  ),
                                                );
                                              },
                                              svgName: "ic_store",
                                              text: tr('key_STORE'),
                                            ),
                                          ],
                                        ),
                                        screenWidth(30).ph,
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            CustomCardDetalis(
                                              onPressed: () {
                                                Get.to(
                                                  ContactView(
                                                    currentlocation:
                                                        widget.currentlocation,
                                                    clubType: controller
                                                        .clubInfo
                                                        .value
                                                        .clubType!
                                                        .name!,
                                                    email: controller
                                                        .clubInfo.value.email!,
                                                    phone: controller
                                                        .clubInfo.value.phone!,
                                                    nameClub: widget.nameClub,
                                                    address: widget.address,
                                                    imageClub: widget.imageClub,
                                                    clubId: widget.clubId,
                                                    latitude: double.parse(
                                                        controller.clubInfo
                                                            .value.latitude!),
                                                    longtitude: double.parse(
                                                        controller.clubInfo
                                                            .value.longtitude!),
                                                  ),
                                                );
                                              },
                                              svgName: "contact",
                                              text: tr('key_CONTACT'),
                                              prefixDistance: 30,
                                            ),
                                            GetPlatform.isIOS
                                                ? const SizedBox.shrink()
                                                : CustomCardDetalis(
                                                    onPressed: () {
                                                      Get.to(
                                                        SpecialOfferView(
                                                          currentlocation: widget
                                                              .currentlocation,
                                                          clubType: controller
                                                              .clubInfo
                                                              .value
                                                              .clubType!
                                                              .name!,
                                                          nameClub:
                                                              widget.nameClub,
                                                          address:
                                                              widget.address,
                                                          imageClub:
                                                              widget.imageClub,
                                                          clubId: widget.clubId,
                                                        ),
                                                      );
                                                    },
                                                    svgName: "special_offer",
                                                    text:
                                                        tr('key_SPECIAL_OFFER'),
                                                  ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                  },
                ),
                screenHeight(40).ph,
                Padding(
                  padding: EdgeInsetsDirectional.symmetric(
                      horizontal: screenWidth(20)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomText(
                            textType: TextStyleType.CUSTOM,
                            fontSize: screenWidth(18),
                            text: tr('key_ABOUT_THE_CLUB'),
                            textColor: AppColors.mainYellowColor,
                          ),
                          Obx(
                            () {
                              return RatingBar.builder(
                                initialRating: controller.clubInfo.value.rating
                                        ?.toDouble() ??
                                    0,
                                //unratedColor: AppColors.mainWhiteColor,
                                glowColor: AppColors.mainYellowColor,
                                itemSize: screenWidth(20),
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                ignoreGestures: true,

                                itemBuilder: (context, _) => const Icon(
                                  Icons.star,
                                  color: AppColors.mainYellowColor,
                                ),
                                onRatingUpdate: (rating) {},
                              );
                            },
                          ),
                        ],
                      ),
                      screenWidth(20).ph,
                      Obx(
                        () {
                          return CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: controller.clubInfo.value.history ?? "",
                            textColor: AppColors.mainBlackColor,
                          );
                        },
                      ),
                      screenWidth(20).ph,
                      Center(
                        child: Obx(
                          () {
                            return controller.isClubInfoLoading
                                ? const ButtonShimmer()
                                : controller.clubInfo.value.name == null
                                    ? CustomText(
                                        textType: TextStyleType.CUSTOM,
                                        text: tr('key_No_Internet_Connection'),
                                        textColor: AppColors.mainRedColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: screenWidth(20),
                                      )
                                    : CustomButton(
                                        text: tr('key_Rate_Club'),
                                        onPressed: () {
                                          storage.getUserId() == 0
                                              ? Get.defaultDialog(
                                                  title:
                                                      tr('key_login_complete'),
                                                  titleStyle: const TextStyle(
                                                      color: AppColors
                                                          .mainYellowColor),
                                                  content: Column(
                                                    children: [
                                                      CustomButton(
                                                        text: tr('key_Yes'),
                                                        onPressed: () async {
                                                          storage
                                                              .setCartList([]);
                                                          storage
                                                              .setFavoriteList(
                                                                  []);
                                                          storage
                                                              .setFirstLanuch(
                                                                  true);
                                                          storage.setIsLoggedIN(
                                                              false);
                                                          storage
                                                              .globalSharedPrefs
                                                              .remove(storage
                                                                  .PREF_TOKEN_INFO);
                                                          //storage.setTokenInfo(null);
                                                          await storage
                                                              .setUserId(0);
                                                          storage
                                                              .globalSharedPrefs
                                                              .remove(storage
                                                                  .PREF_USER_NAME);

                                                          storage
                                                              .setGoogle(false);
                                                          storage
                                                              .globalSharedPrefs
                                                              .remove(storage
                                                                  .PREF_MOBILE_NUMBER);
                                                          storage
                                                              .globalSharedPrefs
                                                              .remove(storage
                                                                  .PREF_EMAIL);

                                                          Get.offAll(() =>
                                                              const SignInView());
                                                        },
                                                        widthButton: 4,
                                                        circularBorder:
                                                            screenWidth(10),
                                                      ),
                                                      screenWidth(20).ph,
                                                      CustomButton(
                                                        text: tr('key_No'),
                                                        onPressed: () {
                                                          Get.back();
                                                        },
                                                        widthButton: 4,
                                                        circularBorder:
                                                            screenWidth(10),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              : Get.bottomSheet(
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      color: AppColors
                                                          .mainWhiteColor,
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        topLeft:
                                                            Radius.circular(
                                                                screenWidth(8)),
                                                        topRight:
                                                            Radius.circular(
                                                                screenWidth(8)),
                                                      ),
                                                    ),
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsetsDirectional
                                                              .only(
                                                                  top:
                                                                      screenHeight(
                                                                          30)),
                                                      child: Padding(
                                                        padding: EdgeInsetsDirectional
                                                            .symmetric(
                                                                horizontal:
                                                                    screenWidth(
                                                                        20)),
                                                        child: ListView(
                                                          children: [
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                CustomText(
                                                                  fontSize:
                                                                      screenWidth(
                                                                          20),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  textType:
                                                                      TextStyleType
                                                                          .CUSTOM,
                                                                  text: tr(
                                                                      'key_Rate'),
                                                                  textColor:
                                                                      AppColors
                                                                          .mainYellowColor,
                                                                ),
                                                                RatingBar
                                                                    .builder(
                                                                  initialRating:
                                                                      1,
                                                                  //unratedColor: AppColors.mainWhiteColor,
                                                                  glowColor:
                                                                      AppColors
                                                                          .mainYellowColor,
                                                                  itemSize:
                                                                      screenWidth(
                                                                          20),
                                                                  minRating: 1,
                                                                  direction: Axis
                                                                      .horizontal,
                                                                  allowHalfRating:
                                                                      false,
                                                                  itemCount: 5,

                                                                  itemBuilder:
                                                                      (context,
                                                                              _) =>
                                                                          const Icon(
                                                                    Icons.star,
                                                                    color: AppColors
                                                                        .mainYellowColor,
                                                                  ),
                                                                  onRatingUpdate:
                                                                      (rating) {
                                                                    controller
                                                                            .rating
                                                                            .value =
                                                                        rating;
                                                                  },
                                                                ),
                                                              ],
                                                            ),
                                                            screenWidth(20).ph,
                                                            CustomText(
                                                              fontSize:
                                                                  screenWidth(
                                                                      20),
                                                              textAlign:
                                                                  TextAlign
                                                                      .start,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              textType:
                                                                  TextStyleType
                                                                      .CUSTOM,
                                                              text: tr(
                                                                  'key_Your_Comment'),
                                                              textColor: AppColors
                                                                  .mainYellowColor,
                                                            ),
                                                            CustomTextField(
                                                              hintText: "",
                                                              controller: controller
                                                                  .commentController,
                                                              maxLines: 5,
                                                              maxLength: 150,
                                                              borderColor: AppColors
                                                                  .mainYellowColor,
                                                              contentPaddingLeft:
                                                                  screenWidth(
                                                                      20),
                                                              contentPaddingTop:
                                                                  screenWidth(
                                                                      20),
                                                              contentPaddingBottom:
                                                                  screenWidth(
                                                                      20),
                                                              contentPaddingRight:
                                                                  screenWidth(
                                                                      20),
                                                            ),
                                                            CustomButton(
                                                              text: tr(
                                                                  'key_Send'),
                                                              onPressed: () {
                                                                controller
                                                                    .addComment();
                                                              },
                                                              widthButton: 2,
                                                              heightButton: 10,
                                                              circularBorder:
                                                                  screenWidth(
                                                                      10),
                                                            ),
                                                            Obx(
                                                              () {
                                                                return controller
                                                                        .clubInfo
                                                                        .value
                                                                        .reviews!
                                                                        .isEmpty
                                                                    ? const SizedBox
                                                                        .shrink()
                                                                    : Column(
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment.start,
                                                                        children: [
                                                                          CustomText(
                                                                            fontSize:
                                                                                screenWidth(20),
                                                                            fontWeight:
                                                                                FontWeight.bold,
                                                                            textType:
                                                                                TextStyleType.CUSTOM,
                                                                            text:
                                                                                tr('key_Comments'),
                                                                            textColor:
                                                                                AppColors.mainYellowColor,
                                                                          ),
                                                                          ListView
                                                                              .builder(
                                                                            shrinkWrap:
                                                                                true,
                                                                            scrollDirection:
                                                                                Axis.vertical,
                                                                            physics:
                                                                                const NeverScrollableScrollPhysics(),
                                                                            itemCount:
                                                                                controller.clubInfo.value.reviews!.length,
                                                                            itemBuilder:
                                                                                (BuildContext context, int index) {
                                                                              return controller.clubInfo.value.reviews![index].description == null
                                                                                  ? const SizedBox.shrink()
                                                                                  : Column(
                                                                                      children: [
                                                                                        Row(
                                                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                          children: [
                                                                                            CustomText(
                                                                                              fontSize: screenWidth(20),
                                                                                              textType: TextStyleType.CUSTOM,
                                                                                              text: controller.clubInfo.value.reviews![index].userName == null ? tr('key_member') : "${controller.clubInfo.value.reviews![index].userName!.name!}:",
                                                                                              textColor: AppColors.mainBlueColor,
                                                                                            ),
                                                                                            RatingBar.builder(
                                                                                              initialRating: controller.clubInfo.value.reviews![index].rating!.toDouble(),
                                                                                              //unratedColor: AppColors.mainWhiteColor,
                                                                                              glowColor: AppColors.mainYellowColor,
                                                                                              itemSize: screenWidth(20),
                                                                                              minRating: 1,
                                                                                              direction: Axis.horizontal,
                                                                                              allowHalfRating: false,
                                                                                              itemCount: 5,
                                                                                              ignoreGestures: true,

                                                                                              itemBuilder: (context, _) => const Icon(
                                                                                                Icons.star,
                                                                                                color: AppColors.mainYellowColor,
                                                                                              ),
                                                                                              onRatingUpdate: (rating) {
                                                                                                controller.rating.value = rating;
                                                                                              },
                                                                                            ),
                                                                                          ],
                                                                                        ),
                                                                                        SizedBox(
                                                                                          width: double.infinity,
                                                                                          child: Card(
                                                                                            elevation: 10,
                                                                                            shadowColor: AppColors.mainYellowColor,
                                                                                            margin: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(30), vertical: screenWidth(30)),
                                                                                            child: Padding(
                                                                                              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20), vertical: screenWidth(20)),
                                                                                              child: CustomText(
                                                                                                textType: TextStyleType.CUSTOM,
                                                                                                textAlign: TextAlign.start,
                                                                                                text: controller.clubInfo.value.reviews![index].description!,
                                                                                                textColor: AppColors.mainBlackColor,
                                                                                              ),
                                                                                            ),
                                                                                          ),
                                                                                        ),
                                                                                      ],
                                                                                    );
                                                                            },
                                                                          ),
                                                                        ],
                                                                      );
                                                              },
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                );
                                        },
                                        widthButton: 2,
                                        heightButton: 10,
                                        circularBorder: screenWidth(10),
                                      );
                          },
                        ),
                      ),
                      (screenWidth(20)).ph,
                      Center(
                        child: CustomText(
                          textType: TextStyleType.CUSTOM,
                          fontSize: screenWidth(20),
                          text: tr('key_REPORT_CLUB'),
                          textColor: AppColors.mainYellowColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      CustomTextField(
                        hintText: "",
                        controller: controller.reportController,
                        maxLines: 15,
                        borderColor: AppColors.mainYellowColor,
                        contentPaddingLeft: screenWidth(20),
                        contentPaddingTop: screenWidth(20),
                        contentPaddingBottom: screenWidth(20),
                        contentPaddingRight: screenWidth(20),
                        validator: (value) {
                          return value!.isEmpty
                              ? tr('key_Please_Check_Your_Report')
                              : null;
                        },
                      ),
                      screenWidth(20).ph,
                      Center(
                        child: CustomButton(
                          text: tr('key_SUBMIT'),
                          circularBorder: screenWidth(10),
                          onPressed: () {
                            storage.getUserId() == 0
                                ? Get.defaultDialog(
                                    title: tr('key_login_complete'),
                                    titleStyle: const TextStyle(
                                        color: AppColors.mainYellowColor),
                                    content: Column(
                                      children: [
                                        CustomButton(
                                          text: tr('key_Yes'),
                                          onPressed: () async {
                                            storage.setCartList([]);
                                            storage.setFavoriteList([]);
                                            storage.setFirstLanuch(true);
                                            storage.setIsLoggedIN(false);
                                            storage.globalSharedPrefs.remove(
                                                storage.PREF_TOKEN_INFO);
                                            //storage.setTokenInfo(null);
                                            await storage.setUserId(0);
                                            storage.globalSharedPrefs
                                                .remove(storage.PREF_USER_NAME);

                                            storage.setGoogle(false);
                                            storage.globalSharedPrefs.remove(
                                                storage.PREF_MOBILE_NUMBER);
                                            storage.globalSharedPrefs
                                                .remove(storage.PREF_EMAIL);

                                            Get.offAll(
                                                () => const SignInView());
                                          },
                                          widthButton: 4,
                                          circularBorder: screenWidth(10),
                                        ),
                                        screenWidth(20).ph,
                                        CustomButton(
                                          text: tr('key_No'),
                                          onPressed: () {
                                            Get.back();
                                          },
                                          widthButton: 4,
                                          circularBorder: screenWidth(10),
                                        ),
                                      ],
                                    ),
                                  )
                                : controller.addReport();
                          },
                          heightButton: 8,
                          widthButton: 2,
                        ),
                      ),
                      // CustomText(
                      //   fontSize: screenWidth(20),
                      //   textType: TextStyleType.CUSTOM,
                      //   text: "SPONSOR",
                      //   textColor: AppColors.mainYellowColor,
                      // ),
                    ],
                  ),
                ),
                // CustomSpon(),
                // CustomContact()
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
