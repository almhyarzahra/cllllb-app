import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/user_job_details_view/user_job_details_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/data/models/apis/looking_job_model.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';
import '../photo_view/photo_view.dart';
import '../sign_in_view/sign_in_view.dart';
import '../user_communication_view/user_communication_view.dart';

class UserJobDetailsView extends StatefulWidget {
  const UserJobDetailsView({super.key, required this.currentlocation, required this.lookingJob});
  final LocationData? currentlocation;
  final LookingJobModel lookingJob;

  @override
  State<UserJobDetailsView> createState() => _UserJobDetailsViewState();
}

class _UserJobDetailsViewState extends State<UserJobDetailsView> {
  late UserJobDetailsController controller;
  @override
  void initState() {
    controller = Get.put(UserJobDetailsController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
              ],
            ),
            InkWell(
              onTap: () {
                Get.to(
                  PhotoWidgetView(
                    imageUrl: [widget.lookingJob.user!.image ?? ""],
                  ),
                );
              },
              child: Container(
                width: screenWidth(5),
                height: screenHeight(8),
                decoration: BoxDecoration(
                  border: Border.all(color: AppColors.mainYellowColor),
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    fit: BoxFit.contain,
                    image: CachedNetworkImageProvider(
                        Uri.https(NetworkUtil.baseUrl, widget.lookingJob.user!.image ?? "")
                            .toString()),
                  ),
                ),
              ),
            ),
            screenWidth(15).ph,
            Padding(
              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: widget.lookingJob.user!.name!,
                    textColor: AppColors.mainBlackColor,
                  ),
                  screenWidth(20).ph,
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: tr('key_job_title'),
                    textColor: AppColors.mainYellowColor,
                    fontSize: screenWidth(20),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: widget.lookingJob.jobTitle!,
                    textColor: AppColors.mainBlackColor,
                  ),
                  screenWidth(20).ph,
                  Row(
                    children: [
                      CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: "${widget.lookingJob.yearExperince!} ",
                        textColor: AppColors.mainYellowColor,
                      ),
                      Flexible(
                        child: CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('key_years_experience'),
                          textColor: AppColors.mainBlackColor,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                  screenWidth(20).ph,
                  Row(
                    children: [
                      CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: tr('key_number'),
                        textColor: AppColors.mainYellowColor,
                      ),
                      Flexible(
                        child: CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: widget.lookingJob.user!.phone ?? tr('key_not_found'),
                          textColor: AppColors.mainGreyColor,
                        ),
                      ),
                    ],
                  ),
                  screenWidth(20).ph,
                  Row(
                    children: [
                      CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: tr('key_mail'),
                        textColor: AppColors.mainYellowColor,
                      ),
                      Flexible(
                        child: CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: widget.lookingJob.user!.email!,
                          textColor: AppColors.mainGreyColor,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                  screenWidth(20).ph,
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: tr('key_skills'),
                    textColor: AppColors.mainYellowColor,
                    fontSize: screenWidth(20),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: widget.lookingJob.skills ?? tr('key_not_found'),
                    textColor: AppColors.mainGreyColor,
                  ),
                  screenWidth(20).ph,
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: tr('key_educations'),
                    textColor: AppColors.mainYellowColor,
                    fontSize: screenWidth(20),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: widget.lookingJob.education ?? tr('key_not_found'),
                    textColor: AppColors.mainGreyColor,
                  ),
                  screenWidth(20).ph,
                ],
              ),
            ),
            Obx(
              () {
                return controller.loader.value
                    ? Column(
                        children: [
                          const CircularProgressIndicator(),
                          CustomText(
                            textColor: AppColors.mainBlackColor,
                            fontSize: screenWidth(35),
                            textType: TextStyleType.CUSTOM,
                            text: controller.progress.value.toString(),
                          ),
                        ],
                      )
                    : Padding(
                        padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(5)),
                        child: CustomButton(
                          text: tr('key_download_cv'),
                          onPressed: () {
                            controller.loader.value = true;
                            controller.downloadCV(
                              url: Uri.https(NetworkUtil.baseUrl, widget.lookingJob.cv!).toString(),
                              name: widget.lookingJob.user!.name!,
                            );
                          },
                          circularBorder: screenWidth(20),
                        ),
                      );
              },
            ),
            screenWidth(20).ph,
            Padding(
              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(3)),
              child: CustomButton(
                text: tr('key_communication'),
                onPressed: () {
                  storage.getUserId() == 0
                      ? Get.defaultDialog(
                          title: tr('key_login_complete'),
                          titleStyle: const TextStyle(color: AppColors.mainYellowColor),
                          content: Column(
                            children: [
                              CustomButton(
                                text: tr('key_Yes'),
                                onPressed: () async {
                                  storage.setCartList([]);
                                  storage.setFavoriteList([]);
                                  storage.setFirstLanuch(true);
                                  storage.setIsLoggedIN(false);
                                  storage.globalSharedPrefs.remove(storage.PREF_TOKEN_INFO);
                                  //storage.setTokenInfo(null);
                                  await storage.setUserId(0);
                                  storage.globalSharedPrefs.remove(storage.PREF_USER_NAME);

                                  storage.setGoogle(false);
                                  storage.globalSharedPrefs.remove(storage.PREF_MOBILE_NUMBER);
                                  storage.globalSharedPrefs.remove(storage.PREF_EMAIL);

                                  Get.offAll(() => const SignInView());
                                },
                                widthButton: 4,
                                circularBorder: screenWidth(10),
                              ),
                              screenWidth(20).ph,
                              CustomButton(
                                text: tr('key_No'),
                                onPressed: () {
                                  Get.back();
                                },
                                widthButton: 4,
                                circularBorder: screenWidth(10),
                              ),
                            ],
                          ),
                        )
                      : Get.to(
                          UserCommunicationView(
                            currentlocation: widget.currentlocation,
                            lookingJob: widget.lookingJob,
                          ),
                        );
                },
                heightButton: 8,
                textSize: screenWidth(38),
                circularBorder: screenWidth(20),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
