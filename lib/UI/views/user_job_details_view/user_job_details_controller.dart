import 'package:flutter/material.dart';
import 'package:flutter_file_downloader/flutter_file_downloader.dart';
import 'package:get/get.dart';
import '../../../core/enums/message_type.dart';
import '../../../core/services/base_controller.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_toast.dart';

class UserJobDetailsController extends BaseController {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxDouble progress = 0.0.obs;
  RxBool loader = false.obs;
  void downloadCV({required String url, required String name}) {
    FileDownloader.downloadFile(
        url: url,
        name: name,
        notificationType: NotificationType.all,
        onProgress: (String? fileName, double progress) {
          this.progress.value = progress;
        },
        onDownloadCompleted: (String path) {
          CustomToast.showMessage(
              message: tr('key_succsess_download'),
              messageType: MessageType.SUCCSESS);
          loader.value = false;
        },
        onDownloadError: (String error) {
          CustomToast.showMessage(
              message: "Error $error", messageType: MessageType.REJECTED);
          loader.value = false;
        });
  }
}
