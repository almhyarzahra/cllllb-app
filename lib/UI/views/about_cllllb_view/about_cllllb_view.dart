import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/about_cllllb_view/about_cllllb_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';

class AboutCllllbView extends StatefulWidget {
  const AboutCllllbView({super.key, required this.currentlocation});
  final LocationData? currentlocation;

  @override
  State<AboutCllllbView> createState() => _AboutCllllbViewState();
}

class _AboutCllllbViewState extends State<AboutCllllbView> {
  AboutCllllbController controller = AboutCllllbController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          children: [
            CustomBar(
              text: tr('key_About_Cllllb'),
            ),
            screenHeight(25).ph,
            Transform.scale(
              scale: 1.8,
              child: Image.asset(
                "images/cllllb.png",
                height: screenHeight(9),
              ),
            ),
            Padding(
              padding: EdgeInsetsDirectional.symmetric(
                  horizontal: screenWidth(15), vertical: screenHeight(15)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    textAlign: TextAlign.start,
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    text: tr('key_describtion'),
                  ),
                  CustomText(
                    textAlign: TextAlign.start,
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    text: tr('key_communicating'),
                  ),
                  CustomText(
                    textAlign: TextAlign.start,
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    text: tr('key_saving'),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    textAlign: TextAlign.start,
                    text: tr('key_electronically'),
                  ),
                  CustomText(
                    textAlign: TextAlign.start,
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    text: tr('key_ofSaving'),
                  ),
                  CustomText(
                    textAlign: TextAlign.start,
                    textType: TextStyleType.CUSTOM,
                    text: tr('key_All_Concerned'),
                    textColor: AppColors.mainBlackColor,
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    textColor: AppColors.mainBlackColor,
                    textAlign: TextAlign.start,
                    text: tr('key_All Services'),
                  ),
                ],
              ),
            ),
            //CustomContact()
          ],
        ),
      ),
    );
  }
}
