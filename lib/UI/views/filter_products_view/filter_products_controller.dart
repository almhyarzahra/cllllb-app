import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/data/models/apis/all_products_model.dart';
import '../main_view/home_view/home_controller.dart';

class FilterProductsController extends BaseController {
  FilterProductsController({required List<AllProductsModel> products}) {
    this.products.value = products;
  }
  RxList<AllProductsModel> products = <AllProductsModel>[].obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  TextEditingController lowestPriceController = TextEditingController();
  TextEditingController highestPriceController = TextEditingController();
  RxList<AllProductsModel> productsChanged = <AllProductsModel>[].obs;
  RxList<AllProductsModel> test = <AllProductsModel>[].obs;
  RxList<AllProductsModel> test1 = <AllProductsModel>[].obs;
  RxList<AllProductsModel> test2 = <AllProductsModel>[].obs;

  RxSet<String> clubs = <String>{}.obs;
  RxList<bool> isCheckedClub = <bool>[].obs;
  RxList<String> finalNameClub = <String>["All Clubs"].obs;
  RxBool isCheckedAllPrice = true.obs;
  RxSet<String> type = <String>{}.obs;
  RxList<bool> isCheckedType = <bool>[].obs;
  RxList<String> finalType = <String>["All Type"].obs;
  @override
  void onClose() {
    Get.find<HomeController>().getAllProducts();
    super.onClose();
  }

  @override
  void onInit() {
    productsChanged = products;
    getInformation();

    super.onInit();
  }

  void getInformation() {
    products.map((element) {
      if (element.club != null) {
        clubs.add(element.club!.name!);
      }
      element.productCategory!.map((e) {
        type.add(e.categoryInfo!.name!);
      }).toSet();
    }).toSet();

    type.map((element) {
      isCheckedType.add(false);
    }).toSet();
    type.add("All Type");
    isCheckedType.add(true);

    clubs.map((element) {
      isCheckedClub.add(false);
    }).toSet();
    clubs.add("All Clubs");

    isCheckedClub.add(true);
  }

  void addClubToFilter({required String nameClub}) {
    if (finalNameClub.contains(nameClub)) {
      finalNameClub.remove(nameClub);
    } else {
      finalNameClub.add(nameClub);
    }
    //print(finalNameClub);
  }

  void addTypeToFilter({required String type}) {
    if (finalType.contains(type)) {
      finalType.remove(type);
    } else {
      finalType.add(type);
    }
    //print(finalType);
  }

  Future<void> filter() async {
    if (!finalNameClub.contains("All Clubs")) {
      finalNameClub.map((e) {
        productsChanged.map((element) {
          if (element.club != null && element.club!.name == e) {
            test1.add(element);
          }
        }).toSet();
      }).toSet();
      productsChanged = test1;
    }
    if (!finalType.contains("All Type")) {
      finalType.map((el) {
        productsChanged.map((element) {
          element.productCategory!.map((e) {
            if (e.categoryInfo!.name! == el) test.add(element);
          }).toSet();
        }).toSet();
      }).toSet();
      productsChanged = test;
    }
    if (isCheckedAllPrice.isFalse &&
        lowestPriceController.text.isNotEmpty &&
        highestPriceController.text.isNotEmpty) {
      productsChanged.map((element) {
        if (element.price! >= double.parse(lowestPriceController.text.trim().toString()) &&
            element.price! <= double.parse(highestPriceController.text.trim().toString())) {
          test2.add(element);
        }
      }).toSet();
      productsChanged = test2;
    }
  }
}
