import 'package:com.tad.cllllb/UI/views/all_products_view/all_products_view.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/data/models/apis/all_products_model.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/custom_widgets/custom_text_field.dart';
import '../../shared/utils.dart';
import 'filter_products_controller.dart';

class FilterProductsView extends StatefulWidget {
  const FilterProductsView({super.key, required this.currentlocation, required this.products});
  final LocationData? currentlocation;
  final List<AllProductsModel> products;

  @override
  State<FilterProductsView> createState() => _FilterProductsViewState();
}

class _FilterProductsViewState extends State<FilterProductsView> {
  late FilterProductsController controller;
  @override
  void initState() {
    controller = Get.put(FilterProductsController(products: widget.products));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsetsDirectional.symmetric(
                  horizontal: screenWidth(75), vertical: screenWidth(25)),
              child: Card(
                elevation: 4,
                child: Column(
                  children: [
                    Obx(
                      () {
                        return Padding(
                          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(10)),
                          child: DropdownButton(
                            hint: Padding(
                              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(8)),
                              child: CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_Filter_By_Clubs'),
                                textColor: AppColors.mainYellowColor,
                              ),
                            ),
                            items: controller.clubs.map(
                              (element) {
                                return DropdownMenuItem(
                                  value: element,
                                  child: Obx(
                                    () {
                                      return Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: element,
                                            textColor: AppColors.mainBlackColor,
                                          ),
                                          Checkbox(
                                            value: controller.isCheckedClub[
                                                controller.clubs.toList().indexOf(element)],
                                            onChanged: (value) {
                                              controller.isCheckedClub[controller.clubs
                                                  .toList()
                                                  .indexOf(element)] = value!;
                                              controller.addClubToFilter(nameClub: element);
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                );
                              },
                            ).toList(),
                            onChanged: (value) {},
                          ),
                        );
                      },
                    ),
                    screenWidth(30).ph,
                    Obx(
                      () {
                        return Padding(
                          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(10)),
                          child: DropdownButton(
                            hint: Padding(
                              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(5)),
                              child: CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_Filter_By_Type'),
                                textColor: AppColors.mainYellowColor,
                              ),
                            ),
                            items: controller.type.map(
                              (element) {
                                return DropdownMenuItem(
                                  value: element,
                                  child: Obx(
                                    () {
                                      return Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: element,
                                            textColor: AppColors.mainBlackColor,
                                          ),
                                          Checkbox(
                                            value: controller.isCheckedType[
                                                controller.type.toList().indexOf(element)],
                                            onChanged: (value) {
                                              controller.isCheckedType[controller.type
                                                  .toList()
                                                  .indexOf(element)] = value!;
                                              controller.addTypeToFilter(type: element);
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                );
                              },
                            ).toList(),
                            onChanged: (value) {},
                          ),
                        );
                      },
                    ),
                    screenWidth(15).ph,
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_filter_price'),
                      textColor: AppColors.mainYellowColor,
                      textAlign: TextAlign.start,
                      fontWeight: FontWeight.bold,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 1,
                          child: CustomTextField(
                            hintText: tr('key_lowest_price'),
                            controller: controller.lowestPriceController,
                            contentPaddingLeft: screenWidth(10),
                            typeInput: TextInputType.number,
                          ),
                        ),
                        screenWidth(50).pw,
                        Expanded(
                          flex: 1,
                          child: CustomTextField(
                            hintText: tr('key_highest_price'),
                            controller: controller.highestPriceController,
                            contentPaddingLeft: screenWidth(10),
                            typeInput: TextInputType.number,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('key_all_price'),
                          textColor: AppColors.mainBlackColor,
                        ),
                        Obx(
                          () {
                            return Checkbox(
                              value: controller.isCheckedAllPrice.value,
                              onChanged: (value) {
                                controller.isCheckedAllPrice.value = value!;
                              },
                            );
                          },
                        ),
                      ],
                    ),
                    screenWidth(20).ph,
                    Padding(
                      padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                      child: CustomButton(
                        circularBorder: screenWidth(20),
                        text: tr('key_Finish'),
                        onPressed: () async {
                          await controller.filter();

                          Get.off(
                            AllProductsView(
                                type: "app",
                                currentlocation: widget.currentlocation,
                                //allProducts: controller.productsChanged,
                                ),
                          );
                        },
                      ),
                    ),
                    screenWidth(12).ph,
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
