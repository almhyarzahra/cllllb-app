import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/views/food_drink_view/food_drink_view.dart';
import 'package:com.tad.cllllb/UI/views/online_store_view/online_store_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:lottie/lottie.dart';
import '../../../core/data/models/apis/online_store_model.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/utils.dart';

class OnlineStoreView extends StatefulWidget {
  const OnlineStoreView({super.key, required this.onlineStore, required this.currentlocation});
  final List<OnlineStoreModel> onlineStore;
  final LocationData? currentlocation;
  @override
  State<OnlineStoreView> createState() => _OnlineStoreViewState();
}

class _OnlineStoreViewState extends State<OnlineStoreView> {
  OnlineStoreController controller = OnlineStoreController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
         bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
                screenWidth(15).pw,
                storage.getAppLanguage() == 'ar'
                    ? const CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: "منتجات Cllllb على الانترنت",
                        textColor: AppColors.mainBlackColor,
                      )
                    : Image.asset("images/cllllb_text.png"),
              ],
            ),
            screenWidth(20).ph,
            GridView.count(
              childAspectRatio: 0.6,
              crossAxisCount: 2,
              crossAxisSpacing: 0,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              children: widget.onlineStore.map(
                (e) {
                  return Column(
                    children: [
                      InkWell(
                        onTap: () {
                          Get.to(
                            FoodDrinkView(
                                currentlocation: widget.currentlocation,
                                clubType: null,
                                nameClub: null,
                                address: null,
                                imageClub: null,
                                clubId: 0,
                                categoryId: e.id!),
                          );
                        },
                        child: CachedNetworkImage(
                          imageUrl: Uri.https(NetworkUtil.baseUrl, e.image ?? '').toString(),
                          placeholder: (context, url) => Lottie.asset("images/download.json"),
                          errorWidget: (context, url, error) => const Icon(Icons.error),
                          width: screenWidth(2),
                          height: screenHeight(3),
                        ),
                      ),
                      CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: e.name ?? '',
                        textColor: AppColors.mainGreyColor,
                        fontSize: screenWidth(22),
                      ),
                    ],
                  );
                },
              ).toList(),
            )
          ],
        ),
      ),
    );
  }
}
