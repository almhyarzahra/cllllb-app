import 'dart:developer';

import 'package:com.tad.cllllb/UI/views/lottery_result_view/lottery_result_controller.dart';
import 'package:com.tad.cllllb/core/data/models/apis/lottery_result_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FilterLotteryResultController extends BaseController {
  FilterLotteryResultController({
    required List<LotteryResultModel> lotteryResultModel,
  }) {
    lotteries.value = lotteryResultModel;
  }
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<LotteryResultModel> lotteries =
      <LotteryResultModel>[LotteryResultModel()].obs;
  bool get isMoreResultLoading =>
      listType.contains(OperationType.filterMoreResult);
  RxInt pageKey = 2.obs;
  RxBool getMore = true.obs;
  RxBool enableScrollListner = true.obs;

  Future<void> getMoreLottary({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
        type: OperationType.filterMoreResult,
        function: UserRepository()
            .getLotteryResult(
          page: page,
          date: Get.find<LotterResultController>().date.value.isEmpty
              ? null
              : Get.find<LotterResultController>().date.value,
          winner: Get.find<LotterResultController>().myLottery.value,
        )
            .then((value) {
          value.fold((l) {
            pageKey.value = pageKey.value - 1;
          }, (r) {
            if (r.isEmpty) {
              enableScrollListner.value = false;
            } else {
              lotteries.addAll(r);
            }
          });
        })).then((value) => getMore.value = true);
  }
}
