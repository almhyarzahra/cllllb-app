import 'dart:developer';

import 'package:com.tad.cllllb/UI/views/filter_all_products/filter_all_products.dart';
import 'package:com.tad.cllllb/UI/views/main_view/home_view/home_controller.dart';
import 'package:com.tad.cllllb/core/data/repositories/club_repositories.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/data/models/apis/all_products_model.dart';
import '../../../core/enums/message_type.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/general_util.dart';
import '../../shared/custom_widgets/custom_toast.dart';

class AllProductsController extends BaseController {
  AllProductsController({
    //required List<AllProductsModel> allProducts,
    this.currentlocation,
  }) {
    //this.allProducts.value = allProducts;
  }
  LocationData? currentlocation;

  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  Rx<String> goal = "".obs;
  TextEditingController searchController = TextEditingController();
  RxList<AllProductsModel> allProducts =
      <AllProductsModel>[AllProductsModel()].obs;
  RxList<AllProductsModel> resultSearch = <AllProductsModel>[].obs;
  RxList<AllProductsModel> resultsPage = <AllProductsModel>[].obs;
  TextEditingController lowestPriceController = TextEditingController();
  TextEditingController highestPriceController = TextEditingController();
  RxInt displayIndex = 0.obs;
  bool get isAllProductsLoading =>
      listType.contains(OperationType.ALL_PRODUCTS);
  bool get isMoreProductsLoading =>
      listType.contains(OperationType.moreProducts);
  RxInt pageKey = 2.obs;
  RxBool enableScrollListner = true.obs;
  RxBool getMore = true.obs;
  RxList<int> clubIds = <int>[].obs;
  RxList<bool> isCheckedClub = <bool>[].obs;

  @override
  void onInit() {
    getAllProducts();
    Get.find<HomeController>().clubsModel.map((element) {
      isCheckedClub.add(false);
    }).toSet();
    // log(currentlocation.toString());
    // resultSearch.value = allProducts;
    // log(resultSearch.length.toString());
    // if (resultSearch.length <= 12) {
    //   resultsPage.value = resultSearch;
    // } else {
    //   resultsPage.value = resultSearch.sublist(0, 12);
    // }
    super.onInit();
  }

  void addClubToFilter({required int clubId}) {
    if (clubIds.contains(clubId)) {
      clubIds.remove(clubId);
    } else {
      clubIds.add(clubId);
    }
    print(clubIds);
  }
  // List<AllProductsModel> search() {
  //   if (goal.isEmpty) {
  //     resultSearch.value = allProducts;
  //   } else {
  //     resultSearch.value =
  //         allProducts.where((p0) => p0.name!.toLowerCase().contains(goal.toLowerCase())).toList();
  //   }
  //   if (resultSearch.length <= 12) {
  //     resultsPage.value = resultSearch;
  //   } else {
  //     resultsPage.value = resultSearch.sublist(0, 12);
  //   }
  //   return resultSearch;
  // }

  void addTofavorite({required AllProductsModel productsModel}) {
    if (favoriteService.getCartModel(productsModel) == null) {
      favoriteService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Favorite'),
                messageType: MessageType.SUCCSESS);
          });
    } else {
      CustomToast.showMessage(
          message: productsModel.name! + tr('key_already_in_your_favourites'),
          messageType: MessageType.INFO);
    }
  }

  void addToCart({required AllProductsModel productsModel}) {
    if (currentlocation == null) {
      CustomToast.showMessage(
          message: tr('key_Please_turn_on_location_feature'),
          messageType: MessageType.INFO);
    } else if (productsModel.inProduct == 1 &&
        calculateDistance(
                currentlocation!.latitude!,
                currentlocation!.longitude!,
                double.parse(productsModel.club!.latitude!),
                double.parse(productsModel.club!.longtitude!)) >
            100.0) {
      CustomToast.showMessage(
          message: tr('key_within_100_metres'), messageType: MessageType.INFO);
    } else {
      cartService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Card'),
                messageType: MessageType.SUCCSESS);
          });
    }
  }

  double calculateDistance(double lat1, double lng1, double lat2, double lng2) {
    double distance = Geolocator.distanceBetween(lat1, lng1, lat2, lng2);
    return distance;
  }

  void getAllProducts() {
    runLoadingFutureFunction(
        type: OperationType.ALL_PRODUCTS,
        function: ClubRepositories().getClubProcucts(page: 1).then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: tr('key_No_Internet_Connection'),
                messageType: MessageType.REJECTED);
          }, (r) {
            if (allProducts.isNotEmpty) {
              allProducts.clear();
            }
            allProducts.addAll(r);
            allProducts.map((element) {
              element.message = "l";
            }).toSet();
          });
        }));
  }

  Future<void> getMoreProducts({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
        type: OperationType.moreProducts,
        function: ClubRepositories().getClubProcucts(page: page).then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: tr('key_No_Internet_Connection'),
                messageType: MessageType.REJECTED);
            pageKey.value = pageKey.value - 1;
          }, (r) {
            if (r.isEmpty) {
              enableScrollListner.value = false;
            } else {
              allProducts.addAll(r);
            }
          });
        })).then((value) => getMore.value = true);
  }

  void filter() {
    runFullLoadingFutureFunction(
      function: ClubRepositories()
          .filterAllProducts(
        page: 1,
        clubId: clubIds.isEmpty ? null : clubIds,
        maxPrice: highestPriceController.text.trim().isEmpty
            ? null
            : double.parse(highestPriceController.text.trim()),
        minPrice: lowestPriceController.text.trim().isEmpty
            ? null
            : double.parse(lowestPriceController.text.trim()),
      )
          .then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_try_again'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (r.isEmpty) {
                CustomToast.showMessage(
                    message: tr('noMatchingResults'),
                    messageType: MessageType.INFO);
              } else {
                Get.to(
                  FilterAllProducts(
                    currentlocation: currentlocation,
                    allProducts: r,
                    clubId: clubIds,
                    maxPrice:highestPriceController.text.trim().isEmpty? null: double.parse(highestPriceController.text.trim()) ,
                    minPrice:lowestPriceController.text.trim().isEmpty?null: double.parse(lowestPriceController.text.trim()),
                  ),
                );
              }
            },
          );
        },
      ),
    );
  }
}
