import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/main_view/home_view/home_controller.dart';
import 'package:com.tad.cllllb/UI/views/search_all_products/search_all_products.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:com.tad.cllllb/core/utils/network_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/translation/app_translation.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../food_sport_detalis_view/food-sport_detalis_view.dart';
import 'all_products_controller.dart';

class AllProductsView extends StatefulWidget {
  const AllProductsView(
      {super.key,
      //required this.allProducts,
      required this.type,
      this.currentlocation});
  //final List<AllProductsModel> allProducts;
  final String type;
  final LocationData? currentlocation;
  @override
  State<AllProductsView> createState() => _AllProductsViewState();
}

class _AllProductsViewState extends State<AllProductsView> {
  late AllProductsController controller;

  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    controller = Get.put(AllProductsController(
        //allProducts: widget.allProducts,
        currentlocation: widget.currentlocation));
    scrollController.addListener(_scrollListener);

    super.initState();
  }

  void _scrollListener() async {
    if (controller.getMore.value) {
      if (controller.enableScrollListner.value &&
          scrollController.position.maxScrollExtent ==
              scrollController.offset &&
          !scrollController.position.outOfRange) {
        controller.getMore.value = false;
        await controller.getMoreProducts(page: controller.pageKey.value);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
         bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        // bottomNavigationBar: BottomAppBar(
        //   child: Row(
        //     mainAxisAlignment: MainAxisAlignment.spaceAround,
        //     children: [
        //       CustomTextButton(
        //         text: tr('key_previous'),
        //         colorText: AppColors.mainRedColor,
        //         fontWeight: FontWeight.bold,
        //         onPressed: () {
        //           if (controller.displayIndex.value - 12 >= 0) {
        //             controller.displayIndex.value -= 12;
        //             controller.resultsPage.value = controller.resultSearch
        //                 .sublist(controller.displayIndex.value,
        //                     controller.displayIndex.value + 12);
        //           }
        //         },
        //       ),
        //       CustomTextButton(
        //         text: tr('key_next'),
        //         colorText: AppColors.mainColorGreen,
        //         fontWeight: FontWeight.bold,
        //         fontSize: screenWidth(20),
        //         onPressed: () {
        //           //print('Reached the end of ListView!');
        //           if (controller.displayIndex.value + 24 <
        //               controller.resultSearch.length) {
        //             controller.displayIndex.value += 12;
        //             //print(controller.displayIndex.value);
        //             controller.resultsPage.value = controller.resultSearch
        //                 .sublist(controller.displayIndex.value,
        //                     controller.displayIndex.value + 12);
        //             // _scrollController.animateTo(
        //             //     _scrollController.position.minScrollExtent,
        //             //     duration: Duration(milliseconds: 500),
        //             //     curve: Curves.easeInOut);
        //           } else {
        //             controller.resultsPage.value = controller.resultSearch
        //                 .sublist(controller.displayIndex.value,
        //                     controller.resultSearch.length);
        //             // _scrollController.animateTo(
        //             //     _scrollController.position.minScrollExtent,
        //             //     duration: Duration(milliseconds: 10000),
        //             //     curve: Curves.easeInOut);
        //           }
        //         },
        //       ),
        //     ],
        //   ),
        // ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          controller: scrollController,
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    size: screenWidth(20),
                  ),
                ),
                Padding(
                  padding: EdgeInsetsDirectional.symmetric(
                      horizontal: screenWidth(20), vertical: screenWidth(20)),
                  child: CustomText(
                    textType: TextStyleType.TITLE,
                    text: widget.type == "app"
                        ? tr('key_New_Products_at_All_Clubs')
                        : tr('key_Special_Products_From_Clubs'),
                    textColor: AppColors.mainYellowColor,
                    textAlign: TextAlign.start,
                  ),
                ),
              ],
            ),
            screenWidth(50).ph,
            InkWell(
              onTap: () {
                Get.bottomSheet(
                  Container(
                    decoration: BoxDecoration(
                        color: AppColors.mainWhiteColor,
                        borderRadius: BorderRadius.circular(30)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Obx(
                          () {
                            return Padding(
                              padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(10)),
                              child: DropdownButton(
                                isExpanded: true,
                                hint: CustomText(
                                  textType: TextStyleType.CUSTOM,
                                  text: tr('key_Filter_By_Clubs'),
                                  textColor: AppColors.mainYellowColor,
                                ),
                                items:
                                    Get.find<HomeController>().clubsModel.map(
                                  (element) {
                                    return DropdownMenuItem(
                                      value: element.id,
                                      child: Obx(
                                        () {
                                          return Row(
                                            children: [
                                              Expanded(
                                                child: CustomText(
                                                  textType:
                                                      TextStyleType.CUSTOM,
                                                  text: element.name!,
                                                  textAlign: TextAlign.start,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  textColor:
                                                      AppColors.mainBlackColor,
                                                ),
                                              ),
                                              Checkbox(
                                                value: controller.isCheckedClub[
                                                    Get.find<HomeController>()
                                                        .clubsModel
                                                        .indexOf(element)],
                                                onChanged: (value) {
                                                  controller
                                                      .isCheckedClub[Get.find<
                                                          HomeController>()
                                                      .clubsModel
                                                      .indexOf(
                                                          element)] = value!;
                                                  controller.addClubToFilter(
                                                      clubId: element.id!);
                                                },
                                              ),
                                            ],
                                          );
                                        },
                                      ),
                                    );
                                  },
                                ).toList(),
                                onChanged: (value) {},
                              ),
                            );
                          },
                        ),
                        screenWidth(10).ph,
                        CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: tr('key_filter_price'),
                          textColor: AppColors.mainYellowColor,
                          textAlign: TextAlign.start,
                          fontWeight: FontWeight.bold,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 1,
                              child: CustomTextField(
                                hintText: tr('key_lowest_price'),
                                controller: controller.lowestPriceController,
                                contentPaddingLeft: screenWidth(10),
                                typeInput: TextInputType.number,
                              ),
                            ),
                            screenWidth(50).pw,
                            Expanded(
                              flex: 1,
                              child: CustomTextField(
                                hintText: tr('key_highest_price'),
                                controller: controller.highestPriceController,
                                contentPaddingLeft: screenWidth(10),
                                typeInput: TextInputType.number,
                              ),
                            ),
                          ],
                        ),
                        screenWidth(10).ph,
                        CustomButton(
                          circularBorder: screenWidth(20),
                          text: tr('key_Finish'),
                          onPressed: () async {
                            controller.filter();
                          },
                        ),
                      ],
                    ),
                  ),
                );
                // Get.off(
                //   FilterProductsView(
                //       currentlocation: widget.currentlocation,
                //       products: storage.getAllProductsList()),
                // );
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset("images/filter.svg"),
                  screenWidth(30).pw,
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: tr('key_Filter'),
                    textColor: AppColors.mainYellowColor,
                  ),
                ],
              ),
            ),
            screenWidth(50).ph,
            Padding(
              padding:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
              child: GestureDetector(
                onTap: () {
                  Get.to(SearchAllProducts(
                    currentlocation: widget.currentlocation,
                  ));
                },
                child: Container(
                  width: double.infinity,
                  height: 40,
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.mainGreyColor),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: tr('key_Search'),
                        textColor: AppColors.mainGreyColor,
                      ),
                      Container(
                        height: screenHeight(25),
                        width: screenWidth(10),
                        decoration: BoxDecoration(
                          color: AppColors.mainYellowColor,
                          borderRadius: BorderRadius.circular(screenWidth(10)),
                        ),
                        child: const Icon(
                          Icons.search,
                          color: AppColors.mainWhiteColor,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              //  CustomTextField(
              //   onChanged: (value) {
              //     controller.goal.value = value;
              //     controller.search();
              //   },
              //   hintText: tr('key_Search'),
              //   contentPaddingLeft: screenWidth(10),
              //   controller: controller.searchController,
              //   suffixIcon: Container(
              //     height: screenHeight(25),
              //     width: screenWidth(10),
              //     decoration: BoxDecoration(
              //       color: AppColors.mainYellowColor,
              //       borderRadius: BorderRadius.circular(screenWidth(10)),
              //     ),
              //     child: const Icon(
              //       Icons.search,
              //       color: AppColors.mainWhiteColor,
              //     ),
              //   ),
              // ),
            ),
            screenWidth(20).ph,
            Obx(
              () {
                return controller.isAllProductsLoading
                    ? const SpinKitCircle(color: AppColors.mainYellowColor)
                    : controller.allProducts.isEmpty
                        ? const SizedBox.shrink()
                        : controller.allProducts[0].message == null
                            ? customFaildConnection(onPressed: () {
                                controller.getAllProducts();
                              })
                            : GridView.count(
                                childAspectRatio: 0.55,
                                crossAxisCount: 2,
                                crossAxisSpacing: 2,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                children: controller.allProducts.map(
                                  (e) {
                                    return Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Card(
                                        elevation: 4,
                                        child: SizedBox(
                                          width: screenWidth(2.2),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Row(
                                                children: [
                                                  screenWidth(40).pw,
                                                  CustomNetworkImage(
                                                    context.image(
                                                        e.club!.mainImage!),
                                                    width: screenWidth(8.7),
                                                    height: screenHeight(10),
                                                    border: Border.all(
                                                        color: AppColors
                                                            .mainYellowColor),
                                                    shape: BoxShape.circle,
                                                  ),
                                                  screenWidth(40).pw,
                                                  Flexible(
                                                    child: Column(
                                                      children: [
                                                        CustomText(
                                                          textType:
                                                              TextStyleType
                                                                  .CUSTOM,
                                                          text:
                                                              "${e.club!.name}",
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          textColor: AppColors
                                                              .mainBlackColor,
                                                          fontSize:
                                                              screenWidth(25),
                                                        ),
                                                        CustomText(
                                                          textType:
                                                              TextStyleType
                                                                  .CUSTOM,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          text:
                                                              "${e.club!.city}",
                                                          textColor: AppColors
                                                              .mainYellowColor,
                                                          fontSize:
                                                              screenWidth(25),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              CustomText(
                                                textType: TextStyleType.CUSTOM,
                                                text: e.name!,
                                                overflow: TextOverflow.ellipsis,
                                                textColor:
                                                    AppColors.mainBlackColor,
                                              ),
                                              if (e.pointsValue != null)
                                                CustomText(
                                                  textType:
                                                      TextStyleType.CUSTOM,
                                                  text:
                                                      "+${e.pointsValue} ${tr('rewardPoints')}",
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  fontSize: 12,
                                                  textColor:
                                                      AppColors.mainYellowColor,
                                                ),
                                              if (e.pointsValue == null)
                                                const SizedBox(height: 13),
                                              screenWidth(30).ph,
                                              InkWell(
                                                      onTap: () {
                                                        Get.to(
                                                            FoodSportDetalisView(
                                                          currentlocation:
                                                              controller
                                                                  .currentlocation,
                                                          productsModel: e,
                                                        ));
                                                      },
                                                      child: CachedNetworkImage(
                                                        memCacheWidth: 150,
                                                        memCacheHeight: 170,
                                                        // maxHeightDiskCache: 40,
                                                        // maxWidthDiskCache: 20,
                                                        imageUrl: Uri.https(
                                                                NetworkUtil
                                                                    .baseUrl,
                                                                e
                                                                    .img!)
                                                            .toString(),
                                                        placeholder: (context,
                                                                url) =>
                                                            Image.asset(
                                                                'images/cllllb.png'),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            Image.asset(
                                                                'images/cllllb.png'),
                                                        width: screenWidth(1),
                                                        height: screenHeight(7),
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                              // CustomNetworkImage(
                                              //   context.image(e.img!),
                                              //   width: screenWidth(1),
                                              //   height: screenHeight(7),
                                              //   memCacheWidth:100,
                                              //   memCacheHeight: 80,
                                              //   onTap: () => Get.to(
                                              //     FoodSportDetalisView(
                                              //       currentlocation:
                                              //           widget.currentlocation,
                                              //       productsModel: e,
                                              //     ),
                                              //   ),
                                              // ),
                                              screenWidth(25).ph,
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  InkWell(
                                                    onTap: () {
                                                      controller.addTofavorite(
                                                          productsModel: e);
                                                    },
                                                    child: SizedBox(
                                                      height: screenWidth(10),
                                                      width: screenWidth(10),
                                                      child: ClipRRect(
                                                        child: Card(
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          18)),
                                                          elevation: 4,
                                                          child: Center(
                                                            child: SvgPicture.asset(
                                                                "images/like.svg"),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  CustomText(
                                                    textType:
                                                        TextStyleType.CUSTOM,
                                                    textColor: AppColors
                                                        .mainYellowColor,
                                                    text: e.price.toString() +
                                                        tr('key_AED'),
                                                  ),
                                                  InkWell(
                                                    onTap: () {
                                                      controller.addToCart(
                                                          productsModel: e);
                                                    },
                                                    child: SizedBox(
                                                      height: screenWidth(10),
                                                      width: screenWidth(10),
                                                      child: ClipRRect(
                                                        child: Card(
                                                          shape:
                                                              RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18),
                                                          ),
                                                          elevation: 4,
                                                          child: Center(
                                                            child: SvgPicture.asset(
                                                                "images/shopping-cart.svg"),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ).toList(),
                              );
              },
            ),
            Obx(
              () {
                return controller.isMoreProductsLoading
                    ? const SpinKitCircle(
                        color: AppColors.mainYellowColor,
                      )
                    : const SizedBox.shrink();
              },
            ),
          ],
        ),
      ),
    );
  }
}
