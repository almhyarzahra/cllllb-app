import 'dart:developer';

import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/data/models/apis/all_products_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/club_repositories.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class FilterAllProductsController extends BaseController {
  FilterAllProductsController({
    this.currentlocation,
    required List<AllProductsModel> allProductsModel,
    required this.clubId,
    required this.maxPrice,
    required this.minPrice,
  }) {
    allProducts.value = allProductsModel;
  }
  List<int> clubId = [];
  double? maxPrice;
  double? minPrice;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<AllProductsModel> allProducts = <AllProductsModel>[].obs;
  RxBool getMore = true.obs;
  RxInt pageKey = 2.obs;
  RxBool enableScrollListner = true.obs;
  LocationData? currentlocation;
  bool get isMoreFilterLoading => listType.contains(OperationType.moreFilter);

  void addTofavorite({required AllProductsModel productsModel}) {
    if (favoriteService.getCartModel(productsModel) == null) {
      favoriteService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Favorite'),
                messageType: MessageType.SUCCSESS);
          });
    } else {
      CustomToast.showMessage(
          message: productsModel.name! + tr('key_already_in_your_favourites'),
          messageType: MessageType.INFO);
    }
  }

  void addToCart({required AllProductsModel productsModel}) {
    if (currentlocation == null) {
      CustomToast.showMessage(
          message: tr('key_Please_turn_on_location_feature'),
          messageType: MessageType.INFO);
    } else if (productsModel.inProduct == 1 &&
        calculateDistance(
                currentlocation!.latitude!,
                currentlocation!.longitude!,
                double.parse(productsModel.club!.latitude!),
                double.parse(productsModel.club!.longtitude!)) >
            100.0) {
      CustomToast.showMessage(
          message: tr('key_within_100_metres'), messageType: MessageType.INFO);
    } else {
      cartService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Card'),
                messageType: MessageType.SUCCSESS);
          });
    }
  }

  double calculateDistance(double lat1, double lng1, double lat2, double lng2) {
    double distance = Geolocator.distanceBetween(lat1, lng1, lat2, lng2);
    return distance;
  }

  Future<void> getMoreProducts({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
      type: OperationType.moreFilter,
      function: ClubRepositories()
          .filterAllProducts(
        page: page,
        clubId: clubId.isEmpty ? null : clubId,
        maxPrice: maxPrice,
        minPrice: minPrice,
      )
          .then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                message: tr('key_No_Internet_Connection'),
                messageType: MessageType.REJECTED,
              );
              pageKey.value = pageKey.value - 1;
            },
            (r) {
              if (r.isEmpty) {
                pageKey.value = pageKey.value - 1;
              } else {
                allProducts.addAll(r);
              }
            },
          );
        },
      ),
    ).then((value) => getMore.value = true);
  }
}
