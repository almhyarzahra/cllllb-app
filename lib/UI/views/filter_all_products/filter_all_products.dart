import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_app_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_drawer.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/filter_all_products/filter_all_products_controller.dart';
import 'package:com.tad.cllllb/UI/views/food_sport_detalis_view/food-sport_detalis_view.dart';
import 'package:com.tad.cllllb/core/data/models/apis/all_products_model.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class FilterAllProducts extends StatefulWidget {
  final LocationData? currentlocation;
  final List<AllProductsModel> allProducts;
  final List<int> clubId;
  final double? maxPrice;
  final double? minPrice;


  const FilterAllProducts(
      {super.key, required this.currentlocation, required this.allProducts, required this.clubId, required this.maxPrice, required this.minPrice});

  @override
  State<FilterAllProducts> createState() => _FilterAllProductsState();
}

class _FilterAllProductsState extends State<FilterAllProducts> {
  late FilterAllProductsController controller;
  ScrollController scrollController = ScrollController();
  @override
  void initState() {
    controller = Get.put(FilterAllProductsController(
      currentlocation: widget.currentlocation,
      allProductsModel: widget.allProducts,
      clubId: widget.clubId,
      maxPrice: widget.maxPrice,
      minPrice: widget.minPrice
    ));
    scrollController.addListener(_scrollListener);
    super.initState();
  }
   void _scrollListener() async {
    if (controller.getMore.value) {
      if (controller.enableScrollListner.value &&
          scrollController.position.maxScrollExtent ==
              scrollController.offset &&
          !scrollController.position.outOfRange) {
        controller.getMore.value = false;
        await controller.getMoreProducts(page: controller.pageKey.value);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: controller.key,
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: ListView(
          controller: scrollController,
          children: [
            Obx(
              () {
                return 
                // controller.isAllProductsLoading
                //     ? const SpinKitCircle(color: AppColors.mainYellowColor)
                //     : controller.allProducts.isEmpty
                //         ? const SizedBox.shrink()
                //         : controller.allProducts[0].message == null
                //             ? customFaildConnection(onPressed: () {
                //                 controller.getAllProducts();
                //               })
                //             :
                             GridView.count(
                                childAspectRatio: 0.6,
                                crossAxisCount: 2,
                                crossAxisSpacing: 2,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                children: controller.allProducts.map(
                                  (e) {
                                    return Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Card(
                                        elevation: 4,
                                        child: SizedBox(
                                          width: screenWidth(2.2),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Row(
                                                children: [
                                                  screenWidth(40).pw,
                                                  CustomNetworkImage(
                                                    context.image(
                                                        e.club!.mainImage!),
                                                    width: screenWidth(8.7),
                                                    height: screenHeight(10),
                                                    border: Border.all(
                                                        color: AppColors
                                                            .mainYellowColor),
                                                    shape: BoxShape.circle,
                                                  ),
                                                  screenWidth(40).pw,
                                                  Flexible(
                                                    child: Column(
                                                      children: [
                                                        CustomText(
                                                          textType:
                                                              TextStyleType
                                                                  .CUSTOM,
                                                          text:
                                                              "${e.club!.name}",
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          textColor: AppColors
                                                              .mainBlackColor,
                                                          fontSize:
                                                              screenWidth(25),
                                                        ),
                                                        CustomText(
                                                          textType:
                                                              TextStyleType
                                                                  .CUSTOM,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          text:
                                                              "${e.club!.city}",
                                                          textColor: AppColors
                                                              .mainYellowColor,
                                                          fontSize:
                                                              screenWidth(25),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              CustomText(
                                                textType: TextStyleType.CUSTOM,
                                                text: e.name!,
                                                overflow: TextOverflow.ellipsis,
                                                textColor:
                                                    AppColors.mainBlackColor,
                                              ),
                                              screenWidth(30).ph,
                                              CustomNetworkImage(
                                                context.image(e.img!),
                                                width: screenWidth(1),
                                                height: screenHeight(7),
                                                onTap: () => Get.to(
                                                  FoodSportDetalisView(
                                                    currentlocation:
                                                        widget.currentlocation,
                                                    productsModel: e,
                                                  ),
                                                ),
                                              ),
                                              screenWidth(25).ph,
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  InkWell(
                                                    onTap: () {
                                                      controller.addTofavorite(
                                                          productsModel: e);
                                                    },
                                                    child: SizedBox(
                                                      height: screenWidth(10),
                                                      width: screenWidth(10),
                                                      child: ClipRRect(
                                                        child: Card(
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          18)),
                                                          elevation: 4,
                                                          child: Center(
                                                            child: SvgPicture.asset(
                                                                "images/like.svg"),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  CustomText(
                                                    textType:
                                                        TextStyleType.CUSTOM,
                                                    textColor: AppColors
                                                        .mainYellowColor,
                                                    text: e.price.toString() +
                                                        tr('key_AED'),
                                                  ),
                                                  InkWell(
                                                    onTap: () {
                                                      controller.addToCart(
                                                          productsModel: e);
                                                    },
                                                    child: SizedBox(
                                                      height: screenWidth(10),
                                                      width: screenWidth(10),
                                                      child: ClipRRect(
                                                        child: Card(
                                                          shape:
                                                              RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18),
                                                          ),
                                                          elevation: 4,
                                                          child: Center(
                                                            child: SvgPicture.asset(
                                                                "images/shopping-cart.svg"),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ).toList(),
                              );
              },
            ),
            Obx(
              () {
                return controller.isMoreFilterLoading
                    ? const SpinKitCircle(
                        color: AppColors.mainYellowColor,
                      )
                    : const SizedBox.shrink();
              },
            ),
          ],
        ),
      ),
    );
  }
}
