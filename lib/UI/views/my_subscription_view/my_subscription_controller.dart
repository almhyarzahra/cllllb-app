import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/data/repositories/booking_repository.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/data/models/apis/user_supscription_model.dart';
import '../../../core/enums/message_type.dart';
import '../../../core/enums/request_status.dart';
import '../../../core/translation/app_translation.dart';

class MySubscriptionController extends BaseController {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<UserSubscriptionModel> mySubscription =
      <UserSubscriptionModel>[UserSubscriptionModel()].obs;
  bool get isSubscriptionLoading => requestStatus.value == RequestStatus.LOADING;

  @override
  void onInit() {
    getUserSubscription();
    super.onInit();
  }

  void getUserSubscription() {
    runLoadingFutureFunction(
        function:
            BookingRepository().getUserSubscription(userId: storage.getUserId()).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_check_connection'), messageType: MessageType.REJECTED);
      }, (r) {
        if (mySubscription.isNotEmpty) {
          mySubscription.clear();
        }
        mySubscription.addAll(r);
        mySubscription.map((element) {
          element.message = "l";
        }).toSet();
      });
    }));
  }
}
