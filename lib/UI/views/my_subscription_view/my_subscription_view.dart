import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bar.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/views/my_subscription_view/my_subscription_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/utils.dart';

class MySubscriptionView extends StatefulWidget {
  const MySubscriptionView({super.key, required this.currentlocation});
  final LocationData? currentlocation;

  @override
  State<MySubscriptionView> createState() => _MySubscriptionViewState();
}

class _MySubscriptionViewState extends State<MySubscriptionView> {
  late MySubscriptionController controller;
  @override
  void initState() {
    controller = Get.put(MySubscriptionController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            children: [
              CustomBar(text: tr('key_subscription')),
              Obx(() {
                return controller.isSubscriptionLoading
                    ? const SpinKitCircle(
                        color: AppColors.mainYellowColor,
                      )
                    : controller.mySubscription.isEmpty
                        ? Padding(
                            padding: EdgeInsetsDirectional.only(
                                top: screenHeight(3)),
                            child: CustomText(
                              textType: TextStyleType.CUSTOM,
                              text: tr('key_No_Booking_Available'),
                              textColor: AppColors.mainBlackColor,
                            ),
                          )
                        : controller.mySubscription[0].message == null
                            ? customFaildConnection(
                                onPressed: () {
                                  controller.onInit();
                                },
                              )
                            : Column(
                                children:
                                    controller.mySubscription.map((element) {
                                return SizedBox(
                                  height: screenHeight(8),
                                  child: Card(
                                    child: Padding(
                                      padding: EdgeInsetsDirectional.symmetric(
                                          horizontal: screenWidth(20)),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          CustomText(
                                            textType: TextStyleType.CUSTOM,
                                            text: tr('key_number') +
                                                element.id.toString(),
                                            textColor:
                                                AppColors.mainYellowColor,
                                            fontSize: screenWidth(50),
                                          ),
                                          CustomNetworkImage(
                                            context.image(
                                                element.subscription!.image!),
                                            width: screenWidth(5),
                                            height: screenWidth(10),
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              CustomText(
                                                fontSize: screenWidth(30),
                                                textType: TextStyleType.CUSTOM,
                                                text: element.startDate! +
                                                    tr('key_to') +
                                                    element.endDate!,
                                                textColor:
                                                    AppColors.mainBlackColor,
                                              ),
                                              CustomText(
                                                textType: TextStyleType.CUSTOM,
                                                text:
                                                    "${element.price}${tr('key_AED')}",
                                                textColor:
                                                    AppColors.mainYellowColor,
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              }).toList());
              }),
            ],
          ),
        ),
      ),
    );
  }
}
