import 'package:com.tad.cllllb/core/data/models/apis/subscription_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/booking_repository.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:lottie/lottie.dart';

import '../../../core/data/repositories/payment_repositories.dart';
import '../../../core/enums/message_type.dart';
import '../../../core/enums/request_status.dart';
import '../../../core/translation/app_translation.dart';
import '../../../core/utils/general_util.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/custom_widgets/custom_toast.dart';
import '../../shared/utils.dart';
import '../my_subscription_view/my_subscription_view.dart';

class SubscriptionController extends BaseController {
  SubscriptionController({required int fieldId, this.coachId, this.currentlocation}) {
    this.fieldId.value = fieldId;
  }
  LocationData? currentlocation;
  RxInt fieldId = 0.obs;
  int? coachId;
  RxList<SubscriptionModel> saubscription = <SubscriptionModel>[SubscriptionModel()].obs;
  RxString hint = "".obs;
  RxDouble price = 0.0.obs;
  RxInt subId = 0.obs;
  Rx<DateTime> date = DateTime.now().obs;
  RxDouble wallet = 0.0.obs;

  bool get isSubscriptionLoading => requestStatus.value == RequestStatus.LOADING;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  @override
  void onInit() {
    getSubscription();
    super.onInit();
  }

  void getSubscription() {
    runLoadingFutureFunction(
        function: BookingRepository()
            .getSubscription(fieldId: fieldId.value, coachId: coachId)
            .then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_No_Internet_Connection'), messageType: MessageType.REJECTED);
      }, (r) {
        if (saubscription.isNotEmpty) {
          saubscription.clear();
        }

        saubscription.addAll(r);
        hint.value = "${saubscription[0].duration} ${saubscription[0].durationName}";
        price.value = saubscription[0].price!;
        subId.value = saubscription[0].id!;
        saubscription.map((element) {
          element.message = "l";
        }).toSet();
      });
    }));
  }

  void getWalletBalance() {
    runFullLoadingFutureFunction(
        function: PaymentRepositories().getWalletBalance(userId: storage.getUserId()).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_check_connection'), messageType: MessageType.REJECTED);
      }, (r) {
        wallet.value = r.userBalance!;
        r.code == 1
            ? Get.defaultDialog(
                title: tr('key_Wallet_contains') + wallet.value.toString() + tr('key_AED'),
                titleStyle: const TextStyle(color: AppColors.mainYellowColor),
                content: Column(
                  children: [
                    Lottie.asset(
                      "images/wallet.json",
                      width: screenWidth(3),
                    ),
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_fill_wallet'),
                      textColor: AppColors.mainRedColor,
                    ),
                    wallet.value < price.value
                        ? CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: tr('key_wallet_not_contain_the_full_amount!'),
                            textColor: AppColors.mainRedColor,
                          )
                        : Column(
                            children: [
                              CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_Do_you_want_pay_from_the_wallet'),
                                textColor: AppColors.mainBlackColor,
                              ),
                              screenWidth(30).ph,
                              CustomButton(
                                  widthButton: 4,
                                  circularBorder: screenWidth(20),
                                  text: tr('key_Yes'),
                                  onPressed: () {
                                    paySubscription(payMethod: 2);
                                  }),
                              screenWidth(30).ph,
                              CustomButton(
                                  widthButton: 4,
                                  circularBorder: screenWidth(20),
                                  text: tr('key_No'),
                                  onPressed: () {
                                    Get.back();
                                  }),
                            ],
                          ),
                  ],
                ))
            : Get.defaultDialog(
                title: tr('key_You_Do_not_have_wallet_yet'),
                titleStyle: const TextStyle(color: AppColors.mainRedColor),
                content: Column(
                  children: [
                    Lottie.asset(
                      "images/wallet.json",
                      width: screenWidth(3),
                    ),
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_fill_wallet'),
                      textColor: AppColors.mainRedColor,
                    )
                  ],
                ));
      });
    }));
  }

  void paySubscription({required int payMethod}) {
    runFullLoadingFutureFunction(
        function: BookingRepository()
            .paySubscription(
      userId: storage.getUserId(),
      subscriptionId: subId.value,
      payMethod: payMethod,
      date: DateFormat("yyyy-MM-dd").format(date.value),
    )
            .then((value) {
      value.fold((l) {
        paySubscription(payMethod: payMethod);
      }, (r) {
         if(r.draw!=null){
          CustomToast.showMessage(message: "${tr('wonEntryLottery')} '${r.draw}'",messageType: MessageType.SUCCSESS);
        }
       else if (r.points != null) {
          CustomToast.showMessage(
              message: "${r.points} points have been added to your account",
              messageType: MessageType.SUCCSESS);
        } else {
          CustomToast.showMessage(
              message: tr('key_Succsess_Payment'),
              messageType: MessageType.SUCCSESS);
        }
        Get.to(MySubscriptionView(
          currentlocation: currentlocation,
        ));
      });
    }));
  }
}
