import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/subscription_view/subscription_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:lottie/lottie.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../../core/utils/network_util.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_book_bar.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/utils.dart';
import '../shopping_view/stripe_payment/payment_manager.dart';
import '../sign_in_view/sign_in_view.dart';

class SubscriptionView extends StatefulWidget {
  const SubscriptionView(
      {super.key,
      required this.currentlocation,
      required this.imageClub,
      required this.nameClub,
      required this.address,
      required this.clubType,
      required this.fieldId,
      this.coachId});
  final LocationData? currentlocation;
  final String imageClub;
  final String nameClub;
  final String address;
  final String clubType;
  final int fieldId;
  final int? coachId;

  @override
  State<SubscriptionView> createState() => _SubscriptionViewState();
}

class _SubscriptionViewState extends State<SubscriptionView> {
  late SubscriptionController controller;
  @override
  void initState() {
    controller = Get.put(SubscriptionController(
        fieldId: widget.fieldId, coachId: widget.coachId, currentlocation: widget.currentlocation));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar:const CustomBottomNavigation(),
        drawer: CustomDrawer(
          currentlocation: widget.currentlocation,
        ),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            children: [
              screenHeight(80).ph,
              CustomBookBar(
                imageUrl: widget.imageClub,
                imageName: Uri.https(NetworkUtil.baseUrl, widget.imageClub).toString(),
                text: "${widget.nameClub}/ ${widget.address}/ ${widget.clubType}",
              ),
              screenHeight(80).ph,
              Obx(() {
                return controller.isSubscriptionLoading
                    ? const SpinKitCircle(
                        color: AppColors.mainYellowColor,
                      )
                    : controller.saubscription[0].message == null
                        ? customFaildConnection(onPressed: () {
                            controller.onInit();
                          })
                        : Padding(
                            padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(30)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(screenWidth(20)),
                                  child: CachedNetworkImage(
                                    imageUrl: Uri.https(
                                            NetworkUtil.baseUrl, controller.saubscription[0].image!)
                                        .toString(),
                                    placeholder: (context, url) =>
                                        Lottie.asset("images/download.json"),
                                    errorWidget: (context, url, error) => const Icon(Icons.error),
                                    width: double.infinity,
                                    height: screenHeight(3),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                screenWidth(20).ph,
                                CustomText(
                                  textType: TextStyleType.CUSTOM,
                                  text: tr('key_subscription'),
                                  textColor: AppColors.mainBlackColor,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    DropdownButton(
                                      hint: CustomText(
                                        text: controller.hint.value,
                                        textType: TextStyleType.CUSTOM,
                                        textColor: AppColors.mainYellowColor,
                                      ),
                                      items: controller.saubscription.map(
                                        (e) {
                                          return DropdownMenuItem(
                                            value: e.price,
                                            onTap: () {
                                              controller.hint.value =
                                                  "${e.duration} ${e.durationName}";
                                              controller.subId.value = e.id!;
                                            },
                                            child: Text(
                                              "${e.duration} ${e.durationName}",
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          );
                                        },
                                      ).toList(),
                                      onChanged: (v) {
                                        controller.price.value = v!;
                                      },
                                    ),
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text:
                                          "${tr('key_Price')}: ${controller.price.value}${tr('key_AED')}",
                                      textColor: AppColors.mainYellowColor,
                                    )
                                  ],
                                ),
                                screenWidth(20).ph,
                                CustomText(
                                  textType: TextStyleType.CUSTOM,
                                  text: tr('description'),
                                  textColor: AppColors.mainYellowColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: screenWidth(20),
                                ),
                                CustomText(
                                  textType: TextStyleType.CUSTOM,
                                  text: controller.saubscription[0].description!,
                                  textColor: AppColors.mainBlackColor,
                                  textAlign: TextAlign.start,
                                ),
                                screenWidth(20).ph,
                                CustomText(
                                  textType: TextStyleType.CUSTOM,
                                  text: tr('select_date_subscription'),
                                  textColor: AppColors.mainYellowColor,
                                  textAlign: TextAlign.start,
                                  fontWeight: FontWeight.bold,
                                ),
                                Obx(() {
                                  log(controller.date.value.toString());
                                  return TableCalendar(
                                    availableGestures: AvailableGestures.horizontalSwipe,
                                    selectedDayPredicate: (day) {
                                      return isSameDay(controller.date.value, day);
                                    },
                                    headerStyle: HeaderStyle(
                                      formatButtonVisible: true,
                                      titleCentered: true,
                                      formatButtonShowsNext: false,
                                      formatButtonDecoration: BoxDecoration(
                                        color: Colors.blue,
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      formatButtonTextStyle: const TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                    calendarStyle: const CalendarStyle(
                                      selectedDecoration: BoxDecoration(
                                        color: AppColors.mainYellowColor,
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                    onDaySelected: (selectedDay, focusedDay) {
                                      //controller.selectDay.value = true;
                                      controller.date.value = selectedDay;
                                      // if (widget.bookingType == 2) {
                                      //   controller.getTimeShots(date: selectedDay);
                                      // } else {
                                      //   controller.getGameTime(
                                      //     date: selectedDay,
                                      //   );
                                      // }
                                    },
                                    availableCalendarFormats: const {
                                      CalendarFormat.month: '',
                                    },
                                    firstDay: DateTime.utc(DateTime.now().year,
                                        DateTime.now().month, DateTime.now().day),
                                    lastDay: DateTime.now().add(const Duration(days: 90)),
                                    focusedDay: DateTime.now(),
                                  );
                                }),
                                screenWidth(20).ph,
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        Get.defaultDialog(
                                          title: tr('key_Payment_Cash?'),
                                          titleStyle:
                                              const TextStyle(color: AppColors.mainYellowColor),
                                          content: Column(
                                            children: [
                                              CustomButton(
                                                widthButton: 4,
                                                circularBorder: screenWidth(20),
                                                text: tr('key_Yes'),
                                                onPressed: () {
                                                  storage.getUserId() == 0
                                                      ? Get.defaultDialog(
                                                          title: tr('key_login_complete'),
                                                          titleStyle: const TextStyle(
                                                              color: AppColors.mainYellowColor),
                                                          content: Column(
                                                            children: [
                                                              CustomButton(
                                                                text: tr('key_Yes'),
                                                                onPressed: () async {
                                                                  storage.setCartList([]);
                                                                  storage.setFavoriteList([]);
                                                                  storage.setFirstLanuch(true);
                                                                  storage.setIsLoggedIN(false);
                                                                  storage.globalSharedPrefs.remove(
                                                                      storage.PREF_TOKEN_INFO);
                                                                  //storage.setTokenInfo(null);
                                                                  await storage.setUserId(0);
                                                                  storage.globalSharedPrefs.remove(
                                                                      storage.PREF_USER_NAME);

                                                                  storage.setGoogle(false);
                                                                  storage.globalSharedPrefs.remove(
                                                                      storage.PREF_MOBILE_NUMBER);
                                                                  storage.globalSharedPrefs
                                                                      .remove(storage.PREF_EMAIL);
                                                                  Get.offAll(const SignInView());
                                                                },
                                                                widthButton: 4,
                                                                circularBorder: screenWidth(10),
                                                              ),
                                                              screenWidth(20).ph,
                                                              CustomButton(
                                                                text: tr('key_No'),
                                                                onPressed: () {
                                                                  Get.back();
                                                                },
                                                                widthButton: 4,
                                                                circularBorder: screenWidth(10),
                                                              ),
                                                            ],
                                                          ),
                                                        )
                                                      : controller.paySubscription(payMethod: 1);
                                                },
                                              ),
                                              screenWidth(30).ph,
                                              CustomButton(
                                                widthButton: 4,
                                                circularBorder: screenWidth(20),
                                                text: tr('key_No'),
                                                onPressed: () {
                                                  Get.back();
                                                },
                                              ),
                                            ],
                                          ),
                                        );
                                      },
                                      child: Container(
                                        width: screenWidth(4),
                                        height: screenWidth(8),
                                        decoration: BoxDecoration(
                                          color: AppColors.mainYellowColor,
                                          borderRadius: BorderRadius.circular(screenWidth(20)),
                                        ),
                                        child: Transform.scale(
                                          scale: 0.5,
                                          child: SvgPicture.asset(
                                            "images/cash.svg",
                                          ),
                                        ),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        storage.getUserId() == 0
                                            ? Get.defaultDialog(
                                                title: tr('key_login_complete'),
                                                titleStyle: const TextStyle(
                                                    color: AppColors.mainYellowColor),
                                                content: Column(
                                                  children: [
                                                    CustomButton(
                                                      text: tr('key_Yes'),
                                                      onPressed: () async {
                                                        storage.setCartList([]);
                                                        storage.setFavoriteList([]);
                                                        storage.setFirstLanuch(true);
                                                        storage.setIsLoggedIN(false);
                                                        storage.globalSharedPrefs
                                                            .remove(storage.PREF_TOKEN_INFO);
                                                        //storage.setTokenInfo(null);
                                                        await storage.setUserId(0);
                                                        storage.globalSharedPrefs
                                                            .remove(storage.PREF_USER_NAME);

                                                        storage.setGoogle(false);
                                                        storage.globalSharedPrefs
                                                            .remove(storage.PREF_MOBILE_NUMBER);
                                                        storage.globalSharedPrefs
                                                            .remove(storage.PREF_EMAIL);

                                                        Get.offAll(() => const SignInView());
                                                      },
                                                      widthButton: 4,
                                                      circularBorder: screenWidth(10),
                                                    ),
                                                    screenWidth(20).ph,
                                                    CustomButton(
                                                      text: tr('key_No'),
                                                      onPressed: () {
                                                        Get.back();
                                                      },
                                                      widthButton: 4,
                                                      circularBorder: screenWidth(10),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            : PaymentManager.makePayment(
                                                    controller.price.value.toInt(), "AED")
                                                .then((value) async {
                                                await Stripe.instance
                                                    .presentPaymentSheet()
                                                    .then((value) {
                                                  controller.paySubscription(payMethod: 3);
                                                });
                                              });
                                      },
                                      child: Container(
                                        width: screenWidth(4),
                                        height: screenWidth(8),
                                        decoration: BoxDecoration(
                                          color: AppColors.mainYellowColor,
                                          borderRadius: BorderRadius.circular(screenWidth(20)),
                                        ),
                                        child: Transform.scale(
                                          scale: 0.5,
                                          child: SvgPicture.asset(
                                            "images/ic_visa.svg",
                                            color: AppColors.mainWhiteColor,
                                          ),
                                        ),
                                      ),
                                    ),
                                    // GetPlatform.isIOS
                                    //     ? SizedBox.shrink()
                                    //     :
                                    InkWell(
                                      onTap: () {
                                        storage.getUserId() == 0
                                            ? Get.defaultDialog(
                                                title: tr('key_login_complete'),
                                                titleStyle: const TextStyle(
                                                    color: AppColors.mainYellowColor),
                                                content: Column(
                                                  children: [
                                                    CustomButton(
                                                      text: tr('key_Yes'),
                                                      onPressed: () async {
                                                        storage.setCartList([]);
                                                        storage.setFavoriteList([]);
                                                        storage.setFirstLanuch(true);
                                                        storage.setIsLoggedIN(false);
                                                        storage.globalSharedPrefs
                                                            .remove(storage.PREF_TOKEN_INFO);
                                                        //storage.setTokenInfo(null);
                                                        await storage.setUserId(0);
                                                        storage.globalSharedPrefs
                                                            .remove(storage.PREF_USER_NAME);

                                                        storage.setGoogle(false);
                                                        storage.globalSharedPrefs
                                                            .remove(storage.PREF_MOBILE_NUMBER);
                                                        storage.globalSharedPrefs
                                                            .remove(storage.PREF_EMAIL);
                                                        Get.offAll(const SignInView());
                                                      },
                                                      widthButton: 4,
                                                      circularBorder: screenWidth(10),
                                                    ),
                                                    screenWidth(20).ph,
                                                    CustomButton(
                                                      text: tr('key_No'),
                                                      onPressed: () {
                                                        Get.back();
                                                      },
                                                      widthButton: 4,
                                                      circularBorder: screenWidth(10),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            : controller.getWalletBalance();
                                      },
                                      child: Container(
                                        width: screenWidth(4),
                                        height: screenWidth(8),
                                        decoration: BoxDecoration(
                                          color: AppColors.mainYellowColor,
                                          borderRadius: BorderRadius.circular(screenWidth(20)),
                                        ),
                                        child: Transform.scale(
                                          scale: 0.5,
                                          child: SvgPicture.asset(
                                            "images/wallet.svg",
                                            color: AppColors.mainWhiteColor,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                screenWidth(20).ph,
                              ].animate(interval: 50.ms).scale(delay: 100.ms),
                            ),
                          );
              })
            ].animate(interval: 50.ms).scale(delay: 100.ms),
          ),
        ),
      ),
    );
  }
}
