import 'dart:developer';

import 'package:com.tad.cllllb/core/data/models/apis/user_vouchers_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FilterMyVouchersController extends BaseController{
  FilterMyVouchersController({
    required List<UserVouchersModel> userVouchersModel
  }){
    vouchers.value=userVouchersModel;
  }
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<UserVouchersModel> vouchers = <UserVouchersModel>[UserVouchersModel()].obs;
  bool get isMoreVouchersLoading =>listType.contains(OperationType.filtermoreMyVouchers);
  RxInt pageKey = 2.obs;
  RxBool getMore = true.obs;
  RxBool enableScrollListner = true.obs;


  Future<void> getMoreVouchers({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
      type: OperationType.filtermoreMyVouchers,
      function: UserRepository().getUserVouchers(page: page).then(
        (value) {
          value.fold((l) {
            pageKey.value = pageKey.value - 1;
          }, (r) {
            if (r.isEmpty) {
              enableScrollListner.value = false;
            } else {
              vouchers.addAll(r);
            }
          });
        },
      ),
    ).then((value) => getMore.value = true);
  }
}