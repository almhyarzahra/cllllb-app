import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import '../../../shared/colors.dart';
import '../../../shared/utils.dart';

class BookGameShimmer extends StatelessWidget {
  const BookGameShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: Padding(
        padding:
            EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20), vertical: screenWidth(20)),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: screenWidth(5),
                  height: screenWidth(6),
                  decoration: BoxDecoration(
                    color: AppColors.mainOrangeColor,
                    borderRadius: BorderRadius.circular(screenWidth(35)),
                  ),
                ),
                Container(
                  width: screenWidth(10),
                  height: screenWidth(10),
                  decoration: BoxDecoration(
                    color: AppColors.mainOrangeColor,
                    borderRadius: BorderRadius.circular(screenWidth(35)),
                  ),
                ),
                Container(
                  width: screenWidth(5),
                  height: screenWidth(10),
                  decoration: BoxDecoration(
                    color: AppColors.mainOrangeColor,
                    borderRadius: BorderRadius.circular(screenWidth(35)),
                  ),
                ),
              ],
            ),
            screenWidth(20).ph,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: screenWidth(5),
                  height: screenWidth(6),
                  decoration: BoxDecoration(
                    color: AppColors.mainOrangeColor,
                    borderRadius: BorderRadius.circular(
                      screenWidth(35),
                    ),
                  ),
                ),
                Container(
                  width: screenWidth(10),
                  height: screenWidth(10),
                  decoration: BoxDecoration(
                    color: AppColors.mainOrangeColor,
                    borderRadius: BorderRadius.circular(screenWidth(35)),
                  ),
                ),
                Container(
                  width: screenWidth(5),
                  height: screenWidth(10),
                  decoration: BoxDecoration(
                    color: AppColors.mainOrangeColor,
                    borderRadius: BorderRadius.circular(screenWidth(35)),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
