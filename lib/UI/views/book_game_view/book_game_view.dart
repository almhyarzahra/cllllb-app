import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_bottom_navigation.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/book_game_view/book_game_controller.dart';
import 'package:com.tad.cllllb/UI/views/book_now_view/book_now_view.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

import '../../../core/translation/app_translation.dart';
import '../../../core/utils/network_util.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_book_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import '../../shared/custom_widgets/virtical_divider.dart';
import '../../shared/utils.dart';
import 'book_game_shimmer/book_game_shimmer.dart';

class BookGameView extends StatefulWidget {
  const BookGameView(
      {super.key,
      required this.nameClub,
      required this.address,
      required this.imageClub,
      required this.clubId,
      required this.clubType,
      required this.currentlocation});
  final String nameClub;
  final String address;
  final String imageClub;
  final int clubId;
  final String clubType;
  final LocationData? currentlocation;

  @override
  State<BookGameView> createState() => _BookGameViewState();
}

class _BookGameViewState extends State<BookGameView> {
  late BookGameController controller;
  @override
  void initState() {
    controller = Get.put(BookGameController(clubId: widget.clubId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(currentlocation: widget.currentlocation),
         bottomNavigationBar:const CustomBottomNavigation(),
        key: controller.key,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenWidth(6)),
          child: CustomAppBar(
            currentlocation: widget.currentlocation,
            drwerOnPressed: () {
              controller.key.currentState!.openDrawer();
            },
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            children: [
              screenHeight(80).ph,
              CustomBookBar(
                imageUrl: widget.imageClub,
                imageName:
                    Uri.https(NetworkUtil.baseUrl, widget.imageClub).toString(),
                text:
                    "${widget.nameClub}/ ${widget.address}/ ${widget.clubType}",
              ),
              Padding(
                padding: EdgeInsetsDirectional.symmetric(
                    horizontal: screenWidth(20)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_GAME'),
                      fontSize: screenWidth(20),
                      textColor: AppColors.mainYellowColor,
                    ),
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_QUANTITY'),
                      fontSize: screenWidth(20),
                      textColor: AppColors.mainYellowColor,
                    ),
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: tr('key_BOOK'),
                      fontSize: screenWidth(20),
                      textColor: AppColors.mainYellowColor,
                    ),
                  ],
                ),
              ),
              Obx(
                () {
                  return controller.isGameLoading
                      ? const BookGameShimmer()
                      : controller.game.isEmpty
                          ? Center(
                              child: CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: tr('key_No_Game_Yet'),
                                textColor: AppColors.mainBlackColor,
                              ),
                            )
                          : controller.game[0].message == null
                              ? customFaildConnection(
                                  onPressed: () {
                                    controller.onInit();
                                  },
                                )
                              : Padding(
                                  padding: EdgeInsetsDirectional.symmetric(
                                      horizontal: screenWidth(20)),
                                  child: ListView.builder(
                                    itemCount: controller.game.length,
                                    shrinkWrap: true,
                                    scrollDirection: Axis.vertical,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Padding(
                                        padding:
                                            EdgeInsetsDirectional.symmetric(
                                                vertical: screenWidth(20)),
                                        child: Column(
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Column(
                                                  children: [
                                                    Container(
                                                      width: screenWidth(5),
                                                      height: screenWidth(6),
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(
                                                                    screenWidth(
                                                                        35)),
                                                        border: Border.all(
                                                            color: AppColors
                                                                .mainGrey3Color),
                                                      ),
                                                      child: CustomNetworkImage(
                                                        context.image(controller
                                                            .game[index]
                                                            .games!
                                                            .bannerImage!),
                                                        width: screenWidth(2),
                                                        height: screenHeight(3),
                                                        margin: const EdgeInsets
                                                            .all(8.0),
                                                      ),
                                                    ),
                                                    CustomText(
                                                      textType:
                                                          TextStyleType.CUSTOM,
                                                      text: controller
                                                          .game[index]
                                                          .games!
                                                          .name!,
                                                      textColor: AppColors
                                                          .mainBlackColor,
                                                      fontSize: screenWidth(40),
                                                    ),
                                                  ],
                                                ),
                                                CustomText(
                                                  textType:
                                                      TextStyleType.CUSTOM,
                                                  fontSize: screenWidth(18),
                                                  text: controller
                                                      .game[index].quantity
                                                      .toString(),
                                                  textColor:
                                                      AppColors.mainYellowColor,
                                                ),
                                                CustomButton(
                                                  text: controller
                                                              .game[index]
                                                              .games!
                                                              .bookingType ==
                                                          3
                                                      ? tr(
                                                          'key_subscription_now')
                                                      : tr('key_BOOK_NOW'),
                                                  textSize: screenWidth(35),
                                                  onPressed: controller
                                                              .game[index]
                                                              .quantity ==
                                                          0
                                                      ? null
                                                      : () {
                                                          Get.to(
                                                            BookNowView(
                                                              bookingType:
                                                                  controller
                                                                      .game[
                                                                          index]
                                                                      .games!
                                                                      .bookingType!,
                                                              currentlocation:
                                                                  widget
                                                                      .currentlocation,
                                                              imageGame: controller
                                                                  .game[index]
                                                                  .games!
                                                                  .bannerImage!,
                                                              nameGame:
                                                                  controller
                                                                      .game[
                                                                          index]
                                                                      .games!
                                                                      .name!,
                                                              quantity:
                                                                  controller
                                                                      .game[
                                                                          index]
                                                                      .quantity!,
                                                              gameId: controller
                                                                  .game[index]
                                                                  .id!,
                                                              address: widget
                                                                  .address,
                                                              clubId:
                                                                  widget.clubId,
                                                              clubType: widget
                                                                  .clubType,
                                                              imageClub: widget
                                                                  .imageClub,
                                                              nameClub: widget
                                                                  .nameClub,
                                                            ),
                                                          );
                                                        },
                                                  widthButton: 3,
                                                  heightButton: 12,
                                                  circularBorder:
                                                      screenWidth(10),
                                                ),
                                              ],
                                            ),
                                            const VirticalDivider()
                                          ],
                                        ),
                                      );
                                    },
                                  ),
                                );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
