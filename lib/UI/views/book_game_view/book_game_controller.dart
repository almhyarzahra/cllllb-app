import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/data/models/apis/game_booking_model.dart';
import '../../../core/data/repositories/booking_repository.dart';
import '../../../core/enums/request_status.dart';
import '../../../core/translation/app_translation.dart';

class BookGameController extends BaseController {
  BookGameController({required int clubId}) {
    this.clubId.value = clubId;
  }
  RxInt clubId = 0.obs;
  RxList<GameBookingModel> game = <GameBookingModel>[GameBookingModel()].obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  @override
  void onInit() {
    getBookingGame();
    super.onInit();
  }

  bool get isGameLoading => requestStatus.value == RequestStatus.LOADING;

  void getBookingGame() {
    runLoadingFutureFunction(
        function: BookingRepository()
            .getBookingGame(clubId: clubId.value)
            .then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_No_Internet_Connection'),
            messageType: MessageType.REJECTED);
      }, (r) {
        if (game.isNotEmpty) {
          game.clear();
        }

        game.addAll(r);
        game.map((element) {
          element.message = "l";
        }).toSet();
      });
    }));
  }
}
