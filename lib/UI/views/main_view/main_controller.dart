import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/enums/bottom_navigation_enum.dart';
import '../../../core/services/base_controller.dart';

class MainController extends BaseController {
  MainController({required int value}){
    initialPage.value=value;
  }
  late PageController pageController ;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  late Rx<BottomNavigationEnum> selected = BottomNavigationEnum.HOME.obs;
  LocationData? currentlocation;
  RxInt initialPage=0.obs;
  @override
  Future<void> onInit() async {
    pageController=PageController(initialPage: initialPage.value);
    //currentlocation = await locationService.getCurrentLocation();
    super.onInit();
  }

  void onClick(BottomNavigationEnum select, int pageNumber) {
    selected.value = select;
    pageController.jumpToPage(pageNumber);
  }
}
