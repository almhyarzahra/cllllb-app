import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/main_view/book_view/book_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:com.tad.cllllb/core/utils/network_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

import '../../../../core/translation/app_translation.dart';
import '../../../shared/custom_widgets/custom_text.dart';
import '../../sign_in_view/sign_in_view.dart';

class BookView extends StatefulWidget {
  const BookView({super.key});

  @override
  State<BookView> createState() => _BookViewState();
}

class _BookViewState extends State<BookView>
    with AutomaticKeepAliveClientMixin {
  late BookController controller;
  final ScrollController _scrollController = ScrollController();
  //final ScrollController _scrollController1 = ScrollController();

  @override
  void initState() {
    controller = Get.put(BookController());
    _scrollController.addListener(_scrollListener);
    // _scrollController1.addListener(_scrollListener1);

    super.initState();
  }

  void _scrollListener() async {
    if (controller.getMore.value) {
      if (controller.enableScrollListner.value &&
          _scrollController.position.maxScrollExtent ==
              _scrollController.offset &&
          !_scrollController.position.outOfRange) {
        controller.getMore.value = false;
        await controller.getMoreGames(page: controller.pageKey.value);
      }
    }
  }

  // void _scrollListener1() async {
  //   if (controller.getMore1.value) {
  //     if (controller.enableScrollListner1.value &&
  //         _scrollController1.position.maxScrollExtent ==
  //             _scrollController1.offset &&
  //         !_scrollController1.position.outOfRange) {
  //       controller.getMore1.value = false;
  //       await controller.getMoreFavoriteGames(page: controller.pageKey1.value);
  //     }
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return storage.getUserId() == 0
        ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: tr('key_login_complete'),
                  textColor: AppColors.mainYellowColor,
                ),
              ),
              Column(
                children: [
                  CustomButton(
                    text: tr('key_Yes'),
                    onPressed: () async {
                      storage.setCartList([]);
                      storage.setFavoriteList([]);
                      storage.setFirstLanuch(true);
                      storage.setIsLoggedIN(false);
                      storage.globalSharedPrefs.remove(storage.PREF_TOKEN_INFO);
                      //storage.setTokenInfo(null);
                      await storage.setUserId(0);
                      storage.globalSharedPrefs.remove(storage.PREF_USER_NAME);
                      storage.setGoogle(false);
                      storage.globalSharedPrefs
                          .remove(storage.PREF_MOBILE_NUMBER);
                      storage.globalSharedPrefs.remove(storage.PREF_EMAIL);
                      Get.offAll(() => const SignInView());
                    },
                    widthButton: 4,
                    circularBorder: screenWidth(10),
                  ),
                  screenWidth(20).ph,
                  CustomButton(
                    text: tr('key_No'),
                    onPressed: () {
                      Get.back();
                    },
                    widthButton: 4,
                    circularBorder: screenWidth(10),
                  ),
                ],
              ),
            ],
          )
        : Padding(
            padding: EdgeInsetsDirectional.only(bottom: screenHeight(15)),
            child: Obx(
              () {
                return controller.isFavoriteGameLoading
                    ? const SpinKitCircle(
                        color: AppColors.mainYellowColor,
                      )
                    : controller.userGame.isEmpty
                        ? RefreshIndicator(
                            onRefresh: () async {
                              controller.onInit();
                              controller.enableScrollListner.value = true;
                              //controller.enableScrollListner1.value = true;
                              controller.pageKey.value = 2;
                              //controller.pageKey1.value = 2;
                              controller.getMore.value = true;
                              //controller.getMore1.value = true;
                            },
                            child: ListView(
                              children: [
                                screenWidth(10).ph,
                                CustomText(
                                  textType: TextStyleType.CUSTOM,
                                  text: tr('key_No_Favorite_Game'),
                                  textColor: AppColors.mainYellowColor,
                                ),
                                Obx(() {
                                  return controller.isGameLoading
                                      ? const SpinKitCircle(
                                          color: AppColors.mainYellowColor,
                                        )
                                      : controller
                                              .allGameModel.value.data!.isEmpty
                                          ? const SizedBox.shrink()
                                          : controller.allGameModel.value
                                                      .data![0].id ==
                                                  null
                                              ? customFaildConnection(
                                                  onPressed: () {
                                                    controller.getAllGame();
                                                  },
                                                )
                                              : GestureDetector(
                                                  onTap: () {
                                                    Get.bottomSheet(
                                                      CustomScrollView(
                                                        controller:
                                                            _scrollController,
                                                        slivers: [
                                                          SliverToBoxAdapter(
                                                              child: Obx(() {
                                                            return Column(
                                                              children: controller
                                                                  .allGameModel
                                                                  .value
                                                                  .data!
                                                                  .map((e) {
                                                                return Padding(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      horizontal:
                                                                          10),
                                                                  child: Row(
                                                                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                    children: [
                                                                      // CustomNetworkImage(
                                                                      //   context.image(
                                                                      //       e.bannerImage!),
                                                                      //   // key:
                                                                      //   //     ValueKey<int>(
                                                                      //   //         e.id!),
                                                                      //   width: 30,
                                                                      //   height: 30,
                                                                      //   radius: 0,
                                                                      //   memCacheWidth: 100,
                                                                      //   memCacheHeight: 80,
                                                                      // ),
                                                                      CachedNetworkImage(
                                                                        imageUrl:
                                                                            Uri.https(NetworkUtil.baseUrl, e.bannerImage!).toString(),
                                                                        placeholder:
                                                                            (context, url) =>
                                                                                Image.asset('images/cllllb.png'),
                                                                        errorWidget: (context,
                                                                                url,
                                                                                error) =>
                                                                            Image.asset('images/cllllb.png'),
                                                                        width:
                                                                            30,
                                                                        height:
                                                                            30,
                                                                            memCacheWidth: 100,
                                                                            memCacheHeight: 80,
                                                                      ),
                                                                      screenWidth(
                                                                              20)
                                                                          .pw,
                                                                      Expanded(
                                                                        child:
                                                                            Text(
                                                                          e.name!,
                                                                          overflow:
                                                                              TextOverflow.ellipsis,
                                                                        ),
                                                                      ),
                                                                      Checkbox(
                                                                        focusColor:
                                                                            AppColors.mainYellowColor,
                                                                        value: controller.isChecked[controller
                                                                            .allGameModel
                                                                            .value
                                                                            .data!
                                                                            .indexOf(e)],
                                                                        onChanged:
                                                                            (value) {
                                                                          controller.isChecked[controller
                                                                              .allGameModel
                                                                              .value
                                                                              .data!
                                                                              .indexOf(e)] = value!;
                                                                          controller.addIdGame(
                                                                              id: e.id!);
                                                                        },
                                                                      ),
                                                                    ],
                                                                  ),
                                                                );
                                                              }).toList(),
                                                            );
                                                          })),
                                                          SliverToBoxAdapter(
                                                            child: Obx(
                                                              () {
                                                                return controller
                                                                        .isMoreGamesLoading
                                                                    ? const SpinKitCircle(
                                                                        color: AppColors
                                                                            .mainYellowColor,
                                                                      )
                                                                    : const SizedBox
                                                                        .shrink();
                                                              },
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      backgroundColor: AppColors
                                                          .mainWhiteColor,
                                                    );
                                                  },
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Container(
                                                        width: 150,
                                                        height: 30,
                                                        decoration:
                                                            const BoxDecoration(
                                                          border: Border(
                                                              bottom: BorderSide(
                                                                  color: AppColors
                                                                      .mainYellowColor)),
                                                        ),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            CustomText(
                                                              text: tr(
                                                                  'key_SELECT_GAME'),
                                                              textType:
                                                                  TextStyleType
                                                                      .CUSTOM,
                                                              textColor: AppColors
                                                                  .mainBlackColor,
                                                            ),
                                                            const Icon(Icons
                                                                .arrow_downward)
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                );
                                }),
                                screenWidth(20).ph,
                                Padding(
                                  padding: EdgeInsetsDirectional.symmetric(
                                      horizontal: screenWidth(5)),
                                  child: CustomButton(
                                    circularBorder: screenWidth(20),
                                    text: tr('key_Add_game'),
                                    onPressed: controller.idGame!.isEmpty
                                        ? null
                                        : () {
                                            controller.addUserGame();
                                          },
                                  ),
                                ),
                              ],
                            ),
                          )
                        : controller.userGame[0].message == null
                            ? Padding(
                                padding: EdgeInsetsDirectional.symmetric(
                                    vertical: screenWidth(2)),
                                child: customFaildConnection(
                                  onPressed: () {
                                    controller.onInit();
                                  },
                                ),
                              )
                            : RefreshIndicator(
                                onRefresh: () async {
                                  controller.onInit();
                                  controller.enableScrollListner.value = true;
                                  //controller.enableScrollListner1.value = true;
                                  controller.pageKey.value = 2;
                                  //controller.pageKey1.value = 2;
                                  controller.getMore.value = true;
                                  //controller.getMore1.value = true;
                                },
                                child: ListView(
                                  //controller: _scrollController1,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: CustomText(
                                        text: tr('key_SELECT_GAME'),
                                        textType: TextStyleType.CUSTOM,
                                        textColor: AppColors.mainYellowColor,
                                        textAlign: TextAlign.start,
                                        fontSize: screenWidth(20),
                                      ),
                                    ),
                                    screenWidth(20).ph,
                                    GridView.count(
                                      crossAxisCount: 5,
                                      crossAxisSpacing: 1,
                                      childAspectRatio: 0.7,
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      children: controller.userGame.map(
                                        (e) {
                                          return Padding(
                                            padding:
                                                EdgeInsetsDirectional.symmetric(
                                              horizontal: screenWidth(100),
                                            ),
                                            child: InkWell(
                                              onTap: () {
                                                // print(e.id);
                                                controller.getClubGame(context,
                                                    gameId: e.gameId!,
                                                    nameGame: e.name!);
                                              },
                                              child: Column(
                                                children: [
                                                  CustomNetworkImage(
                                                    context.image(e.gameImage!),
                                                    width: screenWidth(3.5),
                                                    height: screenHeight(11),
                                                    border: Border.all(
                                                        color: AppColors
                                                            .mainBlackColor),
                                                    radius: screenWidth(20),
                                                    memCacheWidth: 150,
                                                    memCacheHeight: 170,
                                                  ),
                                                  Flexible(
                                                    child: Text(
                                                      e.name!,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      maxLines: 2,
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                          fontSize:
                                                              screenWidth(35),
                                                          color: AppColors
                                                              .mainBlackColor),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      ).toList(),
                                    ),
                                    // Obx(
                                    //   () {
                                    //     return controller
                                    //             .isMoreFavoriteGameLoading
                                    //         ? const SpinKitCircle(
                                    //             color:
                                    //                 AppColors.mainYellowColor,
                                    //           )
                                    //         : const SizedBox.shrink();
                                    //   },
                                    // ),

                                    Column(
                                      children: [
                                        controller.isGameLoading
                                            ? const SpinKitCircle(
                                                color:
                                                    AppColors.mainYellowColor,
                                              )
                                            : controller.allGameModel.value
                                                    .data!.isEmpty
                                                ? const SizedBox.shrink()
                                                : controller.allGameModel.value
                                                            .data![0].id ==
                                                        null
                                                    ? customFaildConnection(
                                                        onPressed: () {
                                                          controller
                                                              .getAllGame();
                                                        },
                                                      )
                                                    : GestureDetector(
                                                        onTap: () {
                                                          Get.bottomSheet(
                                                            CustomScrollView(
                                                              controller:
                                                                  _scrollController,
                                                              slivers: [
                                                                SliverToBoxAdapter(
                                                                    child:
                                                                        Obx(() {
                                                                  return Column(
                                                                    children: controller
                                                                        .allGameModel
                                                                        .value
                                                                        .data!
                                                                        .map(
                                                                            (e) {
                                                                      return Padding(
                                                                        padding:
                                                                            const EdgeInsets.symmetric(horizontal: 10),
                                                                        child:
                                                                            Row(
                                                                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                          children: [
                                                                            // CustomNetworkImage(
                                                                            //   context.image(
                                                                            //       e.bannerImage!),
                                                                            //   // key:
                                                                            //   //     ValueKey<int>(
                                                                            //   //         e.id!),
                                                                            //   width: 30,
                                                                            //   height: 30,
                                                                            //   radius: 0,
                                                                            //   memCacheWidth: 100,
                                                                            //   memCacheHeight: 80,
                                                                            // ),
                                                                            CachedNetworkImage(
                                                                              imageUrl: Uri.https(NetworkUtil.baseUrl, e.bannerImage!).toString(),
                                                                              placeholder: (context, url) => Image.asset('images/cllllb.png'),
                                                                              errorWidget: (context, url, error) => Image.asset('images/cllllb.png'),
                                                                              width: 30,
                                                                              height: 30,
                                                                              memCacheWidth: 100,
                                                                            memCacheHeight: 80,
                                                                            ),
                                                                            screenWidth(20).pw,
                                                                            Expanded(
                                                                              child: Text(
                                                                                e.name!,
                                                                                overflow: TextOverflow.ellipsis,
                                                                              ),
                                                                            ),
                                                                            Checkbox(
                                                                              focusColor: AppColors.mainYellowColor,
                                                                              value: controller.isChecked[controller.allGameModel.value.data!.indexOf(e)],
                                                                              onChanged: (value) {
                                                                                controller.isChecked[controller.allGameModel.value.data!.indexOf(e)] = value!;
                                                                                controller.addIdGame(id: e.id!);
                                                                              },
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      );
                                                                    }).toList(),
                                                                  );
                                                                })),
                                                                SliverToBoxAdapter(
                                                                  child: Obx(
                                                                    () {
                                                                      return controller
                                                                              .isMoreGamesLoading
                                                                          ? const SpinKitCircle(
                                                                              color: AppColors.mainYellowColor,
                                                                            )
                                                                          : const SizedBox
                                                                              .shrink();
                                                                    },
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                            backgroundColor:
                                                                AppColors
                                                                    .mainWhiteColor,
                                                          );
                                                        },
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Container(
                                                              width: 150,
                                                              height: 30,
                                                              decoration:
                                                                  const BoxDecoration(
                                                                border: Border(
                                                                    bottom: BorderSide(
                                                                        color: AppColors
                                                                            .mainYellowColor)),
                                                              ),
                                                              child: Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .center,
                                                                children: [
                                                                  CustomText(
                                                                    text: tr(
                                                                        'key_SELECT_GAME'),
                                                                    textType:
                                                                        TextStyleType
                                                                            .CUSTOM,
                                                                    textColor:
                                                                        AppColors
                                                                            .mainBlackColor,
                                                                  ),
                                                                  const Icon(Icons
                                                                      .arrow_downward)
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                        // Padding(
                                        //     padding: const EdgeInsets
                                        //             .symmetric(
                                        //         horizontal: 10),
                                        //     child: DropdownButton(
                                        //       isExpanded: true,
                                        //       hint: CustomText(
                                        //         text: tr(
                                        //             'key_SELECT_GAME'),
                                        //         textType:
                                        //             TextStyleType
                                        //                 .CUSTOM,
                                        //         textColor: AppColors
                                        //             .mainBlackColor,
                                        //       ),
                                        //       items: controller
                                        //           .allGameModel
                                        //           .map(
                                        //         (e) {
                                        //           return DropdownMenuItem(
                                        //             value: e.id,
                                        //             child: Obx(
                                        //               () {
                                        //                 return Row(
                                        //                   mainAxisAlignment:
                                        //                       MainAxisAlignment
                                        //                           .spaceBetween,
                                        //                   children: [
                                        //                     Row(
                                        //                       children: [
                                        //                         CustomNetworkImage(
                                        //                           context.image(e.bannerImage!),
                                        //                           width:
                                        //                               screenWidth(10),
                                        //                           memCacheHeight:
                                        //                               20,
                                        //                           memCacheWidth:
                                        //                               5,
                                        //                          ),
                                        //                         // CachedNetworkImage(
                                        //                         //   imageUrl:
                                        //                         //       Uri.https(NetworkUtil.baseUrl, e.bannerImage!).toString(),
                                        //                         //   placeholder:
                                        //                         //       (context, url) =>
                                        //                         //           Lottie.asset("images/download.json"),
                                        //                         //   errorWidget: (context,
                                        //                         //           url,
                                        //                         //           error) =>
                                        //                         //       const Icon(Icons.error),
                                        //                         //   width: screenWidth(
                                        //                         //       10),
                                        //                         // ),
                                        //                         screenWidth(20)
                                        //                             .pw,
                                        //                         Text(
                                        //                           e.name!,
                                        //                           overflow:
                                        //                               TextOverflow.ellipsis,
                                        //                         ),
                                        //                       ],
                                        //                     ),
                                        //                     Checkbox(
                                        //                       focusColor:
                                        //                           AppColors.mainYellowColor,
                                        //                       value: controller.isChecked[controller
                                        //                           .allGameModel
                                        //                           .indexOf(e)],
                                        //                       onChanged:
                                        //                           (value) {
                                        //                         controller.isChecked[controller
                                        //                             .allGameModel
                                        //                             .indexOf(e)] = value!;
                                        //                         controller.addIdGame(
                                        //                             id: e.id!);
                                        //                       },
                                        //                     ),
                                        //                   ],
                                        //                 );
                                        //               },
                                        //             ),
                                        //           );
                                        //         },
                                        //       ).toList(),
                                        //       onChanged: (v){},
                                        //     ),
                                        //   ),

                                        screenWidth(20).ph,
                                        Padding(
                                          padding:
                                              EdgeInsetsDirectional.symmetric(
                                                  horizontal: screenWidth(5)),
                                          child: CustomButton(
                                            circularBorder: screenWidth(20),
                                            text: tr('key_Add_game'),
                                            onPressed: controller
                                                    .idGame!.isEmpty
                                                ? null
                                                : () {
                                                    controller.addUserGame();
                                                  },
                                          ),
                                        ),
                                      ],
                                    ),
                                    // CustomSpon(),
                                    // screenWidth(20).ph,

                                    // CustomContact()
                                  ],
                                ),
                              );
              },
            ),
          );
  }

  @override
  bool get wantKeepAlive => true;
}
