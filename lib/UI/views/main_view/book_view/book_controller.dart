import 'dart:developer';

import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/main_view/home_view/home_controller.dart';
import 'package:com.tad.cllllb/core/data/models/apis/favorite_game_model.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/data/models/apis/all_game_model.dart';
import '../../../../core/data/models/apis/club_game_model.dart';
import '../../../../core/data/repositories/user_repository.dart';
import '../../../../core/enums/message_type.dart';
import '../../../../core/translation/app_translation.dart';
import '../../../../core/utils/general_util.dart';
import '../../../shared/custom_widgets/custom_toast.dart';
import '../../../shared/utils.dart';
import '../../detalis_view/detalis_view.dart';

class BookController extends BaseController {
  // Rx<UserInfoModel> userInfo = UserInfoModel().obs;
  // bool get isUserInfoLoading => listType.contains(OperationType.USER_INFO);
  RxList<FavoriteGameModel> userGame = <FavoriteGameModel>[].obs;
  bool get isFavoriteGameLoading =>
      listType.contains(OperationType.favoriteGame);
  bool get isMoreFavoriteGameLoading =>
      listType.contains(OperationType.moreFavoriteGame);
  bool get isGameLoading => listType.contains(OperationType.g);
  bool get isMoreGamesLoading => listType.contains(OperationType.moreG);
  RxBool enableScrollListner = true.obs;
  //RxBool enableScrollListner1 = true.obs;

  RxList<ClubGameModel> clubs = <ClubGameModel>[].obs;
  Rx<AllGameModel> allGameModel = AllGameModel().obs;
  RxList<bool> isChecked = <bool>[].obs;
  RxList<int>? idGame = <int>[].obs;
  RxInt pageKey = 2.obs;
  //RxInt pageKey1 = 2.obs;

  RxBool getMore = true.obs;
  //RxBool getMore1 = true.obs;

  @override
  void onInit() {
    storage.getUserId() == 0 ? null : {getUserFavoriteGame(), getAllGame()};
    super.onInit();
  }

  void getUserFavoriteGame() {
    runLoadingFutureFunction(
      type: OperationType.favoriteGame,
      function: UserRepository()
          .getUserFavoriteGame(userId: storage.getUserId(), page: 1)
          .then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_check_connection'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (userGame.isNotEmpty) {
                userGame.clear();
              }
              userGame.addAll(r);
              userGame.map((element) {
                element.message = "l";
              }).toSet();
            },
          );
        },
      ),
    );
  }

  // Future<void> getMoreFavoriteGames({required int page}) async {
  //   log(pageKey1.value.toString());
  //   pageKey1.value = pageKey1.value + 1;
  //   await runLoadingFutureFunction(
  //       type: OperationType.moreFavoriteGame,
  //       function: UserRepository()
  //           .getUserFavoriteGame(userId: storage.getUserId(), page: page)
  //           .then((value) {
  //         value.fold((l) {
  //           CustomToast.showMessage(
  //               message: tr('key_No_Internet_Please Refresh'),
  //               messageType: MessageType.REJECTED);
  //           pageKey1.value = pageKey1.value - 1;
  //         }, (r) {
  //           if (r.isEmpty) {
  //             enableScrollListner1.value = false;
  //           } else {
  //             userGame.addAll(r);
  //           }
  //         });
  //       })).then((value) => getMore1.value = true);
  // }

  // void getUserInfo() {
  //   runLoadingFutureFunction(
  //       type: OperationType.USER_INFO,
  //       function: UserRepository()
  //           .getUserInfo(userId: storage.getUserId())
  //           .then((value) {
  //         value.fold((l) {
  //           getUserInfo();
  //         }, (r) {
  //           userInfo.value = r;
  //           // requestStatus.value=RequestStatus.LOADING;
  //           // getAllGame();
  //         });
  //       }));
  // }

  void getClubGame(BuildContext context,
      {required int gameId, required String nameGame}) {
    runFullLoadingFutureFunction(
      function: UserRepository().getClubsGames(gameId: gameId).then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_check_connection'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (clubs.isNotEmpty) {
                clubs.clear();
              }
              clubs.addAll(r);
              Get.defaultDialog(
                title: nameGame + tr("key_belongs_to_clubs:"),
                titleStyle: const TextStyle(color: AppColors.mainYellowColor),
                content: r.isEmpty
                    ? CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: tr('key_No_Clubs_Yet'),
                        textColor: AppColors.mainBlackColor,
                      )
                    : Column(
                        children: r.map(
                          (e) {
                            return InkWell(
                              onTap: () {
                                Get.to(DetalisView(
                                  currentlocation: Get.find<HomeController>()
                                      .currentlocation,
                                  imageClub: e.mainImage!,
                                  address: e.city!,
                                  clubId: e.id!,
                                  nameClub: e.name!,
                                ));
                              },
                              child: Row(
                                children: [
                                  CustomNetworkImage(
                                    context.image(e.mainImage!),
                                    width: screenWidth(7),
                                    height: screenHeight(10),
                                    border: Border.all(
                                        color: AppColors.mainYellowColor),
                                    shape: BoxShape.circle,
                                  ),
                                  // Container(
                                  //   width: screenWidth(7),
                                  //   height: screenHeight(10),
                                  //   decoration: BoxDecoration(
                                  //       border: Border.all(color: AppColors.mainYellowColor),
                                  //       shape: BoxShape.circle,
                                  //       image: DecorationImage(
                                  //           fit: BoxFit.contain,
                                  //           image: CachedNetworkImageProvider(
                                  //               Uri.https(NetworkUtil.baseUrl, e.mainImage!)
                                  //                   .toString()))),
                                  // ),
                                  screenWidth(20).pw,
                                  Expanded(
                                    child: CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: e.name!,
                                      overflow: TextOverflow.ellipsis,
                                      textColor: AppColors.mainBlackColor,
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ).toList(),
                      ),
              );
            },
          );
        },
      ),
    );
  }

  void getAllGame() {
    runLoadingFutureFunction(
      type: OperationType.g,
      function: UserRepository().getAllGame(page: 1).then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_No_Internet_Please Refresh'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              allGameModel.value = r;
              // isChecked.clear();
              r.data!.map((e) {
                isChecked.add(false);
              }).toSet();
            },
          );
        },
      ),
    );
  }

  Future<void> getMoreGames({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
      type: OperationType.moreG,
      function: UserRepository().getAllGame(page: page).then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_No_Internet_Please Refresh'),
                  messageType: MessageType.REJECTED);
              pageKey.value = pageKey.value - 1;
            },
            (r) {
              if (r.data!.isEmpty) {
                enableScrollListner.value = false;
              } else {
                allGameModel.value.data!.addAll(r.data!);
                r.data!.map((e) {
                  isChecked.add(false);
                }).toSet();
              }
            },
          );
        },
      ),
    ).then((value) => getMore.value = true);
  }
  // void getAllGame() {
  //   runLoadingFutureFunction(
  //       type: OperationType.GAMES,
  //       function: UserRepository().getAllGame().then((value) {
  //         value.fold((l) {
  //           CustomToast.showMessage(
  //               message: tr('key_No_Internet_Please Refresh'),
  //               messageType: MessageType.REJECTED);
  //         }, (r) {
  //           if (allGameModel.isNotEmpty) {
  //             allGameModel.clear();
  //           }
  //           allGameModel.addAll(r);
  //           allGameModel.map((element) {
  //             element.message = "l";
  //           }).toSet();
  //           isChecked.clear();
  //           r.map((e) {
  //             isChecked.add(false);
  //           }).toSet();
  //         });
  //       }));
  // }

  void addIdGame({required int id}) {
    if (idGame!.contains(id)) {
      idGame!.remove(id);
    } else {
      idGame!.add(id);
    }
    //print(idGame);
  }

  void addUserGame() {
    runFullLoadingFutureFunction(
      function: UserRepository()
          .addUserGame(userId: storage.getUserId(), gameId: idGame!)
          .then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_Please_Try_Again'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              idGame!.clear();
              onInit();
            },
          );
        },
      ),
    );
  }
}
