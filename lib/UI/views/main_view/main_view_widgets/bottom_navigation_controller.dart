import 'package:get/get.dart';
import '../../../../core/enums/bottom_navigation_enum.dart';
import '../../../../core/services/base_controller.dart';

class BottomNavigationController extends BaseController {
  BottomNavigationController(BottomNavigationEnum type) {
    type1.value = type;
  }
  Rx<BottomNavigationEnum> type1 = BottomNavigationEnum.BOOK.obs;
}
