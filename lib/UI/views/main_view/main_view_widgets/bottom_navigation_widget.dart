import 'package:com.tad.cllllb/UI/views/main_view/main_view_widgets/bottom_navigation_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../../core/enums/bottom_navigation_enum.dart';
import '../../../shared/colors.dart';
import '../../../shared/utils.dart';

class BottomNavigationWidget extends StatefulWidget {
  final BottomNavigationEnum bottomNavigation;
  final Function(BottomNavigationEnum, int) onTap;
  const BottomNavigationWidget(
      {super.key, required this.bottomNavigation, required this.onTap});

  @override
  State<BottomNavigationWidget> createState() => _BottomNavigationWidgetState();
}

class _BottomNavigationWidgetState extends State<BottomNavigationWidget> {
  late BottomNavigationController controller;
  @override
  void initState() {
    controller = BottomNavigationController(widget.bottomNavigation);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: AppColors.mainWhiteColor, boxShadow: [
        BoxShadow(
            color: AppColors.mainBlackColor.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 50,
            offset: const Offset(0, 15))
      ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // screenWidth(50).pw,
          Padding(
            padding: EdgeInsetsDirectional.only(start: screenWidth(20)),
            child: IconButton(
                onPressed: () {
                  widget.onTap(BottomNavigationEnum.HOME, 0);
                  controller.type1.value = BottomNavigationEnum.HOME;
                },
                icon: Transform.scale(
                  scale: 1.2,
                  child: SvgPicture.asset(
                    "images/home.svg",
                    width: screenWidth(12),
                  ),
                )),
          ),
          IconButton(
              onPressed: () {
                widget.onTap(BottomNavigationEnum.BOOK, 1);
                controller.type1.value = BottomNavigationEnum.BOOK;
              },
              icon: Transform.scale(
                scale: 3,
                child: SvgPicture.asset(
                  "images/book.svg",
                  //width: screenWidth(2),

                  //color: AppColors.mainYellowColor,
                ),
              )),
          //screenWidth(2).pw,
          Padding(
            padding: EdgeInsetsDirectional.only(end: screenWidth(20)),
            child: IconButton(
                onPressed: () {
                  widget.onTap(BottomNavigationEnum.PROFILE, 2);
                  controller.type1.value = BottomNavigationEnum.PROFILE;
                },
                icon: Transform.scale(
                  scale: 1.2,
                  child: SvgPicture.asset(
                    "images/profile.svg",
                    width: screenWidth(12),
                  ),
                )),
          ),
          // screenWidth(10).pw,
        ],
      ),
    );
  }
}
