import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/main_view/book_view/book_view.dart';
import 'package:com.tad.cllllb/UI/views/main_view/home_view/home_view.dart';
import 'package:com.tad.cllllb/UI/views/main_view/main_controller.dart';
import 'package:com.tad.cllllb/UI/views/main_view/profile_view/profile_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/enums/bottom_navigation_enum.dart';
import '../../../core/translation/app_translation.dart';
import '../../shared/custom_widgets/custom_app_bar.dart';
import '../../shared/custom_widgets/custom_drawer.dart';
import 'main_view_widgets/bottom_navigation_widget.dart';

class MainView extends StatefulWidget {
  final int initialPage;
  const MainView({super.key, required this.initialPage});

  @override
  State<MainView> createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
   late MainController controller ;
  @override
  void initState() {
    controller = Get.put(MainController(value: widget.initialPage));
    super.initState();
  }

  int backButtonCounter = 0;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (backButtonCounter < 1) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(tr('key_Press_again_to_exit')),
              duration: const Duration(seconds: 2),
            ),
          );
          backButtonCounter++;
          Future.delayed(
            const Duration(seconds: 2),
            () {
              backButtonCounter = 0;
            },
          );
          return false;
        } else {
          return true;
        }
      },
      child: SafeArea(
        child: Scaffold(
          key: controller.key,
          drawer: CustomDrawer(
            currentlocation: controller.currentlocation,
          ),
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(screenWidth(6)),
            child: CustomAppBar(
              onPressedLogo: () {
                if (controller.selected.value != BottomNavigationEnum.HOME) {
                  controller.selected.value = BottomNavigationEnum.HOME;
                  controller.pageController.jumpToPage(0);
                }
              },
              currentlocation: controller.currentlocation,
              drwerOnPressed: () {
                controller.key.currentState!.openDrawer();
              },
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          floatingActionButton: Obx(
            () {
              return BottomNavigationWidget(
                bottomNavigation: controller.selected.value,
                onTap: (select, pageNumber) {
                  controller.selected.value = select;
                  controller.pageController.jumpToPage(pageNumber);
                },
              );
            },
          ),
          body: PageView(
            physics: const NeverScrollableScrollPhysics(),
            controller: controller.pageController,
            children: const [
              HomeView(),
              BookView(),
              ProfileView(),
            ],
          ),
        ),
      ),
    );
  }
}
