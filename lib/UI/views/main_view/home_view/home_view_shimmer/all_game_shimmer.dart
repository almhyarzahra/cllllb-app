import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import '../../../../shared/colors.dart';

class AllGameShimmer extends StatelessWidget {
  const AllGameShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: Center(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(screenWidth(10)),
          child: Container(
            height: screenHeight(4),
            width: screenWidth(2),
            color: AppColors.mainOrangeColor,
          ),
        ),
      ),
    );
  }
}
