import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ClubsShimmer extends StatelessWidget {
  const ClubsShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: ListView.builder(
        itemCount: 4,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return Transform.scale(
            scale: 0.7,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(screenWidth(20)),
              child: Container(
                color: AppColors.mainOrangeColor,
                width: screenWidth(3),
              ),
            ),
          );
        },
      ),
    );
  }
}
