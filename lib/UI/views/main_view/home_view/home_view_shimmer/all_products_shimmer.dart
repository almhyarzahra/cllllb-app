import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class AllProductsShimmer extends StatelessWidget {
  const AllProductsShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: Padding(
        padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(screenWidth(10)),
              child: Container(
                height: screenHeight(3),
                width: screenWidth(2.3),
                color: AppColors.mainOrangeColor,
              ),
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(screenWidth(10)),
              child: Container(
                height: screenHeight(3),
                width: screenWidth(2.3),
                color: AppColors.mainOrangeColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
