import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ProductUserShimmer extends StatelessWidget {
  const ProductUserShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: ListView.builder(
        itemCount: 4,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsetsDirectional.only(start: screenWidth(40)),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(screenWidth(15)),
              child: Container(
                color: AppColors.mainOrangeColor,
                width: screenWidth(3.8),
                
              ),
            ),
          );
        },
      ),
    );
  }
}
