import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import '../../../../shared/colors.dart';

class AdvertisementsShimmer extends StatelessWidget {
  const AdvertisementsShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: Padding(
        padding: const EdgeInsetsDirectional.all(8.0),
        child: Container(
          height: screenHeight(5),
          color: AppColors.mainOrangeColor,
        ),
      ),
    );
  }
}
