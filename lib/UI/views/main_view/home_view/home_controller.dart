import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/UI/views/food_sport_detalis_view/food-sport_detalis_view.dart';
import 'package:com.tad.cllllb/core/data/models/apis/jobs_model.dart';
import 'package:com.tad.cllllb/core/data/models/apis/lottery_result_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:geolocator/geolocator.dart';
import '../../../../core/data/models/apis/advertisements_model.dart';
import '../../../../core/data/models/apis/all_clubs_model.dart';
import '../../../../core/data/models/apis/all_products_model.dart';
import '../../../../core/data/models/apis/looking_job_model.dart';
import '../../../../core/data/models/apis/news_model.dart';
import '../../../../core/data/models/apis/online_store_model.dart';
import '../../../../core/data/models/apis/user_products_model.dart';
import '../../../../core/data/repositories/club_repositories.dart';
import '../../../../core/translation/app_translation.dart';

class HomeController extends BaseController {
  RxList<UserProductsModel> userProducts = <UserProductsModel>[UserProductsModel()].obs;
  RxList<AllClubsModel> clubsModel = <AllClubsModel>[AllClubsModel()].obs;
  RxList<AdvertisementsModel> ads = <AdvertisementsModel>[AdvertisementsModel()].obs;
  RxList<AllProductsModel> allProducts = <AllProductsModel>[AllProductsModel()].obs;
  RxBool loadingHome = true.obs;
  RxList<AllProductsModel> specialOffersApp = <AllProductsModel>[AllProductsModel()].obs;
  RxList<NewsModel> news = <NewsModel>[NewsModel()].obs;
  RxList<JobsModel> jobs = <JobsModel>[JobsModel()].obs;
  RxList<OnlineStoreModel> onlineStore = <OnlineStoreModel>[OnlineStoreModel()].obs;
  RxList<LookingJobModel> lookingJob = <LookingJobModel>[LookingJobModel()].obs;
  RxList<LotteryResultModel> lotteries =<LotteryResultModel>[LotteryResultModel()].obs;

  bool get isProductsUserLoading => listType.contains(OperationType.PRODUCTS_USER);

  bool get isClubsLoading => listType.contains(OperationType.ALL_CLUBS);

  bool get isAdsLoading => listType.contains(OperationType.ADVERTISEMENTS);
  bool get isAllProductsLoading => listType.contains(OperationType.ALL_PRODUCTS);
  bool get isAdminProductsLoading => listType.contains(OperationType.ADMIN_PRODUCTS);
  bool get isNewsLoading => listType.contains(OperationType.NEWS);
  bool get isJobsLoading => listType.contains(OperationType.JOBS);
  bool get isOnLineStoreLoading => listType.contains(OperationType.ONLINE_STORE);
  bool get isLookingJobLoading => listType.contains(OperationType.LOOKING_JOB);
  bool get isResultLoading => listType.contains(OperationType.homeLotteryResult);


  LocationData? currentlocation;

  @override
  Future<void> onInit() async {
    currentlocation = await locationService.getCurrentLocation().then((value) {
      loadingHome.value = false;
      return value;
    });
    // if (currentlocation == null) {
    //   //SystemNavigator.pop();
    // }
    getProductsUser();
    getAllClubs();
    getAdvertisements();
    getAllProducts();
    getLookingJob();
    getNews();
    getJobs();
    getLotteryResult();
    //getOnlineStore();

    super.onInit();
  }

   void getLotteryResult() {
    runLoadingFutureFunction(
      type: OperationType.result,
      function: UserRepository().getHomeLotteryResult(page: 1).then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_check_connection'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (lotteries.isNotEmpty) {
                lotteries.clear();
              }
              lotteries.addAll(r);
              lotteries.map((element) {
                element.message = "l";
              }).toSet();
            },
          );
        },
      ),
    );
  }

  void getProductsUser() {
    runLoadingFutureFunction(
        type: OperationType.PRODUCTS_USER,
        function: UserRepository().getUserProducts(userId: storage.getUserId()).then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: tr('key_No_Internet_Connection'), messageType: MessageType.REJECTED);
          }, (r) {
            if (userProducts.isNotEmpty) {
              userProducts.clear();
            }

            userProducts.addAll(r);
            userProducts.map((element) {
              element.message = "l";
            }).toSet();
          });
        }));
  }

  void getAllClubs() {
    runLoadingFutureFunction(
        type: OperationType.ALL_CLUBS,
        function: ClubRepositories().getAllClubs().then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: tr('key_No_Internet_Connection'), messageType: MessageType.REJECTED);
          }, (r) {
            if (clubsModel.isNotEmpty) {
              clubsModel.clear();
            }
            if (currentlocation == null) {
              clubsModel.addAll(r);
            } else if (currentlocation != null) {
              r.map((e) {
                e.distance = calculateDistance(
                    currentlocation!.latitude!,
                    currentlocation!.longitude!,
                    double.parse(e.latitude!),
                    double.parse(e.longtitude!));
              }).toSet();
              r.sort((a, b) => a.distance!.compareTo(b.distance!));
              clubsModel.addAll(r);
            }

            clubsModel.map((element) {
              element.message = "l";
            }).toSet();
          });
        }));
  }

  void getAdvertisements() {
    runLoadingFutureFunction(
        type: OperationType.ADVERTISEMENTS,
        function: ClubRepositories().getAdvertisements().then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: tr('key_No_Internet_Connection'), messageType: MessageType.REJECTED);
          }, (r) {
            if (ads.isNotEmpty) {
              ads.clear();
            }
            ads.addAll(r);
            ads.map((element) {
              element.message = "l";
            }).toSet();
          });
        }));
  }

  void addClicks({required int id, required String url}) {
    runFullLoadingFutureFunction(
        function: ClubRepositories().addClicks(id: id).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_No_Internet_Connection'), messageType: MessageType.REJECTED);
      }, (r) async {
        final Uri urll = Uri.parse(url);
        if (!await launchUrl(urll, mode: LaunchMode.externalApplication)) {}
      });
    }));
  }

  void getAllProducts() {
    runLoadingFutureFunction(
        type: OperationType.ALL_PRODUCTS,
        function: ClubRepositories().getClubProcucts(page: 1).then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: tr('key_No_Internet_Connection'), messageType: MessageType.REJECTED);
          }, (r) {
            if (allProducts.isNotEmpty) {
              allProducts.clear();
            }
            allProducts.addAll(r);
            allProducts.map((element) {
              element.message = "l";
            }).toSet();
          });
        }));
  }

  void getProductsDetalis({required int productId}) {
    runFullLoadingFutureFunction(
        function: ClubRepositories().getProductsById(productId: productId).then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: tr('key_try_again'), messageType: MessageType.REJECTED);
      }, (r) {
        if (r.isDeleted == 1) {
          CustomToast.showMessage(message: tr('products_available'), messageType: MessageType.INFO);
        } else {
          Get.to(() => FoodSportDetalisView(
                productsModel: r,
                currentlocation: currentlocation,
              ));
        }
      });
    }));
  }

  void getNews() {
    runLoadingFutureFunction(
        type: OperationType.NEWS,
        function: ClubRepositories().getNews().then((value) {
          value.fold((l) {
            //getNews();
            CustomToast.showMessage(
                message: tr('key_No_Internet_Please Refresh'), messageType: MessageType.REJECTED);
          }, (r) {
            if (news.isNotEmpty) {
              news.clear();
            }
            news.addAll(r);
            news.map((element) {
              element.message = "l";
            }).toSet();
          });
        }));
  }

  void getJobs() {
    runLoadingFutureFunction(
        type: OperationType.JOBS,
        function: ClubRepositories().getJobs().then((value) {
          value.fold((l) {
            //getJobs();
            CustomToast.showMessage(
                message: tr('key_No_Internet_Please Refresh'), messageType: MessageType.REJECTED);
          }, (r) {
            if (jobs.isNotEmpty) {
              jobs.clear();
            }
            if (currentlocation == null) {
              jobs.addAll(r);
            } else if (currentlocation != null) {
              r.map((e) {
                e.distance = calculateDistance(
                    currentlocation!.latitude!,
                    currentlocation!.longitude!,
                    double.parse(e.club!.latitude!),
                    double.parse(e.club!.longtitude!));
              }).toSet();
              r.sort((a, b) => a.distance!.compareTo(b.distance!));
              jobs.addAll(r);
            }
            jobs.map((element) {
              element.message = "l";
            }).toSet();
          });
        }));
  }

  void getOnlineStore() {
    runLoadingFutureFunction(
        type: OperationType.ONLINE_STORE,
        function: ClubRepositories().getOnlineStore().then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: tr('key_No_Internet_Please Refresh'), messageType: MessageType.REJECTED);
          }, (r) {
            if (onlineStore.isNotEmpty) {
              onlineStore.clear();
            }
            onlineStore.addAll(r);
            onlineStore.map((element) {
              element.message = "l";
            }).toSet();
          });
        }));
  }

  void getLookingJob() {
    runLoadingFutureFunction(
        type: OperationType.LOOKING_JOB,
        function: ClubRepositories().getLookingJob().then((value) {
          value.fold((l) {
            //getLookingJob();
            CustomToast.showMessage(
                message: tr('key_No_Internet_Please Refresh'), messageType: MessageType.REJECTED);
          }, (r) {
            if (lookingJob.isNotEmpty) {
              lookingJob.clear();
            }
            lookingJob.addAll(r);
            lookingJob.map((element) {
              element.message = "l";
            }).toSet();
          });
        }));
  }

  void addTofavorite({required AllProductsModel productsModel}) {
    if (favoriteService.getCartModel(productsModel) == null) {
      favoriteService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Favorite'), messageType: MessageType.SUCCSESS);
          });
    } else {
      CustomToast.showMessage(
          message: productsModel.name! + tr('key_already_in_your_favourites'),
          messageType: MessageType.INFO);
    }
  }

  void addToCart({required AllProductsModel productsModel}) {
    if (currentlocation == null) {
      CustomToast.showMessage(
          message: tr('key_Please_turn_on_location_feature'), messageType: MessageType.INFO);
    } else if (productsModel.inProduct == 1 &&
        calculateDistance(
                currentlocation!.latitude!,
                currentlocation!.longitude!,
                double.parse(productsModel.club!.latitude!),
                double.parse(productsModel.club!.longtitude!)) >
            100.0) {
      CustomToast.showMessage(message: tr('key_within_100_metres'), messageType: MessageType.INFO);
    } else {
      cartService.addToCart(
          model: productsModel,
          count: 1,
          afterAdd: () {
            CustomToast.showMessage(
                message: tr('key_Succsess_Add_To_Card'), messageType: MessageType.SUCCSESS);
          });
    }
  }

  double calculateDistance(double lat1, double lng1, double lat2, double lng2) {
    double distance = Geolocator.distanceBetween(lat1, lng1, lat2, lng2);
    return distance;
  }
}
