import 'dart:developer';

import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/UI/views/main_view/home_view/filter_lottery_home/filter_lottery_home.dart';
import 'package:com.tad.cllllb/core/data/models/apis/lottery_result_model.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/operation_type.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LotteryHomeController extends BaseController{
   GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
    RxList<LotteryResultModel> lotteries =
      <LotteryResultModel>[LotteryResultModel()].obs;
  bool get isResultLoading => listType.contains(OperationType.homeResult);
  bool get isMoreResultLoading => listType.contains(OperationType.moreHomeResult);
  RxInt pageKey = 2.obs;
  RxBool getMore = true.obs;
  RxBool enableScrollListner = true.obs;

  RxString date = "".obs;
  DateTime? pickedDate;

   @override
  void onInit() {
    super.onInit();
    getLotteryResult();
  }


  void getLotteryResult() {
    runLoadingFutureFunction(
      type: OperationType.homeResult,
      function: UserRepository().getHomeLotteryResult(page: 1).then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_check_connection'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (lotteries.isNotEmpty) {
                lotteries.clear();
              }
              lotteries.addAll(r);
              lotteries.map((element) {
                element.message = "l";
              }).toSet();
            },
          );
        },
      ),
    );
  }

  Future<void> getMoreLottary({required int page}) async {
    log(pageKey.value.toString());
    pageKey.value = pageKey.value + 1;
    await runLoadingFutureFunction(
        type: OperationType.moreHomeResult,
        function: UserRepository().getHomeLotteryResult(page: page).then((value) {
          value.fold((l) {
            pageKey.value = pageKey.value - 1;
          }, (r) {
            if (r.isEmpty) {
              enableScrollListner.value = false;
            } else {
              lotteries.addAll(r);
            }
          });
        })).then((value) => getMore.value = true);
  }

  void filter() {
    runFullLoadingFutureFunction(
      function: UserRepository()
          .getHomeLotteryResult(
        page: 1,
        date: date.value.isEmpty ? null : date.value,
        
      )
          .then(
        (value) {
          value.fold(
            (l) {
              CustomToast.showMessage(
                  message: tr('key_try_again'),
                  messageType: MessageType.REJECTED);
            },
            (r) {
              if (r.isEmpty) {
                CustomToast.showMessage(
                    message: tr('noMatchingResults'),
                    messageType: MessageType.INFO);
              }
              else{
                Get.to(()=> FilterLotteryHome(lotteryResultModel: r));
              }
            },
          );
        },
      ),
    );
  }
}