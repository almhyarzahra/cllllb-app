import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/detalis_view/detalis_view.dart';
import 'package:com.tad.cllllb/UI/views/main_view/home_view/home_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';

import '../../../../core/translation/app_translation.dart';
import '../../../../core/utils/network_util.dart';
import '../../../shared/custom_widgets/custom_list_clubs.dart';
import '../../../shared/custom_widgets/custom_view_all.dart';
import '../../../shared/custom_widgets/virtical_divider.dart';
import '../../all_clubs_view/all_clubs_view.dart';
import '../../all_jobs_view/all_jobs_view.dart';
import '../../all_looking_job_view/all_looking_job_view.dart';
import '../../all_news_view/all_news_view.dart';
import '../../all_products_view/all_products_view.dart';
import '../../food_sport_detalis_view/food-sport_detalis_view.dart';
import '../../jobs_detalis_view/jobs_detalis_view.dart';
import '../../last_order_view/last_order_view.dart';
import '../../news_detalis_view/news_detalis_view.dart';
import '../../user_job_details_view/user_job_details_view.dart';
import 'home_view_shimmer/advertisements_shimmer.dart';
import 'home_view_shimmer/all_game_shimmer.dart';
import 'home_view_shimmer/all_products_shimmer.dart';
import 'home_view_shimmer/clubs_shimmer.dart';
import 'home_view_shimmer/product_user_shimmer.dart';
import 'lottery_home/lottery_home.dart';

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView>with AutomaticKeepAliveClientMixin {
  late HomeController controller;
  @override
  void initState() {
    controller = Get.put(HomeController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    // debugInvertOversizedImages = true;
    return Obx(() {
      return controller.loadingHome.value
          ? Lottie.asset("images/download_home.json", fit: BoxFit.fill)
          : Padding(
              padding: EdgeInsetsDirectional.only(bottom: screenHeight(15)),
              child: RefreshIndicator(
                onRefresh: () async {
                  controller.onInit();
                },
                child: SingleChildScrollView(
                  child: Column(
                    // cacheExtent: 5,
                    // addAutomaticKeepAlives: true,
                    children: [
                      screenHeight(80).ph,
                      SizedBox(
                        height: screenHeight(5.5),
                        child: Card(
                          elevation: 2,
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                                child: Row(
                                  mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                  children: [
                                    storage.getUserId() == 0
                                        ? const SizedBox.shrink()
                                        : Row(
                                            children: [
                                              CustomText(
                                                fontSize: screenWidth(35),
                                                textType: TextStyleType.CUSTOM,
                                                text:tr('key_LAST_ORDER_FOR_MR'),
                                                textColor: AppColors.mainBlackColor,
                                              ),
                                              CustomText(
                                                fontSize: screenWidth(35),
                                                textType: TextStyleType.CUSTOM,
                                                text: storage.getUserName()!.toUpperCase(),
                                                textColor: AppColors.mainYellowColor,
                                              ),
                                            ],
                                          ),
                                    Obx(() {
                                      return controller.isProductsUserLoading
                                          ? const SizedBox.shrink()
                                          : controller.userProducts.isEmpty
                                              ? const SizedBox.shrink()
                                              : controller.userProducts[0].message ==null
                                                  ? const SizedBox.shrink()
                                                  : Row(
                                                      children: [
                                                        CustomViewAll(
                                                            onPressed: () {
                                                          storage.setUserProductsList(controller.userProducts);
                                                          Get.to(LastOrderView(
                                                            currentlocation:controller.currentlocation,
                                                            productsUser: controller.userProducts,
                                                          ));
                                                        })
                                                      ],
                                                    );
                                    }),
                                  ],
                                ),
                              ),
                              screenHeight(40).ph,
                              SizedBox(
                                height: screenHeight(10),
                                child: Obx(() {
                                  return controller.isProductsUserLoading
                                      ? const ProductUserShimmer()
                                      : controller.userProducts.isEmpty
                                          ? Center(
                                              child: CustomText(
                                                textType: TextStyleType.CUSTOM,
                                                text: tr('key_No_Order_Yet'),
                                                textColor:AppColors.mainBlackColor,
                                              ),
                                            )
                                          : controller.userProducts[0].message ==null
                                              ? customFaildConnection(
                                                  onPressed: () {
                                                  controller.getProductsUser();
                                                })
                                              : ListView.builder(
                                                  cacheExtent: 2,
                                                  addAutomaticKeepAlives: true,
                                                  itemCount: controller.userProducts.length <=5? controller.userProducts.length: 5,
                                                  shrinkWrap: true,
                                                  scrollDirection:Axis.horizontal,
                                                  itemBuilder:(BuildContext context, int index) {
                                                    return InkWell(
                                                        onTap: () {
                                                          controller.getProductsDetalis(productId: controller.userProducts[index].productId!);
                                                        },
                                                        child: Stack(
                                                          children: [
                                                            CustomNetworkImage(
                                                              context.image(controller.userProducts[index].img!),
                                                              width:screenWidth(4),
                                                              height:screenWidth(5),
                                                              margin: EdgeInsetsDirectional.symmetric(horizontal:screenWidth(40)),
                                                              radius:screenWidth(30),
                                                            ),
                                                            Padding(
                                                              padding: EdgeInsetsDirectional.only(top:screenWidth(8.5),start:screenWidth(25)),
                                                              child: Container(
                                                                constraints:BoxConstraints(maxWidth:screenWidth(4.5)),
                                                                color: AppColors.mainBlackColor.withOpacity(0.5),
                                                                child: Row(
                                                                  mainAxisAlignment:MainAxisAlignment.spaceAround,
                                                                  children: [
                                                                    Flexible(
                                                                      child: CustomText(
                                                                          textType: TextStyleType.CUSTOM,
                                                                          fontSize: screenWidth(40),
                                                                          overflow: TextOverflow.ellipsis,
                                                                          text: controller.userProducts[index].productName!),
                                                                    ),
                                                                    Flexible(
                                                                      child: CustomText(
                                                                          textType: TextStyleType.CUSTOM,
                                                                          fontSize: screenWidth(40),
                                                                          overflow: TextOverflow.ellipsis,
                                                                          text: controller.userProducts[index].price.toString() +tr('key_AED')),
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        ));
                                                  },
                                                );
                                }),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(3.1),
                        child: Card(
                            elevation: 2,
                            child: Obx(() {
                              return controller.isClubsLoading
                                  ? const ClubsShimmer()
                                  : controller.clubsModel[0].message == null
                                      ? customFaildConnection(onPressed: () {
                                          controller.getAllClubs();
                                        })
                                      : Column(
                                          children: [
                                            Padding(
                                              padding: EdgeInsetsDirectional.symmetric(horizontal:screenWidth(30)),
                                              child: Row(
                                                mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                children: [
                                                  CustomText(
                                                    textType:TextStyleType.CUSTOM,
                                                    text: tr('key_All_Clubs'),
                                                    textColor: AppColors.mainBlackColor,
                                                  ),
                                                  CustomViewAll(
                                                    onPressed: () {
                                                      storage.setAllClubsList(controller.clubsModel);
                                                      Get.to(AllClubsView(
                                                        currentlocation:controller.currentlocation,
                                                        clubs: controller.clubsModel,
                                                      ));
                                                    },
                                                  )
                                                ],
                                              ),
                                            ),
                                            SizedBox(
                                                height: screenHeight(3.9),
                                                child: ListView.builder(
                                                  cacheExtent: 2,
                                                  addAutomaticKeepAlives: true,
                                                  itemCount: controller.clubsModel .length <=5 ? controller.clubsModel.length  : 5,
                                                  shrinkWrap: true,
                                                  scrollDirection:Axis.horizontal,
                                                  itemBuilder:(BuildContext context,int index) {
                                                    return CustomListClubs(
                                                      thirdText: controller.clubsModel[index].city!,
                                                      imageUrl: context.image(controller.clubsModel[index].mainImage!),
                                                      onPressed: () {
                                                        Get.to(DetalisView(
                                                          currentlocation:controller.currentlocation,
                                                          nameClub: controller.clubsModel[index] .name!,
                                                          address: controller.clubsModel[index].city!,
                                                          clubId: controller.clubsModel[index] .id!,
                                                          imageClub: controller.clubsModel[index].mainImage!,
                                                        ));
                                                      },
                                                      title: controller.clubsModel[index].name!,
                                                      secondText: controller.clubsModel[index].distance == null? ""
                                                          : controller.clubsModel[index].distance!.toInt() >1000
                                                              ? tr('key_near') +(controller.clubsModel[index].distance!.toInt() /1000).toString() +tr('key_km')
                                                              : tr('key_near') +controller.clubsModel[index].distance!.toInt().toString() +tr('key_meter'),
                                                    );
                                                  },
                                                )),
                                          ],
                                        );
                            })),
                      ),
                      Obx(() {
                        return controller.isAdsLoading
                            ? const AdvertisementsShimmer()
                            : controller.ads.isEmpty
                                ? const SizedBox.shrink()
                                : controller.ads[0].message == null
                                    ? customFaildConnection(onPressed: () {
                                        controller.getAdvertisements();
                                      })
                                    : CarouselSlider.builder(
                                        itemCount: controller.ads.length,
                                        options: CarouselOptions(
                                            reverse: false,
                                            autoPlay: true,
                                            height: screenWidth(2),
                                            enlargeCenterPage: true,
                                            viewportFraction: 0.9,
                                            enlargeStrategy: CenterPageEnlargeStrategy.scale,
                                            enlargeFactor: 0.25,
                                            onPageChanged: (index, reason) {}),
                                        itemBuilder:(context, index, realIndex) {
                                          return CustomNetworkImage(
                                            context.image(controller.ads[index].image!),
                                            width: screenWidth(1),
                                            height: screenHeight(5),
                                            radius: screenWidth(50),
                                            onTap: () => controller.addClicks(
                                                id: controller.ads[index].id!,
                                                url:controller.ads[index].url!),
                                          );
                                        },
                                      );
                      }),
                      screenWidth(20).ph,
                      Obx(() {
                        return controller.isAllProductsLoading
                            ? const AllProductsShimmer()
                            : controller.allProducts[0].message == null
                                ? customFaildConnection(onPressed: () {
                                    controller.getAllProducts();
                                  })
                                : Column(
                                    children: [
                                      Padding(
                                        padding:EdgeInsetsDirectional.symmetric(horizontal: screenWidth(30)),
                                        child: Row(
                                          mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                          children: [
                                            CustomText(
                                              textType: TextStyleType.CUSTOM,
                                              text: tr('key_New_Products_at_AllClubs'),
                                              textColor: AppColors.mainBlackColor,
                                            ),
                                            CustomViewAll(
                                              onPressed: () {
                                                Get.to(AllProductsView(
                                                  currentlocation: controller.currentlocation,
                                                  type: "app",
                                                ));
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      screenWidth(30).ph,
                                      CarouselSlider.builder(
                                        itemCount: controller.allProducts.length <= 5? controller.allProducts.length: 5,
                                        options: CarouselOptions(
                                            viewportFraction: 0.5,
                                            enableInfiniteScroll: false,
                                            height: screenHeight(2.4),
                                            padEnds: false,
                                            onPageChanged: (index, reason) {}),
                                        itemBuilder:(context, index, realIndex) {
                                          return Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Card(
                                              elevation: 4,
                                              child: SizedBox(
                                                width: screenWidth(2.2),
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        screenWidth(40).pw,
                                                        CustomNetworkImage(
                                                          context.image(controller.allProducts[index].club!.mainImage!),
                                                          width:screenWidth(8.7),
                                                          height:screenHeight(10),
                                                          border: Border.all(color: AppColors.mainYellowColor),
                                                          shape:BoxShape.circle,
                                                        ),
                                                        screenWidth(40).pw,
                                                        Flexible(
                                                          child: Column(
                                                            children: [
                                                              CustomText(
                                                                textType:TextStyleType.CUSTOM,
                                                                overflow:TextOverflow.ellipsis,
                                                                text:"${controller.allProducts[index].club!.name}",
                                                                textColor: AppColors.mainBlackColor,
                                                                fontSize:screenWidth(27),
                                                              ),
                                                              CustomText(
                                                                textType:TextStyleType.CUSTOM,
                                                                overflow:TextOverflow.ellipsis,
                                                                text:"${controller.allProducts[index].club!.city}",
                                                                textColor: AppColors.mainYellowColor,
                                                              )
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                    CustomText(
                                                      textType:TextStyleType.CUSTOM,
                                                      text: controller.allProducts[index].name!,
                                                      overflow:TextOverflow.ellipsis,
                                                      textColor: AppColors.mainBlackColor,
                                                    ),
                                                    if (controller.allProducts[index].pointsValue !=null)
                                                      CustomText(
                                                        textType: TextStyleType.CUSTOM,
                                                        text:"+${controller.allProducts[index].pointsValue} ${tr('rewardPoints')}",
                                                        overflow: TextOverflow.ellipsis,
                                                        fontSize: 12,
                                                        textColor: AppColors.mainYellowColor,
                                                      ),
                                                    if (controller.allProducts[index].pointsValue == null)
                                                      const SizedBox(height: 13),
                                                    screenWidth(30).ph,
                                                    InkWell(
                                                      onTap: () {
                                                        Get.to(
                                                            FoodSportDetalisView(
                                                          currentlocation:controller.currentlocation,
                                                          productsModel: controller.allProducts[index],
                                                        ));
                                                      },
                                                      child: CachedNetworkImage(
                                                        memCacheWidth: 150,
                                                        memCacheHeight: 170,
                                                        imageUrl: Uri.https(NetworkUtil.baseUrl,controller.allProducts[index].img!).toString(),
                                                        placeholder: (context,url) => Image.asset('images/cllllb.png'),
                                                        errorWidget: (context,url, error) =>Image.asset( 'images/cllllb.png'),
                                                        width: screenWidth(1),
                                                        height: screenHeight(7),
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                    screenWidth(25).ph,
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        InkWell(
                                                            onTap: () {
                                                              controller.addTofavorite(productsModel:controller.allProducts[index]);
                                                            },
                                                            child: SizedBox(
                                                              height:screenWidth(10),
                                                              width:screenWidth(10),
                                                              child: ClipRRect(
                                                                child: Card(
                                                                  shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(18)),
                                                                  elevation: 4,
                                                                  child: Center(
                                                                    child: SvgPicture.asset("images/like.svg"),
                                                                  ),
                                                                ),
                                                              ),
                                                            )),
                                                        CustomText(
                                                            textType:TextStyleType.CUSTOM,
                                                            textColor: AppColors.mainYellowColor,
                                                            text: controller.allProducts[index].price.toString() +tr('key_AED')),
                                                        InkWell(
                                                            onTap: () {
                                                              controller.addToCart(productsModel:controller .allProducts[index]);
                                                            },
                                                            child: SizedBox(
                                                              height:screenWidth(10),
                                                              width:screenWidth( 10),
                                                              child: ClipRRect(
                                                                child: Card(
                                                                  shape: RoundedRectangleBorder(
                                                                      borderRadius: BorderRadius.circular(18)),
                                                                  elevation: 4,
                                                                  child: Center(
                                                                    child: SvgPicture.asset("images/shopping-cart.svg"),
                                                                  ),
                                                                ),
                                                              ),
                                                            ))
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    ],
                                  );
                      }),

                      Obx(() {
                        return controller.isResultLoading ||
                                controller.lotteries.isEmpty ||
                                controller.lotteries[0].message == null
                            ? const SizedBox.shrink()
                            : Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                                child: Row(
                                  mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                  children: [
                                    CustomText(
                                      textType: TextStyleType.CUSTOM,
                                      text: tr('lotteryResults'),
                                      fontWeight: FontWeight.bold,
                                      textColor: AppColors.mainBlackColor,
                                    ),
                                    CustomViewAll(
                                      onPressed: () {
                                        Get.to(() => const LotteryHome());
                                      },
                                    ),
                                  ],
                                ),
                              );
                      }),

                      Obx(() {
                        return controller.isResultLoading
                            ? const AdvertisementsShimmer()
                            : controller.lotteries.isEmpty
                                ? const SizedBox.shrink()
                                : controller.lotteries[0].message == null
                                    ? customFaildConnection(onPressed: () {
                                        controller.getLotteryResult();
                                      })
                                    : CarouselSlider.builder(
                                        itemCount: controller.lotteries.length,
                                        options: CarouselOptions(
                                            reverse: false,
                                            autoPlay: true,
                                            height: screenWidth(2),
                                            enlargeCenterPage: true,
                                            viewportFraction: 0.9,
                                            enlargeStrategy:CenterPageEnlargeStrategy.scale,
                                            enlargeFactor: 0.25,
                                            onPageChanged: (index, reason) {}),
                                        itemBuilder:(context, index, realIndex) {
                                          return Container(
                                            width: double.infinity,
                                            decoration: BoxDecoration(
                                              color: AppColors.mainGrey2Color.withOpacity(0.5),
                                              borderRadius:BorderRadius.circular(10),
                                            ),
                                            child: Padding(
                                              padding:const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                                              child: Column(
                                                mainAxisAlignment:MainAxisAlignment.center,
                                                crossAxisAlignment:CrossAxisAlignment.start,
                                                children: [
                                                  // Center(
                                                  //     child: SvgPicture.asset("images/discount.svg")),
                                                 Row(
                                          children: [
                                            Expanded(
                                              child: CustomText(
                                                textType: TextStyleType.CUSTOM,
                                                textAlign: TextAlign.start,
                                                text: controller
                                                    .lotteries[index].title!
                                                    .useCorrectEllipsis(),
                                                overflow: TextOverflow.ellipsis,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16,
                                                textColor:
                                                    AppColors.mainBlackColor,
                                              ),
                                            ),
                                            CustomNetworkImage(
                                              context.image(controller
                                                      .lotteries[index]
                                                      .club!
                                                      .logoImage ??
                                                  ""),
                                              width: 40,
                                              height: 40,
                                              shape: BoxShape.circle,
                                            ),
                                          ],
                                        ),

                                        Text(
                                          controller
                                              .lotteries[index].description!
                                              .useCorrectEllipsis(),
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                          style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: AppColors.mainBlackColor,
                                          ),
                                        ),
                                        CustomText(
                                          textType: TextStyleType.CUSTOM,
                                          text:
                                              "${tr('clubName')}${controller.lotteries[index].club!.name ?? "Cllllb"}",
                                          textColor: AppColors.mainBlackColor,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        // Text(
                                        //   "${tr('winnerName')}${controller.lotteries[index].winner}"
                                        //       .useCorrectEllipsis(),
                                        //   overflow: TextOverflow.ellipsis,
                                        //   style: const TextStyle(
                                        //     fontSize: 13,
                                        //   ),
                                        // ),
                                        Text(
                                                "${tr('startDate')} ${controller.lotteries[index].createdAt}",
                                                style: TextStyle(
                                                    color:
                                                        AppColors.mainGreyColor,
                                                    fontSize: screenWidth(30)),
                                              ),
                                        Text(
                                          "${tr('lotteryDate')}${DateFormat("dd-MM-yyyy").format(controller.lotteries[index].exipresAt!)}"
                                              .useCorrectEllipsis(),
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                            fontSize: 13,
                                          ),
                                        ),
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                      }),

                      screenWidth(20).ph,
                      const VirticalDivider(width: 1.5,),

                      screenWidth(20).ph,

                      Obx(() {
                        return controller.isNewsLoading
                            ? const AllGameShimmer()
                            : controller.news.isEmpty
                                ? const SizedBox.shrink()
                                : controller.news[0].message == null
                                    ? customFaildConnection(onPressed: () {
                                        controller.getNews();
                                      })
                                    : Column(
                                        children: [
                                          Padding(
                                            padding:EdgeInsetsDirectional.symmetric( horizontal:screenWidth(30)),
                                            child: Row(
                                              mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                              children: [
                                                CustomText(
                                                  textType:TextStyleType.CUSTOM,
                                                  text: tr('key_New_news_ADS_For_Clubs'),
                                                  textColor: AppColors.mainBlackColor,
                                                ),
                                                // controller.news.length == 0
                                                //     ? SizedBox.shrink()
                                                //     :
                                                CustomViewAll(
                                                  onPressed: () {
                                                    Get.to(AllNewsView(
                                                      currentlocation:controller .currentlocation,
                                                      news: controller.news,
                                                    ));
                                                  },
                                                )
                                              ],
                                            ),
                                          ),
                                          screenWidth(30).ph,
                                          // controller.news.length == 0
                                          //     ? CustomText(
                                          //         textType: TextStyleType.CUSTOM,
                                          //         text: tr('key_no_ads'),
                                          //         textColor: AppColors.mainBlackColor,
                                          //       )
                                          //     :
                                          SizedBox(
                                            height: screenHeight(2.8),
                                            child: ListView.builder(
                                              cacheExtent: 2,
                                              addAutomaticKeepAlives: true,
                                              itemCount:controller.news.length <= 5 ? controller.news.length: 5,
                                              shrinkWrap: true,
                                              scrollDirection: Axis.horizontal,
                                              itemBuilder:(BuildContext context,int index) {
                                                return Card(
                                                  elevation: 4,
                                                  child: SizedBox(
                                                    width: screenWidth(2),
                                                    child: Column(
                                                      children: [
                                                        CustomNetworkImage(
                                                          context.image(controller.news[index] .club!.mainImage!),
                                                          width: screenWidth(7),
                                                          height:screenHeight(10),
                                                          border: Border.all(color: AppColors.mainYellowColor),
                                                          shape:BoxShape.circle,
                                                        ),
                                                        CustomText(
                                                          textType:TextStyleType.CUSTOM,
                                                          text: controller.news[index].title!,
                                                          textColor: AppColors.mainBlackColor,
                                                          overflow: TextOverflow.ellipsis,
                                                        ),
                                                        CustomNetworkImage(
                                                          context.image(controller.news[index].image!),
                                                          width:screenWidth(2.6),
                                                          height:screenHeight(11),
                                                          radius:screenWidth(20),
                                                        ),
                                                        screenWidth(20).ph,
                                                        CustomButton(
                                                          text: tr('key_news_detalis'),
                                                          onPressed: () {
                                                            Get.to(NewsDetalisView(
                                                              currentlocation:controller.currentlocation,
                                                              news: controller.news[index],
                                                            ));
                                                          },
                                                          textSize:screenWidth(35),
                                                          circularBorder:screenWidth(20),
                                                          widthButton: 3,
                                                          heightButton: 15,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              },
                                            ),
                                          )
                                        ],
                                      );
                      }),
                      controller.news.isEmpty
                          ? const SizedBox.shrink()
                          : Column(
                              children: [
                                screenWidth(20).ph,
                                const VirticalDivider(
                                  width: 1.5,
                                ),
                              ],
                            ),
                      screenWidth(20).ph,
                      Obx(() {
                        return controller.isJobsLoading
                            ? const AllGameShimmer()
                            : controller.jobs.isEmpty
                                ? const SizedBox.shrink()
                                : controller.jobs[0].message == null
                                    ? customFaildConnection(onPressed: () {
                                        controller.getJobs();
                                      })
                                    : Column(
                                        children: [
                                          Padding(
                                            padding:EdgeInsetsDirectional.symmetric(horizontal:screenWidth(30)),
                                            child: Row(
                                              mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                              children: [
                                                CustomText(
                                                  textType:TextStyleType.CUSTOM,
                                                  text: tr('New_Job_from_All_Clubs'),
                                                  textColor:AppColors.mainBlackColor,
                                                ),
                                                // controller.jobs.length == 0
                                                //     ? SizedBox.shrink()
                                                //     :
                                                CustomViewAll(
                                                  onPressed: () {
                                                    Get.to(AllJobsView(
                                                      currentlocation:controller.currentlocation,
                                                      jobs: controller.jobs,
                                                    ));
                                                  },
                                                )
                                              ],
                                            ),
                                          ),
                                          screenWidth(30).ph,
                                          // controller.jobs.length == 0
                                          //     ? CustomText(
                                          //         textType: TextStyleType.CUSTOM,
                                          //         text: tr('key_no_jobs'),
                                          //         textColor: AppColors.mainBlackColor,
                                          //       )
                                          //     :
                                          SizedBox(
                                            height: screenHeight(3),
                                            child: ListView.builder(
                                              cacheExtent: 2,
                                              addAutomaticKeepAlives: true,
                                              itemCount:controller.jobs.length <= 5? controller.jobs.length: 5,
                                              shrinkWrap: true,
                                              scrollDirection: Axis.horizontal,
                                              itemBuilder:(BuildContext context,int index) {
                                                return Padding(
                                                  padding: EdgeInsetsDirectional.symmetric(horizontal:screenWidth(40)),
                                                  child: Container(
                                                    width: screenWidth(2),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(color: AppColors.mainBlackColor),
                                                        borderRadius:BorderRadius.circular(screenWidth(20))),
                                                    child: Padding(
                                                      padding:EdgeInsetsDirectional.symmetric( horizontal:screenWidth(20)),
                                                      child: Column(
                                                        children: [
                                                        Row(
                                                          children: [
                                                            CustomNetworkImage(
                                                              context.image(controller.jobs[index].club!.mainImage!),
                                                              width:screenWidth(7),
                                                              height:screenHeight(10),
                                                              border: Border.all(color: AppColors.mainYellowColor),
                                                              shape: BoxShape.circle,
                                                            ),
                                                            screenWidth(40).pw,
                                                            Flexible(
                                                              child: Column(
                                                                children: [
                                                                  CustomText(
                                                                    textType:TextStyleType.CUSTOM,
                                                                    text: controller.jobs[index].club!.name!,
                                                                    overflow:TextOverflow.ellipsis,
                                                                    textColor:AppColors.mainBlackColor,
                                                                    fontSize:screenWidth(25),
                                                                  ),
                                                                  CustomText(
                                                                    textType:TextStyleType.CUSTOM,
                                                                    overflow:TextOverflow.ellipsis,
                                                                    text: controller.jobs[index].club!.city!,
                                                                    textColor:AppColors.mainYellowColor,
                                                                    fontSize:screenWidth(25),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        CustomText(
                                                          textType:TextStyleType.CUSTOM,
                                                          text: controller.jobs[index].distance ==null
                                                              ? ""
                                                              : controller.jobs[index].distance!.toInt() >1000
                                                                  ? tr('key_near') +(controller.jobs[index].distance!.toInt() /1000).toString() +tr('key_km')
                                                                  : tr('key_near') +controller.jobs[index].distance!.toInt().toString() +tr('key_meter'),
                                                          textColor: AppColors.mainYellowColor,
                                                          fontSize:screenWidth(40),
                                                        ),
                                                        CustomNetworkImage(
                                                          context.image(controller.jobs[index].image!),
                                                          width:screenWidth(2.6),
                                                          height:screenHeight(10),
                                                          onTap: () => Get.to(
                                                            JobsDetalisView(
                                                              currentlocation: controller.currentlocation,
                                                              job: controller.jobs[index],
                                                            ),
                                                          ),
                                                        ),
                                                        CustomText(
                                                          textType:TextStyleType.CUSTOM,
                                                          text: controller.jobs[index].jobTitle!,
                                                          textColor: AppColors.mainYellowColor,
                                                          fontSize:screenWidth(30),
                                                          overflow: TextOverflow.ellipsis,
                                                          fontWeight:FontWeight.bold,
                                                        ),
                                                        screenWidth(50).ph,
                                                        CustomText(
                                                          textType:TextStyleType.CUSTOM,
                                                          text: tr('key_expected_salary'),
                                                          textColor: AppColors.mainYellowColor,
                                                        ),
                                                        CustomText(
                                                          textType:TextStyleType.CUSTOM,
                                                          text: controller.jobs[index].expectSalary! +tr('key_AED'),
                                                          textColor: AppColors.mainYellowColor,
                                                        )
                                                      ]),
                                                    ),
                                                  ),
                                                );
                                              },
                                            ),
                                          )
                                        ],
                                      );
                      }),
                      controller.jobs.isEmpty
                          ? const SizedBox.shrink()
                          : Column(
                              children: [
                                screenWidth(20).ph,
                                const VirticalDivider(
                                  width: 1.5,
                                ),
                              ],
                            ),
                      screenWidth(20).ph,

                      Obx(() {
                        return controller.isLookingJobLoading
                            ? const AllGameShimmer()
                            : controller.lookingJob.isEmpty
                                ? const SizedBox.shrink()
                                : controller.lookingJob[0].message == null
                                    ? customFaildConnection(onPressed: () {
                                        controller.getLookingJob();
                                      })
                                    : Column(
                                        children: [
                                          Padding(
                                            padding:EdgeInsetsDirectional.symmetric(horizontal:screenWidth(30)),
                                            child: Row(
                                              mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                              children: [
                                                CustomText(
                                                  textType:TextStyleType.CUSTOM,
                                                  text: tr('key_looking_job'),
                                                  textColor:AppColors.mainBlackColor,
                                                ),
                                                // controller.lookingJob.length == 0
                                                //     ? SizedBox.shrink()
                                                //     :
                                                CustomViewAll(
                                                  onPressed: () {
                                                    Get.to(AllLookingJobView(
                                                      currentlocation: controller.currentlocation,
                                                      lookingJob: controller.lookingJob,
                                                    ));
                                                  },
                                                )
                                              ],
                                            ),
                                          ),
                                          screenWidth(30).ph,
                                          // controller.lookingJob.length == 0
                                          //     ? CustomText(
                                          //         textType: TextStyleType.CUSTOM,
                                          //         text: tr('key_no_jobs'),
                                          //         textColor: AppColors.mainBlackColor,
                                          //       )
                                          //     :
                                          SizedBox(
                                            height: screenHeight(3),
                                            child: ListView.builder(
                                              cacheExtent: 2,
                                              addAutomaticKeepAlives: true,
                                              shrinkWrap: true,
                                              scrollDirection: Axis.horizontal,
                                              itemCount: controller.lookingJob.length <=5
                                                  ? controller.lookingJob.length
                                                  : 5,
                                              itemBuilder:(BuildContext context,int index) {
                                                return Padding(
                                                  padding: EdgeInsetsDirectional.symmetric(horizontal:screenWidth(40)),
                                                  child: Container(
                                                    width: screenWidth(2),
                                                    decoration: BoxDecoration(
                                                      border: Border.all(color: AppColors.mainBlackColor),
                                                        borderRadius:BorderRadius.circular(screenWidth(20))),
                                                    child: Padding(
                                                      padding:EdgeInsetsDirectional.symmetric(horizontal:screenWidth(20)),
                                                      child: Column(
                                                        children: [
                                                          Row(
                                                            children: [
                                                              CustomNetworkImage(
                                                                context.image(controller.lookingJob[index].user!.image ??""),
                                                                width:screenWidth(7),
                                                                height:screenHeight(10),
                                                                border: Border.all(color: AppColors.mainYellowColor),
                                                                shape: BoxShape.circle,
                                                              ),
                                                              screenWidth(40)
                                                                  .pw,
                                                              Flexible(
                                                                child: Column(
                                                                  children: [
                                                                    CustomText(
                                                                      textType:TextStyleType.CUSTOM,
                                                                      text: controller.lookingJob[index].user!.name!,
                                                                      overflow:TextOverflow.ellipsis,
                                                                      textColor:AppColors.mainGreyColor,
                                                                      fontSize:screenWidth(25),
                                                                    ),
                                                                    CustomText(
                                                                      textType:TextStyleType.CUSTOM,
                                                                      overflow:TextOverflow.ellipsis,
                                                                      text: controller.lookingJob[index].jobTitle!,
                                                                      textColor:AppColors.mainYellowColor,
                                                                      fontSize:screenWidth(25),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:MainAxisAlignment.center,
                                                            children: [
                                                              CustomText(
                                                                textType:TextStyleType.CUSTOM,
                                                                text: tr('key_number'),
                                                                textColor: AppColors.mainYellowColor,
                                                                fontSize:screenWidth(35),
                                                              ),
                                                              Flexible(
                                                                  child: CustomText(
                                                                      textType:TextStyleType.CUSTOM,
                                                                      text: controller.lookingJob[index].user!.phone ??tr('key_not_found'),
                                                                      textColor:AppColors.mainGreyColor,
                                                                      fontSize:screenWidth(35),
                                                                      overflow:TextOverflow.ellipsis)),
                                                            ],
                                                          ),
                                                          Row(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            children: [
                                                              CustomText(
                                                                textType: TextStyleType.CUSTOM,
                                                                text: tr('key_mail'),
                                                                textColor: AppColors.mainYellowColor,
                                                                fontSize:screenWidth(35),
                                                              ),
                                                              Flexible(
                                                                child: CustomText(
                                                                    textType:TextStyleType.CUSTOM,
                                                                    text: controller.lookingJob[index].user!.email!,
                                                                    textColor:AppColors.mainGreyColor,
                                                                    fontSize:screenWidth(35),
                                                                    overflow:TextOverflow.ellipsis),
                                                              )
                                                            ],
                                                          ),
                                                          CustomButton(
                                                            text: tr('key_details'),
                                                            onPressed: () {
                                                              Get.to(
                                                                  UserJobDetailsView(
                                                                currentlocation:controller.currentlocation,
                                                                lookingJob:controller.lookingJob[index],
                                                              ));
                                                            },
                                                            heightButton: 15,
                                                            circularBorder:screenWidth(20),
                                                          ),
                                                          screenWidth(20).ph,
                                                          CustomText(
                                                            textType:TextStyleType.CUSTOM,
                                                            text: tr('key_years_experience'),
                                                            textColor: AppColors.mainYellowColor,
                                                          ),
                                                          CustomText(
                                                            textType:TextStyleType.CUSTOM,
                                                            text: controller.lookingJob[index].yearExperince!,
                                                            textColor: AppColors.mainGreyColor,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                );
                                              },
                                            ),
                                          )
                                        ],
                                      );
                      }),
                      controller.lookingJob.isEmpty
                          ? const SizedBox.shrink()
                          : Column(
                              children: [
                                screenWidth(20).ph,
                                const VirticalDivider( width: 1.5,),
                              ],
                            ),
                      screenWidth(20).ph,
                    ],
                  ),
                ),
              ),
            );
    });
  }

  @override
  bool get wantKeepAlive => true;
}
