import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

class InvitationCode extends StatelessWidget {
  final String invitationCode;
  const InvitationCode(
    this.invitationCode, {
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 54,
      margin: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: const Color(0xFFEEEDED),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SvgPicture.asset("images/ticket_star.svg"),
          Padding(
            padding: const EdgeInsetsDirectional.only(start: 2, end: 2),
            child: Text(
              tr('invitationCode'),
              style: const TextStyle(
                fontSize: 12,
              ),
            ),
          ),
          Container(
            width: 170,
            height: 30,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: AppColors.mainWhiteColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Text(
              invitationCode,
              style: const TextStyle(
                fontSize: 12,
              ),
            ),
          ),
          IconButton(
            onPressed: () {
              Clipboard.setData(ClipboardData(text: invitationCode)).then(
                (value) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(tr('copiedInvitionCode')),
                    ),
                  );
                },
              );
            },
            icon: SvgPicture.asset("images/copy_code.svg"),
          ),
        ],
      ),
    );
  }
}
