import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/views/photo_view/photo_view.dart';
import 'package:com.tad.cllllb/core/data/models/apis/user_info_model.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class ProfileCard extends StatelessWidget {
  final UserInfo userInfo;
  const ProfileCard({super.key, required this.userInfo});

  @override
  Widget build(BuildContext context) {
    RxDouble point=storage.getUserPoint().obs;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 3,
            child: Row(
              children: [
                CustomNetworkImage(
                  context.image(userInfo.image ?? ""),
                  shape: BoxShape.circle,
                  height: 60,
                  width: 60,
                  boxFit: BoxFit.contain,
                  onTap: () {
                    Get.to(
                      PhotoWidgetView(imageUrl: [userInfo.image ?? ""]),
                    );
                  },
                  border: Border.all(
                    color: AppColors.mainYellowColor,
                    width: 3,
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        userInfo.name!,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          color: AppColors.mainBlackColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      Text(
                        userInfo.email!,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          fontSize: 12,
                          color: AppColors.mainGreyColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 35.0,
            padding: const EdgeInsetsDirectional.symmetric(horizontal: 5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: AppColors.mainGrey2Color,
            ),
            child: Row(
              children: [
                Obx(() {
                  return Text(
                  "$point",
                  style: const TextStyle(
                    color: AppColors.mainBlackColor,
                  ),
                );
                }),
                const SizedBox(
                  width: 6,
                ),
                SvgPicture.asset("images/coin.svg"),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
