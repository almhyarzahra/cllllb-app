import 'package:flutter/material.dart';

class SharedContainer extends StatelessWidget {
  final String text;
  const SharedContainer(
    this.text, {
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
      margin: const EdgeInsetsDirectional.only(bottom: 5, start: 7.5, end: 7.5),
      decoration: BoxDecoration(
        color: const Color(0xFFEEEDED),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Text(text),
    );
  }
}
