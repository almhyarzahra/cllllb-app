import 'dart:io';

import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/main_view/home_view/home_controller.dart';
import 'package:com.tad.cllllb/UI/views/main_view/profile_view/profile_controller.dart';
import 'package:com.tad.cllllb/UI/views/main_view/profile_view/widgets/invitation_code.dart';
import 'package:com.tad.cllllb/UI/views/main_view/profile_view/widgets/profile_card.dart';
import 'package:com.tad.cllllb/UI/views/main_view/profile_view/widgets/shared_container.dart';
import 'package:com.tad.cllllb/UI/views/rewards_view/rewards_view.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../../core/translation/app_translation.dart';
import '../../../shared/custom_widgets/custom_text_field.dart';
import '../../job_advertisement_view/job_advertisement_view.dart';
import '../../sign_in_view/sign_in_view.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({super.key});

  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView>
    with AutomaticKeepAliveClientMixin {
  late ProfileController controller;
  @override
  void initState() {
    controller = Get.put(ProfileController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return storage.getUserId() == 0
        ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: tr('key_login_complete'),
                  textColor: AppColors.mainYellowColor,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomButton(
                    text: tr('key_Yes'),
                    onPressed: () async {
                      storage.setCartList([]);
                      storage.setFavoriteList([]);
                      storage.setFirstLanuch(true);
                      storage.setIsLoggedIN(false);
                      storage.globalSharedPrefs.remove(storage.PREF_TOKEN_INFO);
                      //storage.setTokenInfo(null);
                      await storage.setUserId(0);
                      storage.globalSharedPrefs.remove(storage.PREF_USER_NAME);

                      storage.setGoogle(false);
                      storage.globalSharedPrefs
                          .remove(storage.PREF_MOBILE_NUMBER);
                      storage.globalSharedPrefs.remove(storage.PREF_EMAIL);

                      Get.offAll(() => const SignInView());
                    },
                    widthButton: 4,
                    circularBorder: screenWidth(10),
                  ),
                  screenWidth(20).pw,
                  CustomButton(
                    text: tr('key_No'),
                    onPressed: () {
                      Get.back();
                    },
                    widthButton: 4,
                    circularBorder: screenWidth(10),
                  ),
                ],
              ),
              screenHeight(10).ph,
              CustomText(
                textType: TextStyleType.CUSTOM,
                text: tr('scanYourFriends'),
                textColor: AppColors.mainBlackColor,
              ),
              InkWell(
                onTap: () async {
                  controller.scanBarcodeNormal();
                },
                child: const Icon(
                  Icons.qr_code_scanner_sharp,
                  size: 60,
                ),
              ),
            ],
          )
        : Obx(
            () {
              return controller.isUserInfoLoading
                  ? const SpinKitCircle(
                      color: AppColors.mainYellowColor,
                    )
                  : controller.userInfo.value.userInfo == null
                      ? Padding(
                          padding: EdgeInsetsDirectional.symmetric(
                              vertical: screenWidth(2)),
                          child: customFaildConnection(
                            onPressed: () {
                              controller.onInit();
                            },
                          ),
                        )
                      : RefreshIndicator(
                          onRefresh: () async {
                            controller.onInit();
                          },
                          child: ListView(
                            children: [
                              ProfileCard(
                                userInfo: controller.userInfo.value.userInfo!,
                              ),
                              IconButton(
                                  onPressed: () {
                                    Get.defaultDialog(
                                      title: "Your QRCode",
                                      content: SvgPicture.network(
                                        controller.userInfo.value.userInfo!
                                            .shareBarcode!,
                                        width: 200,
                                        height: 200,
                                      ),
                                    );
                                  },
                                  icon: const Icon(
                                    Icons.qr_code_2,
                                    size: 40,
                                  )),
                              screenWidth(20).ph,
                              Obx(
                                () {
                                  return controller
                                              .userInfo.value.userInfo!.phone ==
                                          null
                                      ? const SizedBox.shrink()
                                      : SharedContainer(
                                          "${tr('key_Phone')}${controller.userInfo.value.userInfo!.phone!}");
                                },
                              ),
                              SharedContainer(
                                  "${tr('key_Wallet_contains')}${controller.userInfo.value.userWallet}${tr('key_AED')}"),
                              Center(
                                child: CustomText(
                                  textType: TextStyleType.CUSTOM,
                                  text: tr('key_fill_wallet'),
                                  textColor: AppColors.mainRedColor,
                                ),
                              ),
                              screenWidth(10).ph,
                              InvitationCode(controller
                                  .userInfo.value.userInfo!.referenceCode!),

                              screenWidth(6).ph,
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      Get.to(
                                        JobAdvertisementView(
                                          currentlocation:
                                              Get.find<HomeController>()
                                                  .currentlocation,
                                          userInfo: controller.userInfo.value,
                                        ),
                                      );
                                    },
                                    child: Container(
                                      width: 150,
                                      height: 125,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        color: AppColors.mainYellowColor,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Text(
                                        tr('key_job_advertisement'),
                                        style: const TextStyle(
                                          color: AppColors.mainWhiteColor,
                                        ),
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Get.to(
                                        () => RewardsView(
                                          currentlocation:
                                              Get.find<HomeController>()
                                                  .currentlocation,
                                          userPoints: double.parse(controller
                                              .userInfo
                                              .value
                                              .userInfo!
                                              .totalPoints!
                                              .toString()),
                                        ),
                                      );
                                    },
                                    child: Container(
                                      width: 150,
                                      height: 125,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        color: AppColors.mainYellowColor,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Text(
                                        tr('myRewards'),
                                        style: const TextStyle(
                                          color: AppColors.mainWhiteColor,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              screenWidth(20).ph,
                              Padding(
                                padding: EdgeInsetsDirectional.symmetric(
                                    horizontal: screenWidth(10)),
                                child: CustomButton(
                                  text: tr('key_Edit_Your_Information'),
                                  onPressed: () {
                                    Get.bottomSheet(
                                      Container(
                                        height: screenHeight(1),
                                        decoration: BoxDecoration(
                                          color: AppColors.mainWhiteColor,
                                          borderRadius: BorderRadius.only(
                                            topLeft:
                                                Radius.circular(screenWidth(8)),
                                            topRight:
                                                Radius.circular(screenWidth(8)),
                                          ),
                                        ),
                                        child: Padding(
                                          padding: EdgeInsetsDirectional.only(
                                              top: screenHeight(30)),
                                          child: ListView(
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  Get.defaultDialog(
                                                    title:
                                                        tr('key_Please_Select'),
                                                    content: Column(
                                                      children: [
                                                        CustomButton(
                                                          text:
                                                              tr('key_Camera'),
                                                          onPressed: () {
                                                            controller
                                                                .selectImageFromCamera();
                                                          },
                                                          widthButton: 3,
                                                          circularBorder:
                                                              screenWidth(20),
                                                        ),
                                                        screenWidth(20).ph,
                                                        CustomButton(
                                                          text:
                                                              tr('key_Gallery'),
                                                          onPressed: () {
                                                            controller
                                                                .selectImageFromGallery();
                                                          },
                                                          widthButton: 3,
                                                          circularBorder:
                                                              screenWidth(20),
                                                        ),
                                                      ],
                                                    ),
                                                  );
                                                },
                                                child: Obx(
                                                  () {
                                                    return controller
                                                            .isSelected.value
                                                        ? Container(
                                                            height:
                                                                screenHeight(8),
                                                            decoration:
                                                                BoxDecoration(
                                                              border: Border.all(
                                                                  color: AppColors
                                                                      .mainYellowColor),
                                                              shape: BoxShape
                                                                  .circle,
                                                              image:
                                                                  DecorationImage(
                                                                fit: BoxFit
                                                                    .contain,
                                                                image: FileImage(
                                                                    File(controller
                                                                        .image1!
                                                                        .path)),
                                                              ),
                                                            ),
                                                            child: controller
                                                                    .isSelected
                                                                    .isFalse
                                                                ? Icon(
                                                                    Icons
                                                                        .camera_alt_rounded,
                                                                    size:
                                                                        screenWidth(
                                                                            8),
                                                                    color: AppColors
                                                                        .mainWhiteColor,
                                                                  )
                                                                : null,
                                                          )
                                                        : CircleAvatar(
                                                            radius: 40,
                                                            backgroundColor:
                                                                AppColors
                                                                    .mainYellowColor,
                                                            child: controller
                                                                        .image ==
                                                                    null
                                                                ? Icon(
                                                                    Icons
                                                                        .camera_alt_rounded,
                                                                    size:
                                                                        screenWidth(
                                                                            8),
                                                                    color: AppColors
                                                                        .mainWhiteColor,
                                                                  )
                                                                : null,
                                                          );
                                                  },
                                                ),
                                              ),
                                              CustomText(
                                                textType: TextStyleType.CUSTOM,
                                                text:
                                                    tr('key_Select_Your_Image'),
                                                textColor:
                                                    AppColors.mainBlackColor,
                                              ),
                                              screenWidth(10).ph,
                                              Padding(
                                                padding: EdgeInsetsDirectional
                                                    .symmetric(
                                                        horizontal:
                                                            screenWidth(20)),
                                                child: Form(
                                                  key: controller.formKey,
                                                  child: Column(
                                                    children: [
                                                      CustomTextField(
                                                        hintText:
                                                            tr('key_Your_Name'),
                                                        controller: controller
                                                            .nameController,
                                                        contentPaddingLeft:
                                                            screenWidth(10),
                                                        validator: (value) {
                                                          return value!.isEmpty
                                                              ? tr(
                                                                  'key_Please_Check_Your_Name')
                                                              : null;
                                                        },
                                                      ),
                                                      screenWidth(20).ph,
                                                      CustomTextField(
                                                        hintText: tr(
                                                                'key_Mobile_Number') +
                                                            " example 971...",
                                                        typeInput: TextInputType
                                                            .number,
                                                        controller: controller
                                                            .mobileNumberController,
                                                        contentPaddingLeft:
                                                            screenWidth(10),
                                                        validator: (value) {
                                                          return value!.isEmpty
                                                              ? tr(
                                                                  'key_Check_Number')
                                                              : null;
                                                        },
                                                      ),
                                                      screenWidth(20).ph,
                                                      CustomTextField(
                                                        hintText:
                                                            tr('key_Email'),
                                                        controller: controller
                                                            .emailController,
                                                        contentPaddingLeft:
                                                            screenWidth(10),
                                                        validator: (value) {
                                                          return value!
                                                                      .isEmpty ||
                                                                  !GetUtils
                                                                      .isEmail(
                                                                          value)
                                                              ? tr(
                                                                  'key_Please_Check_Email')
                                                              : null;
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              screenWidth(20).ph,
                                              Padding(
                                                padding: EdgeInsetsDirectional
                                                    .symmetric(
                                                        horizontal:
                                                            screenWidth(5)),
                                                child: CustomButton(
                                                  text: tr('key_Edit'),
                                                  onPressed: () {
                                                    controller.otp();
                                                  },
                                                  circularBorder:
                                                      screenWidth(20),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                  circularBorder: screenWidth(20),
                                ),
                              ),
                              // screenWidth(20).ph,
                              // Padding(
                              //   padding: EdgeInsetsDirectional.symmetric(
                              //       horizontal: screenWidth(4)),
                              //   child: CustomButton(
                              //     textSize: screenWidth(30),
                              //     text: tr('key_job_advertisement'),
                              //     onPressed: () {
                              //       Get.to(
                              //         JobAdvertisementView(
                              //           currentlocation:
                              //               Get.find<HomeController>()
                              //                   .currentlocation,
                              //           userInfo: controller.userInfo.value,
                              //         ),
                              //       );
                              //     },
                              //     circularBorder: screenWidth(20),
                              //   ),
                              // ),
                              screenWidth(5).ph,

                              // screenWidth(25).ph,
                              // CustomText(
                              //   textType: TextStyleType.CUSTOM,
                              //   text: "Member Since :2023",
                              //   textColor: AppColors.mainBlackColor,
                              // ),
                              // screenWidth(15).ph,
                              // Padding(
                              //   padding: EdgeInsetsDirectional.symmetric(
                              //       horizontal: screenWidth(20)),
                              //   child: Row(
                              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              //     children: [
                              //       CustomButton(
                              //         text: "My Membership",
                              //         onPressed: () {
                              //           Get.to(MyMembershipView());
                              //         },
                              //         textSize: screenWidth(30),
                              //         widthButton: 3,
                              //         circularBorder: screenWidth(10),
                              //       ),
                              //       CustomButton(
                              //         text: "Discount Cards",
                              //         textSize: screenWidth(30),
                              //         onPressed: () {
                              //           Get.to(DiscountCardsView());
                              //         },
                              //         widthButton: 3,
                              //         circularBorder: screenWidth(10),
                              //       ),
                              //     ],
                              //   ),
                              // ),
                            ],
                          ),
                        );
            },
          );
  }

  @override
  bool get wantKeepAlive => true;
}
