import 'dart:convert';
import 'dart:developer';

import 'package:com.tad.cllllb/UI/views/personal_information_view/personal_information_view.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/enums/request_status.dart';
import 'package:com.tad.cllllb/core/services/base_controller.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
// ignore: depend_on_referenced_packages
import 'package:path/path.dart' as path;

import '../../../../core/data/models/apis/user_info_model.dart';
import '../../../../core/data/repositories/user_repository.dart';
import '../../../../core/translation/app_translation.dart';
import '../../../shared/colors.dart';
import '../../../shared/custom_widgets/custom_toast.dart';
import '../../verification_view/verification_view.dart';

class ProfileController extends BaseController {
  TextEditingController mobileNumberController =
      TextEditingController(text: storage.getmobileNumber() ?? "");
  TextEditingController emailController =
      TextEditingController(text: storage.getEmail());
  TextEditingController nameController =
      TextEditingController(text: storage.getUserName());
  Rx<UserInfoModel> userInfo = UserInfoModel().obs;
  bool get isUserInfoLoading => requestStatus.value == RequestStatus.LOADING;
  RxBool isSelected = false.obs;
  final ImagePicker picker = ImagePicker();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  RxInt code = 0.obs;

  XFile? image;
  XFile? image1;
  CroppedFile? croppedImage;
  String? base64Image;

  @override
  void onInit() {
    log(storage.getTokenInfo().toString());
    if (storage.getUserId() != 0) {
      getUserInfo();
    }
    //storage.getUserId ? null : getUserInfo();
    super.onInit();
  }

  void getUserInfo() {
    runLoadingFutureFunction(
        function: UserRepository()
            .getUserInfo(userId: storage.getUserId())
            .then((value) {
      value.fold((l) {
        getUserInfo();
      }, (r) {
        userInfo.value = r;
        storage.setUserPoint(double.parse(r.userInfo!.totalPoints!.toString()));
      });
    }));
  }

  Future<void> scanBarcodeNormal() async {
    String? barcodeScanRes;
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
        '#f99417',
        tr('back'),
        true,
        ScanMode.DEFAULT,
      );

      log(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'failed';
    }
    if (barcodeScanRes != "-1") {
      Get.offAll(PersonalInformationView(invitationCode: barcodeScanRes));
      // getProductBarCode(barCode: barcodeScanRes);
      log("message");
    }
  }

  void selectImageFromCamera() async {
    isSelected.value = false;

    image = await picker.pickImage(source: ImageSource.camera);
    croppedImage = await ImageCropper().cropImage(
      sourcePath: image!.path,
      cropStyle: CropStyle.rectangle,
      compressFormat: ImageCompressFormat.jpg,
      compressQuality: 100,
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: tr('key_Cropper'),
            toolbarColor: AppColors.mainYellowColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
      ],
    );
    if (croppedImage != null) {
      image1 = XFile(path.absolute(croppedImage!.path));
      isSelected.value = true;
      imageToBase64();
    } else if (image != null) {
      image1 = image;
      isSelected.value = true;
      imageToBase64();
    }
    //if (image1 != null) {}

    Get.back();
  }

  void selectImageFromGallery() async {
    isSelected.value = false;

    image = await picker.pickImage(source: ImageSource.gallery);
    croppedImage = await ImageCropper().cropImage(
      sourcePath: image!.path,
      cropStyle: CropStyle.rectangle,
      compressFormat: ImageCompressFormat.jpg,
      compressQuality: 100,
      uiSettings: [
        AndroidUiSettings(
          toolbarTitle: tr("key_Cropper"),
          toolbarColor: AppColors.mainYellowColor,
          toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.original,
          lockAspectRatio: false,
        ),
      ],
    );
    if (croppedImage != null) {
      image1 = XFile(path.absolute(croppedImage!.path));
      isSelected.value = true;
      imageToBase64();
    } else if (image != null) {
      image1 = image;
      isSelected.value = true;
      imageToBase64();
    }

    Get.back();
  }

  Future<void> imageToBase64() async {
    if (image1 != null) {
      final bytes = await image1!.readAsBytes();
      base64Image = base64.encode(bytes);
    }
  }

  void otp() {
    if (formKey.currentState!.validate()) {
      if (mobileNumberController.text.trim().toString() ==
          storage.getmobileNumber()) {
        editProfile();
      } else {
        runFullLoadingFutureFunction(
            function: UserRepository().getCodeRandom().then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: tr('key_check_connection'),
                messageType: MessageType.REJECTED);
          }, (r) {
            code.value = r.code!;
            //print(code.value);
            runFullLoadingFutureFunction(
                function: UserRepository()
                    .otp(
                        code:
                            mobileNumberController.text.trim() == "971559039837"
                                ? "000000"
                                : code.value.toString(),
                        phone: mobileNumberController.text.trim().toString())
                    .then((value) {
              value.fold((l) {
                CustomToast.showMessage(
                    message: tr('key_try_again'),
                    messageType: MessageType.REJECTED);
              }, (r) {
                Get.to(VerificationView(
                  code: mobileNumberController.text.trim() == "971559039837"
                      ? 000000
                      : code.value,
                  type: "edit",
                ));
              });
            }));
          });
        }));
      }
    }
  }

  void editProfile() {
    if (formKey.currentState!.validate()) {
      runFullLoadingFutureFunction(
          function: UserRepository()
              .editProfile(
                  userId: storage.getUserId(),
                  email: emailController.text.trim().toString(),
                  phone: mobileNumberController.text.trim().toString(),
                  name: nameController.text.trim().toString(),
                  image: base64Image)
              .then((value) {
        value.fold((l) {
          if (l == null) {
            CustomToast.showMessage(
                message: tr('key_Please_Try_Again'),
                messageType: MessageType.REJECTED);
          } else {
            CustomToast.showMessage(
                message: tr('key_Please_Check_Information'),
                messageType: MessageType.REJECTED);
          }
        }, (r) {
          storage.setUserName(nameController.text.trim().toString());
          storage.setEmail(emailController.text.trim().toString());
          storage
              .setmobileNumber(mobileNumberController.text.trim().toString());
          CustomToast.showMessage(
              message: tr('key_success'), messageType: MessageType.SUCCSESS);
          onInit();

          Get.back();
        });
      }));
    }
  }
}
