import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../views/photo_view/photo_view.dart';
import '../colors.dart';
import '../utils.dart';

class CustomBookBar extends StatefulWidget {
  const CustomBookBar(
      {super.key,
      required this.imageName,
      required this.text,
      required this.imageUrl});
  final String imageName;
  final String text;
  final String imageUrl;

  @override
  State<CustomBookBar> createState() => _CustomBookBarState();
}

class _CustomBookBarState extends State<CustomBookBar> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: screenHeight(8),
      child: Card(
        elevation: 2,
        child: Padding(
          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(50)),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                IconButton(
                    onPressed: () {
                      Get.back();
                    },
                    icon: Icon(
                      Icons.arrow_back_ios_new,
                      size: screenWidth(20),
                    )),
                CustomNetworkImage(
                  widget.imageName,
                  height: screenHeight(5),
                  width: screenWidth(5),
                  border: Border.all(color: AppColors.mainYellowColor),
                  shape: BoxShape.circle,
                  onTap: () => Get.to(() => PhotoWidgetView(
                        imageUrl: [widget.imageUrl],
                      )),
                ),
                CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: "  ${widget.text}",
                  fontSize: screenWidth(25),
                  textColor: AppColors.mainBlackColor,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
