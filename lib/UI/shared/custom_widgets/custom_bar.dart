import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../colors.dart';
import '../utils.dart';

class CustomBar extends StatefulWidget {
  const CustomBar({super.key, required this.text});
  final String text;

  @override
  State<CustomBar> createState() => _CustomBarState();
}

class _CustomBarState extends State<CustomBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: screenHeight(18),
      color: AppColors.mainGrey3Color,
      child: Row(
        children: [
          IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(
                Icons.arrow_back_ios_new,
                size: screenWidth(20),
                color: AppColors.mainWhiteColor,
              )),
          CustomText(
            text: widget.text,
            textType: TextStyleType.TITLE,
            textAlign: TextAlign.start,
          ),
        ],
      ),
    );
  }
}
