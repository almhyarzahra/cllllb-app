import 'package:flutter/material.dart';
import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';

class VirticalDivider extends StatelessWidget {
  const VirticalDivider({super.key, this.width});
  final double? width;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: screenWidth(width ?? 2),
        height: screenHeight(500),
        color: AppColors.mainGrey3Color.withOpacity(0.5),
      ),
    );
  }
}
