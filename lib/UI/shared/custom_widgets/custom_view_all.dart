import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:flutter/material.dart';
import '../../../core/translation/app_translation.dart';
import '../colors.dart';
import '../utils.dart';

class CustomViewAll extends StatefulWidget {
  const CustomViewAll({super.key, required this.onPressed});
  final VoidCallback onPressed;

  @override
  State<CustomViewAll> createState() => _CustomViewAllState();
}

class _CustomViewAllState extends State<CustomViewAll> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: screenWidth(15),
      width: screenWidth(4),
      child: CustomButton(
        text: tr('key_View_All'),
        circularBorder: screenWidth(10),
        onPressed: () {
          widget.onPressed();
        },
        backgroundColor: AppColors.mainBlackColor,
        widthButton: 4.4,
        textFontWight: FontWeight.bold,
        textSize: screenWidth(35),
        heightButton: 20,
      ),
    );
  }
}
