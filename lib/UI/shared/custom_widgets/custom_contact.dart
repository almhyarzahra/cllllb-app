import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../colors.dart';

class CustomContact extends StatefulWidget {
  const CustomContact({super.key});

  @override
  State<CustomContact> createState() => _CustomContactState();
}

class _CustomContactState extends State<CustomContact> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.mainGrey3Color,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Transform.scale(
            scale: 1.9,
            child: IconButton(
                onPressed: () {},
                icon: SvgPicture.asset("images/facebook-logo.svg")),
          ),
          Transform.scale(
            scale: 1.9,
            child: IconButton(
                onPressed: () {}, icon: SvgPicture.asset("images/twitter.svg")),
          ),
          Transform.scale(
            scale: 1.9,
            child: IconButton(
                onPressed: () {},
                icon: SvgPicture.asset("images/LinkedIn.svg")),
          ),
          Transform.scale(
            scale: 1.7,
            child: IconButton(
                onPressed: () {},
                icon: SvgPicture.asset("images/instagram .svg")),
          ),
          Transform.scale(
            scale: 1.9,
            child: IconButton(
                onPressed: () {}, icon: SvgPicture.asset("images/youtube.svg")),
          ),
        ],
      ),
    );
  }
}
