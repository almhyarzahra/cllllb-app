import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';

import '../colors.dart';
import '../utils.dart';

class CustomSecondBar extends StatefulWidget {
  const CustomSecondBar(
      {super.key,
      required this.svgName,
      required this.text1,
      required this.text2});
  final String svgName;
  final String text1;
  final String text2;

  @override
  State<CustomSecondBar> createState() => _CustomSecondBarState();
}

class _CustomSecondBarState extends State<CustomSecondBar> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: screenHeight(11),
      child: Card(
        elevation: 2,
        child: Padding(
          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomNetworkImage(
                context.image(widget.svgName),
                width: screenWidth(8),
                height: screenHeight(10),
              ),
              // CachedNetworkImage(
              //   imageUrl:
              //       Uri.https(NetworkUtil.baseUrl, widget.svgName).toString(),
              //   placeholder: (context, url) =>
              //       Lottie.asset("images/download.json"),
              //   errorWidget: (context, url, error) => const Icon(Icons.error),
              //   width: screenWidth(8),
              //   height: screenHeight(10),
              //   fit: BoxFit.contain,
              // ),
              CustomText(
                textType: TextStyleType.CUSTOM,
                text: widget.text1,
                textColor: AppColors.mainBlackColor,
              ),
              CustomText(
                textType: TextStyleType.CUSTOM,
                text: widget.text2,
                textColor: AppColors.mainBlackColor,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
