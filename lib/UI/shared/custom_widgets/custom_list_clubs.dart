import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:flutter/material.dart';

import '../utils.dart';

class CustomListClubs extends StatefulWidget {
  const CustomListClubs(
      {super.key,
      required this.onPressed,
      required this.imageUrl,
      required this.title,
      required this.secondText,
      required this.thirdText});

  final VoidCallback onPressed;
  final String imageUrl;
  final String title;
  final String secondText;
  final String thirdText;

  @override
  State<CustomListClubs> createState() => _CustomListClubsState();
}

class _CustomListClubsState extends State<CustomListClubs> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.onPressed();
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(screenWidth(20)),
        child: SizedBox(
          width: screenWidth(3),
          child: Card(
            elevation: 3,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomNetworkImage(
                    widget.imageUrl,
                    height: screenHeight(8),
                    border: Border.all(color: AppColors.mainYellowColor),
                    shape: BoxShape.circle,
                  ),
                  // Container(
                  //   height: screenHeight(8),
                  //   decoration: BoxDecoration(
                  //       border: Border.all(color: AppColors.mainYellowColor),
                  //       shape: BoxShape.circle,
                  //       image: DecorationImage(
                  //           fit: BoxFit.contain,
                  //           image: CachedNetworkImageProvider(widget.imageUrl))),
                  // ),
                  //screenWidth(40).ph,
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    fontSize: screenWidth(30),
                    text: widget.title,
                    overflow: TextOverflow.ellipsis,
                    textColor: AppColors.mainBlackColor,
                    textAlign: TextAlign.start,
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    fontSize: screenWidth(30),
                    text: widget.thirdText,
                    overflow: TextOverflow.ellipsis,
                    textColor: AppColors.mainBlackColor,
                    textAlign: TextAlign.center,
                  ),
                  //screenWidth(40).ph,
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    fontSize: screenWidth(30),
                    text: widget.secondText,
                    overflow: TextOverflow.ellipsis,
                    textColor: AppColors.mainYellowColor,
                    textAlign: TextAlign.start,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
