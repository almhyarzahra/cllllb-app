import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text_button.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/about_cllllb_view/about_cllllb_view.dart';
import 'package:com.tad.cllllb/UI/views/main_view/main_view.dart';
import 'package:com.tad.cllllb/UI/views/my_booking_view/my_booking_view.dart';
import 'package:com.tad.cllllb/UI/views/orders_view/orders_view.dart';
import 'package:com.tad.cllllb/UI/views/sign_in_view/sign_in_view.dart';
import 'package:com.tad.cllllb/core/data/repositories/user_repository.dart';
import 'package:com.tad.cllllb/core/enums/message_type.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:location/location.dart';
import '../../../core/services/base_controller.dart';
import '../../../core/translation/app_translation.dart';
import '../../views/contanct_cllllb_view/contanct_cllllb_view.dart';
import '../../views/my_subscription_view/my_subscription_view.dart';
import '../../views/settings_view/settings_view.dart';
import '../../views/terms_condition_view/terms_condition_view.dart';

class CustomDrawer extends StatefulWidget {
  const CustomDrawer({super.key, required this.currentlocation});
  final LocationData? currentlocation;

  @override
  State<CustomDrawer> createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  late DrawerController controller;
  @override
  void initState() {
    controller = Get.put(DrawerController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.only(top: screenWidth(6), bottom: screenWidth(7)),
      child: Container(
        width: double.infinity,
        color: AppColors.mainBlackColor.withOpacity(0.5),
        child: Padding(
          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
          child: ListView(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  screenHeight(30).ph,
                  IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: Icon(
                        Icons.arrow_back_ios_new,
                        size: screenWidth(20),
                        color: AppColors.mainWhiteColor,
                      )),
                  CustomTextButton(
                    colorText: AppColors.mainWhiteColor,
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(21),
                    onPressed: () {
                      Get.to(SettingsView(
                        currentlocation: widget.currentlocation,
                      ));
                    },
                    text: tr('key_Settings'),
                  ),
                  screenHeight(70).ph,
                  CustomTextButton(
                    colorText: AppColors.mainWhiteColor,
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(21),
                    onPressed: () {
                      Get.to(OrdersView(
                        currentlocation: widget.currentlocation,
                      ));
                    },
                    text: tr('key_Orders'),
                  ),
                  screenHeight(70).ph,
                  CustomTextButton(
                    colorText: AppColors.mainWhiteColor,
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(21),
                    onPressed: () {
                      Get.to(MyBookingView(
                        currentlocation: widget.currentlocation,
                      ));
                    },
                    text: tr('key_Booking'),
                  ),
                  // screenHeight(70).ph,
                  // CustomTextButton(
                  //   colorText: AppColors.mainWhiteColor,
                  //   fontWeight: FontWeight.bold,
                  //   fontSize: screenWidth(21),
                  //   onPressed: () {
                  //     Get.to(ChangeNumberView());
                  //   },
                  //   text: "Change Number",
                  // ),
                  screenHeight(70).ph,

                  CustomTextButton(
                    colorText: AppColors.mainWhiteColor,
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(21),
                    onPressed: () {
                      Get.to(MySubscriptionView(
                        currentlocation: widget.currentlocation,
                      ));
                    },
                    text: tr('key_subscription'),
                  ),
                  screenHeight(70).ph,
                  CustomTextButton(
                    colorText: AppColors.mainWhiteColor,
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(21),
                    onPressed: () {
                      Get.to(AboutCllllbView(
                        currentlocation: widget.currentlocation,
                      ));
                    },
                    text: tr('key_About_Cllllb'),
                  ),
                  screenHeight(70).ph,
                  CustomTextButton(
                    colorText: AppColors.mainWhiteColor,
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(21),
                    onPressed: () {
                      Get.to(TermsConditionView(
                        currentlocation: widget.currentlocation,
                      ));
                    },
                    text: tr('key_Terms_Condition'),
                  ),
                  screenHeight(70).ph,
                  CustomTextButton(
                    colorText: AppColors.mainWhiteColor,
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(21),
                    onPressed: () {
                      Get.to(ContanctCllllbView(
                        currentlocation: widget.currentlocation,
                      ));
                    },
                    text: tr('key_Contanct_Cllllb'),
                  ),
                  screenHeight(70).ph,
                  // CustomTextButton(
                  //   colorText: AppColors.mainWhiteColor,
                  //   fontWeight: FontWeight.bold,
                  //   fontSize: screenWidth(21),
                  //   onPressed: () {},
                  //   text: "Share Cllllb",
                  // ),
                  // screenHeight(70).ph,
                  // CustomTextButton(
                  //   colorText: AppColors.mainWhiteColor,
                  //   fontWeight: FontWeight.bold,
                  //   fontSize: screenWidth(21),
                  //   onPressed: () {},
                  //   text: "Rate Us",
                  // ),
                  // screenHeight(70).ph,
                  storage.getUserId() == 0
                      ? const SizedBox.shrink()
                      : CustomTextButton(
                          colorText: AppColors.mainWhiteColor,
                          fontWeight: FontWeight.bold,
                          fontSize: screenWidth(21),
                          onPressed: () {
                            Get.defaultDialog(
                                title: tr('key_Are_You_Sure'),
                                titleStyle: const TextStyle(color: AppColors.mainYellowColor),
                                content: Column(
                                  children: [
                                    CustomButton(
                                      text: tr('key_Yes'),
                                      onPressed: () async {
                                        if (storage.getGoogle()) {
                                          await GoogleSignIn().signOut();
                                          await FirebaseAuth.instance.signOut();
                                        }

                                        storage.setCartList([]);
                                        storage.setFavoriteList([]);
                                        storage.setFirstLanuch(true);
                                        storage.setIsLoggedIN(false);
                                        storage.globalSharedPrefs.remove(storage.PREF_TOKEN_INFO);
                                        //storage.setTokenInfo(null);
                                        await storage.setUserId(0);
                                        storage.globalSharedPrefs.remove(storage.PREF_USER_NAME);

                                        storage.setGoogle(false);
                                        storage.globalSharedPrefs
                                            .remove(storage.PREF_MOBILE_NUMBER);
                                        storage.globalSharedPrefs.remove(storage.PREF_EMAIL);

                                        Get.offAll(() => const SignInView(),
                                            fullscreenDialog: true);
                                      },
                                      widthButton: 4,
                                      circularBorder: screenWidth(10),
                                    ),
                                    screenWidth(20).ph,
                                    CustomButton(
                                      text: tr('key_No'),
                                      onPressed: () {
                                        Get.back();
                                      },
                                      widthButton: 4,
                                      circularBorder: screenWidth(10),
                                    ),
                                  ],
                                ));
                          },
                          text: tr('key_Log_Out'),
                        ),
                  screenHeight(70).ph,
                  storage.getUserId() == 0
                      ? const SizedBox.shrink()
                      : CustomTextButton(
                          colorText: AppColors.mainWhiteColor,
                          fontWeight: FontWeight.bold,
                          fontSize: screenWidth(21),
                          onPressed: () {
                            Get.defaultDialog(
                                title: tr('key_Are_You_Sure'),
                                titleStyle: const TextStyle(color: AppColors.mainYellowColor),
                                content: Column(
                                  children: [
                                    CustomButton(
                                      text: tr('key_Yes'),
                                      onPressed: () async {
                                        controller.deleteAccount();
                                      },
                                      widthButton: 4,
                                      circularBorder: screenWidth(10),
                                    ),
                                    screenWidth(20).ph,
                                    CustomButton(
                                      text: tr('key_No'),
                                      onPressed: () {
                                        Get.back();
                                      },
                                      widthButton: 4,
                                      circularBorder: screenWidth(10),
                                    ),
                                  ],
                                ));
                          },
                          text: tr('key_delete_account'),
                        ),
                  screenHeight(70).ph,

                  CustomTextButton(
                    colorText: AppColors.mainWhiteColor,
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(21),
                    onPressed: () {
                      Get.offAll(const MainView(initialPage: 0,));
                    },
                    text: tr('key_Go_To_Home'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DrawerController extends BaseController {
  void deleteAccount() {
    runFullLoadingFutureFunction(
        function: UserRepository().deleteUser(userId: storage.getUserId()).then((value) {
      value.fold((l) {
        CustomToast.showMessage(message: tr('key_try_again'), messageType: MessageType.REJECTED);
      }, (r) async {
        if (storage.getGoogle()) {
          await GoogleSignIn().signOut();
          await FirebaseAuth.instance.signOut();
        }

        storage.setCartList([]);
        storage.setFavoriteList([]);
        storage.setFirstLanuch(true);
        storage.setIsLoggedIN(false);
        storage.globalSharedPrefs.remove(storage.PREF_TOKEN_INFO);
        //storage.setTokenInfo(null);
        storage.setUserId(0);
        storage.globalSharedPrefs.remove(storage.PREF_USER_NAME);

        storage.setGoogle(false);
        storage.globalSharedPrefs.remove(storage.PREF_MOBILE_NUMBER);
        storage.globalSharedPrefs.remove(storage.PREF_EMAIL);
        CustomToast.showMessage(
            message: tr('key_succsess_delete'), messageType: MessageType.SUCCSESS);

        Get.offAll(const SignInView());
      });
    }));
  }
}
