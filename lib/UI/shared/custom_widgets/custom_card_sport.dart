import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_network_image.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../colors.dart';
import '../utils.dart';

class CustomCardStore extends StatefulWidget {
  const CustomCardStore(
      {super.key,
      required this.imageUrl,
      required this.textItem,
      required this.textLength,
      required this.textPrice,
      required this.textBunch,
      required this.onPressedLike,
      required this.onPressedCart,
      required this.onPressedImage,
      required this.rewardPoint});
  final String imageUrl;
  final String textItem;
  final String textLength;
  final String textPrice;
  final String textBunch;
  final double? rewardPoint;
  final VoidCallback onPressedLike;
  final VoidCallback onPressedCart;
  final VoidCallback onPressedImage;

  @override
  State<CustomCardStore> createState() => _CustomCardStoreState();
}

class _CustomCardStoreState extends State<CustomCardStore> {
  final imageBoxFit = Rx<BoxFit>(BoxFit.fill);
  @override
  Widget build(BuildContext context) {
    // final deviceInfo = DeviceInfoPlugin();

    // deviceInfo.iosInfo.then((iosInfo) {
    //   if (iosInfo.model == 'iPhone SE') {
    //     imageBoxFit.value = BoxFit.contain;
    //   }
    // });

    return Stack(
      children: [
        Column(
          children: [
            SizedBox(
              height: screenHeight(2.7),
              width: screenWidth(2),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    screenWidth(20),
                  ),
                ),
                elevation: 4,
                child: Column(
                  children: [
                    CustomNetworkImage(
                      widget.imageUrl,
                      width: screenWidth(1),
                      height: screenHeight(6),
                      radius: screenWidth(20),
                      onTap: () => widget.onPressedImage(),
                    ),
                    screenWidth(20).ph,
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: widget.textItem,
                      overflow: TextOverflow.ellipsis,
                      textColor: AppColors.mainBlackColor,
                      fontSize: screenWidth(22),
                    ),
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      fontSize: screenWidth(40),
                      text: widget.textLength,
                      textColor: AppColors.mainGrey3Color,
                    ),
                    if (widget.rewardPoint != null)
                      CustomText(
                        textType: TextStyleType.CUSTOM,
                        fontSize: screenWidth(30),
                        fontWeight: FontWeight.bold,
                        text: "+${widget.rewardPoint} ${tr('rewardPoints')}",
                        textColor: AppColors.mainYellowColor,
                      ),
                    if (widget.rewardPoint == null) screenWidth(35).ph,
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      fontSize: screenWidth(30),
                      fontWeight: FontWeight.bold,
                      text: widget.textPrice,
                      textColor: AppColors.mainYellowColor,
                    ),
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      fontSize: screenWidth(30),
                      text: widget.textBunch,
                      overflow: TextOverflow.ellipsis,
                      textColor: AppColors.mainBlackColor,
                    ),
                    screenWidth(40).ph,
                  ],
                ),
              ),
            ),
          ],
        ),
        Padding(
          padding: EdgeInsetsDirectional.only(
            top: screenHeight(3),
          ),
          child: SizedBox(
            width: screenWidth(2.1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                    onTap: () {
                      widget.onPressedLike();
                    },
                    child: SizedBox(
                      height: screenWidth(9),
                      width: screenWidth(9),
                      child: ClipRRect(
                        child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18)),
                          elevation: 4,
                          child: Center(
                            child: SvgPicture.asset(
                              "images/like.svg",
                              width: screenWidth(15),
                            ),
                          ),
                        ),
                      ),
                    )),
                screenWidth(15).pw,
                InkWell(
                  onTap: () {
                    widget.onPressedCart();
                  },
                  child: SizedBox(
                    height: screenWidth(9),
                    width: screenWidth(9),
                    child: ClipRRect(
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18)),
                        elevation: 4,
                        child: Center(
                          child: SvgPicture.asset(
                            "images/shopping-cart.svg",
                            width: screenWidth(15),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
