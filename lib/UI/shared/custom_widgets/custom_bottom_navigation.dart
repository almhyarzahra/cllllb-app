import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/UI/views/main_view/main_controller.dart';
import 'package:com.tad.cllllb/UI/views/main_view/main_view.dart';
import 'package:com.tad.cllllb/core/enums/bottom_navigation_enum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class CustomBottomNavigation extends StatelessWidget {
  const CustomBottomNavigation({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: AppColors.mainWhiteColor, boxShadow: [
        BoxShadow(
            color: AppColors.mainBlackColor.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 50,
            offset: const Offset(0, 15))
      ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // screenWidth(50).pw,
          Padding(
            padding: EdgeInsetsDirectional.only(start: screenWidth(20)),
            child: IconButton(
                onPressed: () async{
                  Get.delete<MainController>();
                 await Get.offAll(()=> const MainView(initialPage: 0,));
                 Get.find<MainController>().selected.value = BottomNavigationEnum.HOME;
                },
                icon: Transform.scale(
                  scale: 1.2,
                  child: SvgPicture.asset(
                    "images/home.svg",
                    width: screenWidth(12),
                  ),
                )),
          ),
          IconButton(
              onPressed: () async{
                Get.delete<MainController>();
                 await Get.offAll(()=> const MainView(initialPage: 1,));
                 Get.find<MainController>().selected.value = BottomNavigationEnum.BOOK;

              },
              icon: Transform.scale(
                scale: 3,
                child: SvgPicture.asset(
                  "images/book.svg",
                  //width: screenWidth(2),

                  //color: AppColors.mainYellowColor,
                ),
              )),
          //screenWidth(2).pw,
          Padding(
            padding: EdgeInsetsDirectional.only(end: screenWidth(20)),
            child: IconButton(
                onPressed: () async{
                  Get.delete<MainController>();
                  await Get.offAll(()=> const MainView(initialPage: 2,));
                  Get.find<MainController>().selected.value = BottomNavigationEnum.PROFILE;

                },
                icon: Transform.scale(
                  scale: 1.2,
                  child: SvgPicture.asset(
                    "images/profile.svg",
                    width: screenWidth(12),
                  ),
                )),
          ),
          // screenWidth(10).pw,
        ],
      ),
    );
  }
}