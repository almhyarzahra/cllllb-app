import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/utils/network_util.dart';
import '../utils.dart';

class CustomCardStore extends StatefulWidget {
  const CustomCardStore(
      {super.key,
      required this.imageName,
      required this.text,
      this.distans,
      required this.onPressed});
  final String imageName;
  final String text;
  final double? distans;
  final VoidCallback onPressed;

  @override
  State<CustomCardStore> createState() => _CustomCardStoreState();
}

class _CustomCardStoreState extends State<CustomCardStore> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: () {
            widget.onPressed();
          },
          child: Container(
            width: screenWidth(2.3),
            height: screenHeight(4),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(screenWidth(20)),
                border: Border.all(color: AppColors.mainGrey3Color)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                screenWidth(50).ph,
                SvgPicture.network(
                  Uri.https(NetworkUtil.baseUrl, widget.imageName).toString(),
                  placeholderBuilder: (context) => Image.asset(
                    "images/cllllb.png",
                    width: screenWidth(5),
                    height: screenHeight(7),
                  ),
                  width: screenWidth(5),
                  height: screenHeight(7),
                  fit: BoxFit.contain,
                ),
                screenWidth(widget.distans ?? 50).ph,
                Container(
                  width: double.infinity,
                  height: screenHeight(40),
                  color: AppColors.mainYellowColor,
                  child: Center(
                    child: CustomText(
                      textType: TextStyleType.CUSTOM,
                      fontSize: screenWidth(30),
                      fontWeight: FontWeight.bold,
                      text: widget.text,
                    ),
                  ),
                ),
                screenWidth(20).ph
              ],
            ),
          ),
        ),
      ],
    );
  }
}
