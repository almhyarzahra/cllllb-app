import 'package:flutter/material.dart';
import '../colors.dart';
import '../utils.dart';

enum TextStyleType {
  // ignore: constant_identifier_names
  TITLE, // 40px
  // ignore: constant_identifier_names
  SUBTITLE, // 25px
  // ignore: constant_identifier_names
  BODYBIG, // 22px
  // ignore: constant_identifier_names
  BODY, // 20px
  // ignore: constant_identifier_names
  SMALL, // 14px
  // ignore: constant_identifier_names
  CUSTOM,
}

class CustomText extends StatelessWidget {
  const CustomText({
    super.key,
    required this.textType,
    required this.text,
    this.textColor = AppColors.mainWhiteColor,
    this.fontSize,
    this.fontWeight,
    this.textAlign = TextAlign.center,
    this.textDecoration = TextDecoration.none,
    this.overflow,
  });

  final TextStyleType textType;
  final String text;
  final Color? textColor;
  final TextAlign? textAlign;
  final double? fontSize;
  final FontWeight? fontWeight;
  final TextDecoration? textDecoration;
  final TextOverflow? overflow;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      overflow: overflow ,
      softWrap: true,
      //maxLines: 6,
      textScaleFactor: 1.0,
      style: getTextStyle(),
    );
  }

  TextStyle getTextStyle() {
    switch (textType) {
      case TextStyleType.TITLE:
        return TextStyle(
          height: 1.2,
          decoration: textDecoration,
          color: textColor,
          fontSize: screenWidth(22),
          fontWeight: FontWeight.w900,
        );

      case TextStyleType.SUBTITLE:
        return TextStyle(
          height: 1.2,
          decoration: textDecoration,
          color: textColor,
          fontSize: screenWidth(24),
          //fontWeight: fontWeight ?? FontWeight.w400,
        );

      case TextStyleType.BODYBIG:
        return TextStyle(
          height: 1.2,
          decoration: textDecoration,
          color: textColor,
          fontSize: screenWidth(25),
          // fontWeight: fontWeight ?? FontWeight.w700,
        );

      case TextStyleType.BODY:
        return TextStyle(
          height: 1.2,
          decoration: textDecoration,
          color: textColor,
          fontSize: screenWidth(25),
          //fontWeight: fontWeight ?? FontWeight.w400,
        );

      case TextStyleType.SMALL:
        return TextStyle(
          height: 1.2,
          decoration: textDecoration,
          color: textColor,
          fontSize: screenWidth(32),
          //fontWeight: fontWeight ?? FontWeight.w400,
        );

      case TextStyleType.CUSTOM:
        return TextStyle(
          height: 1.2,
          decoration: textDecoration,
          color: textColor,
          fontSize: fontSize,
          fontWeight: fontWeight,
        );
    }
  }
}
