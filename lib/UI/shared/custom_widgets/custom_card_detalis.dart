import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../colors.dart';
import '../utils.dart';

class CustomCardDetalis extends StatefulWidget {
  const CustomCardDetalis(
      {super.key,
      required this.onPressed,
      required this.svgName,
      required this.text,
      this.prefixDistance});
  final VoidCallback onPressed;
  final String svgName;
  final String text;
  final double? prefixDistance;

  @override
  State<CustomCardDetalis> createState() => _CustomCardDetalisState();
}

class _CustomCardDetalisState extends State<CustomCardDetalis> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.onPressed();
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(screenWidth(20)),
        child: SizedBox(
          width: screenWidth(2.5),
          height: screenHeight(6),
          child: Card(
            elevation: 3,
            shadowColor: AppColors.mainGrey3Color,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                screenWidth(widget.prefixDistance ?? 50).ph,
                SvgPicture.asset("images/${widget.svgName}.svg"),
                screenWidth(50).ph,
                CustomText(
                  textType: TextStyleType.CUSTOM,
                  fontSize: screenWidth(35),
                  text: widget.text,
                  textColor: AppColors.mainBlackColor,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
