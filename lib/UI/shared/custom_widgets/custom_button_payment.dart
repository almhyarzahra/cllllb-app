import 'package:flutter/material.dart';
import '../colors.dart';
import '../utils.dart';

class CustomButtonPayment extends StatefulWidget {
  const CustomButtonPayment(
      {super.key, required this.onPressed, required this.icon});
  final VoidCallback onPressed;
  final IconData icon;

  @override
  State<CustomButtonPayment> createState() => _CustomButtonPaymentState();
}

class _CustomButtonPaymentState extends State<CustomButtonPayment> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.onPressed();
      },
      child: Container(
          width: screenWidth(4),
          height: screenHeight(17),
          decoration: BoxDecoration(
              color: AppColors.mainYellowColor,
              borderRadius: BorderRadius.circular(screenWidth(10))),
          child: Icon(
            widget.icon,
            color: AppColors.mainWhiteColor,
          )),
    );
  }
}
