import 'package:com.tad.cllllb/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.cllllb/UI/views/favorite_view/favorite_view.dart';
import 'package:com.tad.cllllb/UI/views/main_view/main_controller.dart';
import 'package:com.tad.cllllb/UI/views/main_view/main_view.dart';
import 'package:com.tad.cllllb/UI/views/shopping_view/shopping_view.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import '../../../core/enums/bottom_navigation_enum.dart';
import '../../views/notifications_view/notifications_view.dart';
import '../colors.dart';
import '../utils.dart';

class CustomAppBar extends StatefulWidget {
  const CustomAppBar(
      {super.key, required this.drwerOnPressed, required this.currentlocation, this.onPressedLogo});
  final VoidCallback drwerOnPressed;
  final LocationData? currentlocation;
  final void Function()? onPressedLogo;

  @override
  State<CustomAppBar> createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: AppColors.mainWhiteColor,
      leading: IconButton(
          onPressed: () {
            widget.drwerOnPressed();
          },
          icon: SvgPicture.asset(
            "images/drawer.svg",
            height: screenHeight(30),
          )),
      actions: [
        InkWell(
          onTap: widget.onPressedLogo ??
              () {
                if (Get.find<MainController>().selected.value != BottomNavigationEnum.HOME) {
                  Get.find<MainController>().selected.value = BottomNavigationEnum.HOME;
                  Get.find<MainController>().pageController.jumpToPage(0);
                }
                Get.offAll(const MainView(initialPage: 0,));

                // Get.offAll(Get.find<MainController>().onClick(BottomNavigationEnum.HOME, 0));
              },
          child: Image.asset(
            "images/logo1.png",
            width: Get.width * (100.0 / Get.width),
            height: Get.height * (45.0 / Get.height),
          ),
        ),
        (screenWidth(13)).pw,
        Row(
          children: [
            Obx(() {
              return InkWell(
                onTap: () {
                  Get.to(FavoriteView(
                    currentlocation: widget.currentlocation,
                  ));
                },
                child: Stack(
                  children: [
                    SvgPicture.asset(
                      "images/like.svg",
                      width: screenWidth(15),
                    ),
                    identical(favoriteService.cartCount, 0)
                        ? const SizedBox.shrink()
                        : Padding(
                            padding: EdgeInsets.only(
                              left: screenWidth(25),
                              top: screenWidth(50),
                            ),
                            child: Container(
                              // width: screenWidth(13),
                              height: screenHeight(50),
                              decoration: BoxDecoration(
                                color: AppColors.mainRedColor,
                                borderRadius: BorderRadius.circular(screenWidth(50)),
                              ),
                              child: Center(
                                  child: CustomText(
                                text: favoriteService.cartCount.toString(),
                                textColor: AppColors.mainWhiteColor,
                                textType: TextStyleType.CUSTOM,
                              )),
                            ),
                          ),
                  ],
                ),
              );
            }),
            screenWidth(20).pw,
            Obx(() {
              return InkWell(
                onTap: () {
                  Get.to(ShoppingView(
                    currentlocation: widget.currentlocation,
                  ));
                },
                child: Stack(
                  children: [
                    SvgPicture.asset(
                      "images/store.svg",
                      width: screenWidth(15),
                    ),
                    identical(cartService.cartCount, 0)
                        ? const SizedBox.shrink()
                        : Padding(
                            padding: EdgeInsets.only(
                              left: screenWidth(25),
                              top: screenWidth(50),
                            ),
                            child: Container(
                              // width: screenWidth(13),
                              height: screenHeight(50),
                              decoration: BoxDecoration(
                                color: AppColors.mainRedColor,
                                borderRadius: BorderRadius.circular(screenWidth(50)),
                              ),
                              child: Center(
                                  child: CustomText(
                                text: cartService.cartCount.toString(),
                                textColor: AppColors.mainWhiteColor,
                                textType: TextStyleType.CUSTOM,
                              )),
                            ),
                          ),
                  ],
                ),
              );
            }),
            screenWidth(20).pw,
            Obx(() {
              return InkWell(
                onTap: () {
                  notificationService.counter.value = 0;
                  Get.to(NotificationsView(
                    currentlocation: widget.currentlocation,
                  ));
                },
                child: Stack(
                  children: [
                    SvgPicture.asset(
                      "images/notification.svg",
                      width: screenWidth(20),
                    ),
                    notificationService.counter.value == 0
                        ? const SizedBox.shrink()
                        : Padding(
                            padding: EdgeInsets.only(
                              left: screenWidth(25),
                              top: screenWidth(50),
                            ),
                            child: Container(
                              //width: screenWidth(13),
                              height: screenHeight(50),
                              decoration: BoxDecoration(
                                color: AppColors.mainRedColor,
                                borderRadius: BorderRadius.circular(screenWidth(50)),
                              ),
                              child: Center(
                                  child: CustomText(
                                text: notificationService.counter.value.toString(),
                                textColor: AppColors.mainWhiteColor,
                                textType: TextStyleType.CUSTOM,
                              )),
                            ),
                          ),
                  ],
                ),
              );
            }),
            screenWidth(30).pw
          ],
        )
      ],
    );
  }
}
