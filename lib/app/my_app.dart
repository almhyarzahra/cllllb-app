import 'package:bot_toast/bot_toast.dart';
import 'package:com.tad.cllllb/UI/views/main_view/main_view.dart';
import 'package:com.tad.cllllb/core/translation/app_translation.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  //mmmm
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        theme: ThemeData(fontFamily: 'Al-Jazeera-Arabic'),
        defaultTransition: GetPlatform.isAndroid
            ? Transition.rightToLeftWithFade
            : Transition.cupertino,
        transitionDuration:const Duration(milliseconds: 400),
        translations: AppTranlation(),
        locale: getLocal(),
        fallbackLocale: getLocal(),
        builder: BotToastInit(),
        navigatorObservers: [BotToastNavigatorObserver()],
        debugShowCheckedModeBanner: false,
        // theme: ThemeData(
        //   bottomNavigationBarTheme: BottomNavigationBarThemeData(
        //       // backgroundColor: Colors.transparent,
        //       ),
        // ),
        title: 'Cllllb',
        home:
            //storage.getTokenInfo() != null
            ///?
            const MainView(initialPage: 0,)
        //: const SignInView()
        );
  }
}

Locale getLocal() {
  if (storage.getAppLanguage() == 'ar') {
    return const Locale('ar', 'SA');
  } else {
    return const Locale('en', 'US');
  }
}
