class ARLanguage {
  static Map<String, String> get map => {
        "key_signIn_or_createAccount": "تسجيل الدخول أو إنشاء حساب",
        "key_choose_signIn_options": "قم بتحديد أحد الخيارات التالية لتسجيل الدخول\n أو ابدأ",
        "key_Mobile_Number": "رقم الموبايل",
        "key_Check_Number": "الرجاء التأكد من الرقم",
        "key_logIn": "تسجيل الدخول",
        "key_google": "Google",
        "key_Don't_have_account": "ليس لديك حساب؟",
        "key_signUp": "إنشاء حساب",
        "key_try_again": "الرجاء المحاولة مرة أخرى",

        //sign in controller
        "key_success": "تمت العملية بنجاح",
        "key_check_connection": "الرجاء التحقق من الانترنت",
        "key_check_connection2": "تحقق من الانترنت",
        "key_You_don't_have_account": "ليس لديك حساب",

        //personal information view
        "key_enter_information": "الرجاء إدخال المعلومات الخاصة بك",
        "key_Please_Select": "الرجاء الاختيار",
        "key_Camera": "الكاميرا",
        "key_Gallery": "المعرض",
        "key_Select_Your_Image": "اختر صورة",
        "key_your_Name": "الاسم",
        "key_Please_Check_Name": "الرجاء التأكد من الاسم",
        "key_Please_Check_Phone": "الرجاء التحقق من رقم الهاتف",
        "key_Email": "الايميل",
        "key_Please_Check_Email": "الرجاء التأكد من الايميل",
        "key_Select_Game": "اختر لعبة",
        "key_continue": "متابعة",
        //personal information controller
        "key_No_Internet_Please Refresh": "لايوجد انترنت، الرجاء إعادة المحاولة",
        "key_Cropper": "اقتصاص",
        "key_No_Internet_Connection": "لا يوجد اتصال بالانترنت",
        "key_Please_Check_Information": "الايميل او الرقم موجود مسبقا",

        //verfication view
        "key_PIN_has_been_sent":
            "تم إرسال رقم التعريف الشخصي المكون من 6 أرقام إلى رقم هاتفك، أدخله أدناه للمتابعة",
        "key_Please_Enter_Verification_Number": "الرجاء إدخال رمز التحقق",

        //main view
        "key_Press_again_to_exit": "اضغط مرة أخرى للخروج من التطبيق",

        //book view
        "key_No_Favorite_Game": "لا يوجد لعبة مفضلة",
        "key_Add_game": "إضافة لعبة",
        "key_SELECT_GAME": "اختر لعبة",

        //book controller
        "key_belongs_to_clubs:": " تنتمي إلى النوادي التالية: ", //
        "key_No_Clubs_Yet": "لا يوجد نوادي بعد",

        //home view
        "key_LAST_ORDER_FOR_MR": "الطلب الأخير للسيد ",
        "key_No_Order_Yet": "لا يوجد طلبية بعد",
        "key_AED": " درهم",
        "key_All_Clubs": "كل النوادي",
        "key_near": "على قرابة: ",
        "key_km": "كم ",
        "key_meter": "متر ",
        //home controller
        "key_Succsess_Add_To_Favorite": "تم الأضافة إلى المفضلة بنجاح",
        "key_already_in_your_favourites": " موجود أساساً في المفضلة",
        "key_Please_turn_on_location_feature": "الرجاء تفعيل خاصية الموقع",
        "key_within_100_metres": "يجب أن تكون على بعد 100 متر من النادي",
        "key_Succsess_Add_To_Card": " تمت عملية الإضافة إلى المفضلة بنجاح",

        //profile view
        "key_Name": "الاسم: ",
        "key_Phone": "رقم الموبايل: ",
        "key_Email_Address": "عنوان البريد الالكتروني: ",
        "key_Wallet_contains": "المحفظة لديك تتضمن: ",
        "key_Edit_Your_Information": "تعديل المعلومات الشخصية",
        "key_Your_Name": "اسمك",
        "key_Please_Check_Your_Name": "الرجاء التأكد من اسمك",
        "key_Edit": "تعديل",

        //AboutCllllb view
        "key_About_Cllllb": "حول Cllllb",
        "key_describtion":
            "برنامج إلكتروني متخصص في\n\n المجال الرياضي يساعد المنشآت الخاصة\n\n في إدارة خدمات البيع والشراء\n\n",
        "key_communicating":
            "والتواصل مع عملائهم\n\nمن خلال الأندية. برنامج بطريقة\n\nمتقدمة من خلال منح نقاط التسوق\n\n",
        "key_saving":
            "وعروض التوفير يحتوي هذا البرنامج على العديد من الخصائص \n\nالتي تساعد\n\nالعميل الرياضي على التواصل\n\n",
        "key_electronically":
            "إلكترونياً مع نادي خاص، للحصول على\n\nخدماته في أقل وقت ممكن\n\nوجهد وفائدة كبيرة في شروط\n\n",
        "key_ofSaving":
            "التوفير والقيمة التي\n\nستستفيد منها جميع الأطراف المعنية\n\nفي هذا النظام الذكي، كما يربط البرنامج\n\n",
        "key_All_Concerned":
            " كافة الجهات المعنية إلكترونياً في مكان واحد\n\n يخدم كافة الأطراف ويسهل عليهم التعامل والوصول إليها\n\n",
        "key_All Services": "جميع الخدمات متاحة عبر التطبيق الذكي\n\n",

        //all clubs view
        "key_No_Clubs": "لا يوجد نوادي",
        "key_Filter": "فرز",
        "key_Type": "النوع: ",

        //all offers app view

        //all products view
        "key_Search": "البحث",
        "key_New_Products_at_All_Clubs": "منتجات جديدة في كل النوادي",
        "key_Special_Products_From_Clubs": "منتجات خاصة من النوادي",

        //book game view
        "key_GAME": "اللعبة",
        "key_QUANTITY": "الكمية",
        "key_BOOK": "الحجز",
        "key_No_Game_Yet": "لا يوجد ألعاب بعد",
        "key_BOOK_NOW": "احجز الآن",

        //book now view
        "key_QUANTITY2": " الكمية",
        "key_PLAYERS_IN_THE_HALL ": "اللاعبون في الصالة ",
        "key_NOW": "الآن",
        "key_No_Players_Now": "لا يوجد لاعبين الآن",
        "key_NEW_BOOKING": "حجز جديد",
        "key_Price": "السعر",
        "key_AED_Half_hours": "درهم/نصف ساعة",

        //change number view
        "key_Change_Number": "تغيير الرقم",
        "key_Old_Number": "الرقم القديم",
        "key_New_Number": "الرقم الجديد",
        "key_SUBMIT": "موافق",

        //contact view
        "key_phone: ": "هاتف: ",
        "Email: ": "ايميل: ",

        //Contanct Cllllb view
        "key_Contanct_Cllllb": "التواصل مع Cllllb",
        "key_World_Club_Portal_L.L.C": "World Club Portal L.L.C",
        "key_phone_04 123 4567": "هاتف: 04 123 4567\n",
        "key_info@cllllb.com": "ايميل: info@cllllb.com",
        "key_whatsapp": "واتس آب: 056 1534995\n",

        //details view
        "key_No_image_Yet": "لا يوجد صورة بعد",
        "key_BOOK_GAME": "احجز لعبة",
        "key_STORE": "المخزن",
        "key_SPECIAL_OFFER": "عرض خاص",
        "key_CONTACT": "تواصل",
        "key_ABOUT_THE_CLUB": "حول النادي",
        "key_Rate_Club": "تقييم النادي",
        "key_Rate": "التقييم: ",
        "key_Your_Comment": "تعليقك",
        "key_Send": "إرسال",
        "key_Comments": "التعليقات: ",
        "key_member": "عضو",
        "key_REPORT_CLUB": "التبليغ عن النادي",
        "key_Please_Check_Your_Report": "الرجاء التأكد من الإبلاغ",

        //favorite view
        "key_The_Favorite_Is_Empty": "المفضلة فارغة !!",
        "key_My_Favorite_Products": "منتجاتي المفضلة",
        "key_you_sure_to_clear_your_favorite": "هل أنت متأكد من حذف المفضلة لديك؟",
        "key_Yes": "نعم",
        "key_No": "لا",
        "key_Empty_Favorite": "تفريغ المفضلة",
        "key_Club": "النادي: ",

        //filter all clubs view
        "key_Filter_By_Clubs": "الفرز حسب النوادي",
        "key_Filter_By_Type": "الفرز حسب النوع",
        "key_Filter_By_City": "الفرز حسب المدينة",
        "key_Filter_By_Distance": "الفرز حسب المسافة",
        "key_Finish": "إنهاء",

        //filter last order view
        "key_Filter_By_Date": "الفرز حسب التاريخ",
        "key_First_Date": "أول تاريخ",
        "key_Last_Date": "آخر تاريخ",
        "key_All_Date": "كل التواريخ",

        //food drink view
        "key_No_Products_Yet": "لا يوجد منتجات بعد",
        "Club :": "Club :",

        //food sport details view
        "key_Products_From_App": "منتجات من التطبيق",
        "key_Description:": "الوصف: ",
        "key_ADD_TO_CARD": "إضافة إلى عربة التسوق",

        //last order view
        "key_No_Product": "لا يوجد منتج",
        "key_Product_Appr": "Product App",

        //member ships view
        "key_SUBSCRIPTIONS": "خدمات اخرى",
        "key_No_Subscriptions_Yet": "لا يوجد اشتراكات بعد",

        // my booking view
        "key_Booking": "الحجز",
        "key_No_Booking_Available": "لا يوجد حجز متاح ",
        "key_Book_number": "رقم الحجز: ",
        "key_to": " إلى ",
        "key_sure_to_cancel_your_Booking": "هل أنت متأكد من إلغاء حجزك؟",
        "key_Can_not_Cancle": "لا يمكن الإلغاء",

        //notification view
        "key_Notifications": "الإشعارات",
        "key_No_Notifications_Yet": "لا يوجد إشعارات بعد",

        //ordeers view
        "key_Orders": "الطلبيات",
        "key_No_Order_Available": "لا يوجد طلبيات متاحة",
        "key_Order": "الطلب ",
        "key_Product": "المنتج",
        "key_Quantity": "الكمية",
        "key_Total": "المجموع",
        "key_Cancel": "مُلغى",
        "key_Order_number": "رقم الطلب ",
        "key_Incomplete": "غير مكتمل",
        "key_Complete": " مكتمل      ",
        "key_Are_you_sure_to_cancel_your_Order": "هل أنت متأكد من إلغاء طلبك؟ ",

        //payment view
        "key_No_Booking_Yet": "لا يوجد حجز بعد !!",
        "key_BOOKING_SUMMARY": "ملخص الحجز",
        "key_Time_Slots": "الفترات الزمنية:      ",
        "key_Total_Price": "السعر الكلي:      ",
        "key_Payment": "الدفع",
        "key_Payment_Cash?": "الدفع نقداً؟",

        //payment controller
        "key_wallet_not_contain_the_full_amount!": "محفظتك لا تحتوي على المبلغ كاملا!",
        "key_Do_you_want_pay_from_the_wallet": "هل تريد الدفع من المحفظة؟",
        "key_You_Do_not_have_wallet_yet": "ليس لديك محفظة بعد",
        "key_Succsess_Payment": "تمت عملية الدفع بنجاح",

        //settings view
        "key_Settings": "الإعدادات",
        "key_Notification :": "الإشعارات: ",

        //shopping view
        "key_Inside/Outside_the_club": "داخل /خارج النادي",
        "key_inside_the_club": "داخل النادي",
        "key_Are_yousure_to_delete_the_product?": "هل أنت متأكد من حذف المنتج؟",
        "key_Are_you_sure_to_clear_your_cart?": "هل أنت متأكد من تفريغ عربة التسوق الخاصة بك؟",
        "key_Empty_Cart": "تفريغ عربة التسوق",
        "key_The_Cart_Is_Empty": "عربة التسوق فارغة !!",
        "key_My_Cart": "عربة التسوق الخاصة بي",
        "key_Name*": "الاسم*",
        "key_Email*": "الايميل*",
        "key_Phone*": "الهاتف*",
        "key_City*": "المدينة*",
        "key_Country*": "البلد*",
        "key_Please_Check_Your_Street": "الرجاء التأكد من الشارع",
        "key_Street*": "الشارع*",
        "key_Please_Check_Your_City": "الرجاء التأكد من المدينة",
        "key_Please_Check_Your_Country": "الرجاء التأكد من البلد",
        "key_Total: ": "المجموع: ",
        "key_Price: ": "السعر: ",

        //special offer view
        "key_Rent_Tabel": "Rent Tabel For 6 H",
        "key_150_AED": "150 درهم",
        "key_Date_11/11/2023": "التاريخ : 11/11/2023",
        "key_Time_5:00pm- 11:00pm": "الوقت : 5:00pm- 11:00pm",
        "key_Copy_Code : AL8870 ": "كود النسخ : AL8870 ",

        //store view
        "key_SPORT_ITEMS": "أوقات الرياضة",
        "key_FOOD&DRINKS": "الأكل والمشروبات",

        //table details view
        "key_Can_not_Select_All_Day": "لا يمكن تحديد كل اليوم",
        "key_The_Time ": "الوقت (",
        "key_is_not_available": " ) غير متاح",
        "key_Select_All_Day": "تحديد كل اليوم",
        "key_Make_an_Appointment": "إنشاء موعد",

        //term and condition view
        "key_Terms_Condition": "الشروط والأحكام",
        "key_WORLD_CLUB":
            "تحتفظ WORLD CLUB PORTAL L.L.C بموقع\n\nCLLLLB.Com الإلكتروني. فيما يلي\n\nالشروط والأحكام التي تحكم\n\n",
        "key_Use_Of_The_Site":
            "استخدام الموقع .\n\nباستخدام الموقع فإنك توافق صراحةً \n\n على الالتزام بهذه الشروط والأحكام\n\n",
        "key_And_The_WORLD":
            "و The WORLD CLUB PORTAL L.L.C\n\n(Cllllb) سياسة الخصوصية ومتابعة هذه\n\nالشروط والأحكام وكل ما ينطبق\n\n",
        "key_Laws_And_Regulations ":
            "لقوانين واللوائح التي تحكم\n\nاستخدام الموقع.\n\n  WORLD CLUB PORTAL L.L.C تحتفظ بـ\n\n",
        "key_Right_To_Change":
            "الحق في تغيير هذه الشروط والأحكام\n\n في أي وقت، ويسري فورًا\n\nالنشر على الموقع. الرجاء مراجعة هذه الصفحة\n\n",
        "key_Of_The_Site":
            "من الموقع بشكل دوري. سنلاحظ\n\nعندما تكون هناك تحديثات على \n\n الشروط والأحكام في الجزء السفلي\n\n",
        "key_Terms_&_Conditions":
            "البنود و الظروف. إذا قمت بانتهاك هذه \n\n الشروط والأحكام\n\nقد يتم إنهاء WORLD CLUB PORTAL L.L.C\n\n",
        "key_Your_Use_Of_The Site":
            "إن استخدامك للموقع يمنعك من المستقبل \n\n استخدام الموقع و/أو اتخاذ الإجراء القانوني المناسب \n\n ضدك \n\n",

        //drawer
        "key_Are_You_Sure": "هل أنت متأكد؟",
        "key_Log_Out": "تسجيل الخروج",
        "key_Go_To_Home": "الذهاب إلى الرئيسية",

        "key_View_All": "عرض الكل",
        "Change_Language": "تغيير اللغة",
        "products_available": "هذا المنتج لم يعد متوفر",
        "to": "الى",
        "all_day": "كل اليوم",
        "key_New_Products_at_AllClubs": "منتجات جديدة في جميع الأندية",
        "key_New_news_ADS_For_Clubs": "إعلانات الأخبار الجديدة لكل الأندية",
        "New_Job_from_All_Clubs": "وظيفة جديدة من النوادي",
        "Try Agin": "حاول مجددا",
        "key_no_ads": "لا يوجد إعلانات بعد",
        "key_news_detalis": "تفاصيل الخبر",
        "key_no_jobs": "لا يوجد وظائف بعد",
        "key_expected_salary": "الراتب المتوقع",
        "key_job_title": "المسمى الوظيفي: ",
        "key_dead_line": "الموعد النهائي لتقديم طلبات العمل هو:",
        "key_apply_now": "قدم الآن",
        "key_cv": "ادخل CV",
        "key_edit_cv": "تعديل CV",
        "key_looking_job": "البحث عن وظيفة",
        "key_number": "الرقم :",
        "key_not_found": "لا يوجد",
        "key_mail": "الايميل :",
        "key_details": "التفاصيل",
        "key_years_experience": "سنوات الخبرة",
        "key_download_cv": "تنزيل CV",
        "key_succsess_download": "تم التنزيل بنجاح",
        "key_skills": "المهارات :",
        "key_educations": "التعليم :",
        "key_communication": "التواصل",
        "key_call_now": "اتصل الآن",
        "key_send_mail": "أرسل ايميل",
        "key_job_advertisement": "اعلان عن وظيفة",
        "key_field_required": "هذا الحقل مطلوب",
        "key_upload_cv": "رفع CV",
        "key_ok": "موافق",
        "key_exit": "إلغاء",
        "key_delete_account": "حذف الحساب",
        "key_succsess_delete": "تم حذف الحساب",
        "key_select_weapon": "حدد نوع السلاح",
        "key_select_strikes": "حدد عدد الضربات",
        "key_time_finish_strikes": "الوقت المحدد لإنهاء الضربات",
        "key_min": "دقيقة",
        "key_no_weapon": "لا يوجد أسلحة بعد",
        "key_news": "الأخبار",
        "key_jobs": "الوظائف",
        "key_lowest_price": "السعر الأدنى",
        "key_highest_price": "السعر الأعلى",
        "key_filter_price": "الفرز حسب السعر",
        "key_all_price": "كل الأسعار",
        "key_filter_delivery": "الفرز حسب التوصيل",
        "key_continue_visitor": "المتابعة كزائر",
        "key_login_complete": "يرجى تسجيل الدخول لإتمام العملية",
        "key_delete_ad": "حذف الإعلان",
        "key_dont_ad": "لا تمتلك اعلان بعد",
        "key_next": "التالي",
        "key_previous": "السابق",
        "key_share_payment_friend": "مشاركة صديق بالدفع",
        "key_add_friend": "إضافة صديق",
        "key_check": "تحقق",
        "key_pay": "دفع",
        "key_code_wrong": "الكود خاطئ",
        "key_is_wrong": "خاطئ",
        "key_doesnt_wallet": "ليس لديه محفظة",
        "key_wallet_less": "محفظته أقل من المبلغ",
        "key_amount_pay": "الكمية المدفوعة من قبل كل شخص",
        "key_fill_wallet": "لتعبئة محفظتك يرجى مراجعة أقرب نادي",
        "key_approval_friends": "الحصول على موافقة الأصدقاء",
        "key_booking_cancelled": "تم إلغاء الحجز",
        "key_waiting_approval": "في انتظار موافقة الأصدقاء: ",
        "key_didnt_agree": " لم يوافق",
        "key_menus": "القوائم",
        "key_not_available": "غير متوفر بعد",
        "key_no_products": "لا يوجد منتجات بعد",
        "key_subscription_now": "اشترك الآن",
        "key_new_subscription": "اشتراك جديد",
        "key_choose_coach": "اختر مدربا",
        "key_subscription": "اشتراكات",
        "description": "الوصف",
        "select_date_subscription": "حدد تاريخ بدء الاشتراك",
        "key_subscription_menu": "قائمة اشتراك الوجبات",
        "sub_meals": "للاشتراك بالوجبات يرجى التواصل مع النادي",
        "key_no_element_yet":"لايوجد عناصر بعد",
        "congratulations":"تهانينا!!",
        "youGot":"لقد حصلت على",
        "rewardPointsRegister":"نقطة مكافأة لإنشاء حساب!",
        "inviteFriendsAndGet":"قم بدعوة أصدقائك واحصل على ",
        "rewardPoints":"نقطة مكافأة",
        "inviteFreind":"دعوة صديق",
        "invitationCode":"كود الدعوة",
        "copiedInvitionCode":"تم نسخ كود الدعوة الى الحافظة",
        "myRewards":"مكافآتي",
        "communicateApplicationAd":"للتواصل مع إدارة التطبيق والإبلاغ عن مشكلة يمكنك ملئ النموذج التالي والمتابعة",
        "title":"العنوان",
        "attachPicture":"ارفاق صورة",
        "tryAgainAfterMinute":"حاول مرة أخرى بعد دقيقة من الآن",
        "noProductsMatchingSearch":"لا توجد منتجات مطابقة لبحثك",
        "noMatchingResults":"لا توجد نتائج مطابقة",
        "scanYourFriends":"او يمكنك مسح ال QRCode الخاص بصديقك والدخول عبر رابط الدعوة الخاص به ",
        "back":"رجوع",
        "howUsePoints":"آلية استخدام النقاط :",
        "vouchers":"القسائم",
        "replacePoints":"استبدال النقاط",
        "lottery":"ادخل السحب",
        "cllllbOffers":"عروض Cllllb",
        "donatePoints":"اهداء نقاط",
        "giftPointsFriends":"قم بإهداء النقاط إلى أصدقاءك عن طريق إدخالك رقم الهاتف الخاص بهم.",
        "numberPointsGift":"عدد النقاط التي تريد إهداءها",
        "checkNumberPoints":"يرجى التحقق من عدد نقاطك ",
        "checkTheNumber":"تحقق من الرقم",
        "noLottery":"لا يوجد سحوبات في الوقت الحالي",
        "lotteryDate":"تاريخ السحب: ",
        "entryFee":"رسم الدخول: ",
        "joining":"انضمام",
        "joined":"منضم",
        "point":"نقطة",
        "winner":"الفائز",
        "lotteryResults":"نتائج السحوبات",
        "winnerName":"اسم الفائز: ",
        "exchangeYourPoints":"قم باستبدال نقاطك بدراهم",
        "forEvery":"مقابل كل ",
        "willBeAdded":"سيتم اضافة ",
        "toYourWallet":"الى محفظتك",
        "numberPointsReplace":"عدد النقاط التي تريد استبدالها",
        "noResult":"لا يوجد نتائج في الوقت الحالي",
        "replacing":"استبدال",
        "minimumPoints":"الحد الأدنى لتبديل النقاط هو ",
        "startDate":"تاريخ البدء: ",
        "endDate":"تاريخ الانتهاء: ",
        "buy":"شراء",
        "noVouchers":"لا يوجد قسائم في الوقت الحالي",
        "willGetDiscount":"سوف تحصل على خصم ",
        "ownThisVoucher":"تملك هذه القسيمة",
        "dontHavePoint":"ليس لديك نقاط كافية",
        "voucherNotAvailable":"القسيمة غير متاحة",
        "myVouchers":"قسائمي",
        "dontHaveVouchers":"لا تمتلك قسائم بعد",
        "vouchersInvalid":"هذه القسيمة غير صالحة",
        "voucherCopied":"تم نسخ كود القسيمة الى الحافظة",
        "enterYourVouchers":"يمكنك ادخال احدى قسائمك للحصول على خصم",
        "confirm":"تأكيد",
        "withoutCoupon":"بدون قسيمة",
        "withCoupon":"مع قسيمة",
        "newTotal":"المجموع الجديد ",
        "filterByPoints":"الفرز حسب النقاط",
        "lotteryStatus":"الرجاء ادخال حالة السحب",
        "finished":"منتهي",
        "notFinished":"غير منتهي",
        "myLottery":"سحوباتي",
        "filterByDiscount":"الفرز حسب نسبة الخصم",
        "wonEntryLottery":"لقد فزت بالدخول الى السحب مجانا",
        "clubName":"اسم النادي:",
        "awards":"الجوائز",
        "winners":"الرابحين",
        "noWinners":"لا يوجد رابحين",
        "chancesOfWinning":"فرص الفوز : "

      };
}
