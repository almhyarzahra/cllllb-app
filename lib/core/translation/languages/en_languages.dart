class ENLanguage {
  static Map<String, String> get map => {
        "key_signIn_or_createAccount": "Sign In Or Create An Account",
        "key_choose_signIn_options": "Choose Any Of The Options Below To Sign In\n Or Star",
        "key_Mobile_Number": "Mobile Number",
        "key_Check_Number": "Please Check Your Number",
        "key_logIn": "Log in",
        "key_google": "Google",
        "key_Don't_have_account": "Don't have an account?",
        "key_signUp": "Sign up",
        "key_try_again": "Please Try Again",

        //sign in controller
        "key_success": "Succsess",
        "key_check_connection": "Please Check Your Connection",
        "key_check_connection2": "Check Your Connection",
        "key_You_don't_have_account": "You don't have an account",

        //personal information view
        "key_enter_information": "Please Enter Your Information",
        "key_Please_Select": "Please Select",
        "key_Camera": "Camera",
        "key_Gallery": "Gallery",
        "key_Select_Your_Image": "Select Your Image",
        "key_your_Name": "Your Name",
        "key_Please_Check_Name": "Please Check Your Name",
        "key_Please_Check_Phone": "Please Check Your Phone",
        "key_Email": "Email",
        "key_Please_Check_Email": "Please Check Your Email",
        "key_Select_Game": "Select Game",
        "key_continue": "CONTINUE",
        //personal information controller
        "key_No_Internet_Please Refresh": "No Internet, Please Refresh",
        "key_Cropper": "Cropper",
        "key_No_Internet_Connection": "No Internet Connection",
        "key_Please_Check_Information": "The email or number already exists",

        //verfication view
        "key_PIN_has_been_sent":
            "A 6 - Digit PIN has been sent to your phone number, enter it below to continue",
        "key_Please_Enter_Verification_Number": "Please Enter Your Verification Number",

        //main view
        "key_Press_again_to_exit": "Press again to exit the application",

        //book view
        "key_No_Favorite_Game": "No Favorite Game",
        "key_Add_game": "Add game",
        "key_SELECT_GAME": "SELECT GAME",

        //book controller
        "key_belongs_to_clubs:": " belongs to clubs:",
        "key_No_Clubs_Yet": "No Clubs Yet",

        //home view
        "key_LAST_ORDER_FOR_MR": "LAST ORDER FOR MR ",
        "key_No_Order_Yet": "No Order Yet",
        "key_AED": " AED",
        "key_All_Clubs": "All Clubs",
        "key_near": "Near: ",
        "key_km": "km ",
        "key_meter": "meter ",
        //home controller
        "key_Succsess_Add_To_Favorite": "Succsess Add To Favorite",
        "key_already_in_your_favourites": " is already in your favourites",
        "key_Please_turn_on_location_feature": "Please turn on the location feature",
        "key_within_100_metres": "You must be within 100 metres of the club",
        "key_Succsess_Add_To_Card": "Succsess Add To Card",

        //profile view
        "key_Name": "Name : ",
        "key_Phone": "Phone Number : ",
        "key_Email_Address": "Email Address : ",
        "key_Wallet_contains": "Your Wallet contains : ",
        "key_Edit_Your_Information": "Edit Your Information",
        "key_Your_Name": "Your Name",
        "key_Please_Check_Your_Name": "Please Check Your Name",
        "key_Edit": "Edit",

        //AboutCllllb view
        "key_About_Cllllb": "About Cllllb",
        "key_describtion":
            "An Electronic Program Specialized In The\n\nSports Field That Helps Private Facilities\n\nManaging The Buying And Selling Services\n\n",
        "key_communicating":
            "And Communicating With Their Customers\n\nThrough The Clubs . Program In An\n\nAdvanced Way By Giving Shopping Points\n\n",
        "key_saving":
            "And Savings Offers This Program Has\n\nMany Features And Services That Help\n\nThe Sports Client To Communicate\n\n",
        "key_electronically":
            "Electronically With Own Club, To Get Its\n\nServices In The Least Possible Time And\n\nEffort And With Great Benefit In Terms\n\n",
        "key_ofSaving":
            "Of Savings And Offers The Value, Which\n\nAll Concerned Parties Will Benefit From\n\nThis Smart System, As The Program Links\n\n",
        "key_All_Concerned":
            " All Concerned Parties Electronically In One\n\n Place That Serves All Parties And Makes\n\nIt Easy For Them Dealing And Access To\n\n",
        "key_All Services": "All Services Available Through The Smart\n\nApplication",

        //all clubs view
        "key_No_Clubs": "No Clubs",
        "key_Filter": "Filter",
        "key_Type": "Type :",

        //all offers app view

        //all products view
        "key_Search": "Search",
        "key_New_Products_at_All_Clubs": "New Products at All Clubs",
        "key_Special_Products_From_Clubs": "Special Products From Clubs",

        //book game view
        "key_GAME": "GAME",
        "key_QUANTITY": "QUANTITY",
        "key_BOOK": "BOOK",
        "key_No_Game_Yet": "No Game Yet",
        "key_BOOK_NOW": "BOOK NOW",

        //book now view
        "key_QUANTITY2": " QUANTITY",
        "key_PLAYERS_IN_THE_HALL ": "PLAYERS IN THE HALL ",
        "key_NOW": "NOW",
        "key_No_Players_Now": "No Players Now",
        "key_NEW_BOOKING": "NEW BOOKING",
        "key_Price": "Price",
        "key_AED_Half_hours": "AED /Half hours",

        //change number view
        "key_Change_Number": "Change Number",
        "key_Old_Number": "Old Number",
        "key_New_Number": "New Number",
        "key_SUBMIT": "SUBMIT",

        //contact view
        "key_phone: ": "phone: ",
        "Email: ": "Email: ",

        //Contanct Cllllb view
        "key_Contanct_Cllllb": "Contact Cllllb",
        "key_World_Club_Portal_L.L.C": "World Club Portal L.L.C",
        "key_phone_04 123 4567": "phone: 04 123 4567\n",
        "key_info@cllllb.com": "Email: info@cllllb.com\n",
        "key_whatsapp": "whatsapp : 056 1534995\n",

        //details view
        "key_No_image_Yet": "No image Yet",
        "key_BOOK_GAME": "BOOK GAME",
        "key_STORE": "STORE",
        "key_SPECIAL_OFFER": "SPECIAL OFFER",
        "key_CONTACT": "CONTACT",
        "key_ABOUT_THE_CLUB": "ABOUT THE CLUB",
        "key_Rate_Club": "Rate Club",
        "key_Rate": "Rate: ",
        "key_Your_Comment": "Your Comment",
        "key_Send": "Send",
        "key_Comments": "Comments:",
        "key_member": "member",
        "key_REPORT_CLUB": "REPORT CLUB",
        "key_Please_Check_Your_Report": "Please Check Your Report",

        //favorite view
        "key_The_Favorite_Is_Empty": "The Favorite Is Empty !!",
        "key_My_Favorite_Products": "My Favorite Products",
        "key_you_sure_to_clear_your_favorite": "Are you sure to clear your favorite?",
        "key_Yes": "Yes",
        "key_No": "No",
        "key_Empty_Favorite": "Empty Favorite",
        "key_Club": "Club :",

        //filter all clubs view
        "key_Filter_By_Clubs": "Filter By Clubs",
        "key_Filter_By_Type": "Filter By Type",
        "key_Filter_By_City": "Filter By City",
        "key_Filter_By_Distance": "Filter By Distance",
        "key_Finish": "Finish",

        //filter last order view
        "key_Filter_By_Date": "Filter By Date",
        "key_First_Date": "First Date",
        "key_Last_Date": "Last Date",
        "key_All_Date": "All Date",

        //food drink view
        "key_No_Products_Yet": "No Products Yet",
        "Club :": "Club :",

        //food sport details view
        "key_Products_From_App": "Products From App",
        "key_Description:": "Description: ",
        "key_ADD_TO_CARD": "ADD TO CARD",

        //last order view
        "key_No_Product": "No Product",
        //"key_Filter": "Filter",
        "key_Product_Appr": "Product App",

        //member ships view
        "key_SUBSCRIPTIONS": "Other Services",
        "key_No_Subscriptions_Yet": "No Subscriptions Yet",

        // my booking view
        "key_Booking": "Booking",
        "key_No_Booking_Available": "No Booking Available ",
        "key_Book_number": "Book number: ",
        "key_to": " to ",
        "key_sure_to_cancel_your_Booking": "Are you sure to cancel your Booking?",
        "key_Can_not_Cancle": "Can't Cancle",

        //notification view
        "key_Notifications": "Notifications",
        "key_No_Notifications_Yet": "No Notifications Yet",

        //ordeers view
        "key_Orders": "Orders",
        "key_No_Order_Available": "No Order Available ",
        "key_Order": "Order ",
        "key_Cancel": "Cancel",
        "key_Product": "Product",
        "key_Quantity": "Quantity",
        "key_Total": "Total",
        "key_Order_number": "Order number ",
        "key_Incomplete": "Incomplete",
        "key_Complete": "Complete      ",
        "key_Are_you_sure_to_cancel_your_Order": "Are you sure to cancel your Order?",

        //payment view
        "key_No_Booking_Yet": "No Booking Yet !!",
        "key_BOOKING_SUMMARY": "BOOKING SUMMARY",
        "key_Time_Slots": "Time Slots:      ",
        "key_Total_Price": "Total Price:      ",
        "key_Payment": "Payment",
        "key_Payment_Cash?": "Payment Cash?",

        //payment controller
        "key_wallet_not_contain_the_full_amount!": "Your wallet does not contain the full amount!",
        "key_Do_you_want_pay_from_the_wallet": "Do you want to pay from the wallet?",
        "key_You_Do_not_have_wallet_yet": "You Don't have a wallet yet",
        "key_Succsess_Payment": "Succsess Payment",

        //settings view
        "key_Settings": "Settings",
        "key_Language_:English": "Language  :English",
        "key_Notification :": "Notification :",

        //shopping view
        "key_Inside/Outside_the_club": "Inside/Outside the club",
        "key_inside_the_club": "Inside the club",
        "key_Are_yousure_to_delete_the_product?": "Are you sure to delete the product?",
        "key_Are_you_sure_to_clear_your_cart?": "Are you sure to clear your cart?",
        "key_Empty_Cart": "Empty Cart",
        "key_The_Cart_Is_Empty": "The Cart Is Empty !!",
        "key_My_Cart": "My Cart",
        "key_Name*": "Name*",
        "key_Email*": "Email*",
        "key_Phone*": "Phone*",
        "key_City*": "City*",
        "key_Street*": "Street*",
        "key_Country*": "Country*",
        "key_Please_Check_Your_Street": "Please Check Your Street",
        "key_Please_Check_Your_City": "Please Check Your City",
        "key_Please_Check_Your_Country": "Please Check Your Country",
        "key_Total: ": "Total: ",
        "key_Price: ": "Price: ",

        //special offer view
        "key_Rent_Tabel": "Rent Tabel For 6 H",
        "key_150_AED": "150 AED",
        "key_Date_11/11/2023": "Date : 11/11/2023",
        "key_Time_5:00pm- 11:00pm": "Time : 5:00pm- 11:00pm",
        "key_Copy_Code : AL8870 ": "Copy Code : AL8870 ",

        //store view
        "key_SPORT_ITEMS": "SPORT ITEMS",
        "key_FOOD&DRINKS": "FOOD&DRINKS",

        //table details view
        "key_Can_not_Select_All_Day": "Can't Select All Day",
        "key_The_Time ": "The Time (",
        "key_is_not_available": ") is not available",
        "key_Select_All_Day": "Select All Day",
        "key_Make_an_Appointment": "Make an Appointment",

        //term and condition view
        "key_Terms_Condition": "Terms & Condition",
        "key_WORLD_CLUB":
            "WORLD CLUB PORTAL L.L.C Maintains The\n\nCLLLLB.Com Website (“Site”). The Following\n\nAre The Terms & Conditions That Govern\n\n",
        "key_Use_Of_The_Site":
            "Use Of The Site (“Terms & Conditions”).\n\nBy Using The Site You Expressly Agree\n\nTo Be Bound By These Terms & Conditions\n\n",
        "key_And_The_WORLD":
            "And The WORLD CLUB PORTAL L.L.C\n\n(Cllllb) Privacy Policy And To Follow These\n\nTerms & Conditions And All Applicable\n\n",
        "key_Laws_And_Regulations ":
            "Laws And Regulations Governing\n\nUse Of The Site.\n\nWORLD CLUB PORTAL L.L.C Reserves The\n\n",
        "key_Right_To_Change":
            "Right To Change These Terms & Conditions\n\nAt Any Time, Effective Immediately Upon\n\nPosting On The Site. Please Check This Page\n\n",
        "key_Of_The_Site":
            "Of The Site Periodically. We Will Note\n\nWhen There Are Updates To The\n\nTerms & Conditions At The Bottom Of The\n\n",
        "key_Terms_&_Conditions":
            "Terms & Conditions. If You Violate These\n\nTerms & Conditions\n\nWORLD CLUB PORTAL L.L.C May Terminate\n\n",
        "key_Your_Use_Of_The Site":
            "Your Use Of The Site, Bar You From Future\n\nUse Of The Site, And/Or Take Appropriate\n\nLegal Action Against You\n\n",

        //drawer
        "key_Are_You_Sure": "Are You Sure?",
        "key_Log_Out": "Log Out",
        "key_Go_To_Home": "Go To Home",

        "key_View_All": "View All",
        "Change_Language": "Change_Language",
        "products_available": "This product is no longer available",
        "to": "to",
        "all_day": "All Day",
        "key_New_Products_at_AllClubs": "New Products at All Clubs",
        "key_New_news_ADS_For_Clubs": "New news ADS at For Clubs",
        "New_Job_from_All_Clubs": "New Job from All Clubs",
        "Try Agin": "Try Agin",
        "key_no_ads": "No ADS yet",
        "key_news_detalis": "News details",
        "key_no_jobs": 'No Jobs yet',
        "key_expected_salary": "Expected Salary",
        "key_job_title": "Job Title: ",
        "key_dead_line": "The Deadline For Job Applications Is :",
        "key_apply_now": "Apply Now",
        "key_cv": "Put CV",
        "key_edit_cv": "Edit CV",
        "key_looking_job": "Looking for a job",
        "key_number": "Number :",
        "key_not_found": "Not Found",
        "key_mail": "Mail :",
        "key_details": "Details",
        "key_years_experience": "Years Of Experience",
        "key_download_cv": "Download CV",
        "key_succsess_download": "Succsess Download",
        "key_skills": "Skills :",
        "key_educations": "Educations :",
        "key_communication": "Communication",
        "key_call_now": "Call Now",
        "key_send_mail": "Send Mail",
        "key_job_advertisement": "Job Advertisement",
        "key_field_required": "This field is required",
        "key_upload_cv": "Upload CV",
        "key_ok": "OK",
        "key_exit": "Exit",
        "key_delete_account": "Delete Account",
        "key_succsess_delete": "The account has been deleted",
        "key_select_weapon": "Select the weapon type",
        "key_select_strikes": "Select the number strikes",
        "key_time_finish_strikes": "The exact time to finish the strikes",
        "key_min": "min",
        "key_no_weapon": "No weapons yet",
        "key_news": "NEWS",
        "key_jobs": "JOBS",
        "key_lowest_price": "Lowest price",
        "key_highest_price": "Highest price",
        "key_filter_price": "Filter By Price",
        "key_all_price": "All Price",
        "key_filter_delivery": "Filter By Delivery",
        "key_continue_visitor": "Continue_as_a_visitor",
        "key_login_complete": "Please log in to complete the process",
        "key_delete_ad": "DeleteAD",
        "key_dont_ad": "You don't have an ad yet",
        "key_next": "Next",
        "key_previous": "Previous",
        "key_share_payment_friend": "Share payment with a friend",
        "key_add_friend": "Add Friend",
        "key_check": "Check",
        "key_pay": "Pay",
        "key_code_wrong": "Code Wrong",
        "key_is_wrong": "wrong",
        "key_doesnt_wallet": "doesn't have a wallet",
        "key_wallet_less": "His wallet is less than the amount",
        "key_amount_pay": "The amount paid by each person",
        "key_fill_wallet": "To fill your wallet, please visit the nearest club.",
        "key_approval_friends": "Get approval from friends",
        "key_booking_cancelled": "Booking Cancelled",
        "key_waiting_approval": "Waiting for friends' approval: ",
        "key_didnt_agree": " He did not agree",
        "key_menus": "Menus",
        "key_not_available": "Not available yet",
        "key_no_products": "No Products yet",
        "key_subscription_now": "Subscription Now",
        "key_new_subscription": "New Subscription",
        "key_choose_coach": "CHOOSE A COACh",
        "key_subscription": "SUBSCRIPTION",
        "description": "Description",
        "select_date_subscription": "Select the subscription start date",
        "key_subscription_menu": "Subscription Menu",
        "sub_meals":"To subscribe to meals, please contact the club",
        "key_no_element_yet":"No element yet",
        "congratulations":"Congratulations!!",
        "youGot":"You’ve got ",
        "rewardPointsRegister":"Reward points for creating an account!",
        "inviteFriendsAndGet":"Invite your friends and get ",
        "rewardPoints":"Reward points.",
        "inviteFreind":"Invite freind",
        "invitationCode":"Invitation code",
        "copiedInvitionCode":"The invitation code has been copied to the clipboard",
        "myRewards":"My Rewards",
        "communicateApplicationAd":"To communicate with the application administration and report a problem, you can fill out the following form and follow up",
        "title":"Title",
        "attachPicture":"Attach a picture",
        "tryAgainAfterMinute":"Try again a minute from now",
        "noProductsMatchingSearch":"There are no products matching your search",
        "noMatchingResults":"There are no matching results",
        "scanYourFriends":"Or you can scan your friend's Qrcode and enter through his invitation code",
        "back":"Back",
        "howUsePoints":"How to use points:",
        "vouchers":"Vouchers",
        "replacePoints":"Replace points",
        "lottery":"Enter the lottery",
        "cllllbOffers":"Cllllb offers",
        "donatePoints":"Donate points",
        "giftPointsFriends":"Gift points to your friends by entering their phone number.",
        "numberPointsGift":"The number of points you want to gift",
        "checkNumberPoints":"Please check the number of your points",
        "checkTheNumber":"Check the number",
        "noLottery":"There are no lottery at the moment",
        "lotteryDate":"Lottery Date: ",
        "entryFee":"Entry Fee: ",
        "joining":"joining",
        "joined":"Joined",
        "point":"Points",
        "winner":"Winner",
        "lotteryResults":"Lottery results",
        "winnerName":"Winner's name: ",
        "exchangeYourPoints":"Exchange your points for AED",
        "forEvery":"For every ",
        "willBeAdded":"will be added ",
        "toYourWallet":"to your wallet",
        "numberPointsReplace":"The number of points you want to replace",
        "noResult":"There are no results at the moment",
        "replacing":"Replacing",
        "minimumPoints":"The minimum amount to exchange points is ",
        "startDate":"Start Date: ",
        "endDate":"End Date: ",
        "buy":"Buy",
        "noVouchers":"There are no vouchers at the moment",
        "willGetDiscount":"You will get discount ",
        "ownThisVoucher":"Own this voucher",
        "dontHavePoint":"You don't have enough points",
        "voucherNotAvailable":"Voucher not available",
        "myVouchers":"My Vouchers",
        "dontHaveVouchers":"You don't have vouchers yet",
        "vouchersInvalid":"This voucher is invalid",
        "voucherCopied":"The voucher code has been copied to the clipboard",
        "enterYourVouchers":"You can enter one of your vouchers to get a discount",
        "confirm":"Confirm",
        "withoutCoupon":"Without coupon",
        "withCoupon":"with coupon",
        "newTotal":"New total ",
        "filterByPoints":"Filter By Points",
        "lotteryStatus":"Please enter Lottery status",
        "finished":"Finished",
        "notFinished":"Not finished",
        "myLottery":"My Lottery",
        "filterByDiscount":"Filter by discount",
        "wonEntryLottery":"You have won entry into the lottery for free",
        "clubName":"Club Name:",
        "awards":"Awards",
        "winners":"Winners",
        "noWinners":"There are no winners",
        "chancesOfWinning":"Chances of winning : "
        
      };
}
