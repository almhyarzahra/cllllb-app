import 'dart:convert';
import 'dart:developer';

import 'package:com.tad.cllllb/core/enums/request_type.dart';
import 'package:http/http.dart' as http;
// ignore: depend_on_referenced_packages
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
// ignore: depend_on_referenced_packages
import 'package:path/path.dart' as path;

class NetworkUtil {
  static String baseUrl = 'cllllb.com';
  static var client = http.Client();

  static Future<dynamic> sendRequest({
    required RequestType type,
    required String url,
    Map<String, dynamic>? params,
    Map<String, String>? headers,
    Map<String, dynamic>? body,
  }) async {
    try {
      var uri = Uri.https(baseUrl, url, params);
      late http.Response response;

      Map<String, dynamic> jsonRespons = {};
      log(uri.toString());
      switch (type) {
        case RequestType.GET:
          response = await client.get(uri, headers: headers);
          break;
        case RequestType.POST:
          response =
              await client.post(uri, headers: headers, body: jsonEncode(body));
          break;
        case RequestType.PUT:
          response =
              await client.put(uri, headers: headers, body: jsonEncode(body));
          break;
        case RequestType.DELETE:
          response = await client.delete(uri,
              headers: headers, body: jsonEncode(body));
          break;
        case RequestType.MULTIPART:
          break;
      }

      dynamic result;
      try {
        result = jsonDecode(utf8.decode(response.bodyBytes));
      } catch (e) {
        log(e.toString());
      }

      jsonRespons.putIfAbsent('statusCode', () => response.statusCode);
      jsonRespons.putIfAbsent('response',
          () => result ?? {'title': utf8.decode(response.bodyBytes)});

      return jsonRespons;
    } catch (e) {
      log(e.toString());
      return null;
      //BotToast.showText(text: e.toString());
    }
  }

  static Future<dynamic> sendRequestOTP({
    required RequestType type,
    required String url,
    Map<String, dynamic>? params,
    Map<String, String>? headers,
    Map<String, dynamic>? body,
  }) async {
    try {
      var uri =
          Uri.http('phplaravel-1151234-4172304.cloudwaysapps.com', url, params);
      late http.Response response;
       log(uri.toString());
      Map<String, dynamic> jsonRespons = {};
      log(uri.toString());
      switch (type) {
        case RequestType.GET:
          response = await client.get(uri, headers: headers);
          break;
        case RequestType.POST:
          response =
              await client.post(uri, headers: headers, body: jsonEncode(body));
          break;
        case RequestType.PUT:
          response =
              await client.put(uri, headers: headers, body: jsonEncode(body));
          break;
        case RequestType.DELETE:
          response = await client.delete(uri,
              headers: headers, body: jsonEncode(body));
          break;
        case RequestType.MULTIPART:
          break;
      }

      dynamic result;
      try {
        result = jsonDecode(utf8.decode(response.bodyBytes));
      } catch (e) {
        log(e.toString());
      }

      jsonRespons.putIfAbsent('statusCode', () => response.statusCode);
      jsonRespons.putIfAbsent('response',
          () => result ?? {'title': utf8.decode(response.bodyBytes)});

      return jsonRespons;
    } catch (e) {
      log(e.toString());
      return null;
      //BotToast.showText(text: e.toString());
    }
  }

  static Future<dynamic> sendMultipartRequest({
    required String url,
    required RequestType type,
    Map<String, String>? headers = const {},
    Map<String, String>? fields = const {},
    Map<String, String>? files = const {},
    Map<String, dynamic>? params,
  }) async {
    try {
      var request =
          http.MultipartRequest(type.name, Uri.https(baseUrl, url, params));
      log(Uri.https(baseUrl, url, params).toString());
      var filesKeyList = files!.keys.toList();

      var filesNameList = files.values.toList();

      for (int i = 0; i < filesKeyList.length; i++) {
        if (filesNameList[i].isNotEmpty) {
          var multipartFile = http.MultipartFile.fromPath(
            filesKeyList[i],
            filesNameList[i],
            filename: path.basename(filesNameList[i]),
            contentType:
                MediaType.parse(lookupMimeType(filesNameList[i]) ?? ''),
          );
          request.files.add(await multipartFile);
        }
      }
      request.headers.addAll(headers!);
      request.fields.addAll(fields!);

      var response = await request.send();

      Map<String, dynamic> responseJson = {};
      // ignore: prefer_typing_uninitialized_variables
      var value;
      try {
        value = await response.stream.bytesToString();
      } catch (e) {
        log(e.toString());
      }

      responseJson.putIfAbsent('statusCode', () => response.statusCode);
      responseJson.putIfAbsent('response', () => jsonDecode(value));

      return responseJson;
    } catch (error) {
      log(error.toString());
      return null;
    }
  }

  static MediaType getContentType(String name) {
    var ext = name.split('.').last;
    if (ext == "png" || ext == "jpeg") {
      return MediaType.parse("image/jpg");
    } else if (ext == 'pdf') {
      return MediaType.parse("application/pdf");
    } else {
      return MediaType.parse("image/jpg");
    }
  }
}
