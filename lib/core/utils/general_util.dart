import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:com.tad.cllllb/app/my_app_controller.dart';
import 'package:com.tad.cllllb/core/data/repositories/shared_preference_repository.dart';
import 'package:com.tad.cllllb/core/enums/connectivity_status.dart';
import 'package:com.tad.cllllb/core/services/connectivity_service.dart';
import 'package:com.tad.cllllb/core/services/location_service.dart';
import 'package:com.tad.cllllb/core/services/notification_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../services/cart_services.dart';
import '../services/favorite_services.dart';

SharedPreferenceRepository get storage =>
    Get.find<SharedPreferenceRepository>();

CartService get cartService => Get.find<CartService>();
FavoriteService get favoriteService => Get.find<FavoriteService>();
LocationService get locationService => Get.find<LocationService>();
// ConnectivityService get connectivityService => Get.find<ConnectivityService>();
NotificationService get notificationService => Get.find<NotificationService>();
// bool get isOnline =>
//     Get.find<MyAppController>().connectionStatus == ConnectivityStatus.ONLINE;

// void fadeInTransition(Widget view) {
//   Get.to(view, transition: Transition.fadeIn);
// }

// void checkConnection(Function function) {
//   if (isOnline) function();
//   // else
//   //   CustomToast.showMessage(
//   //       message: 'Please check internet connection',
//   //       messageType: MessageType.WARNING);
// }

extension EmptyPadding on num {
  SizedBox get ph => SizedBox(height: toDouble());
  SizedBox get pw => SizedBox(width: toDouble());
}

extension StringExtension on String {
  String useCorrectEllipsis() {
    return replaceAll('', '\u200B');
  }
}

extension ImageExtension on BuildContext {
  String image(String url) {
    return 'https://cllllb.com$url';
  }

  Future<T?> showBottomSheet<T>(
    BuildContext context, {
    required Widget Function(BuildContext) builder,
    double? maxHeight,
  }) {
    return showModalBottomSheet<T?>(
      context: context,
      isScrollControlled: true,
      enableDrag: true,
      // showDragHandle: true,
      useSafeArea: true,
      useRootNavigator: true,
      backgroundColor: AppColors.mainWhiteColor,
      elevation: 0,
      constraints:
          maxHeight == null ? null : BoxConstraints(maxHeight: maxHeight),
      builder: builder,
    ).then((value) => value);
  }
}
