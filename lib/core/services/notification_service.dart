import 'dart:async';
import 'dart:developer';

import 'package:com.tad.cllllb/UI/views/main_view/main_controller.dart';
import 'package:com.tad.cllllb/UI/views/notifications_view/notifications_view.dart';
import 'package:com.tad.cllllb/core/enums/application_state.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';

import '../data/repositories/notifictions_repositories.dart';
import '../utils/general_util.dart';

class NotificationService {
  StreamController<RemoteMessage> notificationStream = StreamController.broadcast();
  RxInt counter = 0.obs;

  NotificationService() {
    onInit();
  }

  void onInit() async {
    try {
      // ignore: no_leading_underscores_for_local_identifiers
      final _firebaseMessaging = FirebaseMessaging.instance;

      final fcmToken = await _firebaseMessaging.getToken();

      log(fcmToken!);

      //FirebaseMessaging.onBackgroundMessage(addNotifications);
      //!--- Here to call api ----
      storage.getUserId() == 0 ? null : updateFcm(fcmToken: fcmToken);
      FirebaseMessaging.instance.onTokenRefresh.listen((fcmToken) async {
        storage.getUserId() == 0 ? null : updateFcm(fcmToken: fcmToken);
        // Note: This callback is fired at each app startup and whenever a new
        // token is generated.
        //!--- Here to call api ----
      }).onError((err) {
        // Error getting token.
      });

      if (GetPlatform.isIOS) {
        // ignore: unused_local_variable
        NotificationSettings settings = await _firebaseMessaging.requestPermission(
          alert: true,
          provisional: true,
          sound: true,
          badge: true,
          carPlay: false,
          criticalAlert: false,
        );

        // if (settings.authorizationStatus == AuthorizationStatus.denied) {

        // }
      }

      FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
        storage.getEnableNotifications()
            ? {
                counter += 1,
                //addNotifications(message),
                handelNotification(model: message, applicationState: ApplicationState.FOREGROUND)
              }
            : null;

        // await FirebaseMessaging.instance.subscribeToTopic('notifications');

        //NotificationModel model = NotificationModel.fromJson(message.data);
      });

      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        Get.to(NotificationsView(
          currentlocation: Get.find<MainController>().currentlocation,
        ));
        //addNotifications(message);

        // NotificationModel model = NotificationModel.fromJson(message.data);
        handelNotification(model: message, applicationState: ApplicationState.BACKGROUND);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  void handelNotification(
      {required RemoteMessage model, required ApplicationState applicationState}) {
    notificationStream.add(model);
    // if (model.notificatioNType == NotificationType.SUBSECRIPTION.name) {
    //   storage.setSubStatus(model.model == '1' ? true : false);
    // }
  }
}

Future<void> addNotifications(RemoteMessage message) async {
  await NotificationsRepositories()
      .addNotification(title: message.notification!.title!, body: message.notification!.body!)
      .then((value) {
    value.fold((l) {
      addNotifications(message);
    }, (r) => null);
  });
}

Future<void> updateFcm({required String fcmToken}) async {
  await NotificationsRepositories()
      .upateFcm(userId: storage.getUserId(), fcmToken: fcmToken)
      .then((value) {
    value.fold((l) {
      updateFcm(fcmToken: fcmToken);
    }, (r) {});
  });
}

// Future<void> handleBackgroundMessage(RemoteMessage message) async {
//   print(message.notification!.title);
//   print(message.notification!.body);
// }


