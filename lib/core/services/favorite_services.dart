import 'package:com.tad.cllllb/UI/shared/utils.dart';
import 'package:com.tad.cllllb/core/data/models/cart_model.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';
import 'package:get/state_manager.dart';
import '../data/models/apis/all_products_model.dart';

class FavoriteService {
  RxList<CartModel> favoriteList = storage.getFavoriteList().obs;

  RxInt cartCount = 0.obs;

  RxDouble subTotal = 0.0.obs;
  RxDouble tax = 0.0.obs;
  RxDouble delivery = 0.0.obs;
  RxDouble total = 0.0.obs;

  FavoriteService() {
    cartCount.value = getCartCount();
    calcTotals();
  }

  void addToCart({
    required AllProductsModel model,
    required int count,
    Function? afterAdd,
  }) {
    if (getCartModel(model) != null) {
      int index = favoriteList.indexOf(getCartModel(model)!);
      favoriteList[index].count = favoriteList[index].count! + count;
      favoriteList[index].totalItem =
          favoriteList[index].totalItem! + (count * model.price!);
    } else {
      favoriteList.add(CartModel(
        count: count,
        totalItem: (count * model.price!).toDouble(),
        productsModel: model,
      ));
    }
    cartCount.value += count;
    calcTotals();
    storage.setFavoriteList(favoriteList);
    if (afterAdd != null) afterAdd();
  }

  void removeFromCart({required CartModel model, Function? afterRemove}) {
    favoriteList.remove(model);
    storage.setFavoriteList(favoriteList);
    cartCount.value -= model.count!;
    calcTotals();

    if (afterRemove != null) afterRemove();
  }

  void changeCount(
      {required bool incress,
      required CartModel model,
      Function? afterChange}) {
    CartModel result = getCartModel(model.productsModel!)!;
    int index = favoriteList.indexOf(result);

    if (incress) {
      result.count = result.count! + 1;
      result.totalItem = result.totalItem! + model.productsModel!.price!;

      cartCount.value += 1;
    } else {
      if (result.count! > 1) {
        result.count = result.count! - 1;
        result.totalItem = result.totalItem! - model.productsModel!.price!;
        cartCount.value -= 1;
      }
    }

    favoriteList.remove(result);
    favoriteList.insert(index, result);

    calcTotals();

    storage.setFavoriteList(favoriteList);
    if (afterChange != null) afterChange();
  }

  CartModel? getCartModel(AllProductsModel model) {
    try {
      return favoriteList.firstWhere(
        (element) => element.productsModel!.id == model.id,
      );
    } catch (e) {
      return null;
    }
  }

  int getCartCount() {
    return favoriteList.fold(
        0, (previousValue, element) => previousValue + element.count!);
  }

  void calcTotals() {
    subTotal.value = favoriteList.fold(
        0, (previousValue, element) => previousValue + element.totalItem!);

    tax.value = subTotal * taxAmount;
    delivery.value = (subTotal.value + tax.value) * deliveryAmount;
    total.value = subTotal.value + tax.value + delivery.value;
  }

  void clearCart() {
    favoriteList.clear();
    storage.setFavoriteList(favoriteList);
    cartCount.value = 0;
    calcTotals();
  }
}
