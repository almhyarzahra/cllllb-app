import 'package:com.tad.cllllb/core/enums/request_type.dart';
import 'package:com.tad.cllllb/core/utils/general_util.dart';

class NetworkConfig {
  // ignore: non_constant_identifier_names
  static String BASE_API = 'api/';

  static String getFulApiUrl(String api) {
    return BASE_API + api;
  }

  static Map<String, String> getHeaders(
      {bool isMultipartRequest = false,
      bool? needAuth = true,
      RequestType? type = RequestType.POST,
      Map<String, String>? extraHeaders = const {}}) {
    return {
      if (needAuth!) 'Authorization': 'Bearer ${storage.getTokenInfo() ?? ''}',
      if (type != RequestType.GET && isMultipartRequest == false)
        'Content-Type': 'application/json',
      ...extraHeaders!
    };
  }
}
