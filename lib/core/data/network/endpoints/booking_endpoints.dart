import '../network_config.dart';

class BookingEndPoints {
  static String bookingGame = NetworkConfig.getFulApiUrl('booking/club_games');
  static String gameField = NetworkConfig.getFulApiUrl('booking/game_fields');
  static String gameTime = NetworkConfig.getFulApiUrl('booking/get_game_times');
  static String playersNow = NetworkConfig.getFulApiUrl('booking/playersNow');
  static String booking = NetworkConfig.getFulApiUrl('booking/book');
  static String bookingRetreat = NetworkConfig.getFulApiUrl('booking/bookingRetreat');
  static String bookingWalletUpdate = NetworkConfig.getFulApiUrl('booking/Booking_walet_update');
  static String editBookingStatus = NetworkConfig.getFulApiUrl('booking/edit_booking_status');
  static String bookingEmail = NetworkConfig.getFulApiUrl('booking/booking_email');
  static String userBooking = NetworkConfig.getFulApiUrl('booking/get_user_booking');
  static String cancelBooking = NetworkConfig.getFulApiUrl('booking/cancelBook');
  static String getShots = NetworkConfig.getFulApiUrl('booking/get_field_by_id');
  static String timeShots = NetworkConfig.getFulApiUrl('booking/get_shot_games_times');
  static String checkWalletUser = NetworkConfig.getFulApiUrl('booking/check_user');
  static String sendBookNotif = NetworkConfig.getFulApiUrl('booking/send_book_notification');
  static String approveBooking = NetworkConfig.getFulApiUrl('booking/approve_booking');
  static String checkApproved = NetworkConfig.getFulApiUrl('booking/check_user_approved');
  static String checkCoach = NetworkConfig.getFulApiUrl('booking/checkCoach');
  static String coach = NetworkConfig.getFulApiUrl('booking/getGameFieldCoach');
  static String subscription = NetworkConfig.getFulApiUrl('booking/getGameFieldSubscription');
  static String paySubscription = NetworkConfig.getFulApiUrl('gameSubscription/add_user_game_subscription');
  static String userSubscription = NetworkConfig.getFulApiUrl('gameSubscription/get_user_subscription');
  


  
  
}
