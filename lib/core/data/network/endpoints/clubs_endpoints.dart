import 'package:com.tad.cllllb/core/data/network/network_config.dart';

class ClubsEndPoints {
  static String allClub = NetworkConfig.getFulApiUrl('club/get_all_clubs');
  static String advertisements = NetworkConfig.getFulApiUrl('ads/get_admin_ads');
  static String numClicks = NetworkConfig.getFulApiUrl('ads/edit_num_of_clicks');
  static String allProducts = NetworkConfig.getFulApiUrl('club/get_all_products');
  static String clubProducts = NetworkConfig.getFulApiUrl('club/get_club_products');
  static String specialOffersApp = NetworkConfig.getFulApiUrl('club/get_club_products_category');
  static String specialOfferClub = NetworkConfig.getFulApiUrl('club/get_products_category');
  static String clubInfo = NetworkConfig.getFulApiUrl('club/get_club_info');
  static String addReview = NetworkConfig.getFulApiUrl('club/add_reviews');
  static String addReport = NetworkConfig.getFulApiUrl('club/add_complaints');
  static String productById = NetworkConfig.getFulApiUrl('club/get_product_by_id/');
  static String news = NetworkConfig.getFulApiUrl('news_jobs/get_news');
  static String jobs = NetworkConfig.getFulApiUrl('news_jobs/get_jobs');
  static String onlineStore = NetworkConfig.getFulApiUrl('club/get_online_store');
  static String allClubsProducts = NetworkConfig.getFulApiUrl('club/get_all_club_products');
  static String applyJob = NetworkConfig.getFulApiUrl('news_jobs/apply_job');
  static String lookingJob = NetworkConfig.getFulApiUrl('jobs_looking/get_looking_job');
  static String userLookingJob = NetworkConfig.getFulApiUrl('jobs_looking/user_looking_job');
  static String editingLookingJob = NetworkConfig.getFulApiUrl('jobs_looking/edit_looking_job');
  static String addLookingJob = NetworkConfig.getFulApiUrl('jobs_looking/add_looking_job');
  static String clubNews = NetworkConfig.getFulApiUrl('news_jobs/get_club_news');
  static String clubJobs = NetworkConfig.getFulApiUrl('news_jobs/get_club_jobs');
  static String deleteLookingJob = NetworkConfig.getFulApiUrl('jobs_looking/delete_looking_job');
  static String checkMenu = NetworkConfig.getFulApiUrl('menu/checkMenue');
  static String getMenus = NetworkConfig.getFulApiUrl('menu/getMenus');
  static String category = NetworkConfig.getFulApiUrl('menu/getFood');
  static String store = NetworkConfig.getFulApiUrl('club/club_sections');
  static String searchProducts = NetworkConfig.getFulApiUrl('club/search');
  static String filterAllProducts = NetworkConfig.getFulApiUrl('club/filter');
  static String cllllbOffers = NetworkConfig.getFulApiUrl('club/allproductswithpoints');






 
}
