import 'package:com.tad.cllllb/core/data/network/network_config.dart';

class UserEndPoints {
  static String register = NetworkConfig.getFulApiUrl('auth/register');
  static String allGame = NetworkConfig.getFulApiUrl('club/get_all_games');
  static String allProducts = NetworkConfig.getFulApiUrl('products/get_user_products');
  static String userInfo = NetworkConfig.getFulApiUrl('auth/profile');
  static String loginGoogle = NetworkConfig.getFulApiUrl('login/google');
  static String userOrders = NetworkConfig.getFulApiUrl('products/get_user_orders');
  static String gameClub = NetworkConfig.getFulApiUrl('club/club_games');
  static String editProfile = NetworkConfig.getFulApiUrl('auth/edit_profile');
  static String verfication = NetworkConfig.getFulApiUrl('auth/otp');
  static String userGame = NetworkConfig.getFulApiUrl('auth/user_game');
  static String login = NetworkConfig.getFulApiUrl('auth/phone_login');
  static String deleteUser = NetworkConfig.getFulApiUrl('auth/deactivate_user');
  static String checkUser = NetworkConfig.getFulApiUrl('auth/check_phone');
  static String otp = NetworkConfig.getFulApiUrl('auth/otp');
  static String code = NetworkConfig.getFulApiUrl('auth/random_code');
  static String userFavoriteGame = NetworkConfig.getFulApiUrl('club/user_favorite_games');
  static String support = NetworkConfig.getFulApiUrl('support/add');
  static String giftPoints = NetworkConfig.getFulApiUrl('gift/add');
  static String lottery = NetworkConfig.getFulApiUrl('draws/index');
  static String enterLottery = NetworkConfig.getFulApiUrl('draws/enter');
  static String replacePoints = NetworkConfig.getFulApiUrl('auth/pointtomony');
  static String lotteryResult = NetworkConfig.getFulApiUrl('draws/ended');
  static String replacePointUser= NetworkConfig.getFulApiUrl('points/change');
  static String vouchers= NetworkConfig.getFulApiUrl('coupons');
  static String buyVoucher= NetworkConfig.getFulApiUrl('coupons/buy');
  static String userVouchers= NetworkConfig.getFulApiUrl('coupons/purchased');
  static String checkVouchers= NetworkConfig.getFulApiUrl('coupons/check');
  static String homeDraws= NetworkConfig.getFulApiUrl('draws/all');



















}
