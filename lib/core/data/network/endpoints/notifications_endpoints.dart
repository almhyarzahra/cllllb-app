import '../network_config.dart';

class NotificationsEndPoints {
  static String addNotification = NetworkConfig.getFulApiUrl('notifications/add_notification');
  static String getNotifications = NetworkConfig.getFulApiUrl('notifications/get_notifications');
  static String updateFcm = NetworkConfig.getFulApiUrl('auth/update_fcm');
}
