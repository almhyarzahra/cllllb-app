import '../network_config.dart';

class PaymentEndPoints {
  static String addOrder = NetworkConfig.getFulApiUrl('products/add_order');
  static String checkOrder = NetworkConfig.getFulApiUrl('products/edit_order_status');
  static String walletBalance = NetworkConfig.getFulApiUrl('products/wallet_balance');
  static String walletUpdate = NetworkConfig.getFulApiUrl('products/wallet_update');
  static String orderEmail = NetworkConfig.getFulApiUrl('products/order_email');
  static String cancelOrder = NetworkConfig.getFulApiUrl('products/cancel_order');


}
