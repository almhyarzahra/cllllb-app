class ShotsModel {
  int? id;
  int? clubGameId;
  String? name;
  String? image;
  double? price;
  String? date;
  int? timeSlots;
  String? startTime;
  String? endTime;
  int? points;
  int? shots;
  int? time;
  int? withWeapon;
  List<GameWeapon>? gameWeapon;

  ShotsModel(
      {this.id,
      this.clubGameId,
      this.name,
      this.image,
      this.price,
      this.date,
      this.timeSlots,
      this.startTime,
      this.endTime,
      this.points,
      this.shots,
      this.time,
      this.withWeapon,
      this.gameWeapon});

  ShotsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clubGameId = json['club_game_id'];
    name = json['name'];
    image = json['image'];
    price = json['price'] is int ? double.parse(json['price'].toString()) : json['price'];
    date = json['date'];
    timeSlots = json['time_slots'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    points = json['points'];
    shots = json['shots'];
    time = json['time'];
    withWeapon = json['with_weapon'];
    if (json['game_weapon'] != null) {
      gameWeapon = <GameWeapon>[];
      json['game_weapon'].forEach((v) {
        gameWeapon!.add(GameWeapon.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['club_game_id'] = clubGameId;
    data['name'] = name;
    data['image'] = image;
    data['price'] = price;
    data['date'] = date;
    data['time_slots'] = timeSlots;
    data['start_time'] = startTime;
    data['end_time'] = endTime;
    data['points'] = points;
    data['shots'] = shots;
    data['time'] = time;
    data['with_weapon'] = withWeapon;
    if (gameWeapon != null) {
      data['game_weapon'] = gameWeapon!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GameWeapon {
  int? id;
  String? name;
  int? shot;
  int? time;
  double? price;
  String? image;
  int? gameFieldId;

  GameWeapon({this.id, this.name, this.shot, this.time, this.price, this.image, this.gameFieldId});

  GameWeapon.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    shot = json['shot'];
    time = json['time'];
    price = json['price'] is int ? double.parse(json['price'].toString()) : json['price'];
    image = json['image'];
    gameFieldId = json['game_field_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['shot'] = shot;
    data['time'] = time;
    data['price'] = price;
    data['image'] = image;
    data['game_field_id'] = gameFieldId;
    return data;
  }
}
