class LotteryModel {
    int? id;
    String? title;
    String? description;
    double? pointsValue;
    bool? user;
    DateTime? expiresAt;
    String? createdAt;
    bool? ended;
    int? chances;
    String? message;

    LotteryModel({
        this.id,
        this.title,
        this.description,
        this.pointsValue,
        this.user,
        this.expiresAt,
        this.createdAt,
        this.ended,
        this.chances,
    });

    factory LotteryModel.fromJson(Map<String, dynamic> json) => LotteryModel(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        pointsValue: json['points_value'] is int
        ? double.parse(json['points_value'].toString())
        : json['points_value'],
        user: json["user"],
        expiresAt: json["expires_at"] == null ? null : DateTime.parse(json["expires_at"]),
        createdAt: json["created_at"],
        ended: json["ended"],
        chances: json["chances"]
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "points_value": pointsValue,
        "user": user,
        "expires_at": expiresAt?.toIso8601String(),
        "created_at": createdAt,
        "ended": ended,
        "chances": chances,
    };
}
