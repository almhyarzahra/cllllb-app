class LookingJobModel {
  int? id;
  int? userId;
  String? jobTitle;
  String? yearExperince;
  String? skills;
  String? education;
  String? cv;
  User? user;
  String? message;

  LookingJobModel(
      {this.id,
      this.userId,
      this.jobTitle,
      this.yearExperince,
      this.skills,
      this.education,
      this.cv,
      this.user});

  LookingJobModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    jobTitle = json['job_title'];
    yearExperince = json['year_experince'];
    skills = json['skills'];
    education = json['education'];
    cv = json['cv'];
    user = json['user'] != null ? User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['user_id'] = userId;
    data['job_title'] = jobTitle;
    data['year_experince'] = yearExperince;
    data['skills'] = skills;
    data['education'] = education;
    data['cv'] = cv;
    if (user != null) {
      data['user'] = user!.toJson();
    }
    return data;
  }
}

class User {
  int? id;
  String? name;
  String? email;
  String? phone;
  String? image;
  String? emailVerifiedAt;
  int? typeId;
  int? isActive;
  String? fcmToken;
  String? createdAt;
  String? updatedAt;
  String? device;
  String? googleToken;

  User(
      {this.id,
      this.name,
      this.email,
      this.phone,
      this.image,
      this.emailVerifiedAt,
      this.typeId,
      this.isActive,
      this.fcmToken,
      this.createdAt,
      this.updatedAt,
      this.device,
      this.googleToken});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    image = json['image'];
    emailVerifiedAt = json['email_verified_at'];
    typeId = json['type_id'];
    isActive = json['is_active'];
    fcmToken = json['fcm_token'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    device = json['device'];
    googleToken = json['google_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['image'] = image;
    data['email_verified_at'] = emailVerifiedAt;
    data['type_id'] = typeId;
    data['is_active'] = isActive;
    data['fcm_token'] = fcmToken;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['device'] = device;
    data['google_token'] = googleToken;
    return data;
  }
}
