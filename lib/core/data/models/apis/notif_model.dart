class NotifModel {
  int? code;
  List<Notifications>? notifications;
  String? message;

  NotifModel({this.code, this.notifications});

  NotifModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    if (json['notifications'] != null) {
      notifications = <Notifications>[];
      json['notifications'].forEach((v) {
        notifications!.add(Notifications.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    if (notifications != null) {
      data['notifications'] = notifications!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Notifications {
  int? id;
  String? title;
  String? body;
  String? createdAt;
  int? userId;
  int? type;

  Notifications({this.id, this.title, this.body, this.createdAt, this.userId, this.type});

  Notifications.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    body = json['body'];
    createdAt = json['created_at'];
    userId = json['user_id'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['title'] = title;
    data['body'] = body;
    data['created_at'] = createdAt;
    data['user_id'] = userId;
    data['type'] = type;
    return data;
  }
}
