class AdvertisementsModel {
  int? id;
  String? image;
  String? url;
  int? userId;
  int? position;
  String? startDate;
  String? endDate;
  int? clubId;
  int? numOfClicks;
  String? createdAt;
  String? message ;

  AdvertisementsModel(
      {this.id,
      this.image,
      this.url,
      this.userId,
      this.position,
      this.startDate,
      this.endDate,
      this.clubId,
      this.numOfClicks,
      this.createdAt});

  AdvertisementsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    url = json['url'];
    userId = json['user_id'];
    position = json['position'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    clubId = json['club_id'];
    numOfClicks = json['num_of_clicks'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['image'] = image;
    data['url'] = url;
    data['user_id'] = userId;
    data['position'] = position;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    data['club_id'] = clubId;
    data['num_of_clicks'] = numOfClicks;
    data['created_at'] = createdAt;
    return data;
  }
}
