class PlayersModel {
  int? id;
  int? userId;
  int? gameClubFieldId;
  String? startTime;
  String? endTime;
  int? paymentMethod;
  int? paymentStatus;
  int? txnId;
  String? date;
  Field? field;
  User? user;
  String? message;

  PlayersModel(
      {this.id,
      this.userId,
      this.gameClubFieldId,
      this.startTime,
      this.endTime,
      this.paymentMethod,
      this.paymentStatus,
      this.txnId,
      this.date,
      this.field,
      this.user});

  PlayersModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    gameClubFieldId = json['game_club_field_id'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    paymentMethod = json['payment_method'];
    paymentStatus = json['payment_status'];
    txnId = json['txn_id'];
    date = json['date'];
    field = json['field'] != null ? Field.fromJson(json['field']) : null;
    user = json['user'] != null ? User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['user_id'] = userId;
    data['game_club_field_id'] = gameClubFieldId;
    data['start_time'] = startTime;
    data['end_time'] = endTime;
    data['payment_method'] = paymentMethod;
    data['payment_status'] = paymentStatus;
    data['txn_id'] = txnId;
    data['date'] = date;
    if (field != null) {
      data['field'] = field!.toJson();
    }
    if (user != null) {
      data['user'] = user!.toJson();
    }
    return data;
  }
}

class Field {
  int? id;
  int? clubGameId;
  String? name;
  String? image;
  double? price;
  String? date;
  int? timeSlots;
  String? startTime;
  String? endTime;
  int? points;

  Field(
      {this.id,
      this.clubGameId,
      this.name,
      this.image,
      this.price,
      this.date,
      this.timeSlots,
      this.startTime,
      this.endTime,
      this.points});

  Field.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clubGameId = json['club_game_id'];
    name = json['name'];
    image = json['image'];
    price = json['price'] is int ? double.parse(json['price'].toString()) : json['price'];
    date = json['date'];
    timeSlots = json['time_slots'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    points = json['points'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['club_game_id'] = clubGameId;
    data['name'] = name;
    data['image'] = image;
    data['price'] = price;
    data['date'] = date;
    data['time_slots'] = timeSlots;
    data['start_time'] = startTime;
    data['end_time'] = endTime;
    data['points'] = points;
    return data;
  }
}

class User {
  int? id;
  String? name;
  String? email;
  String? phone;
  String? image;
  String? emailVerifiedAt;
  int? typeId;
  int? isActive;
  String? fcmToken;
  String? createdAt;
  String? updatedAt;
  String? device;
  String? googleToken;

  User(
      {this.id,
      this.name,
      this.email,
      this.phone,
      this.image,
      this.emailVerifiedAt,
      this.typeId,
      this.isActive,
      this.fcmToken,
      this.createdAt,
      this.updatedAt,
      this.device,
      this.googleToken});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    image = json['image'];
    emailVerifiedAt = json['email_verified_at'];
    typeId = json['type_id'];
    isActive = json['is_active'];
    fcmToken = json['fcm_token'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    device = json['device'];
    googleToken = json['google_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['image'] = image;
    data['email_verified_at'] = emailVerifiedAt;
    data['type_id'] = typeId;
    data['is_active'] = isActive;
    data['fcm_token'] = fcmToken;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['device'] = device;
    data['google_token'] = googleToken;
    return data;
  }
}
