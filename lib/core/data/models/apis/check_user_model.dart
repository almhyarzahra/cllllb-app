class CheckUserModel {
  int? code;
  String? message;

  CheckUserModel({this.code, this.message});

  CheckUserModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    data['message'] = message;
    return data;
  }
}
