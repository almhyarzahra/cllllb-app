class StoreModel {
  int? code;
  String? section;
  int? count;
  String? image;
  String? message ;

  StoreModel({this.code, this.section, this.count, this.image});

  StoreModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    section = json['section'];
    count = json['count'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    data['section'] = section;
    data['count'] = count;
    data['image'] = image;
    return data;
  }
}
