class OnlineStoreModel {
  int? id;
  String? name;
  int? parentId;
  String? description;
  String? image;
  int? status;
  String? message;

  OnlineStoreModel({this.id, this.name, this.parentId, this.description, this.image, this.status});

  OnlineStoreModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    parentId = json['parent_id'];
    description = json['description'];
    image = json['image'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['parent_id'] = parentId;
    data['description'] = description;
    data['image'] = image;
    data['status'] = status;
    return data;
  }
}
