class AllGameModel {
    List<GameData>? data;

    AllGameModel({
        this.data,
    });

    factory AllGameModel.fromJson(Map<String, dynamic> json) => AllGameModel(
        data: json["data"] == null ? [] : List<GameData>.from(json["data"]!.map((x) => GameData.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": data == null ? [] : List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class GameData {
    int? id;
    dynamic pointsValue;
    DateTime? createdAt;
    int? isDeleted;
    int? withWeapon;
    int? withCoach;
    int? fieldType;
    int? status;
    int? minPlayers;
    String? bannerImage;
    String? name;
    int? bookingType;

    GameData({
        this.id,
        this.pointsValue,
        this.createdAt,
        this.isDeleted,
        this.withWeapon,
        this.withCoach,
        this.fieldType,
        this.status,
        this.minPlayers,
        this.bannerImage,
        this.name,
        this.bookingType,
    });

    factory GameData.fromJson(Map<String, dynamic> json) => GameData(
        id: json["id"],
        pointsValue: json["points_value"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        isDeleted: json["is_deleted"],
        withWeapon: json["with_weapon"],
        withCoach: json["with_coach"],
        fieldType: json["field_type"],
        status: json["status"],
        minPlayers: json["min_players"],
        bannerImage: json["banner_image"],
        name: json["name"],
        bookingType: json["booking_type"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "points_value": pointsValue,
        "created_at": createdAt?.toIso8601String(),
        "is_deleted": isDeleted,
        "with_weapon": withWeapon,
        "with_coach": withCoach,
        "field_type": fieldType,
        "status": status,
        "min_players": minPlayers,
        "banner_image": bannerImage,
        "name": name,
        "booking_type": bookingType,
    };
}
