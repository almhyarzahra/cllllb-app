class UserInfoModel {
    UserInfo? userInfo;
    double? userWallet;

    UserInfoModel({
        this.userInfo,
        this.userWallet,
    });

    factory UserInfoModel.fromJson(Map<String, dynamic> json) => UserInfoModel(
        userInfo: json["user_info"] == null ? null : UserInfo.fromJson(json["user_info"]),
        userWallet: json['user_wallet'] is int
        ? double.parse(json['user_wallet'].toString())
        : json['user_wallet'],
    );

    Map<String, dynamic> toJson() => {
        "user_info": userInfo?.toJson(),
        "user_wallet": userWallet,
    };
}

class UserInfo {
    int? id;
    String? name;
    String? email;
    String? phone;
    String? image;
    dynamic totalPoints;
    String? referenceCode;
    String? shareBarcode;

    UserInfo({
        this.id,
        this.name,
        this.email,
        this.phone,
        this.image,
        this.totalPoints,
        this.referenceCode,
        this.shareBarcode,
    });

    factory UserInfo.fromJson(Map<String, dynamic> json) => UserInfo(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        phone: json["phone"],
        image: json["image"],
        totalPoints: json["total_points"],
        referenceCode: json["reference_code"],
        shareBarcode: json["share_barcode"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "phone": phone,
        "image": image,
        "total_points": totalPoints,
        "reference_code": referenceCode,
        "share_barcode":shareBarcode,
    };
}
