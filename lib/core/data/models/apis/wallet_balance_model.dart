class WalletBalanceModel {
  int? code;
  double? userBalance;

  WalletBalanceModel({this.code, this.userBalance});

  WalletBalanceModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    userBalance = json['user_balance'] is int
        ? double.parse(json['user_balance'].toString())
        : json['user_balance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    data['user_balance'] = userBalance;
    return data;
  }
}
