class GameBookingModel {
  int? id;
  int? clubId;
  int? gameId;
  String? createdAt;
  int? quantity;
  Games? games;
  String? message;

  GameBookingModel({this.id, this.clubId, this.gameId, this.createdAt, this.quantity, this.games});

  GameBookingModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clubId = json['club_id'];
    gameId = json['game_id'];
    createdAt = json['created_at'];
    quantity = json['quantity'];
    games = json['games'] != null ? Games.fromJson(json['games']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['club_id'] = clubId;
    data['game_id'] = gameId;
    data['created_at'] = createdAt;
    data['quantity'] = quantity;
    if (games != null) {
      data['games'] = games!.toJson();
    }
    return data;
  }
}

class Games {
  int? id;
  String? name;
  int? bookingType;
  int? minPlayers;
  String? bannerImage;
  int? status;
  int? fieldType;
  String? createdAt;
  int? isDeleted;

  Games(
      {this.id,
      this.name,
      this.bookingType,
      this.minPlayers,
      this.bannerImage,
      this.status,
      this.fieldType,
      this.createdAt,
      this.isDeleted});

  Games.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    bookingType = json['booking_type'];
    minPlayers = json['min_players'];
    bannerImage = json['banner_image'];
    status = json['status'];
    fieldType = json['field_type'];
    createdAt = json['created_at'];
    isDeleted = json['is_deleted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['booking_type'] = bookingType;
    data['min_players'] = minPlayers;
    data['banner_image'] = bannerImage;
    data['status'] = status;
    data['field_type'] = fieldType;
    data['created_at'] = createdAt;
    data['is_deleted'] = isDeleted;
    return data;
  }
}
