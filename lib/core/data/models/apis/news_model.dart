class NewsModel {
  int? isDeleted;
  int? id;
  String? title;
  String? content;
  String? date;
  int? clubId;
  int? userId;
  String? image;
  String? clubTypeName;
  Club? club;
  String? message ;

  NewsModel(
      {this.isDeleted,
      this.id,
      this.title,
      this.content,
      this.date,
      this.clubId,
      this.userId,
      this.image,
      this.clubTypeName,
      this.club});

  NewsModel.fromJson(Map<String, dynamic> json) {
    clubTypeName = json['club_type_name'];
    isDeleted = json['is_deleted'];
    id = json['id'];
    title = json['title'];
    content = json['content'];
    date = json['date'];
    clubId = json['club_id'];
    userId = json['user_id'];
    image = json['image'];
    club = json['club'] != null ?  Club.fromJson(json['club']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['club_type_name'] = clubTypeName;
    data['is_deleted'] = isDeleted;
    data['id'] = id;
    data['title'] = title;
    data['content'] = content;
    data['date'] = date;
    data['club_id'] = clubId;
    data['user_id'] = userId;
    data['image'] = image;
    if (club != null) {
      data['club'] = club!.toJson();
    }
    return data;
  }
}

class Club {
  int? id;
  String? name;
  String? email;
  int? clubType;
  int? subscriptionId;
  String? activities;
  String? history;
  String? licenceImage;
  String? licenceExpiryDate;
  String? address;
  String? location;
  String? city;
  String? latitude;
  String? longtitude;
  String? phone;
  String? website;
  String? logoImage;
  String? mainImage;
  String? clubVideo;
  int? status;
  String? password;
  String? createdAt;
  String? openTime;
  String? closeTime;
  String? weekends;
  int? isDeleted;

  Club(
      {this.id,
      this.name,
      this.email,
      this.clubType,
      this.subscriptionId,
      this.activities,
      this.history,
      this.licenceImage,
      this.licenceExpiryDate,
      this.address,
      this.location,
      this.city,
      this.latitude,
      this.longtitude,
      this.phone,
      this.website,
      this.logoImage,
      this.mainImage,
      this.clubVideo,
      this.status,
      this.password,
      this.createdAt,
      this.openTime,
      this.closeTime,
      this.weekends,
      this.isDeleted});

  Club.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    clubType = json['club_type'];
    subscriptionId = json['subscription_id'];
    activities = json['activities'];
    history = json['history'];
    licenceImage = json['licence_image'];
    licenceExpiryDate = json['licence_expiry_date'];
    address = json['address'];
    location = json['location'];
    city = json['city'];
    latitude = json['latitude'];
    longtitude = json['longtitude'];
    phone = json['phone'];
    website = json['website'];
    logoImage = json['logo_image'];
    mainImage = json['main_image'];
    clubVideo = json['club_video'];
    status = json['status'];
    password = json['password'];
    createdAt = json['created_at'];
    openTime = json['open_time'];
    closeTime = json['close_time'];
    weekends = json['weekends'];
    isDeleted = json['is_deleted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['club_type'] = clubType;
    data['subscription_id'] = subscriptionId;
    data['activities'] = activities;
    data['history'] = history;
    data['licence_image'] = licenceImage;
    data['licence_expiry_date'] = licenceExpiryDate;
    data['address'] = address;
    data['location'] = location;
    data['city'] = city;
    data['latitude'] = latitude;
    data['longtitude'] = longtitude;
    data['phone'] = phone;
    data['website'] = website;
    data['logo_image'] = logoImage;
    data['main_image'] = mainImage;
    data['club_video'] = clubVideo;
    data['status'] = status;
    data['password'] = password;
    data['created_at'] = createdAt;
    data['open_time'] =openTime;
    data['close_time'] = closeTime;
    data['weekends'] = weekends;
    data['is_deleted'] = isDeleted;
    return data;
  }
}
