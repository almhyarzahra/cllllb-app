class UserVouchersModel {
    String? code;
    String? title;
    int? points;
    DateTime? startDate;
    DateTime? endDate;
    int? couponValue;
    bool? isEnded;
    bool? isDeleted;
    String? message;

    UserVouchersModel({
        this.code,
        this.title,
        this.points,
        this.startDate,
        this.endDate,
        this.couponValue,
        this.isEnded,
        this.isDeleted,
    });

    factory UserVouchersModel.fromJson(Map<String, dynamic> json) => UserVouchersModel(
        code: json["code"],
        title: json["title"],
        points: json["points"],
        startDate: json["startDate"] == null ? null : DateTime.parse(json["startDate"]),
        endDate: json["endDate"] == null ? null : DateTime.parse(json["endDate"]),
        couponValue: json["couponValue"],
        isEnded: json["isEnded"],
        isDeleted: json["isDeleted"],
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "title": title,
        "points": points,
        "startDate": "${startDate!.year.toString().padLeft(4, '0')}-${startDate!.month.toString().padLeft(2, '0')}-${startDate!.day.toString().padLeft(2, '0')}",
        "endDate": "${endDate!.year.toString().padLeft(4, '0')}-${endDate!.month.toString().padLeft(2, '0')}-${endDate!.day.toString().padLeft(2, '0')}",
        "couponValue": couponValue,
        "isEnded": isEnded,
        "isDeleted": isDeleted,
    };
}
