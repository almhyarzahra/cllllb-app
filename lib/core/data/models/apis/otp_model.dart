class OtpModel {
  String? requestId;
  String? destination;
  String? validUnitlTimestamp;
  String? createdTimestamp;
  String? lastEventTimestamp;
  String? status;

  OtpModel(
      {this.requestId,
      this.destination,
      this.validUnitlTimestamp,
      this.createdTimestamp,
      this.lastEventTimestamp,
      this.status});

  OtpModel.fromJson(Map<String, dynamic> json) {
    requestId = json['requestId'];
    destination = json['destination'];
    validUnitlTimestamp = json['validUnitlTimestamp'];
    createdTimestamp = json['createdTimestamp'];
    lastEventTimestamp = json['lastEventTimestamp'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['requestId'] = requestId;
    data['destination'] = destination;
    data['validUnitlTimestamp'] = validUnitlTimestamp;
    data['createdTimestamp'] = createdTimestamp;
    data['lastEventTimestamp'] = lastEventTimestamp;
    data['status'] = status;
    return data;
  }
}
