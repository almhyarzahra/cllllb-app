class FavoriteGameModel {
    int? gameId;
    String? gameImage;
    String? name;
    String? message;

    FavoriteGameModel({
        this.gameId,
        this.gameImage,
        this.name,
    });

    factory FavoriteGameModel.fromJson(Map<String, dynamic> json) => FavoriteGameModel(
        gameId: json["game_id"],
        gameImage: json["image"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "game_id": gameId,
        "image": gameImage,
        "name": name,
    };
}
