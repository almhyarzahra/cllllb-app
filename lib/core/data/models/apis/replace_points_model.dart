class ReplacePointsModel {
    double? money;
    double? pointsNedded;
    double? minValue;

    ReplacePointsModel({
        this.money,
        this.pointsNedded,
        this.minValue,
    });

    factory ReplacePointsModel.fromJson(Map<String, dynamic> json) => ReplacePointsModel(
        money: json['money'] is int
        ? double.parse(json['money'].toString())
        : json['money'],
        pointsNedded:json['points_nedded'] is int
        ? double.parse(json['points_nedded'].toString())
        : json['points_nedded'],
        minValue: json['min_value'] is int
        ? double.parse(json['min_value'].toString())
        : json['min_value'],
    );

    Map<String, dynamic> toJson() => {
        "money": money,
        "points_nedded": pointsNedded,
        "min_value":minValue,
    };
}
