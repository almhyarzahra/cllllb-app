class BookingModel {
  int? code;
  String? success;
  int? bookId;

  BookingModel({this.code, this.success, this.bookId});

  BookingModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    success = json['success'];
    bookId = json['book_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data ={};
    data['code'] = code;
    data['success'] = success;
    data['book_id'] = bookId;
    return data;
  }
}
