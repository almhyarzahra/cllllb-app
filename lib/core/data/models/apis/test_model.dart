// ignore_for_file: unnecessary_this

class TestModel {
  int? outgoingId;
  String? origin;
  String? destination;
  String? message;
  String? dateTime;
  String? status;

  TestModel(
      {this.outgoingId,
      this.origin,
      this.destination,
      this.message,
      this.dateTime,
      this.status});

  TestModel.fromJson(Map<String, dynamic> json) {
    outgoingId = json['outgoing_id'];
    origin = json['origin'];
    destination = json['destination'];
    message = json['message'];
    dateTime = json['dateTime'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['outgoing_id'] = this.outgoingId;
    data['origin'] = this.origin;
    data['destination'] = this.destination;
    data['message'] = this.message;
    data['dateTime'] = this.dateTime;
    data['status'] = this.status;
    return data;
  }
}
