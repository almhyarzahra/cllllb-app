class OrderModel {
  int? code;
  String? success;
  int? orderId;

  OrderModel({
    this.code,
    this.success,
    this.orderId,
  });

  OrderModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    success = json['success'];
    orderId = json['order_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    data['success'] = success;
    data['order_id'] = orderId;

    return data;
  }
}
