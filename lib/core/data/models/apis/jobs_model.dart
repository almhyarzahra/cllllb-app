class JobsModel {
  int? isDeleted;
  int? id;
  int? clubId;
  int? userId;
  String? jobTitle;
  String? jobDescription;
  String? announcementStartDate;
  String? announcementEndDate;
  String? image;
  String? expectSalary;
  Club? club;
  User? user;
  String? message;
  double? distance;

  JobsModel(
      {this.isDeleted,
      this.id,
      this.clubId,
      this.userId,
      this.jobTitle,
      this.jobDescription,
      this.announcementStartDate,
      this.announcementEndDate,
      this.image,
      this.expectSalary,
      this.club,
      this.user});

  JobsModel.fromJson(Map<String, dynamic> json) {
    isDeleted = json['is_deleted'];
    id = json['id'];
    clubId = json['club_id'];
    userId = json['user_id'];
    jobTitle = json['job_title'];
    jobDescription = json['job_description'];
    announcementStartDate = json['announcement_start_date'];
    announcementEndDate = json['announcement_end_date'];
    image = json['image'];
    expectSalary = json['expect_salary'];
    club = json['club'] != null ? Club.fromJson(json['club']) : null;
    user = json['user'] != null ? User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['is_deleted'] = isDeleted;
    data['id'] = id;
    data['club_id'] = clubId;
    data['user_id'] = userId;
    data['job_title'] = jobTitle;
    data['job_description'] = jobDescription;
    data['announcement_start_date'] = announcementStartDate;
    data['announcement_end_date'] = announcementEndDate;
    data['image'] = image;
    data['expect_salary'] = expectSalary;
    if (club != null) {
      data['club'] = club!.toJson();
    }
    if (user != null) {
      data['user'] = user!.toJson();
    }
    return data;
  }
}

class Club {
  int? id;
  String? name;
  String? email;
  int? clubType;
  int? subscriptionId;
  String? activities;
  String? history;
  String? licenceImage;
  String? licenceExpiryDate;
  String? address;
  String? location;
  String? city;
  String? latitude;
  String? longtitude;
  String? phone;
  String? website;
  String? logoImage;
  String? mainImage;
  String? clubVideo;
  int? status;
  String? password;
  String? createdAt;
  String? openTime;
  String? closeTime;
  String? weekends;
  int? isDeleted;

  Club(
      {this.id,
      this.name,
      this.email,
      this.clubType,
      this.subscriptionId,
      this.activities,
      this.history,
      this.licenceImage,
      this.licenceExpiryDate,
      this.address,
      this.location,
      this.city,
      this.latitude,
      this.longtitude,
      this.phone,
      this.website,
      this.logoImage,
      this.mainImage,
      this.clubVideo,
      this.status,
      this.password,
      this.createdAt,
      this.openTime,
      this.closeTime,
      this.weekends,
      this.isDeleted});

  Club.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    clubType = json['club_type'];
    subscriptionId = json['subscription_id'];
    activities = json['activities'];
    history = json['history'];
    licenceImage = json['licence_image'];
    licenceExpiryDate = json['licence_expiry_date'];
    address = json['address'];
    location = json['location'];
    city = json['city'];
    latitude = json['latitude'];
    longtitude = json['longtitude'];
    phone = json['phone'];
    website = json['website'];
    logoImage = json['logo_image'];
    mainImage = json['main_image'];
    clubVideo = json['club_video'];
    status = json['status'];
    password = json['password'];
    createdAt = json['created_at'];
    openTime = json['open_time'];
    closeTime = json['close_time'];
    weekends = json['weekends'];
    isDeleted = json['is_deleted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['club_type'] = clubType;
    data['subscription_id'] = subscriptionId;
    data['activities'] = activities;
    data['history'] = history;
    data['licence_image'] = licenceImage;
    data['licence_expiry_date'] = licenceExpiryDate;
    data['address'] = address;
    data['location'] = location;
    data['city'] = city;
    data['latitude'] = latitude;
    data['longtitude'] = longtitude;
    data['phone'] = phone;
    data['website'] = website;
    data['logo_image'] = logoImage;
    data['main_image'] = mainImage;
    data['club_video'] = clubVideo;
    data['status'] = status;
    data['password'] = password;
    data['created_at'] = createdAt;
    data['open_time'] = openTime;
    data['close_time'] = closeTime;
    data['weekends'] = weekends;
    data['is_deleted'] = isDeleted;
    return data;
  }
}

class User {
  int? id;
  String? name;
  String? email;
  String? phone;
  String? image;
  String? emailVerifiedAt;
  int? typeId;
  int? isActive;
  String? fcmToken;
  String? createdAt;
  String? updatedAt;
  String? device;
  String? googleToken;

  User(
      {this.id,
      this.name,
      this.email,
      this.phone,
      this.image,
      this.emailVerifiedAt,
      this.typeId,
      this.isActive,
      this.fcmToken,
      this.createdAt,
      this.updatedAt,
      this.device,
      this.googleToken});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    image = json['image'];
    emailVerifiedAt = json['email_verified_at'];
    typeId = json['type_id'];
    isActive = json['is_active'];
    fcmToken = json['fcm_token'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    device = json['device'];
    googleToken = json['google_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['image'] = image;
    data['email_verified_at'] = emailVerifiedAt;
    data['type_id'] = typeId;
    data['is_active'] = isActive;
    data['fcm_token'] = fcmToken;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['device'] = device;
    data['google_token'] = googleToken;
    return data;
  }
}
