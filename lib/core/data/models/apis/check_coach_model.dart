class CheckCoachModel {
  int? code;
  String? meassge;

  CheckCoachModel({this.code, this.meassge});

  CheckCoachModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    meassge = json['meassge'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    data['meassge'] = meassge;
    return data;
  }
}
