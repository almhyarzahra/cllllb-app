class ClubInfoModel {
  int? id;
  String? name;
  String? email;
  ClubType? clubType;
  int? subscriptionId;
  String? activities;
  String? history;
  String? licenceImage;
  String? licenceExpiryDate;
  String? address;
  String? location;
  String? city;
  String? latitude;
  String? longtitude;
  String? phone;
  String? website;
  String? logoImage;
  String? mainImage;
  String? clubVideo;
  int? status;
  String? password;
  String? createdAt;
  String? openTime;
  String? closeTime;
  String? weekends;
  int? rating;
  List<String>? images;
  List<Reviews>? reviews;
  List<ClubImages>? clubImages;

  ClubInfoModel(
      {this.id,
      this.name,
      this.email,
      this.clubType,
      this.subscriptionId,
      this.activities,
      this.history,
      this.licenceImage,
      this.licenceExpiryDate,
      this.address,
      this.location,
      this.city,
      this.latitude,
      this.longtitude,
      this.phone,
      this.website,
      this.logoImage,
      this.mainImage,
      this.clubVideo,
      this.status,
      this.password,
      this.createdAt,
      this.openTime,
      this.closeTime,
      this.weekends,
      this.rating,
      this.images,
      this.reviews,
      this.clubImages});

  ClubInfoModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    clubType = json['club_type'] != null
        ?  ClubType.fromJson(json['club_type'])
        : null;
    subscriptionId = json['subscription_id'];
    activities = json['activities'];
    history = json['history'];
    licenceImage = json['licence_image'];
    licenceExpiryDate = json['licence_expiry_date'];
    address = json['address'];
    location = json['location'];
    city = json['city'];
    latitude = json['latitude'];
    longtitude = json['longtitude'];
    phone = json['phone'];
    website = json['website'];
    logoImage = json['logo_image'];
    mainImage = json['main_image'];
    clubVideo = json['club_video'];
    status = json['status'];
    password = json['password'];
    createdAt = json['created_at'];
    openTime = json['open_time'];
    closeTime = json['close_time'];
    weekends = json['weekends'];
    rating = json['rating'];
    images = json['images'].cast<String>();
    if (json['reviews'] != null) {
      reviews = <Reviews>[];
      json['reviews'].forEach((v) {
        reviews!.add( Reviews.fromJson(v));
      });
    }
    if (json['club_images'] != null) {
      clubImages = <ClubImages>[];
      json['club_images'].forEach((v) {
        clubImages!.add( ClubImages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['email'] =email;
    if (clubType != null) {
      data['club_type'] = clubType!.toJson();
    }
    data['subscription_id'] = subscriptionId;
    data['activities'] = activities;
    data['history'] = history;
    data['licence_image'] = licenceImage;
    data['licence_expiry_date'] = licenceExpiryDate;
    data['address'] = address;
    data['location'] = location;
    data['city'] = city;
    data['latitude'] = latitude;
    data['longtitude'] = longtitude;
    data['phone'] = phone;
    data['website'] = website;
    data['logo_image'] = logoImage;
    data['main_image'] = mainImage;
    data['club_video'] = clubVideo;
    data['status'] = status;
    data['password'] = password;
    data['created_at'] = createdAt;
    data['open_time'] = openTime;
    data['close_time'] = closeTime;
    data['weekends'] = weekends;
    data['rating'] = rating;
    data['images'] = images;
    if (reviews != null) {
      data['reviews'] = reviews!.map((v) => v.toJson()).toList();
    }
    if (clubImages != null) {
      data['club_images'] = clubImages!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ClubType {
  int? id;
  String? name;

  ClubType({this.id, this.name});

  ClubType.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}

class Reviews {
  int? id;
  int? rating;
  String? description;
  int? userId;
  int? clubId;
  String? createdAt;
  UserName? userName;

  Reviews(
      {this.id,
      this.rating,
      this.description,
      this.userId,
      this.clubId,
      this.createdAt,
      this.userName});

  Reviews.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    rating = json['rating'];
    description = json['description'];
    userId = json['user_id'];
    clubId = json['club_id'];
    createdAt = json['created_at'];
    userName = json['user_name'] != null
        ?  UserName.fromJson(json['user_name'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['rating'] = rating;
    data['description'] = description;
    data['user_id'] = userId;
    data['club_id'] = clubId;
    data['created_at'] = createdAt;
    if (userName != null) {
      data['user_name'] = userName!.toJson();
    }
    return data;
  }
}

class UserName {
  String? name;

  UserName({this.name});

  UserName.fromJson(Map<String, dynamic> json) {
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['name'] = name;
    return data;
  }
}

class ClubImages {
  int? id;
  int? clubId;
  String? image;

  ClubImages({this.id, this.clubId, this.image});

  ClubImages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clubId = json['club_id'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['club_id'] = clubId;
    data['image'] = image;
    return data;
  }
}
