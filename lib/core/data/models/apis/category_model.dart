import 'package:com.tad.cllllb/core/data/models/apis/all_products_model.dart';

class CategoryModel {
  int? id;
  String? name;
  int? kitchenId;
  String? image;
  int? isDeleted;
  List<MenuProduct>? menuProduct;
  String? message ;

  CategoryModel({this.id, this.name, this.kitchenId, this.image, this.isDeleted, this.menuProduct});

  CategoryModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    kitchenId = json['kitchen_id'];
    image = json['image'];
    isDeleted = json['is_deleted'];
    if (json['menu_product'] != null) {
      menuProduct = <MenuProduct>[];
      json['menu_product'].forEach((v) {
        menuProduct!.add( MenuProduct.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['kitchen_id'] = kitchenId;
    data['image'] = image;
    data['is_deleted'] = isDeleted;
    if (menuProduct != null) {
      data['menu_product'] = menuProduct!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MenuProduct {
  int? id;
  int? productId;
  int? kitchenCategoryId;
  int? isDeleted;
  AllProductsModel? product;

  MenuProduct({this.id, this.productId, this.kitchenCategoryId, this.isDeleted, this.product});

  MenuProduct.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    kitchenCategoryId = json['kitchen_category_id'];
    isDeleted = json['is_deleted'];
    product = json['product'] != null ?  AllProductsModel.fromJson(json['product']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['product_id'] = productId;
    data['kitchen_category_id'] = kitchenCategoryId;
    data['is_deleted'] = isDeleted;
    if (product != null) {
      data['product'] = product!.toJson();
    }
    return data;
  }
}

class Product {
  int? id;
  String? name;
  int? price;
  int? points;
  String? description;
  int? quantity;
  int? categoryId;
  String? img;
  int? status;
  int? clubId;
  int? inProduct;
  String? updatedAt;
  String? createdAt;
  int? isDeleted;
  String? clubTypeName;
  Club? club;
  List<ProductCategory>? productCategory;

  Product(
      {this.id,
      this.name,
      this.price,
      this.points,
      this.description,
      this.quantity,
      this.categoryId,
      this.img,
      this.status,
      this.clubId,
      this.inProduct,
      this.updatedAt,
      this.createdAt,
      this.isDeleted,
      this.clubTypeName,
      this.club,
      this.productCategory});

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'];
    points = json['points'];
    description = json['description'];
    quantity = json['quantity'];
    categoryId = json['category_id'];
    img = json['img'];
    status = json['status'];
    clubId = json['club_id'];
    inProduct = json['in_product'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    isDeleted = json['is_deleted'];
    clubTypeName = json['club_type_name'];
    club = json['club'] != null ?  Club.fromJson(json['club']) : null;
    if (json['product_category'] != null) {
      productCategory = <ProductCategory>[];
      json['product_category'].forEach((v) {
        productCategory!.add( ProductCategory.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] =id;
    data['name'] = name;
    data['price'] = price;
    data['points'] = points;
    data['description'] = description;
    data['quantity'] = quantity;
    data['category_id'] = categoryId;
    data['img'] = img;
    data['status'] = status;
    data['club_id'] = clubId;
    data['in_product'] = inProduct;
    data['updated_at'] = updatedAt;
    data['created_at'] = createdAt;
    data['is_deleted'] = isDeleted;
    data['club_type_name'] = clubTypeName;
    if (club != null) {
      data['club'] = club!.toJson();
    }
    if (productCategory != null) {
      data['product_category'] = productCategory!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Club {
  int? id;
  String? name;
  String? email;
  int? clubType;
  int? subscriptionId;
  String? activities;
  String? history;
  String? licenceImage;
  String? licenceExpiryDate;
  String? address;
  String? location;
  String? city;
  String? latitude;
  String? longtitude;
  String? phone;
  String? website;
  String? logoImage;
  String? mainImage;
  String? clubVideo;
  int? status;
  String? password;
  String? createdAt;
  String? openTime;
  String? closeTime;
  String? weekends;
  int? isDeleted;

  Club(
      {this.id,
      this.name,
      this.email,
      this.clubType,
      this.subscriptionId,
      this.activities,
      this.history,
      this.licenceImage,
      this.licenceExpiryDate,
      this.address,
      this.location,
      this.city,
      this.latitude,
      this.longtitude,
      this.phone,
      this.website,
      this.logoImage,
      this.mainImage,
      this.clubVideo,
      this.status,
      this.password,
      this.createdAt,
      this.openTime,
      this.closeTime,
      this.weekends,
      this.isDeleted});

  Club.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    clubType = json['club_type'];
    subscriptionId = json['subscription_id'];
    activities = json['activities'];
    history = json['history'];
    licenceImage = json['licence_image'];
    licenceExpiryDate = json['licence_expiry_date'];
    address = json['address'];
    location = json['location'];
    city = json['city'];
    latitude = json['latitude'];
    longtitude = json['longtitude'];
    phone = json['phone'];
    website = json['website'];
    logoImage = json['logo_image'];
    mainImage = json['main_image'];
    clubVideo = json['club_video'];
    status = json['status'];
    password = json['password'];
    createdAt = json['created_at'];
    openTime = json['open_time'];
    closeTime = json['close_time'];
    weekends = json['weekends'];
    isDeleted = json['is_deleted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['club_type'] = clubType;
    data['subscription_id'] = subscriptionId;
    data['activities'] = activities;
    data['history'] = history;
    data['licence_image'] = licenceImage;
    data['licence_expiry_date'] = licenceExpiryDate;
    data['address'] = address;
    data['location'] = location;
    data['city'] = city;
    data['latitude'] = latitude;
    data['longtitude'] = longtitude;
    data['phone'] = phone;
    data['website'] = website;
    data['logo_image'] = logoImage;
    data['main_image'] = mainImage;
    data['club_video'] = clubVideo;
    data['status'] = status;
    data['password'] =password;
    data['created_at'] = createdAt;
    data['open_time'] = openTime;
    data['close_time'] = closeTime;
    data['weekends'] = weekends;
    data['is_deleted'] = isDeleted;
    return data;
  }
}

class ProductCategory {
  int? id;
  int? productId;
  int? categoryId;
  CategoriesName? categoriesName;

  ProductCategory({this.id, this.productId, this.categoryId, this.categoriesName});

  ProductCategory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    categoryId = json['category_id'];
    categoriesName = json['categories_name'] != null
        ?  CategoriesName.fromJson(json['categories_name'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data ={};
    data['id'] = id;
    data['product_id'] = productId;
    data['category_id'] = categoryId;
    if (categoriesName != null) {
      data['categories_name'] = categoriesName!.toJson();
    }
    return data;
  }
}

class CategoriesName {
  int? id;
  String? name;
  int? parentId;
  String? description;
  String? image;
  int? status;
  int? isFood;

  CategoriesName(
      {this.id, this.name, this.parentId, this.description, this.image, this.status, this.isFood});

  CategoriesName.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    parentId = json['parent_id'];
    description = json['description'];
    image = json['image'];
    status = json['status'];
    isFood = json['is_food'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['parent_id'] = parentId;
    data['description'] = description;
    data['image'] = image;
    data['status'] = status;
    data['is_food'] = isFood;
    return data;
  }
}
