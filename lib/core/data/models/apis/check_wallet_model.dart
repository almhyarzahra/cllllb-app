class CheckWalletModel {
  int? code;
  User? user;
  String? phone;

  CheckWalletModel({this.code, this.user, this.phone});

  CheckWalletModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    user = json['user'] != null ? User.fromJson(json['user']) : null;
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    if (user != null) {
      data['user'] = user!.toJson();
    }
    data['phone'] = phone;
    return data;
  }
}

class User {
  int? id;
  String? name;
  String? email;
  String? phone;
  String? image;
  String? emailVerifiedAt;
  int? typeId;
  int? isActive;
  String? fcmToken;
  String? createdAt;
  String? updatedAt;
  String? device;
  String? googleToken;

  User(
      {this.id,
      this.name,
      this.email,
      this.phone,
      this.image,
      this.emailVerifiedAt,
      this.typeId,
      this.isActive,
      this.fcmToken,
      this.createdAt,
      this.updatedAt,
      this.device,
      this.googleToken});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    image = json['image'];
    emailVerifiedAt = json['email_verified_at'];
    typeId = json['type_id'];
    isActive = json['is_active'];
    fcmToken = json['fcm_token'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    device = json['device'];
    googleToken = json['google_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['image'] = image;
    data['email_verified_at'] = emailVerifiedAt;
    data['type_id'] = typeId;
    data['is_active'] = isActive;
    data['fcm_token'] = fcmToken;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['device'] = device;
    data['google_token'] = googleToken;
    return data;
  }
}
