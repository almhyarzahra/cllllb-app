class MenuModel {
  int? id;
  String? name;
  int? clubId;
  String? description;
  String? image;
  int? isDeleted;

  MenuModel({this.id, this.name, this.clubId, this.description, this.image, this.isDeleted});

  MenuModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    clubId = json['club_id'];
    description = json['description'];
    image = json['image'];
    isDeleted = json['is_deleted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['club_id'] = clubId;
    data['description'] = description;
    data['image'] = image;
    data['is_deleted'] = isDeleted;
    return data;
  }
}
