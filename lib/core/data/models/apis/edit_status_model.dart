class EditStatusModel {
    int? code;
    String? success;
    double? points;
    String? draw;

    EditStatusModel({
        this.code,
        this.success,
        this.points,
        this.draw,
    });

    factory EditStatusModel.fromJson(Map<String, dynamic> json) => EditStatusModel(
        code: json["code"],
        success: json["success"],
        points: json['points'] is int
        ? double.parse(json['points'].toString())
        : json['points'],
        draw: json['draw'],
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "success": success,
        "points": points,
        "draw":draw,
    };
}
