class ApproveModel {
  int? code;
  String? success;

  ApproveModel({this.code, this.success});

  ApproveModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    data['success'] = success;
    return data;
  }
}
