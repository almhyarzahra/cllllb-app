class VouchersModel {
    int? id;
    String? title;
    double? points;
    DateTime? startDate;
    DateTime? endDate;
    int? couponValue;
    bool? userHasEnoughPoints;
    bool? userAlreadyPurchased;
    String? message;

    VouchersModel({
        this.id,
        this.title,
        this.points,
        this.startDate,
        this.endDate,
        this.couponValue,
        this.userHasEnoughPoints,
        this.userAlreadyPurchased,
    });

    factory VouchersModel.fromJson(Map<String, dynamic> json) => VouchersModel(
        id: json["id"],
        title: json["title"],
        points:  json['points'] is int
        ? double.parse(json['points'].toString())
        : json['points'],
        startDate: json["startDate"] == null ? null : DateTime.parse(json["startDate"]),
        endDate: json["endDate"] == null ? null : DateTime.parse(json["endDate"]),
        couponValue: json["couponValue"],
        userHasEnoughPoints:json["userHasEnoughPoints"],
        userAlreadyPurchased:json["userAlreadyPurchased"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "points": points,
        "startDate": "${startDate!.year.toString().padLeft(4, '0')}-${startDate!.month.toString().padLeft(2, '0')}-${startDate!.day.toString().padLeft(2, '0')}",
        "endDate": "${endDate!.year.toString().padLeft(4, '0')}-${endDate!.month.toString().padLeft(2, '0')}-${endDate!.day.toString().padLeft(2, '0')}",
        "couponValue": couponValue,
        "userHasEnoughPoints":userHasEnoughPoints,
        "userAlreadyPurchased":userAlreadyPurchased,
    };
}
