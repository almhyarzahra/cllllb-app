class MyBookingModel {
  int? id;
  int? userId;
  int? gameClubFieldId;
  String? startTime;
  String? endTime;
  int? paymentMethod;
  int? paymentStatus;
  int? total;
  String? txnId;
  String? date;
  GameClubField? gameClubField;
  String? message;

  MyBookingModel(
      {this.id,
      this.userId,
      this.gameClubFieldId,
      this.startTime,
      this.endTime,
      this.paymentMethod,
      this.paymentStatus,
      this.total,
      this.txnId,
      this.date,
      this.gameClubField});

  MyBookingModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    gameClubFieldId = json['game_club_field_id'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    paymentMethod = json['payment_method'];
    paymentStatus = json['payment_status'];
    total = json['total'];
    txnId = json['txn_id'];
    date = json['date'];
    gameClubField =
        json['game_club_field'] != null ? GameClubField.fromJson(json['game_club_field']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['user_id'] = userId;
    data['game_club_field_id'] = gameClubFieldId;
    data['start_time'] = startTime;
    data['end_time'] = endTime;
    data['payment_method'] = paymentMethod;
    data['payment_status'] = paymentStatus;
    data['total'] = total;
    data['txn_id'] = txnId;
    data['date'] = date;
    if (gameClubField != null) {
      data['game_club_field'] = gameClubField!.toJson();
    }
    return data;
  }
}

class GameClubField {
  int? id;
  int? clubGameId;
  String? name;
  String? image;
  double? price;
  String? date;
  int? timeSlots;
  String? startTime;
  String? endTime;
  int? points;

  GameClubField(
      {this.id,
      this.clubGameId,
      this.name,
      this.image,
      this.price,
      this.date,
      this.timeSlots,
      this.startTime,
      this.endTime,
      this.points});

  GameClubField.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clubGameId = json['club_game_id'];
    name = json['name'];
    image = json['image'];
    price = json['price'] is int ? double.parse(json['price'].toString()) : json['price'];
    date = json['date'];
    timeSlots = json['time_slots'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    points = json['points'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['club_game_id'] = clubGameId;
    data['name'] = name;
    data['image'] = image;
    data['price'] = price;
    data['date'] = date;
    data['time_slots'] = timeSlots;
    data['start_time'] = startTime;
    data['end_time'] = endTime;
    data['points'] = points;
    return data;
  }
}
