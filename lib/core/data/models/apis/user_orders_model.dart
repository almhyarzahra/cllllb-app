class UserOrdersModel {
  int? id;
  int? customersId;
  String? customersName;
  String? customersCountry;
  String? customersCity;
  String? customersSuburb;
  String? customersStreetAddress;
  String? customersTelephone;
  String? email;
  double? orderPrice;
  int? shippingCost;
  int? statusId;
  String? paymentMethod;
  String? createdAt;
  String? updatedAt;
  List<OrderProducts>? orderProducts;
  Status? status;
  String? message;

  UserOrdersModel(
      {this.id,
      this.customersId,
      this.paymentMethod,
      this.customersName,
      this.customersCountry,
      this.customersCity,
      this.customersSuburb,
      this.customersStreetAddress,
      this.customersTelephone,
      this.email,
      this.orderPrice,
      this.shippingCost,
      this.statusId,
      this.createdAt,
      this.updatedAt,
      this.orderProducts,
      this.status});

  UserOrdersModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customersId = json['customers_id'];
    customersName = json['customers_name'];
    customersCountry = json['customers_country'];
    customersCity = json['customers_city'];
    customersSuburb = json['customers_suburb'];
    customersStreetAddress = json['customers_street_address'];
    customersTelephone = json['customers_telephone'];
    email = json['email'];
    orderPrice = json['order_price'] is int
        ? double.parse(json['order_price'].toString())
        : json['order_price'];
    shippingCost = json['shipping_cost'];
    statusId = json['status_id'];
    paymentMethod = json['payment_method'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['order_products'] != null) {
      orderProducts = <OrderProducts>[];
      json['order_products'].forEach((v) {
        orderProducts!.add(OrderProducts.fromJson(v));
      });
    }
    status = json['status'] != null ? Status.fromJson(json['status']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['customers_id'] = customersId;
    data['customers_name'] = customersName;
    data['customers_country'] = customersCountry;
    data['customers_city'] = customersCity;
    data['customers_suburb'] = customersSuburb;
    data['customers_street_address'] = customersStreetAddress;
    data['customers_telephone'] = customersTelephone;
    data['email'] = email;
    data['order_price'] = orderPrice;
    data['shipping_cost'] = shippingCost;
    data['status_id'] = statusId;
    data['payment_method'] = paymentMethod;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    if (orderProducts != null) {
      data['order_products'] = orderProducts!.map((v) => v.toJson()).toList();
    }
    if (status != null) {
      data['status'] = status!.toJson();
    }
    return data;
  }
}

class OrderProducts {
  int? id;
  int? orderId;
  String? productName;
  double? price;
  int? quantity;
  double? finalPrice;
  int? productId;
  String? createdAt;
  String? updatedAt;

  OrderProducts(
      {this.id,
      this.orderId,
      this.productName,
      this.price,
      this.quantity,
      this.finalPrice,
      this.productId,
      this.createdAt,
      this.updatedAt});

  OrderProducts.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderId = json['order_id'];
    productName = json['product_name'];
    price = json['price'] is int ? double.parse(json['price'].toString()) : json['price'];
    quantity = json['quantity'];
    finalPrice = json['final_price'] is int
        ? double.parse(json['final_price'].toString())
        : json['final_price'];
    productId = json['product_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['order_id'] = orderId;
    data['product_name'] = productName;
    data['price'] = price;
    data['quantity'] = quantity;
    data['final_price'] = finalPrice;
    data['product_id'] = productId;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class Status {
  int? id;
  String? name;

  Status({this.id, this.name});

  Status.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}
