class GameFieldModel {
  int? id;
  int? clubGameId;
  String? name;
  String? image;
  double? price;
  String? date;
  int? timeSlots;
  String? startTime;
  String? endTime;
  int? points;
  double? pointsValue;
  String? message ;

  GameFieldModel(
      {this.id,
      this.clubGameId,
      this.name,
      this.image,
      this.price,
      this.date,
      this.timeSlots,
      this.startTime,
      this.endTime,
      this.points,
      this.pointsValue,
      });

  GameFieldModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clubGameId = json['club_game_id'];
    name = json['name'];
    image = json['image'];
    price = json['price'] is int
        ? double.parse(json['price'].toString())
        : json['price'];
    date = json['date'];
    timeSlots = json['time_slots'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    points = json['points'];
    pointsValue = json['points_value'] is int
        ? double.parse(json['points_value'].toString())
        : json['points_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data ={};
    data['id'] = id;
    data['club_game_id'] = clubGameId;
    data['name'] = name;
    data['image'] = image;
    data['price'] = price;
    data['date'] = date;
    data['time_slots'] = timeSlots;
    data['start_time'] = startTime;
    data['end_time'] = endTime;
    data['points'] = points;
    data['points_value']=pointsValue;
    return data;
  }
}
