class SubscriptionModel {
  int? id;
  int? durationType;
  int? duration;
  double? price;
  int? coachId;
  int? gameFieldId;
  String? description;
  String? image;
  int? isDeleted;
  String? durationName;
  String? message;

  SubscriptionModel(
      {this.id,
      this.durationType,
      this.duration,
      this.price,
      this.coachId,
      this.gameFieldId,
      this.description,
      this.image,
      this.isDeleted,
      this.durationName});

  SubscriptionModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    durationType = json['duration_type'];
    duration = json['duration'];
    price = json['price'] is int ? double.parse(json['price'].toString()) : json['price'];
    coachId = json['coach_id'];
    gameFieldId = json['game_field_id'];
    description = json['description'];
    image = json['image'];
    isDeleted = json['is_deleted'];
    durationName = json['duration_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['duration_type'] = durationType;
    data['duration'] = duration;
    data['price'] = price;
    data['coach_id'] = coachId;
    data['game_field_id'] = gameFieldId;
    data['description'] = description;
    data['image'] = image;
    data['is_deleted'] = isDeleted;
    data['duration_name'] = durationName;
    return data;
  }
}
