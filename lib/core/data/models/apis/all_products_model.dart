import 'dart:convert';

class AllProductsModel {
  int? id;
  String? name;
  double? price;
  double? pointsValue;
  int? points;
  String? description;
  int? quantity;
  int? categoryId;
  String? img;
  int? status;
  int? clubId;
  int? inProduct;
  String? updatedAt;
  String? createdAt;
  int? isDeleted;
  String? clubTypeName;
  List<ProductCategory>? productCategory;
  Club? club;
  String? message ;

  AllProductsModel(
      {this.id,
      this.name,
      this.price,
      this.pointsValue,
      this.points,
      this.description,
      this.quantity,
      this.categoryId,
      this.img,
      this.status,
      this.clubId,
      this.inProduct,
      this.updatedAt,
      this.createdAt,
      this.isDeleted,
      this.clubTypeName,
      this.productCategory,
      this.club});

  AllProductsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'] is int
        ? double.parse(json['price'].toString())
        : json['price'];
    pointsValue=json['points_value'] is int
        ? double.parse(json['points_value'].toString())
        : json['points_value'];
    points = json['points'];
    description = json['description'];
    quantity = json['quantity'];
    categoryId = json['category_id'];
    img = json['img'];
    status = json['status'];
    clubId = json['club_id'];
    inProduct = json['in_product'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    isDeleted = json['is_deleted'];
    clubTypeName = json['club_type_name'];
    if (json['product_category'] != null) {
      productCategory = <ProductCategory>[];
      json['product_category'].forEach((v) {
        productCategory!.add( ProductCategory.fromJson(v));
      });
    }
    club = json['club'] != null ?  Club.fromJson(json['club']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['price'] = price;
    data['points_value']=pointsValue;
    data['points'] = points;
    data['description'] = description;
    data['quantity'] = quantity;
    data['category_id'] = categoryId;
    data['img'] = img;
    data['status'] = status;
    data['club_id'] = clubId;
    data['in_product'] = inProduct;
    data['updated_at'] = updatedAt;
    data['created_at'] = createdAt;
    data['is_deleted'] = isDeleted;
    data['club_type_name'] = clubTypeName;
    data['message'] = message;
    if (productCategory != null) {
      data['product_category'] =
          productCategory!.map((v) => v.toJson()).toList();
    }
    if (club != null) {
      data['club'] = club!.toJson();
    }
    return data;
  }

  static Map<String, dynamic> toMap(AllProductsModel model) {
    return {
      "id": model.id,
      "name": model.name,
      "price": model.price,
      "points_value":model.pointsValue,
      "points": model.points,
      "description": model.description,
      "quantity": model.quantity,
      "category_id": model.categoryId,
      "img": model.img,
      "status": model.status,
      "club_id": model.clubId,
      "in_product": model.inProduct,
      "updated_at": model.updatedAt,
      "created_at": model.createdAt,
      "is_deleted": model.isDeleted,
      "club_type_name": model.clubTypeName,
      "product_category": model.productCategory,
      "club": model.club,
      "message": model.message,
    };
  }

  static String encode(List<AllProductsModel> list) => json.encode(
        list
            .map<Map<String, dynamic>>(
                (element) => AllProductsModel.toMap(element))
            .toList(),
      );

  static List<AllProductsModel> decode(String strList) =>
      (json.decode(strList) as List<dynamic>)
          .map<AllProductsModel>((item) => AllProductsModel.fromJson(item))
          .toList();
}

class ProductCategory {
  int? id;
  int? productId;
  int? categoryId;
  CategoryInfo? categoryInfo;
  CategoryInfo? categoriesName;

  ProductCategory(
      {this.id,
      this.productId,
      this.categoryId,
      this.categoryInfo,
      this.categoriesName});

  ProductCategory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    categoryId = json['category_id'];
    categoryInfo = json['category_info'] != null
        ?  CategoryInfo.fromJson(json['category_info'])
        : null;
    categoriesName = json['categories_name'] != null
        ?  CategoryInfo.fromJson(json['categories_name'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['product_id'] = productId;
    data['category_id'] = categoryId;
    if (categoryInfo != null) {
      data['category_info'] = categoryInfo!.toJson();
    }
    if (categoriesName != null) {
      data['categories_name'] = categoriesName!.toJson();
    }
    return data;
  }
}

class CategoryInfo {
  int? id;
  String? name;
  int? parentId;
  String? description;
  String? image;
  int? status;

  CategoryInfo(
      {this.id,
      this.name,
      this.parentId,
      this.description,
      this.image,
      this.status});

  CategoryInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    parentId = json['parent_id'];
    description = json['description'];
    image = json['image'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['parent_id'] = parentId;
    data['description'] = description;
    data['image'] = image;
    data['status'] = status;
    return data;
  }
}

class Club {
  int? id;
  String? name;
  String? email;
  int? clubType;
  int? subscriptionId;
  String? activities;
  String? history;
  String? licenceImage;
  String? licenceExpiryDate;
  String? address;
  String? location;
  String? city;
  String? latitude;
  String? longtitude;
  String? phone;
  String? website;
  String? logoImage;
  String? mainImage;
  String? clubVideo;
  int? status;
  String? password;
  String? createdAt;
  String? openTime;
  String? closeTime;
  String? weekends;

  Club(
      {this.id,
      this.name,
      this.email,
      this.clubType,
      this.subscriptionId,
      this.activities,
      this.history,
      this.licenceImage,
      this.licenceExpiryDate,
      this.address,
      this.location,
      this.city,
      this.latitude,
      this.longtitude,
      this.phone,
      this.website,
      this.logoImage,
      this.mainImage,
      this.clubVideo,
      this.status,
      this.password,
      this.createdAt,
      this.openTime,
      this.closeTime,
      this.weekends});

  Club.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    clubType = json['club_type'];
    subscriptionId = json['subscription_id'];
    activities = json['activities'];
    history = json['history'];
    licenceImage = json['licence_image'];
    licenceExpiryDate = json['licence_expiry_date'];
    address = json['address'];
    location = json['location'];
    city = json['city'];
    latitude = json['latitude'];
    longtitude = json['longtitude'];
    phone = json['phone'];
    website = json['website'];
    logoImage = json['logo_image'];
    mainImage = json['main_image'];
    clubVideo = json['club_video'];
    status = json['status'];
    password = json['password'];
    createdAt = json['created_at'];
    openTime = json['open_time'];
    closeTime = json['close_time'];
    weekends = json['weekends'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['club_type'] = clubType;
    data['subscription_id'] = subscriptionId;
    data['activities'] = activities;
    data['history'] = history;
    data['licence_image'] = licenceImage;
    data['licence_expiry_date'] = licenceExpiryDate;
    data['address'] = address;
    data['location'] = location;
    data['city'] = city;
    data['latitude'] = latitude;
    data['longtitude'] = longtitude;
    data['phone'] = phone;
    data['website'] = website;
    data['logo_image'] = logoImage;
    data['main_image'] = mainImage;
    data['club_video'] = clubVideo;
    data['status'] = status;
    data['password'] = password;
    data['created_at'] = createdAt;
    data['open_time'] = openTime;
    data['close_time'] = closeTime;
    data['weekends'] = weekends;
    return data;
  }
}
