class LotteryResultModel {
    String? title;
    String? description;
    DateTime? exipresAt;
    String? createdAt;
    List<Prize>? prizes;
    Club? club;
    String? message;

    LotteryResultModel({
        this.title,
        this.description,
        this.exipresAt,
        this.createdAt,
        this.prizes,
        this.club,
    });

    factory LotteryResultModel.fromJson(Map<String, dynamic> json) => LotteryResultModel(
        title: json["title"],
        description: json["description"],
        exipresAt: json["exipres_at"] == null ? null : DateTime.parse(json["exipres_at"]),
        createdAt: json["created_at"],
        prizes: json["prizes"] == null ? [] : List<Prize>.from(json["prizes"]!.map((x) => Prize.fromJson(x))),
        club: json["club"] == null ? null : Club.fromJson(json["club"]),
    );

    Map<String, dynamic> toJson() => {
        "title": title,
        "description": description,
        "created_at": createdAt,
        "exipres_at": "${exipresAt!.year.toString().padLeft(4, '0')}-${exipresAt!.month.toString().padLeft(2, '0')}-${exipresAt!.day.toString().padLeft(2, '0')}",
        "prizes": prizes == null ? [] : List<dynamic>.from(prizes!.map((x) => x.toJson())),
        "club": club?.toJson(),
    };
}

class Club {
    String? name;
    String? logoImage;

    Club({
        this.name,
        this.logoImage,
    });

    factory Club.fromJson(Map<String, dynamic> json) => Club(
        name: json["name"],
        logoImage: json["logo_image"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "logo_image": logoImage,
    };
}

class Prize {
    String? prizeName;
    List<Winner>? winners;

    Prize({
        this.prizeName,
        this.winners,
    });

    factory Prize.fromJson(Map<String, dynamic> json) => Prize(
        prizeName: json["prize_name"],
        winners: json["winners"] == null ? [] : List<Winner>.from(json["winners"]!.map((x) => Winner.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "prize_name": prizeName,
        "winners": winners == null ? [] : List<dynamic>.from(winners!.map((x) => x.toJson())),
    };
}

class Winner {
    String? name;
    String? image;

    Winner({
        this.name,
        this.image,
    });

    factory Winner.fromJson(Map<String, dynamic> json) => Winner(
        name: json["name"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "image": image,
    };
}
