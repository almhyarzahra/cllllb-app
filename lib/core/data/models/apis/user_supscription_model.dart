class UserSubscriptionModel {
  int? id;
  int? gameSubscriptionId;
  int? userId;
  String? startDate;
  String? endDate;
  double? price;
  int? paymentMethod;
  int? paymentStatus;
  Subscription? subscription;
  String? message;

  UserSubscriptionModel(
      {this.id,
      this.gameSubscriptionId,
      this.userId,
      this.startDate,
      this.endDate,
      this.price,
      this.paymentMethod,
      this.paymentStatus,
      this.subscription});

  UserSubscriptionModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    gameSubscriptionId = json['game_subscription_id'];
    userId = json['user_id'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    price = json['price'] is int
        ? double.parse(json['price'].toString())
        : json['price'];
    paymentMethod = json['payment_method'];
    paymentStatus = json['payment_status'];
    subscription = json['subscription'] != null
        ? Subscription.fromJson(json['subscription'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['game_subscription_id'] = gameSubscriptionId;
    data['user_id'] = userId;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    data['price'] = price;
    data['payment_method'] = paymentMethod;
    data['payment_status'] = paymentStatus;
    if (subscription != null) {
      data['subscription'] = subscription!.toJson();
    }
    return data;
  }
}

class Subscription {
  int? id;
  int? durationType;
  int? duration;
  double? price;
  int? coachId;
  int? gameFieldId;
  String? description;
  String? image;
  int? isDeleted;
  String? durationName;
  GameField? gameField;

  Subscription(
      {this.id,
      this.durationType,
      this.duration,
      this.price,
      this.coachId,
      this.gameFieldId,
      this.description,
      this.image,
      this.isDeleted,
      this.durationName,
      this.gameField});

  Subscription.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    durationType = json['duration_type'];
    duration = json['duration'];
    price = json['price'] is int
        ? double.parse(json['price'].toString())
        : json['price'];
    coachId = json['coach_id'];
    gameFieldId = json['game_field_id'];
    description = json['description'];
    image = json['image'];
    isDeleted = json['is_deleted'];
    durationName = json['duration_name'];
    gameField = json['game_field'] != null
        ? GameField.fromJson(json['game_field'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['duration_type'] = durationType;
    data['duration'] = duration;
    data['price'] = price;
    data['coach_id'] = coachId;
    data['game_field_id'] = gameFieldId;
    data['description'] = description;
    data['image'] = image;
    data['is_deleted'] = isDeleted;
    data['duration_name'] = durationName;
    if (gameField != null) {
      data['game_field'] = gameField!.toJson();
    }
    return data;
  }
}

class GameField {
  int? id;
  int? clubGameId;
  String? name;
  String? image;
  dynamic price;
  String? date;
  int? timeSlots;
  String? startTime;
  String? endTime;
  int? points;
  dynamic shots;
  dynamic time;
  int? withWeapon;
  int? withCoach;
  int? isdeleted;

  GameField(
      {this.id,
      this.clubGameId,
      this.name,
      this.image,
      this.price,
      this.date,
      this.timeSlots,
      this.startTime,
      this.endTime,
      this.points,
      this.shots,
      this.time,
      this.withWeapon,
      this.withCoach,
      this.isdeleted});

  GameField.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clubGameId = json['club_game_id'];
    name = json['name'];
    image = json['image'];
    price = json['price'];
    date = json['date'];
    timeSlots = json['time_slots'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    points = json['points'];
    shots = json['shots'];
    time = json['time'];
    withWeapon = json['with_weapon'];
    withCoach = json['with_coach'];
    isdeleted = json['isdeleted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['club_game_id'] = clubGameId;
    data['name'] = name;
    data['image'] = image;
    data['price'] = price;
    data['date'] = date;
    data['time_slots'] = timeSlots;
    data['start_time'] = startTime;
    data['end_time'] = endTime;
    data['points'] = points;
    data['shots'] = shots;
    data['time'] = time;
    data['with_weapon'] = withWeapon;
    data['with_coach'] = withCoach;
    data['isdeleted'] = isdeleted;
    return data;
  }
}
