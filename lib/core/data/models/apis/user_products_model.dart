import 'dart:convert';

class UserProductsModel {
  int? id;
  int? orderId;
  String? productName;
  double? price;
  int? quantity;
  double? finalPrice;
  int? productId;
  String? createdAt;
  String? updatedAt;
  String? date;
  Club? club;
  List<Category>? category;
  String? img;
  String? message;

  UserProductsModel(
      {this.id,
      this.orderId,
      this.productName,
      this.price,
      this.quantity,
      this.finalPrice,
      this.productId,
      this.createdAt,
      this.updatedAt,
      this.date,
      this.club,
      this.category,
      this.img});

  UserProductsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderId = json['order_id'];
    productName = json['product_name'];
    price = json['price'] is int ? double.parse(json['price'].toString()) : json['price'];
    quantity = json['quantity'];
    finalPrice = json['final_price'] is int
        ? double.parse(json['final_price'].toString())
        : json['final_price'];
    productId = json['product_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    date = json['date'];
    club = json['club'] != null ? Club.fromJson(json['club']) : null;
    if (json['category'] != null) {
      category = <Category>[];
      json['category'].forEach((v) {
        category!.add(Category.fromJson(v));
      });
    }
    img = json['img'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['order_id'] = orderId;
    data['product_name'] = productName;
    data['price'] = price;
    data['quantity'] = quantity;
    data['final_price'] = finalPrice;
    data['product_id'] = productId;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['date'] = date;
    if (club != null) {
      data['club'] = club!.toJson();
    }
    if (category != null) {
      data['category'] = category!.map((v) => v.toJson()).toList();
    }
    data['img'] = img;
    return data;
  }

  static Map<String, dynamic> toMap(UserProductsModel model) {
    return {
      "id": model.id,
      "order_id": model.orderId,
      "product_name": model.productName,
      "price": model.price,
      "quantity": model.quantity,
      "final_price": model.finalPrice,
      "product_id": model.productId,
      "created_at": model.createdAt,
      "updated_at": model.updatedAt,
      "date": model.date,
      "club": model.club,
      "category": model.category,
      "img": model.img,
    };
  }

  static String encode(List<UserProductsModel> list) => json.encode(
        list.map<Map<String, dynamic>>((element) => UserProductsModel.toMap(element)).toList(),
      );

  static List<UserProductsModel> decode(String strList) => (json.decode(strList) as List<dynamic>)
      .map<UserProductsModel>((item) => UserProductsModel.fromJson(item))
      .toList();
}

class Club {
  int? id;
  String? name;
  String? email;
  int? clubType;
  int? subscriptionId;
  String? activities;
  String? history;
  String? licenceImage;
  String? licenceExpiryDate;
  String? address;
  String? location;
  String? city;
  String? latitude;
  String? longtitude;
  String? phone;
  String? website;
  String? logoImage;
  String? mainImage;
  String? clubVideo;
  int? status;
  String? password;
  String? createdAt;
  String? openTime;
  String? closeTime;
  String? weekends;

  Club(
      {this.id,
      this.name,
      this.email,
      this.clubType,
      this.subscriptionId,
      this.activities,
      this.history,
      this.licenceImage,
      this.licenceExpiryDate,
      this.address,
      this.location,
      this.city,
      this.latitude,
      this.longtitude,
      this.phone,
      this.website,
      this.logoImage,
      this.mainImage,
      this.clubVideo,
      this.status,
      this.password,
      this.createdAt,
      this.openTime,
      this.closeTime,
      this.weekends});

  Club.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    clubType = json['club_type'];
    subscriptionId = json['subscription_id'];
    activities = json['activities'];
    history = json['history'];
    licenceImage = json['licence_image'];
    licenceExpiryDate = json['licence_expiry_date'];
    address = json['address'];
    location = json['location'];
    city = json['city'];
    latitude = json['latitude'];
    longtitude = json['longtitude'];
    phone = json['phone'];
    website = json['website'];
    logoImage = json['logo_image'];
    mainImage = json['main_image'];
    clubVideo = json['club_video'];
    status = json['status'];
    password = json['password'];
    createdAt = json['created_at'];
    openTime = json['open_time'];
    closeTime = json['close_time'];
    weekends = json['weekends'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['club_type'] = clubType;
    data['subscription_id'] = subscriptionId;
    data['activities'] = activities;
    data['history'] = history;
    data['licence_image'] = licenceImage;
    data['licence_expiry_date'] = licenceExpiryDate;
    data['address'] = address;
    data['location'] = location;
    data['city'] = city;
    data['latitude'] = latitude;
    data['longtitude'] = longtitude;
    data['phone'] = phone;
    data['website'] = website;
    data['logo_image'] = logoImage;
    data['main_image'] = mainImage;
    data['club_video'] = clubVideo;
    data['status'] = status;
    data['password'] = password;
    data['created_at'] = createdAt;
    data['open_time'] = openTime;
    data['close_time'] = closeTime;
    data['weekends'] = weekends;
    return data;
  }
}

class Category {
  int? id;
  String? name;
  int? parentId;
  String? description;
  String? image;
  int? status;

  Category({this.id, this.name, this.parentId, this.description, this.image, this.status});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    parentId = json['parent_id'];
    description = json['description'];
    image = json['image'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['parent_id'] = parentId;
    data['description'] = description;
    data['image'] = image;
    data['status'] = status;
    return data;
  }
}
