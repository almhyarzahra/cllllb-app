class LoginModel {
  int? code;
  String? token;
  int? userId;
  String? userName;
  String? userEmail;
  String? userPhone;
  String? message;

  LoginModel(
      {this.code,
      this.token,
      this.userId,
      this.userName,
      this.userEmail,
      this.userPhone,
      this.message});

  LoginModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    token = json['token'];
    userId = json['user_id'];
    userName = json['user_name'];
    userEmail = json['user_email'];
    userPhone = json['user_phone'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    data['token'] = token;
    data['user_id'] = userId;
    data['user_name'] = userName;
    data['user_email'] = userEmail;
    data['user_phone'] = userPhone;
    data['message'] = message;
    return data;
  }
}
