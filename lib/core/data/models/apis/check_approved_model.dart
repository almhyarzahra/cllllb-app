class CheckApprovedModel {
  int? code;
  String? phone;
  String? status;

  CheckApprovedModel({this.code, this.phone, this.status});

  CheckApprovedModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    phone = json['phone'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    data['phone'] = phone;
    data['status'] = status;
    return data;
  }
}
