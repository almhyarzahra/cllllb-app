class CoachModel {
  int? id;
  String? name;
  String? image;
  int? clubId;
  int? isDeleted;
  String? message ;

  CoachModel({this.id, this.name, this.image, this.clubId, this.isDeleted});

  CoachModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
    clubId = json['club_id'];
    isDeleted = json['is_deleted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['image'] = image;
    data['club_id'] = clubId;
    data['is_deleted'] = isDeleted;
    return data;
  }
}
