import 'package:com.tad.cllllb/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GameTimeModel {
  String? time;
  String? booking;
  Rx<Color>? color = AppColors.mainWhiteColor.obs;
  String? message ;

  GameTimeModel({this.time, this.booking});

  GameTimeModel.fromJson(Map<String, dynamic> json) {
    time = json['time'];
    booking = json['booking'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['time'] = time;
    data['booking'] = booking;
    return data;
  }
}
