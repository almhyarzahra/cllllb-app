class CheckMenuModel {
  int? code;
  String? message;
  int? menu;

  CheckMenuModel({this.code, this.message, this.menu});

  CheckMenuModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    menu = json['menu'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    data['message'] = message;
    data['menu'] = menu;
    return data;
  }
}
