import 'dart:convert';

class AllClubsModel {
  int? id;
  String? name;
  String? email;
  int? clubType;
  int? subscriptionId;
  String? activities;
  String? history;
  String? licenceImage;
  String? licenceExpiryDate;
  String? address;
  String? location;
  String? city;
  String? latitude;
  String? longtitude;
  String? phone;
  String? website;
  String? logoImage;
  String? mainImage;
  String? clubVideo;
  int? status;
  String? password;
  String? createdAt;
  String? openTime;
  String? closeTime;
  String? weekends;
  String? clubTypeName;
  String? message ;
  double? distance;

  AllClubsModel(
      {this.id,
      this.name,
      this.email,
      this.clubType,
      this.subscriptionId,
      this.activities,
      this.history,
      this.licenceImage,
      this.licenceExpiryDate,
      this.address,
      this.location,
      this.city,
      this.latitude,
      this.longtitude,
      this.phone,
      this.website,
      this.logoImage,
      this.mainImage,
      this.clubVideo,
      this.status,
      this.password,
      this.createdAt,
      this.openTime,
      this.closeTime,
      this.weekends,
      this.clubTypeName});

  AllClubsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    clubType = json['club_type'];
    subscriptionId = json['subscription_id'];
    activities = json['activities'];
    history = json['history'];
    licenceImage = json['licence_image'];
    licenceExpiryDate = json['licence_expiry_date'];
    address = json['address'];
    location = json['location'];
    city = json['city'];
    latitude = json['latitude'];
    longtitude = json['longtitude'];
    phone = json['phone'];
    website = json['website'];
    logoImage = json['logo_image'];
    mainImage = json['main_image'];
    clubVideo = json['club_video'];
    status = json['status'];
    password = json['password'];
    createdAt = json['created_at'];
    openTime = json['open_time'];
    closeTime = json['close_time'];
    weekends = json['weekends'];
    clubTypeName = json['club_type_name'];
    message = json['message'];
    distance = json['distance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['club_type'] = clubType;
    data['subscription_id'] = subscriptionId;
    data['activities'] = activities;
    data['history'] = history;
    data['licence_image'] = licenceImage;
    data['licence_expiry_date'] = licenceExpiryDate;
    data['address'] = address;
    data['location'] = location;
    data['city'] = city;
    data['latitude'] = latitude;
    data['longtitude'] = longtitude;
    data['phone'] = phone;
    data['website'] = website;
    data['logo_image'] = logoImage;
    data['main_image'] = mainImage;
    data['club_video'] = clubVideo;
    data['status'] = status;
    data['password'] = password;
    data['created_at'] = createdAt;
    data['open_time'] = openTime;
    data['close_time'] = closeTime;
    data['weekends'] = weekends;
    data['club_type_name'] = clubTypeName;
    data['message'] = message;
    data['distance'] = distance;
    return data;
  }

  static Map<String, dynamic> toMap(AllClubsModel model) {
    return {
      "id": model.id,
      "name": model.name,
      "email": model.email,
      "club_type": model.clubType,
      "subscription_id": model.subscriptionId,
      "activities": model.activities,
      "history": model.history,
      "licence_image": model.licenceImage,
      "licence_expiry_date": model.licenceExpiryDate,
      "address": model.address,
      "location": model.location,
      "city": model.city,
      "latitude": model.latitude,
      "longtitude": model.longtitude,
      "phone": model.phone,
      "website": model.website,
      "logo_image": model.logoImage,
      "main_image": model.mainImage,
      "club_video": model.clubVideo,
      "status": model.status,
      "password": model.password,
      "created_at": model.createdAt,
      "open_time": model.openTime,
      "close_time": model.closeTime,
      "weekends": model.weekends,
      "club_type_name": model.clubTypeName,
      "message": model.message,
      "distance": model.distance,
    };
  }

  static String encode(List<AllClubsModel> list) => json.encode(
        list
            .map<Map<String, dynamic>>(
                (element) => AllClubsModel.toMap(element))
            .toList(),
      );

  static List<AllClubsModel> decode(String strList) =>
      (json.decode(strList) as List<dynamic>)
          .map<AllClubsModel>((item) => AllClubsModel.fromJson(item))
          .toList();
}
