class LoginGoogleModel {
    bool? success;
    Data? data;
    Messages? messages;

    LoginGoogleModel({
        this.success,
        this.data,
        this.messages,
    });

    factory LoginGoogleModel.fromJson(Map<String, dynamic> json) => LoginGoogleModel(
        success: json["success"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        messages: json["messages"] == null ? null : Messages.fromJson(json["messages"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "data": data?.toJson(),
        "messages": messages?.toJson(),
    };
}

class Data {
    String? token;
    String? name;
    String? password;
    int? userId;
    bool? newUser;
    String? userName;
    int? addedPoints;
    int? pointsWillTake;
    String? userEmail;
    dynamic userPhone;
    String? referenceCode;
    String? message;

    Data({
        this.token,
        this.name,
        this.password,
        this.userId,
        this.newUser,
        this.userName,
        this.addedPoints,
        this.pointsWillTake,
        this.userEmail,
        this.userPhone,
        this.referenceCode,
        this.message,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        token: json["token"],
        name: json["name"],
        password: json["password"],
        userId: json["user_id"],
        newUser: json["new_user"],
        userName: json["user_name"],
        addedPoints: json["added_points"],
        pointsWillTake: json["points_will_take"],
        userEmail: json["user_email"],
        userPhone: json["user_phone"],
        referenceCode: json["reference_code"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "token": token,
        "name": name,
        "password": password,
        "user_id": userId,
        "new_user": newUser,
        "user_name": userName,
        "added_points": addedPoints,
        "points_will_take": pointsWillTake,
        "user_email": userEmail,
        "user_phone": userPhone,
        "reference_code": referenceCode,
        "message": message,
    };
}

class Messages {
    String? successMessage;

    Messages({
        this.successMessage,
    });

    factory Messages.fromJson(Map<String, dynamic> json) => Messages(
        successMessage: json["successMessage"],
    );

    Map<String, dynamic> toJson() => {
        "successMessage": successMessage,
    };
}
