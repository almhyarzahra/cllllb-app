class RegisterModel {
  String? token;
  int? userId;
  String? userName;
  double? addedPoints;
  double? pointsWillTake;
  String? userEmail;
  String? userPhone;
  String? referenceCode;
  String? message;

  RegisterModel({
    this.token,
    this.userId,
    this.userName,
    this.userEmail,
    this.userPhone,
    this.message,
    this.addedPoints,
    this.referenceCode,
    this.pointsWillTake,
  });

  RegisterModel.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    userId = json['user_id'];
    userName = json['user_name'];
    addedPoints = json['added_points'] is int
        ? double.parse(json['added_points'].toString())
        : json['added_points'];
    pointsWillTake = json['points_will_take'] is int
        ? double.parse(json['points_will_take'].toString())
        : json['points_will_take'];
    userEmail = json['user_email'];
    userPhone = json['user_phone'];
    referenceCode = json['reference_code'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['token'] = token;
    data['user_id'] = userId;
    data['user_name'] = userName;
    data['added_points'] = addedPoints;
    data['points_will_take'] = pointsWillTake;
    data['user_email'] = userEmail;
    data['user_phone'] = userPhone;
    data['reference_code'] = referenceCode;
    data['message'] = message;
    return data;
  }
}
