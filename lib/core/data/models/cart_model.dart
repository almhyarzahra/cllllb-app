import 'dart:convert';
import 'package:com.tad.cllllb/core/data/models/apis/all_products_model.dart';


class CartModel {
  int? count;
  double? totalItem;
  AllProductsModel? productsModel;

  CartModel({this.count, this.totalItem, this.productsModel});

  CartModel.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    totalItem = json['totalItem'];
    productsModel = json['products_model'] != null
        ?  AllProductsModel.fromJson(json['products_model'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['count'] = count;
    data['totalItem'] = totalItem;
    if (productsModel != null) {
      data['products_model'] = productsModel!.toJson();
    }
    return data;
  }

  static Map<String, dynamic> toMap(CartModel model) {
    return {
      "count": model.count,
      "totalItem": model.totalItem,
      "products_model": model.productsModel,
    };
  }

  static String encode(List<CartModel> list) => json.encode(
        list
            .map<Map<String, dynamic>>((element) => CartModel.toMap(element))
            .toList(),
      );

  static List<CartModel> decode(String strList) =>
      (json.decode(strList) as List<dynamic>)
          .map<CartModel>((item) => CartModel.fromJson(item))
          .toList();
}
