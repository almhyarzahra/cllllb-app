class CartRequestModel {
  int? id;
  String? name;
  String? price;
  int? quantity;

  CartRequestModel({this.id, this.name, this.price, this.quantity});

  CartRequestModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'];
    quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['price'] = price;
    data['quantity'] = quantity;
    return data;
  }
}
