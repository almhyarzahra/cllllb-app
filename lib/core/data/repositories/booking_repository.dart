import 'package:com.tad.cllllb/core/data/models/apis/edit_status_model.dart';
import 'package:dartz/dartz.dart';
import '../../enums/request_type.dart';
import '../../utils/network_util.dart';
import '../models/apis/approve_model.dart';
import '../models/apis/booking_model.dart';
import '../models/apis/check_approved_model.dart';
import '../models/apis/check_coach_model.dart';
import '../models/apis/check_wallet_model.dart';
import '../models/apis/coach_model.dart';
import '../models/apis/game_booking_model.dart';
import '../models/apis/game_field_model.dart';
import '../models/apis/game_time_model.dart';
import '../models/apis/my_booking_model.dart';
import '../models/apis/players_model.dart';
import '../models/apis/shots_model.dart';
import '../models/apis/subscription_model.dart';
import '../models/apis/user_supscription_model.dart';
import '../models/common_response.dart';
import '../network/endpoints/booking_endpoints.dart';
import '../network/network_config.dart';

class BookingRepository {
  Future<Either<String?, List<GameBookingModel>>> getBookingGame({required int clubId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: BookingEndPoints.bookingGame,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "club": clubId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<GameBookingModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(GameBookingModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<GameFieldModel>>> getFieldGame({required int gameId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: BookingEndPoints.gameField,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "club_game_id": gameId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<GameFieldModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(GameFieldModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<GameTimeModel>>> getGameTime(
      {required String date, required int gameFieldId, required String hourNow}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: BookingEndPoints.gameTime,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "date": date.toString(),
            "game_field": gameFieldId.toString(),
            "now": hourNow.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<GameTimeModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(GameTimeModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<PlayersModel>>> getPlayersNow(
      {required int gameId, required String hourNow}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: BookingEndPoints.playersNow,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {"club_game_id": gameId.toString(), "now": hourNow.toString()}).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<PlayersModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(PlayersModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, BookingModel>> booking({
    required String userId,
    required String gameClubFieldId,
    required String startTime,
    required String endTime,
    required String date,
    required double total,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: BookingEndPoints.booking,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "user_id": userId,
            "game_club_field_id": gameClubFieldId,
            "start_time": startTime,
            "end_time": endTime,
            "date": date,
            "total": total,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(BookingModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> bookingRetreat({
    required List<int> id,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: BookingEndPoints.bookingRetreat,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {"id": id}).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> bookingWalletUpdate({
    required int bookId,
    // ignore: non_constant_identifier_names
    required List<int> user_ids,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: BookingEndPoints.bookingWalletUpdate,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "book_id": bookId,
            "user_ids": user_ids,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> editBookingStatus(
      {required int bookId, required int paymentMethod}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: BookingEndPoints.editBookingStatus,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "book_id": bookId,
            "payment_method": paymentMethod,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, EditStatusModel>> sentEmailByBooking({
    required List<int> bookingIds,
    required int userId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: BookingEndPoints.bookingEmail,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "booking_ids": bookingIds,
            "user_id": userId,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(EditStatusModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<MyBookingModel>>> getUserBooking({required int userId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: BookingEndPoints.userBooking,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "user_id": userId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<MyBookingModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(MyBookingModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> cancelBooking({
    required int idBooking,
    required String now,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: BookingEndPoints.cancelBooking,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "id": idBooking.toString(),
            "now": now.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, ShotsModel>> getShots({
    required int gameId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: BookingEndPoints.getShots,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "id": gameId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(ShotsModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<GameTimeModel>>> getGameTimeShots({
    required String date,
    required int gameFieldId,
    required String hourNow,
    required int time,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: BookingEndPoints.timeShots,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "game_field": gameFieldId.toString(),
            "now": hourNow.toString(),
            "date": date.toString(),
            "time": time.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<GameTimeModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(GameTimeModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<CheckWalletModel>>> checkWalletUsers({
    required List<String> phone,
    required double amount,
    // ignore: non_constant_identifier_names
    required int user_id,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: BookingEndPoints.checkWalletUser,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "phone": phone,
            "amount": amount,
            "user_id": user_id,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<CheckWalletModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(CheckWalletModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> sentBookNotification({
    required List<String> phones,
    required double amount,
    required List<int> bookingIDS,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: BookingEndPoints.sendBookNotif,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "phones": phones,
            "amount": amount,
            "booking_ids": bookingIDS,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, ApproveModel>> approveBooking({
    required int notificationId,
    required int userId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: BookingEndPoints.approveBooking,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "notification_id": notificationId,
            "user_id": userId,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(ApproveModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<CheckApprovedModel>>> checkUserApproved({
    required List<String> phones,
    required List<int> bookingIDS,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: BookingEndPoints.checkApproved,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "phones": phones,
            "booking_ids": bookingIDS,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<CheckApprovedModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(CheckApprovedModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, CheckCoachModel>> checkCoach({
    required int fieldId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: BookingEndPoints.checkCoach,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "field_id": fieldId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(CheckCoachModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<CoachModel>>> getGameCoach({
    required int fieldId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: BookingEndPoints.coach,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "field_id": fieldId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<CoachModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(CoachModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<SubscriptionModel>>> getSubscription({
    required int fieldId,
    int? coachId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: BookingEndPoints.subscription,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "field_id": fieldId.toString(),
            if (coachId != null) "coach_id": coachId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<SubscriptionModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(SubscriptionModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, EditStatusModel>> paySubscription({
    required int userId,
    required int subscriptionId,
    required int payMethod,
    required String date,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: BookingEndPoints.paySubscription,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "user_id": userId,
            "subscription_id": subscriptionId,
            "paymethod": payMethod,
            "date": date,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(EditStatusModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<UserSubscriptionModel>>> getUserSubscription({
    required int userId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: BookingEndPoints.userSubscription,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "user_id": userId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<UserSubscriptionModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(UserSubscriptionModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }
}
