import 'package:dartz/dartz.dart';
import '../../enums/request_type.dart';
import '../../utils/network_util.dart';
import '../models/apis/notif_model.dart';
import '../models/common_response.dart';
import '../network/endpoints/notifications_endpoints.dart';
import '../network/network_config.dart';

class NotificationsRepositories {
  Future<Either<String?, bool>> addNotification(
      {required String title, required String body}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: NotificationsEndPoints.addNotification,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "title": title,
            "body": body,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<NotifModel>>> getNotifications({required int userId}) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: NotificationsEndPoints.getNotifications,
        params: {"id": userId.toString()},
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<NotifModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(NotifModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> upateFcm({required int userId, required String fcmToken}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.PUT,
          url: NotificationsEndPoints.updateFcm,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "id": userId,
            "token": fcmToken,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse = CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }
}
