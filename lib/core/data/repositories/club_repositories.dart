import 'package:com.tad.cllllb/core/data/network/endpoints/clubs_endpoints.dart';
import 'package:dartz/dartz.dart';

import '../../enums/request_type.dart';
import '../../utils/network_util.dart';
import '../models/apis/advertisements_model.dart';
import '../models/apis/all_clubs_model.dart';
import '../models/apis/all_products_model.dart';
import '../models/apis/category_model.dart';
import '../models/apis/check_menu_model.dart';
import '../models/apis/club_info_model.dart';
import '../models/apis/jobs_model.dart';
import '../models/apis/looking_job_model.dart';
import '../models/apis/menu_model.dart';
import '../models/apis/news_model.dart';
import '../models/apis/online_store_model.dart';
import '../models/apis/store_model.dart';
import '../models/common_response.dart';
import '../network/network_config.dart';

class ClubRepositories {
  Future<Either<String?, List<AllClubsModel>>> getAllClubs() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: ClubsEndPoints.allClub,
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<AllClubsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(AllClubsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<AdvertisementsModel>>> getAdvertisements() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: ClubsEndPoints.advertisements,
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<AdvertisementsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(AdvertisementsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> addClicks({required int id}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: ClubsEndPoints.numClicks,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {"id": id}).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<AllProductsModel>>> getAllProducts() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: ClubsEndPoints.allProducts,
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<AllProductsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(AllProductsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<AllProductsModel>>> getClubProducts(
      {required int clubId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: ClubsEndPoints.clubProducts,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {"club_id": clubId.toString()}).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<AllProductsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(AllProductsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<AllProductsModel>>> getSpecialOffersApp(
      {required int clubId, required int categoryId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: ClubsEndPoints.specialOffersApp,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "club_id": clubId.toString(),
            "category_id": categoryId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<AllProductsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(AllProductsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<AllProductsModel>>> getSpecialOffersClub(
      {required int categoryId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: ClubsEndPoints.specialOfferClub,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "category_id": categoryId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<AllProductsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(AllProductsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, ClubInfoModel>> getClubInfo({
    required int clubId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: ClubsEndPoints.clubInfo,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {"club_id": clubId.toString()}).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(ClubInfoModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> addReviews(
      {required int rating,
      String? description,
      required int userId,
      required int clubId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: ClubsEndPoints.addReview,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "rating": rating,
            if (description != null) "description": description,
            "user_id": userId,
            "club_id": clubId
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> addReport(
      {required String message,
      required int userId,
      required int clubId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: ClubsEndPoints.addReport,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "message": message,
            "user_id": userId,
            "club_id": clubId,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, AllProductsModel>> getProductsById({
    required int productId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: "${ClubsEndPoints.productById}$productId",
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(AllProductsModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<NewsModel>>> getNews() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: ClubsEndPoints.news,
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<NewsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(NewsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<JobsModel>>> getJobs() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: ClubsEndPoints.jobs,
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<JobsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(JobsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<OnlineStoreModel>>> getOnlineStore() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: ClubsEndPoints.onlineStore,
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<OnlineStoreModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(OnlineStoreModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<AllProductsModel>>> getClubProcucts(
      {required int page}) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: ClubsEndPoints.allClubsProducts,
        params: {"page": page.toString()},
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<AllProductsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(AllProductsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> applyJob({
    required int userId,
    required int jobId,
    required String cv,
  }) async {
    try {
      return NetworkUtil.sendMultipartRequest(
        type: RequestType.POST,
        url: ClubsEndPoints.applyJob,
        fields: {
          'user_id': userId.toString(),
          'job_id': jobId.toString(),
        },
        files: {"cv": cv},
        headers:
            NetworkConfig.getHeaders(needAuth: false, isMultipartRequest: true),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<LookingJobModel>>> getLookingJob() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: ClubsEndPoints.lookingJob,
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<LookingJobModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(LookingJobModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, LookingJobModel>> getUserLookingJob({
    required int userId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: ClubsEndPoints.userLookingJob,
        headers: NetworkConfig.getHeaders(needAuth: false),
        params: {
          "id": userId.toString(),
        },
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(LookingJobModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> editingLookingJob({
    required String jobTitle,
    required int jobId,
    required String yearsExperince,
    required String cv,
    String? skills,
    String? education,
  }) async {
    try {
      return NetworkUtil.sendMultipartRequest(
        type: RequestType.POST,
        url: ClubsEndPoints.editingLookingJob,
        fields: {
          'job_title': jobTitle,
          'id': jobId.toString(),
          'years': yearsExperince,
          if (skills != null) 'skills': skills,
          if (education != null) 'education': education,
        },
        files: {"cv": cv},
        headers:
            NetworkConfig.getHeaders(needAuth: false, isMultipartRequest: true),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> addLookingJob({
    required String jobTitle,
    required int userId,
    required String yearsExperince,
    required String cv,
    String? skills,
    String? education,
  }) async {
    try {
      return NetworkUtil.sendMultipartRequest(
        type: RequestType.POST,
        url: ClubsEndPoints.addLookingJob,
        fields: {
          'job_title': jobTitle,
          'user_id': userId.toString(),
          'years': yearsExperince,
          if (skills != null) 'skills': skills,
          if (education != null) 'education': education,
        },
        files: {"cv": cv},
        headers:
            NetworkConfig.getHeaders(needAuth: false, isMultipartRequest: true),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<NewsModel>>> getNewsClub(
      {required int clubId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: ClubsEndPoints.clubNews,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "id": clubId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<NewsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(NewsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<JobsModel>>> getJobsClub({
    required int clubId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: ClubsEndPoints.clubJobs,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "id": clubId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<JobsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(JobsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  // ignore: non_constant_identifier_names
  Future<Either<String?, bool>> deleteLookingJob({required int user_id}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: ClubsEndPoints.deleteLookingJob,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {"id": user_id}).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, CheckMenuModel>> checkMenu({
    required int clubId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: ClubsEndPoints.checkMenu,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {"club_id": clubId.toString()}).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(CheckMenuModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<MenuModel>>> getMenus(
      {required int clubId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: ClubsEndPoints.getMenus,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "club_id": clubId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<MenuModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(MenuModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<CategoryModel>>> getCategory(
      {required int menuId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: ClubsEndPoints.category,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "menu_id": menuId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<CategoryModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(CategoryModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<StoreModel>>> getClubStore({
    required int clubId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: ClubsEndPoints.store,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "id": clubId.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<StoreModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(StoreModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<AllProductsModel>>> searchAllProducts({
    required int page,
    required String name,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: ClubsEndPoints.searchProducts,
        params: {"page": page.toString(), "name": name},
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<AllProductsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(AllProductsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<AllProductsModel>>> filterAllProducts({
    double? minPrice,
    double? maxPrice,
    List<int>? clubId,
    required int page,
  }) async {
    Map<String, String> result = {};
    if (clubId != null) {
      clubId.map((e) {
        result["club[${clubId.indexOf(e)}]"] = e.toString();
      }).toSet();
    }
    // log(clubId.toString());
    // log(result.toString());
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: ClubsEndPoints.filterAllProducts,
        params: {
          if (minPrice != null) "min_price": minPrice.toString(),
          if (maxPrice != null) "max_price": maxPrice.toString(),
          if (clubId != null) ...result,
          "page": page.toString()
        },
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<AllProductsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(AllProductsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<AllProductsModel>>> getCllllbOffers({
    required int page,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: ClubsEndPoints.cllllbOffers,
        params: {
          "page": page.toString(),
        },
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<AllProductsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(AllProductsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }
}
