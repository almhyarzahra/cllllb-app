import 'dart:developer';

import 'package:com.tad.cllllb/core/data/models/apis/edit_status_model.dart';
import 'package:dartz/dartz.dart';

import '../../enums/request_type.dart';
import '../../utils/network_util.dart';
import '../models/apis/order_model.dart';
import '../models/apis/wallet_balance_model.dart';
import '../models/cart_request_model.dart';
import '../models/common_response.dart';
import '../network/endpoints/payment_endpoints.dart';
import '../network/network_config.dart';

class PaymentRepositories {
  Future<Either<String?, OrderModel>> addOrder({
    required String name,
    required String email,
    required String phone,
    required String street,
    required String city,
    required String country,
    String? suburb,
    String? code,
    required int userId,
    required double total,
    required List<CartRequestModel> cart,
  }) async {
    List<Map<String, dynamic>> myCart = [];
    // ignore: avoid_function_literals_in_foreach_calls
    cart.forEach((element) {
      myCart.add(element.toJson());
    });
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: PaymentEndPoints.addOrder,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "name": name,
            "email": email,
            "phone": phone,
            "street": street,
            "city": city,
            "country": country,
            if (suburb != null) "suburb": suburb,
            "user_id": userId,
            "total": total,
            "cart": myCart,
            if (code != null) "code": code,
          }).then((response) {
        log("$name $email $phone $street $city $country $userId $total $myCart");
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(OrderModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, EditStatusModel>> checkOrder({
    required int orderId,
    required String paymentMethod,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: PaymentEndPoints.checkOrder,
          headers: NetworkConfig.getHeaders(needAuth: true),
          body: {
            "order_id": orderId,
            "payment_method": paymentMethod,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(EditStatusModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, WalletBalanceModel>> getWalletBalance({
    required int userId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: PaymentEndPoints.walletBalance,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {"user_id": userId.toString()}).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(WalletBalanceModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> walletUpdate({
    required int orderId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: PaymentEndPoints.walletUpdate,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "order_id": orderId,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> sentEmailByOrder({
    required int orderId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: PaymentEndPoints.orderEmail,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "order_id": orderId,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> cancelOrder({
    required int orderId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.POST,
        url: PaymentEndPoints.cancelOrder,
        headers: NetworkConfig.getHeaders(needAuth: false),
        params: {"order_id": orderId.toString()},
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }
}
