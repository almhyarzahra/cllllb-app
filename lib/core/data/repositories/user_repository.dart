import 'package:com.tad.cllllb/core/data/models/apis/favorite_game_model.dart';
import 'package:com.tad.cllllb/core/data/models/apis/lottery_model.dart';
import 'package:com.tad.cllllb/core/data/models/apis/lottery_result_model.dart';
import 'package:com.tad.cllllb/core/data/models/apis/replace_points_model.dart';
import 'package:com.tad.cllllb/core/data/models/apis/user_vouchers_model.dart';
import 'package:com.tad.cllllb/core/data/models/apis/vouchers_model.dart';
import 'package:com.tad.cllllb/core/data/models/common_response.dart';
import 'package:com.tad.cllllb/core/data/network/endpoints/user_endpoints.dart';
import 'package:com.tad.cllllb/core/data/network/network_config.dart';
import 'package:com.tad.cllllb/core/enums/request_type.dart';
import 'package:com.tad.cllllb/core/utils/network_util.dart';
import 'package:dartz/dartz.dart';

import '../models/apis/all_game_model.dart';
import '../models/apis/check_user_model.dart';
import '../models/apis/club_game_model.dart';
import '../models/apis/code_model.dart';
import '../models/apis/login_google_model.dart';
import '../models/apis/login_model.dart';
import '../models/apis/register_model.dart';
import '../models/apis/test_model.dart';
import '../models/apis/user_info_model.dart';
import '../models/apis/user_orders_model.dart';
import '../models/apis/user_products_model.dart';

class UserRepository {
  Future<Either<String?, RegisterModel>> register({
    required String name,
    required String email,
    required String phone,
    required String password,
    // ignore: non_constant_identifier_names
    required String password_confirmation,
    String? referenceCode,
    String? image,
    // ignore: non_constant_identifier_names
    List<int>? game_id,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.POST,
        url: UserEndPoints.register,
        body: {
          "name": name,
          "email": email,
          "phone": phone,
          "password": password,
          "password_confirmation": password_confirmation,
          if (image != null) "image": image,
          if (game_id != null) "game_id": game_id,
          if (referenceCode != null) "reference_code": referenceCode,
        },
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(RegisterModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, AllGameModel>> getAllGame({
    required int page,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: UserEndPoints.allGame,
        params: {
          "page": page.toString(),
        },
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        //log(response);
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(AllGameModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<UserProductsModel>>> getUserProducts(
      {required int userId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: UserEndPoints.allProducts,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {"user_id": userId.toString()}).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<UserProductsModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(UserProductsModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, UserInfoModel>> getUserInfo({
    required int userId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.POST,
        url: UserEndPoints.userInfo,
        //body: {"id": userId},
        headers: NetworkConfig.getHeaders(needAuth: true),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(UserInfoModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  // ignore: non_constant_identifier_names
  Future<Either<String?, List<LoginGoogleModel>>> LoginWithGoogle(
      {required String token}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: UserEndPoints.loginGoogle,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "token": token,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<LoginGoogleModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(LoginGoogleModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<UserOrdersModel>>> getUserOrders(
      {required int userId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: UserEndPoints.userOrders,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {"user_id": userId.toString()}).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<UserOrdersModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(UserOrdersModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<ClubGameModel>>> getClubsGames(
      {required int gameId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: UserEndPoints.gameClub,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {"game_id": gameId.toString()}).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<ClubGameModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(ClubGameModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> editProfile({
    required int userId,
    required String email,
    required String phone,
    required String name,
    String? image,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: UserEndPoints.editProfile,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "id": userId,
            "email": email,
            "phone": phone,
            "name": name,
            if (image != null) "image": image,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<TestModel>>> verfication() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.POST,
        url: UserEndPoints.verfication,
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<TestModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(TestModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> addUserGame({
    required int userId,
    required List<int> gameId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: UserEndPoints.userGame,
          headers: NetworkConfig.getHeaders(needAuth: false),
          body: {
            "user_id": userId,
            "game_id": gameId,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, LoginModel>> login({
    required String phone,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.POST,
        url: UserEndPoints.login,
        body: {
          "phone": phone,
        },
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(LoginModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> deleteUser({
    required int userId,
  }) async {
    try {
      return NetworkUtil.sendMultipartRequest(
        type: RequestType.POST,
        url: UserEndPoints.deleteUser,
        fields: {
          'id': userId.toString(),
        },
        headers:
            NetworkConfig.getHeaders(needAuth: false, isMultipartRequest: true),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, CheckUserModel>> checkUser({
    required String phone,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.POST,
        url: UserEndPoints.checkUser,
        body: {
          "phone": phone,
        },
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(CheckUserModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> otp({
    required String phone,
    required String code,
  }) async {
    try {
      return NetworkUtil.sendRequestOTP(
        type: RequestType.POST,
        url: "api/sms",
        body: {
          "phone": phone,
          "code": code,
          "email": "sms.club@clllb.com",
          "pass": "clllb@2023",
        },
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, CodeModel>> getCodeRandom() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: UserEndPoints.code,
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(CodeModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<FavoriteGameModel>>> getUserFavoriteGame({
    required int userId,
    required int page,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.GET,
          url: UserEndPoints.userFavoriteGame,
          headers: NetworkConfig.getHeaders(needAuth: false),
          params: {
            "user_id": userId.toString(),
            "page": page.toString(),
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<FavoriteGameModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(FavoriteGameModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> addPostToSupport({
    required String title,
    required String description,
    required String email,
    String? image,
    String? phone,
  }) async {
    try {
      return NetworkUtil.sendMultipartRequest(
        type: RequestType.POST,
        url: UserEndPoints.support,
        fields: {
          "title": title,
          "description": description,
          "email": email,
          if (phone != null) "phone": phone,
        },
        files: {
          if (image != null) "image": image,
        },
        headers:
            NetworkConfig.getHeaders(needAuth: false, isMultipartRequest: true),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> giftPoints({
    required String reciverNumber,
    required double points,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: UserEndPoints.giftPoints,
          headers: NetworkConfig.getHeaders(needAuth: true),
          body: {
            "reciver_number": reciverNumber,
            "points": points,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<LotteryModel>>> getLottery({
    required int page,
    String? date,
    String? points,
    bool? ended,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: UserEndPoints.lottery,
        params: {
          "page": page.toString(),
          if (date != null) "date": date,
          if (points != null) "points": points,
          if (ended != null) "ended": ended.toString(),
        },
        headers: NetworkConfig.getHeaders(needAuth: true),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<LotteryModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(LotteryModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, bool>> enterLottery({required int drawId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: UserEndPoints.enterLottery,
          headers: NetworkConfig.getHeaders(needAuth: true),
          body: {
            "draw_id": drawId,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, ReplacePointsModel>> getPointMoney() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: UserEndPoints.replacePoints,
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(ReplacePointsModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<LotteryResultModel>>> getLotteryResult({
    required int page,
    String? date,
    bool? winner,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: UserEndPoints.lotteryResult,
        params: {
          "page": page.toString(),
          if (date != null) "date": date,
          if (winner != null) "winner": winner.toString(),
        },
        headers: NetworkConfig.getHeaders(needAuth: true),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<LotteryResultModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(LotteryResultModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<int?, bool>> replaceUserPoints(
      {required double pointsCounts}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: UserEndPoints.replacePointUser,
          headers: NetworkConfig.getHeaders(needAuth: true),
          body: {
            "points_counts": pointsCounts,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.statusCode);
        }
      });
    } catch (e) {
      return const Left(null);
    }
  }

  Future<Either<String?, List<VouchersModel>>> getVouchers({
    required int page,
    String? startDate,
    String? endDate,
    String? price,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: UserEndPoints.vouchers,
        params: {
          "page": page.toString(),
          if (startDate != null) "startdate": startDate,
          if (endDate != null) "enddate": endDate,
          if (price != null) "price": price,
        },
        headers: NetworkConfig.getHeaders(needAuth: true),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<VouchersModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(VouchersModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<int?, bool>> buyVoucher({required int couponId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: UserEndPoints.buyVoucher,
          headers: NetworkConfig.getHeaders(needAuth: true),
          body: {
            "coupon_id": couponId,
          }).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.statusCode);
        }
      });
    } catch (e) {
      return const Left(null);
    }
  }

  Future<Either<String?, List<UserVouchersModel>>> getUserVouchers({
    required int page,
    String? startDate,
    String? endDate,
    String? discount,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: UserEndPoints.userVouchers,
        params: {
          "page": page.toString(),
          if (startDate != null) "startdate": startDate,
          if (endDate != null) "enddate": endDate,
          if (discount != null) "discount": discount,
        },
        headers: NetworkConfig.getHeaders(needAuth: true),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<UserVouchersModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(UserVouchersModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, VouchersModel>> checkVoucher(
      {required String code}) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.POST,
        url: UserEndPoints.checkVouchers,
        headers: NetworkConfig.getHeaders(needAuth: true),
        body: {
          "code": code,
        },
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(VouchersModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  Future<Either<String?, List<LotteryResultModel>>> getHomeLotteryResult({
    required int page,
    String? date,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: UserEndPoints.homeDraws,
        params: {
          "page": page.toString(),
          if(date != null) "date":date,
        },
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return const Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<LotteryResultModel> result = [];
          // ignore: avoid_function_literals_in_foreach_calls
          commonResponse.data!.forEach(
            (element) {
              result.add(LotteryResultModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }
}
