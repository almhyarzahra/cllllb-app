// ignore_for_file: non_constant_identifier_names

import 'package:com.tad.cllllb/core/data/models/cart_model.dart';
import 'package:get/get.dart';
import 'package:com.tad.cllllb/core/enums/data_type.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/apis/all_clubs_model.dart';
import '../models/apis/all_products_model.dart';
import '../models/apis/user_products_model.dart';

class SharedPreferenceRepository {
  SharedPreferences globalSharedPrefs = Get.find();
  String PREF_FIRST_LANUCH = 'first_lanuch';
  String PREF_IS_LOGGEDIN = 'login';
  String PREF_TOKEN_INFO = 'token_info';
  String PREF_APP_LANG = 'app_language';
  String PREF_CART_LIST = 'cart_list';
  String PREF_FAVORITE_LIST = 'favorite_list';
  String PREF_ALL_CLUBS_LIST = 'all_clubs_list';
  String PREF_USER_PRODUCTS = 'user_products';
  String PREF_ORDER_PLACED = 'order_placed';
  String PREF_SUB_STATUS = 'sub_status';
  String PREF_USER_ID = 'user_id';
  String PREF_USER_POINT = 'user_point';
  String PREF_USER_NAME = 'user_name';
  String PREF_GOOGLE = "google";
  String PREF_NOTIFICATIONS = 'notifications';
  String PREF_MOBILE_NUMBER = 'NUMBER';
  String PREF_EMAIL = 'email';
  String PREF_ALL_PRODUCTS_LIST = 'products_list';

  setEnableNotifications(bool value) {
    setPreferance(
      dataType: DataType.BOOL,
      key: PREF_NOTIFICATIONS,
      value: value,
    );
  }

  bool getEnableNotifications() {
    if (globalSharedPrefs.containsKey(PREF_NOTIFICATIONS)) {
      return getPrefrance(key: PREF_NOTIFICATIONS);
    } else {
      return true;
    }
  }

  setSubStatus(bool value) {
    setPreferance(
      dataType: DataType.BOOL,
      key: PREF_SUB_STATUS,
      value: value,
    );
  }

  bool getSubStatus() {
    if (globalSharedPrefs.containsKey(PREF_SUB_STATUS)) {
      return getPrefrance(key: PREF_SUB_STATUS);
    } else {
      return true;
    }
  }

  setGoogle(bool value) {
    setPreferance(
      dataType: DataType.BOOL,
      key: PREF_GOOGLE,
      value: value,
    );
  }

  bool getGoogle() {
    if (globalSharedPrefs.containsKey(PREF_GOOGLE)) {
      return getPrefrance(key: PREF_GOOGLE);
    } else {
      return false;
    }
  }

  setTokenInfo(String? value) {
    setPreferance(
      dataType: DataType.STRING,
      key: PREF_TOKEN_INFO,
      value: value,
    );
  }

  String? getTokenInfo() {
    if (globalSharedPrefs.containsKey(PREF_TOKEN_INFO)) {
      return getPrefrance(key: PREF_TOKEN_INFO);
    } else {
      return null;
    }
  }

  setUserId(int? value) {
    setPreferance(
      dataType: DataType.INT,
      key: PREF_USER_ID,
      value: value,
    );
  }

  int getUserId() {
    if (globalSharedPrefs.containsKey(PREF_USER_ID)) {
      return getPrefrance(key: PREF_USER_ID);
    } else {
      return 0;
    }
  }

   setUserPoint(double value) {
    setPreferance(
      dataType: DataType.DOUBLE,
      key: PREF_USER_POINT,
      value: value,
    );
  }

  double getUserPoint() {
    if (globalSharedPrefs.containsKey(PREF_USER_POINT)) {
      return getPrefrance(key: PREF_USER_POINT);
    } else {
      return 0;
    }
  }

  setUserName(String? value) {
    setPreferance(
      dataType: DataType.STRING,
      key: PREF_USER_NAME,
      value: value,
    );
  }

  String? getUserName() {
    if (globalSharedPrefs.containsKey(PREF_USER_NAME)) {
      return getPrefrance(key: PREF_USER_NAME);
    } else {
      return null;
    }
  }

  setmobileNumber(String? value) {
    setPreferance(
      dataType: DataType.STRING,
      key: PREF_MOBILE_NUMBER,
      value: value,
    );
  }

  String? getmobileNumber() {
    if (globalSharedPrefs.containsKey(PREF_MOBILE_NUMBER)) {
      return getPrefrance(key: PREF_MOBILE_NUMBER);
    } else {
      return null;
    }
  }

  setEmail(String? value) {
    setPreferance(
      dataType: DataType.STRING,
      key: PREF_EMAIL,
      value: value,
    );
  }

  String? getEmail() {
    if (globalSharedPrefs.containsKey(PREF_EMAIL)) {
      return getPrefrance(key: PREF_EMAIL);
    } else {
      return null;
    }
  }

  setIsLoggedIN(bool value) {
    setPreferance(
      dataType: DataType.BOOL,
      key: PREF_IS_LOGGEDIN,
      value: value,
    );
  }

  bool getIsLoggedIn() {
    if (globalSharedPrefs.containsKey(PREF_IS_LOGGEDIN)) {
      return getPrefrance(key: PREF_IS_LOGGEDIN);
    } else {
      return false;
    }
  }

  setFirstLanuch(bool value) {
    setPreferance(
      dataType: DataType.BOOL,
      key: PREF_FIRST_LANUCH,
      value: value,
    );
  }

  bool getFirstLanuch() {
    if (globalSharedPrefs.containsKey(PREF_FIRST_LANUCH)) {
      return getPrefrance(key: PREF_FIRST_LANUCH);
    } else {
      return true;
    }
  }

  setAppLanguage(String value) {
    setPreferance(
      dataType: DataType.STRING,
      key: PREF_APP_LANG,
      value: value,
    );
  }

  String getAppLanguage() {
    if (globalSharedPrefs.containsKey(PREF_APP_LANG)) {
      return getPrefrance(key: PREF_APP_LANG);
    } else {
      return 'ar';
    }
  }

  setCartList(List<CartModel> list) {
    setPreferance(dataType: DataType.STRING, key: PREF_CART_LIST, value: CartModel.encode(list));
  }

  List<CartModel> getCartList() {
    if (globalSharedPrefs.containsKey(PREF_CART_LIST)) {
      return CartModel.decode(getPrefrance(key: PREF_CART_LIST));
    } else {
      return [];
    }
  }

  setFavoriteList(List<CartModel> list) {
    setPreferance(
        dataType: DataType.STRING, key: PREF_FAVORITE_LIST, value: CartModel.encode(list));
  }

  List<CartModel> getFavoriteList() {
    if (globalSharedPrefs.containsKey(PREF_FAVORITE_LIST)) {
      return CartModel.decode(getPrefrance(key: PREF_FAVORITE_LIST));
    } else {
      return [];
    }
  }

  void setAllClubsList(List<AllClubsModel> list) {
    setPreferance(
        dataType: DataType.STRING, key: PREF_ALL_CLUBS_LIST, value: AllClubsModel.encode(list));
  }

  List<AllClubsModel> getAllClubsList() {
    if (globalSharedPrefs.containsKey(PREF_ALL_CLUBS_LIST)) {
      return AllClubsModel.decode(getPrefrance(key: PREF_ALL_CLUBS_LIST));
    } else {
      return [];
    }
  }

  void setAllProductsList(List<AllProductsModel> list) {
    setPreferance(
        dataType: DataType.STRING,
        key: PREF_ALL_PRODUCTS_LIST,
        value: AllProductsModel.encode(list));
  }

  List<AllProductsModel> getAllProductsList() {
    if (globalSharedPrefs.containsKey(PREF_ALL_PRODUCTS_LIST)) {
      return AllProductsModel.decode(getPrefrance(key: PREF_ALL_PRODUCTS_LIST));
    } else {
      return [];
    }
  }

  void setUserProductsList(List<UserProductsModel> list) {
    setPreferance(
        dataType: DataType.STRING, key: PREF_USER_PRODUCTS, value: UserProductsModel.encode(list));
  }

  List<UserProductsModel> getUserProductsList() {
    if (globalSharedPrefs.containsKey(PREF_USER_PRODUCTS)) {
      return UserProductsModel.decode(getPrefrance(key: PREF_USER_PRODUCTS));
    } else {
      return [];
    }
  }

  setOrderPlaced(bool value) {
    setPreferance(
      dataType: DataType.BOOL,
      key: PREF_ORDER_PLACED,
      value: value,
    );
  }

  bool getOrderPlaced() {
    if (globalSharedPrefs.containsKey(PREF_ORDER_PLACED)) {
      return getPrefrance(key: PREF_ORDER_PLACED);
    } else {
      return false;
    }
  }

  setPreferance({
    required DataType dataType,
    required String key,
    required dynamic value,
  }) async {
    switch (dataType) {
      case DataType.INT:
        await globalSharedPrefs.setInt(key, value);
        break;
      case DataType.BOOL:
        await globalSharedPrefs.setBool(key, value);
        break;
      case DataType.STRING:
        await globalSharedPrefs.setString(key, value);
        break;
      case DataType.DOUBLE:
        await globalSharedPrefs.setDouble(key, value);
        break;
      case DataType.LISTSTRING:
        await globalSharedPrefs.setStringList(key, value);
        break;
    }
  }

  dynamic getPrefrance({required String key}) {
    return globalSharedPrefs.get(key);
  }
}
