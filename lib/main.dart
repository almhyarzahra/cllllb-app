import 'package:com.tad.cllllb/UI/views/shopping_view/stripe_payment/stripe_keys.dart';
import 'package:com.tad.cllllb/app/my_app_controller.dart';
import 'package:com.tad.cllllb/core/data/repositories/shared_preference_repository.dart';
import 'package:com.tad.cllllb/core/services/connectivity_service.dart';
import 'package:com.tad.cllllb/core/services/location_service.dart';
import 'package:com.tad.cllllb/core/services/notification_service.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'app/my_app.dart';
import 'core/services/cart_services.dart';
import 'core/services/favorite_services.dart';
import 'firebase_options.dart';

//RxInt counter = 0.obs;
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Stripe.publishableKey = ApiKeys.publishablekey;
  await Get.putAsync(
    () async {
      var sharedPref = await SharedPreferences.getInstance();
      return sharedPref;
    },
  );

  Get.put(SharedPreferenceRepository());
  Get.put(CartService());
  Get.put(FavoriteService());
  Get.put(LocationService());
  // Get.put(ConnectivityService());
  // Get.put(MyAppController());

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  Get.put(NotificationService());
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  // notificationService.notificationStream.stream.listen((event) {
  //   counter += 1;
  //   //addNotifications(event);
  // });

  runApp(const MyApp());
}
